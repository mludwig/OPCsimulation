// the caen api which is linked against the OPC server initially: it replaces only the CAENHV_InitSystem call 
#define CAENHVLIB_API
#ifndef __CAENHVRESULT__                         // Rel. 2.0 - Linux
// The Error Code type
typedef int CAENHVRESULT;
#define __CAENHVRESULT__
#endif


CAENHVLIB_API CAENHVRESULT CAENHV_InitSystem(CAENHV_SYSTEM_TYPE_t system, 
		int LinkType, void *Arg, const char *UserName, const char *Passwd,  int *handle);
		

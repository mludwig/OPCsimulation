// the caen api which is linked against the OPC server initially: it replaces only the CAENHV_InitSystem call
 
#define CAENHVLIB_API
typedef int CAENHVRESULT;
typedef enum {
	SY1527		= 0,
	SY2527		= 1,
	SY4527		= 2,
	SY5527		= 3,
	N568		= 4,
	V65XX		= 5,
	N1470		= 6,
	V8100		= 7,
	N568E		= 8,
	DT55XX		= 9,
	FTK			= 10,
	DT55XXE		= 11,
	N1068		= 12
} CAENHV_SYSTEM_TYPE_t;


CAENHVLIB_API CAENHVRESULT CAENHV_InitSystem(CAENHV_SYSTEM_TYPE_t system, 
		int LinkType, void *Arg, const char *UserName, const char *Passwd,  int *handle);
		

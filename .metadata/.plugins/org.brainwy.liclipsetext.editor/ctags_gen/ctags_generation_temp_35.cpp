// the caen api which is linked against the OPC server initially: it replaces only the CAENHV_InitSystem call
 
 
 struct  _SCaen__CaenMessage
{
  ProtobufCMessage base;
  SCaen__MessageType type;
  SCaen__CAENHVGetEventDataRequestM *caenhv_geteventdatarequest;
  SCaen__CAENHVGetEventDataReplyM *caenhv_geteventdatareply;
  SCaen__CAENHVDeinitSystemRequestM *caenhv_deinitsystemrequest;
  SCaen__CAENHVDeinitSystemReplyM *caenhv_deinitsystemreply;
  SCaen__CAENHVGetCrateMapRequestM *caenhv_getcratemaprequest;
  SCaen__CAENHVGetCrateMapReplyM *caenhv_getcratemapreply;
  SCaen__CAENHVGetSysPropListRequestM *caenhv_getsysproplistrequest;
  SCaen__CAENHVGetSysPropListReplyM *caenhv_getsysproplistreply;
  SCaen__CAENHVGetSysPropInfoRequestM *caenhv_getsyspropinforequest;
  SCaen__CAENHVGetSysPropInfoReplyM *caenhv_getsyspropinforeply;
  SCaen__CAENHVGetSysPropRequestM *caenhv_getsysproprequest;
  SCaen__CAENHVGetSysPropReplyM *caenhv_getsyspropreply;
  SCaen__CAENHVGetBdParamRequestM *caenhv_getbdparamrequest;
  SCaen__CAENHVGetBdParamReplyM *caenhv_getbdparamreply;
  SCaen__CAENHVGetBdParamPropRequestM *caenhv_getbdparamproprequest;
  SCaen__CAENHVGetBdParamPropReplyM *caenhv_getbdparampropreply;
  SCaen__CAENHVGetBdParamInfoRequestM *caenhv_getbdparaminforequest;
  SCaen__CAENHVGetBdParamInfoReplyM *caenhv_getbdparaminforeply;
  SCaen__CAENHVGetChParamInfoRequestM *caenhv_getchparaminforequest;
  SCaen__CAENHVGetChParamInfoReplyM *caenhv_getchparaminforeply;
  SCaen__CAENHVGetChParamPropRequestM *caenhv_getchparamproprequest;
  SCaen__CAENHVGetChParamPropReplyM *caenhv_getchparampropreply;
  SCaen__CAENHVGetChNameRequestM *caenhv_getchnamerequest;
  SCaen__CAENHVGetChNameReplyM *caenhv_getchnamereply;
  SCaen__CAENHVSetChNameRequestM *caenhv_setchnamerequest;
  SCaen__CAENHVSetChNameReplyM *caenhv_setchnamereply;
  SCaen__CAENHVGetChParamRequestM *caenhv_getchparamrequest;
  SCaen__CAENHVGetChParamReplyM *caenhv_getchparamreply;
  SCaen__CAENHVSetChParamRequestM *caenhv_setchparamrequest;
  SCaen__CAENHVSetChParamReplyM *caenhv_setchparamreply;
  SCaen__CAENHVGetExecCommListRequestM *caenhv_getexeccommlistrequest;
  SCaen__CAENHVGetExecCommListReplyM *caenhv_getexeccommlistreply;
  SCaen__CAENHVExecCommRequestM *caenhv_execcommrequest;
  SCaen__CAENHVExecCommReplyM *caenhv_execcommreply;
  SCaen__CAENHVSubscribeSystemParamsRequestM *caenhv_subscribesystemparamsrequest;
  SCaen__CAENHVSubscribeSystemParamsReplyM *caenhv_subscribesystemparamsreply;
  SCaen__CAENHVSubscribeBoardParamsRequestM *caenhv_subscribeboardparamsrequest;
  SCaen__CAENHVSubscribeBoardParamsReplyM *caenhv_subscribeboardparamsreply;
  SCaen__CAENHVSubscribeChannelParamsRequestM *caenhv_subscribechannelparamsrequest;
  SCaen__CAENHVSubscribeChannelParamsReplyM *caenhv_subscribechannelparamsreply;
  SCaen__CAENHVUnSubscribeSystemParamsRequestM *caenhv_unsubscribesystemparamsrequest;
  SCaen__CAENHVUnSubscribeSystemParamsReplyM *caenhv_unsubscribesystemparamsreply;
  SCaen__CAENHVUnSubscribeBoardParamsRequestM *caenhv_unsubscribeboardparamsrequest;
  SCaen__CAENHVUnSubscribeBoardParamsReplyM *caenhv_unsubscribeboardparamsreply;
  SCaen__CAENHVUnSubscribeChannelParamsRequestM *caenhv_unsubscribechannelparamsrequest;
  SCaen__CAENHVUnSubscribeChannelParamsReplyM *caenhv_unsubscribechannelparamsreply;
  SCaen__CAENHVInitSystemRequestM *caenhv_initsystemrequest;
  SCaen__CAENHVInitSystemReplyM *caenhv_initsystemreply;
  SCaen__CAENHVFreeEventDataRequestM *caenhv_freeeventdatarequest;
  SCaen__CAENHVFreeEventDataReplyM *caenhv_freeeventdatareply;
  SCaen__CAENHVFreeRequestM *caenhv_freerequest;
  SCaen__CAENHVFreeReplyM *caenhv_freereply;
  SCaen__CAENHVGetErrorRequestM *caenhv_geterrorrequest;
  SCaen__CAENHVGetErrorReplyM *caenhv_geterrorreply;
  SCaen__CAENHVSetSysPropRequestM *caenhv_setsysproprequest;
  SCaen__CAENHVSetSysPropReplyM *caenhv_setsyspropreply;
  SCaen__CAENHVSetBdParamRequestM *caenhv_setbdparamrequest;
  SCaen__CAENHVSetBdParamReplyM *caenhv_setbdparamreply;
  SCaen__CAENHVTestBdPresenceRequestM *caenhv_testbdpresencerequest;
  SCaen__CAENHVTestBdPresenceReplyM *caenhv_testbdpresencereply;
};
 
 
#define CAENHVLIB_API
typedef int CAENHVRESULT;
typedef enum {
	SY1527		= 0,
	SY2527		= 1,
	SY4527		= 2,
	SY5527		= 3,
	N568		= 4,
	V65XX		= 5,
	N1470		= 6,
	V8100		= 7,
	N568E		= 8,
	DT55XX		= 9,
	FTK			= 10,
	DT55XXE		= 11,
	N1068		= 12
} CAENHV_SYSTEM_TYPE_t;


CAENHVLIB_API CAENHVRESULT CAENHV_InitSystem(CAENHV_SYSTEM_TYPE_t system, 
		int LinkType, void *Arg, const char *UserName, const char *Passwd,  int *handle);
		

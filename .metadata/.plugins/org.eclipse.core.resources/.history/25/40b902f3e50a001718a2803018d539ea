/*
 * Controller.h
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */

#ifndef SRC_CRATE_H_
#define SRC_CRATE_H_

#include <stdint.h>
#include <math.h>
#include <iostream>
#include <map>
#include <vector>

#include "Board.h"
#include "caenAPIdefs.h"
using namespace ns_HAL_Board;

#include "Channel.h"
using namespace ns_HAL_Channel;

#include "API.h"
using namespace ns_HAL_API;

#define SIM_MAX_NB_BOARDS 500 // per crate

/// \todo crate/channel trip handling and propagation from the channels according to trip config registers
namespace ns_HAL_Crate {
/*
 * SY4527 as a generic crate
read only static:
=================
ok ModelName::0::0
ok SwRelease::0::0
ok IPAddr::1::0
ok IPGw::1::0
ok IPNetMsk::1::0

read only dynamic:
==================
ok CPULoad::0::0
ok ClkFreq::0::1
ok MemoryStatus::0::0
ok HVFanStat::0::0
ok PWFanStat::0::0
ok HVFanSpeed::1::1
ok PWVoltage::0::0
ok PWCurrent::1::0

read/write but no logic
=======================
ok SymbolicName::1::0
ok ResFlagCfg::1::1
ok FrontPanIn::0::1
ok FrontPanOut::0::1

don't know yet how they work in detail:
=======================================
HvPwSM::0::0
CmdQueueStatus::0::1
ResFlag::0::1
Sessions::0::0
CMDExecMode::1::1
DummyReg::1::1
GenSignCfg::1::1
HVClkConf::1::0
OutputLevel::1::1
*/
/// \todo add crate properties:don't know yet how they work in detail
class Crate {
public:
	Crate( CAENHV_SYSTEM_TYPE_t s, int h, string modelName );
	virtual ~Crate();

	static bool have( uint32_t handle );

	string key( void ){ return(_key);}
	void deinit( void );
	uint32_t nbOfBoards( void ){ return ( _board_map.size() );}
	void insertBoard( uint32_t islot, Board * );
	Board *board( uint32_t slot ){ return _board_map.at( slot ); };
	bool boardExists( uint32_t slot );
	void addPopulatedSlot( uint32_t pslot ){_populatedSlotsv.push_back( pslot );}

	// RO static
	string ModelName( void ) { return (_modelName );}
	string SwRelease( void );
	string IPAddress( void ){ return( _ipAddress ); }
	string IPGw( void ) { return("127.0.0.0"); }
	string IPNetMsk( void ) { return("255.255.255.192"); }
	uint32_t NbSlots( void ) { return ( _maxNbBoards ); }

	// RO dynamic
	string PWVoltage( void );
	string PWCurrent( void );
	int32_t HVFanSpeed( void );
	string PWFanStat( void );
	string HVFanStat( void );
	string MemoryStatus( void );
	string CPULoad( void );
	int32_t ClkFreq( void );

	// RW but dumb
	string SymbolicName( void ){ return( _symbolicName );}
	void setSymbolicName( string s ){ _symbolicName = s;}
	uint16_t ResFlagCfg( void ) { return( _resFlagCfg ); }
	void setResFlagCfg( uint16_t u ) { _resFlagCfg = u; }
	uint32_t FrontPanIn( void ) { return( _frontPanIn ); }
	void setFrontPanIn( uint32_t u ) { _frontPanIn = u; }
	uint32_t FrontPanOut( void ) { return( _frontPanOut ); }
	void setFrontPanOut( uint32_t u ) { _frontPanOut = u; }

	// configure object behavior to follow crate model characteristics
	void configureSY1527( void );
	void configureSY5527( void );
	void configureSY5527LC( void );
	void setIPAddress( string ip ){ _ipAddress = ip; }

	uint32_t totalNbBoards( void ) { return( _board_map.size() ); }
	vector<string> propertyList( void );
	uint32_t propertyAccessMode( string p ){ return( ( uint32_t ) _prop_map.at( p ).accessMode ); }
	uint32_t propertyDataType( string p ){ return( ( uint32_t ) _prop_map.at( p ).dataType ); }
	void getConfiguration( int32_t *nbcrates, string *configFile, string *engineip, string *enginehostname, string *gluehn );
	void setConfiguration( string engineip, string gluehn );
	void getHealthEngine( int32_t *nbCrates, int 32_t *nbBoards, int32_t *nbChannels,
			int32_t *nbChanelsRampingUp, int32_t *nbChanelsRampingDwn, int32_t *nbChannelsTripped,
			int32_t *nbChannelsOn, int32_t *nbChannelsOff );

	// overloaded version is clearer & simpler: first get the type, than the value.
	void getPropertyHALtype( string p, API::HAL_DATA_TYPE_t *type );
	void getPropertyValue( string p, string *retval );
	void getPropertyValue( string p, int8_t *retval );
	void getPropertyValue( string p, int16_t *retval );
	void getPropertyValue( string p, int32_t *retval );
	void getPropertyValue( string p, uint8_t *retval );
	void getPropertyValue( string p, uint16_t *retval );
	void getPropertyValue( string p, uint32_t *retval );
	void getPropertyValue( string p, float *retval );

	void setPropertyValue( string p, string val );
	void setPropertyValue( string p, int8_t val );
	void setPropertyValue( string p, int16_t val );
	void setPropertyValue( string p, int32_t val );
	void setPropertyValue( string p, uint8_t val );
	void setPropertyValue( string p, uint16_t val );
	void setPropertyValue( string p, uint32_t val );
	void setPropertyValue( string p, float val );


	// pilot interface
	void pilot_setVSEL( bool f );
	void pilot_setISEL( bool f );
	void pilot_setChannelLoad( uint32_t slot, uint32_t channel, float res, float capa );
	void pilot_getChannelLoad( uint32_t slot, uint32_t channel, float *res, float *capa );
	void pilot_setAllChannelLoads( float res, float capa );
	void pilot_rampAllChannels( float v0set, float i0set, float rup, float rdown);
	void pilot_switchOffAllChannels( void );
	uint32_t pilot_getUpdateDelay( void ) { return( _updateDelay_ms ); }
	/// \todo pilot set poller delay per crate for stress testing
	void pilot_setUpdateDelay( uint32_t dd ) { _updateDelay_ms  = dd; }
	void pilot_tripChannelByVoltage( uint32_t slot, uint32_t channel );
	void pilot_tripChannelByCurrent( uint32_t slot, uint32_t channel );
	void pilot_tripChannelByLoad( uint32_t slot, uint32_t channel );

	uint32_t status( void );
	uint32_t handle( void ){ return( _handle );}

	vector<string> getExecCommList( void ){ return ( _execCommList ); }

	typedef struct {
		CAENHVEVENT_TYPE_t caen;
		API::HAL_DATA_TYPE_t haltype;
		uint32_t statusBoard;
	} EVENT_ITEM_t;

	void update( void );
	void collectEventItems( vector<Crate::EVENT_ITEM_t> *evts );
	EVENT_ITEM_t fabricateCrateEventItem( std::map<string,API::HAL_PROPERTY_t>::iterator it );
	EVENT_ITEM_t fabricateBoardEventItem( std::map<string,API::HAL_PROPERTY_t>::iterator it, Board *b );
	EVENT_ITEM_t fabricateChannelEventItem( std::map<string,API::HAL_PROPERTY_t>::iterator it, Channel *c );
	void intelligentDelay( void );
	void setDebug( bool f ){ _debug = f; };

	void showProperties( void );
	void subscribeParameter( string param, bool flag );

private:
	static uint32_t nbCrates;
	static string _engineip;
	static string _gluehn;
	bool _debug;
	string _key; // unique human readable identifier
	CAENHV_SYSTEM_TYPE_t _system;
	int _handle;
	bool _communication;
	uint32_t _status;
	std::map<uint32_t,Board *> _board_map; // key=slot

	// ro static
	string _modelName;
	uint32_t _maxNbBoards;

	// front panel CPU inputs which are present for some crates (SY4527) and can be simulated in a limited way
	bool _FP_VSEL;
	bool _FP_ISEL;
	bool _FP_KILL;
	bool _FP_RESET;
	bool _FP_INTERLOCK;
	// bool _FP_HV_SYNC;
	uint8_t _FP_FAN; // 0=low, 1=mid, 2=high

	// generic functionality, simple, no states
	string _PWVoltage; bool _flipv;
	string _PWCurrent; bool _flipc;
	int32_t _HVFanSpeed; bool _flip0;
	string _PWFanStat; bool _flip1;
	string _HVFanStat; bool _flip2;
	string _MemoryStatus; bool _flip3;
	string _CPULoad; bool _flip4;
	int32_t _ClkFreq; bool _flip5;

	// rw but dumb
	string _symbolicName;
	uint16_t _resFlagCfg;
	uint32_t _frontPanIn;
	uint32_t _frontPanOut;

	// don't know how this works
	vector<string> _execCommList;

	// hal bookeeping
	vector<uint32_t> _populatedSlotsv;

	string _ipAddress;
	struct timeval _now;
	struct timeval _lastUpdate;
	struct timezone _tz;
	struct timespec _tim1, _tim2; // for nanosleep
	uint32_t _updateDelay_ms; // for crates which do not have event mode: extra polling thread delay

	map<string,API::CAEN_PROPERTY_t> _caenProp_map;
	map<string,API::HAL_PROPERTY_t> _prop_map;
	void _propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t );
	void _propinsert( API::HAL_PROPERTY_t h ) { _prop_map.insert( std::pair<string,API::HAL_PROPERTY_t>( h.name, h ));}
	void _testPropExist( string p );
	void _testPropNotExist( string p );
};

} /* namespace ns_HAL_Controller */

#endif /* SRC_CRATE_H_ */

// discoveryCAEN main.cpp
// discover HW layout of CAEN crates on IP numbers

#include "main.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <string.h>
#include <vector>

//#include <boost/property_tree/xml_parser.hpp>
//#include <boost/property_tree/ptree.hpp>


using namespace std;

string standardMeta( void ) {
	string x = "\
<StandardMetaData>\n \
  <Log>\n \
    <GeneralLogLevel logLevel=\"TRC\"/> \n \
    <ComponentLogLevels>\n  \
      <ComponentLogLevel componentName=\"STATES\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"CAEN_API_CALLS\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"SESSION_SOCKETS\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"AS_UPDATES\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"AS_WRITES\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"AS_DISCO\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"HW_POLLER\" logLevel=\"TRC\"/>\n  \
    </ComponentLogLevels> \n \
  </Log>\n\
</StandardMetaData>";
	return( x );
}

string serializeSystemProperty( PROPERTY_t mm, uint32_t nbTabs ){
	string xml;
	string tab = "   ";
	switch( nbTabs ){
	case 0: tab = ""; break;
	case 1: tab = "   "; break;
	case 2: tab = "      "; break;
	case 3: tab = "         "; break;
	default: tab = "   "; break;
	}
	switch( mm.mode ) {
	case SYSPROP_MODE_RDONLY:{
		xml = tab + "<ReadOnlyProperty ";
		break;
	}
	case SYSPROP_MODE_WRONLY:{
		xml = tab + "<WriteOnlyProperty ";
		break;
	}
	case SYSPROP_MODE_RDWR:{
		xml = tab + "<ReadWriteProperty ";
		break;
	}
	} // switch mode
	switch( mm.type ) {
	case SYSPROP_TYPE_STR:{
		xml = xml + "dataType=\"S\" ";
		break;
	}
	case SYSPROP_TYPE_REAL: {
		xml = xml + "dataType=\"F\" ";
		break;
	}
	case SYSPROP_TYPE_UINT2:
	case SYSPROP_TYPE_UINT4:
	case SYSPROP_TYPE_INT2:
	case SYSPROP_TYPE_INT4:
	case SYSPROP_TYPE_BOOLEAN:{
		xml = xml + "dataType=\"I\" ";
		break;
	}
	} // switch type
	xml = xml + "name=\"" + mm.name + "\" propertyName=\"" + mm.name + "\"/>";
	return( xml );
}

string serializeBdChProperty( PROPERTY_t mm, uint32_t nbTabs ){
	string xml;
	string tab = "   ";
	switch( nbTabs ){
	case 0: tab = ""; break;
	case 1: tab = "   "; break;
	case 2: tab = "      "; break;
	case 3: tab = "         "; break;
	default: tab = "   "; break;
	}
	switch( mm.mode ) {
	case PARAM_MODE_RDONLY:{
		xml = tab + "<ReadOnlyProperty ";
		break;
	}
	case PARAM_MODE_WRONLY:{
		xml = tab + "<WriteOnlyProperty ";
		break;
	}
	case PARAM_MODE_RDWR:{
		xml = tab + "<ReadWriteProperty ";
		break;
	}
	} // switch mode

	switch( mm.type ) {
	case PARAM_TYPE_STRING:{
		xml = xml + "dataType=\"S\" ";
		break;
	}
	case PARAM_TYPE_NUMERIC: {
		xml = xml + "dataType=\"F\" ";
		break;
	}
	case PARAM_TYPE_CHSTATUS:
	case PARAM_TYPE_BDSTATUS:
	case PARAM_TYPE_BINARY:
	case PARAM_TYPE_ONOFF: {
		xml = xml + "dataType=\"I\" ";
	}
	} // switch type
	xml = xml + "name=\"" + mm.name + "\" propertyName=\"" + mm.name + "\"/>";
	return( xml );
}


void writeMap( vector<CRATE_DISCOVERED_MAP_t> mapv, string filename ){

	vector<string> xmlpartsv;
	xmlpartsv.push_back("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>");
	xmlpartsv.push_back("<configuration xmlns=\"http://cern.ch/quasar/Configuration\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://cern.ch/quasar/Configuration ../Configuration/Configuration.xsd\">");
	if (0) {
		time_t now = time( NULL );
		string tt = ctime(&now);
		string tt2 = tt.substr(0, tt.length() - 1 );
		string stamp = "<!-- CAEN discovery run at " + tt2 + " --!/>\n";
		xmlpartsv.push_back( stamp );
	}
	xmlpartsv.push_back( standardMeta() );
	string tab = "   ";

	// all crates
	for ( uint32_t icrate = 0; icrate < mapv.size(); icrate++ ){

		// a crate
		CRATE_DISCOVERED_MAP_t crateMap = mapv[ icrate ];
		string xml = "<SYCrate ipAddress=\"" + crateMap.crateip +
				"\" model=\"" + crateMap.crateModel +
				"\" name=\"" + crateMap.crateName + "\">";
		xmlpartsv.push_back( xml );

		for ( uint32_t p = 0; p < crateMap.cratePropertiesv.size(); p++ ){
			xmlpartsv.push_back( serializeSystemProperty( crateMap.cratePropertiesv[ p ], 1 ));
		}

		// the boards
		for ( uint16_t ib = 0; ib < crateMap.discoveredBoardsCount; ib++ ){

			BOARD_DISCOVERED_MAP_t boardMap = crateMap.boardv[ ib ];
			char sslot[ 32 ];
			sprintf( sslot, "%d", boardMap.slot );
			xml = tab + "<Board name=\"" + boardMap.name + "\" slot=\"" + sslot +
					"\" type=\"" + boardMap.model + "\" description=\"" + boardMap.description + "\">";
#if 0
			xml = tab + "<Board name=\"" + boardMap.name + "\" slot=\"" + sslot +
					"\" type=\"" + boardMap.model + "\">";
#endif
			xmlpartsv.push_back( xml );

			for ( uint16_t iboardProp = 0; iboardProp < boardMap.boardPropertiesv.size(); iboardProp++ ){
				PROPERTY_t prop = boardMap.boardPropertiesv[ iboardProp ];
				xmlpartsv.push_back( serializeBdChProperty( prop, 2 ));
			}

			// the channels: normally they are all identical except the channel names. We make up the channel names:
			// like the board names they come from the user and not from CAEN.
			// But in fact for branch controllers they are NOT identical
			boardMap.channelNames.clear();
			for ( uint32_t ichannel = 0; ichannel < boardMap.nbChannels; ichannel++ ){
				char sich[ 32 ];
				sprintf( sich, "%d", ichannel );
				boardMap.channelNames.push_back( string("sim_ch") + string(sich) );
				xml = tab + tab + "<Channel index=\"" + sich + "\" name=\"" + boardMap.channelNames[ ichannel ] + "\">";
				xmlpartsv.push_back( xml );

				for ( uint16_t channelPropertycount = 0; channelPropertycount < boardMap.channelsv[ ichannel ].channelPropsv.size(); channelPropertycount++ ){
					PROPERTY_t channelProperty = boardMap.channelsv[ ichannel].channelPropsv[ channelPropertycount ];
					xmlpartsv.push_back( serializeBdChProperty( channelProperty, 3 ));
				}
				xmlpartsv.push_back( tab + tab + "</Channel>" );
			}
			xmlpartsv.push_back( tab + "</Board>" );
		}
		xmlpartsv.push_back("</SYCrate>");
	}
	xmlpartsv.push_back("</configuration>");

	std::ofstream ofs;
	ofs.open( filename.c_str(), std::ofstream::out | std::ofstream::out );
	for ( uint32_t i = 0; i < xmlpartsv.size(); i++ ){
		ofs << xmlpartsv[ i ] << endl;
		// cout << xmlpartsv[ i ] << endl;
	}
	ofs.close();
	cout << "written xml to " << filename << endl;
}



vector<string> parseCharBuffArray(char* const* charBuffArray, int boardCount) {
	vector<string> result;
	const char* currentChar = *charBuffArray;
	for(unsigned short j=0; j<boardCount;  j++)	{
		if(*currentChar) {
			ostringstream name;
			while(*currentChar)
			{
				name << *currentChar;
				currentChar++;
			}
			result.push_back(name.str());
		} else {
			result.push_back("NA"); // not available
		}
		currentChar++;
	}
	return result;
}

vector<string> parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth)
{
	vector<string> result;
	for(char* i = const_cast<char*>(buffer); i[0]; i+= fixedWidth)
	{
		result.push_back(i);
	}
	return vector<string>(result);
}

void usage( char *p ){
	cout << "full dump of CAEN P.S. crate(s) to xml config, including board models, straight from the CAEN hw lib." << endl;
	cout << "used mainly to create a detailed configuration to clone systems for simulation." << endl;
	cout << "usage: " << p << " -ip <ip0> [ { -ip <ip1> -ip <ip2> ... } | -help | -cfg <fn> | { -sys <ty0> -sys <ty1> ... } ]" << endl;
	cout << "  where <ipx> are the CAEN crates to discover, together with their system-types <tyx>, same order" << endl;
	cout << "     if you don't specify any types then all crates are assumed to be default SY4527 types" << endl;
	cout << "     either you specify all -sys for all -ip, or no -sys at all, otherwise the order will be wrong." << endl;
	cout << "  where <fn> = prefix of filename the config is written to (always prefix.<ip0>.xml)" << endl;
	cout << "               (best is to leave the default)" << endl;
	cout << "  where <tyx> = crate system name as a string, i.e. \"SY4527\" (which is also the default)" << endl;
	cout << "example: " << p << " -ip \"137.138.9.19\" -sys \"SY4527\"" << endl;
	cout << "  will write the config of one crate at 137.138.9.19 to file CaenFullCfgSim.137.138.9.19.xml" << endl << endl;
	cout << "known crate types: -sys SY1527 "
			<< "SY2527 "
			<< "SY4527 "
			<< "SY5527 "
			<< endl;
	exit(0);
}


int discoverCAENcrate( CRATE_DISCOVERED_MAP_t *crateMapp ){
	const size_t PARAM_NAME_WIDTH = 10;
	crateMapp->libswrel = CAENHVLibSwRel();
	cout << "lib SW release: " << crateMapp->libswrel << endl;

	cout << " sytype= " << crateMapp->sytype
			<< " ip= " << crateMapp->crateip.c_str()
			<< endl;

	CAENHVRESULT result = CAENHV_InitSystem( crateMapp->sytype, LINKTYPE_TCPIP, ( char *) crateMapp->crateip.c_str(), "admin", "admin", &crateMapp->handle);
	cout << "init crate result= " << result << endl;
	if ( result != CAENHV_OK ){
		cout << "error init crate at " << crateMapp->crateip << " exiting. " << endl;
		return(-1);
	} else {
		cout << "init crate at " << crateMapp->crateip << " OK " << endl;
	}
	char *ModelList = 0;
	char *DescriptionList = 0;
	result = CAENHV_GetCrateMap( crateMapp->handle, &crateMapp->slotsCount, &crateMapp->NrofChList,
			&ModelList, &DescriptionList, &crateMapp->SerNumList,
			&crateMapp->FmwRelMinList, &crateMapp->FmwRelMaxList );

	cout << crateMapp->crateip << " handle= " << crateMapp->handle << " queryCrateMap: slots= " << crateMapp->slotsCount << endl;
	crateMapp->boardModels = parseCharBuffArray( ( char * const * ) &ModelList, crateMapp->slotsCount );
	crateMapp->boardDescriptions = parseCharBuffArray( ( char * const * ) &DescriptionList, crateMapp->slotsCount );
	CAENHV_Free(ModelList);
	CAENHV_Free(DescriptionList);

	crateMapp->discoveredBoardsCount = 0;
	for ( int i = 0; i < crateMapp->slotsCount; i++ ){
		if ( crateMapp->NrofChList[ i ] > 0 ){
			cout << "slot" << i << " board model= " << crateMapp->boardModels[ i ] << " boardDescription= " << crateMapp->boardDescriptions[ i ] << endl;
			crateMapp->discoveredBoardsCount++;
		}
	}
	cout << crateMapp->crateip << " handle= " << crateMapp->handle << " queryCrateMap: discoveredBoardsCount= " << crateMapp->discoveredBoardsCount << endl;

	// crate properties
	char *PropNameList = 0;
	result =  CAENHV_GetSysPropList( crateMapp->handle, &crateMapp->nbCrateProperties, &PropNameList );
	vector<string> propNames = parseCharBuffArray( ( char * const * ) &PropNameList, crateMapp->nbCrateProperties );
	CAENHV_Free(PropNameList);
	for ( int i = 0; i < crateMapp->nbCrateProperties; i++ ){
		PROPERTY_t property;
		property.name = propNames[ i ];
		result = CAENHV_GetSysPropInfo( crateMapp->handle, propNames[ i ] .c_str(), &property.mode, &property.type );
		crateMapp->cratePropertiesv.push_back( property );
	}

	// discover boards in crates, populated slots
	for ( uint32_t islot = 0; islot < crateMapp->slotsCount; islot++ ){
		if ( crateMapp->NrofChList[ islot ] > 0 ){

			uint32_t nbChannels = crateMapp->NrofChList[ islot ];
			cout << crateMapp->crateip << " handle= " << crateMapp->handle <<
					" slot" << islot << " discover board type= "<< crateMapp->boardModels[ islot ] << " nbChannels= " << nbChannels << endl;
			char *boardParameterList = 0;
			result = CAENHV_GetBdParamInfo( crateMapp->handle, islot, &boardParameterList);

			BOARD_DISCOVERED_MAP_t boardMap;
			boardMap.boardPropertyNames = parseFixedWidthArrayOfCharBuffers( ( const char * ) boardParameterList, PARAM_NAME_WIDTH );
			CAENHV_Free( boardParameterList );

			//for ( uint32_t p = 0; p < boardMap.boardPropertyNames.size(); p++ ){
			//	cout << "  bd.property= " << boardMap.boardPropertyNames[ p ] << endl;
			// }
			boardMap.slot = islot;
			// boardMap.description = crateMapp->boardDescriptions[ ic ];
			boardMap.description = crateMapp->boardDescriptions[ islot ];
			boardMap.nbChannels = nbChannels;
			boardMap.model = crateMapp->boardModels[ islot ];
			char xx[32] = "";
			sprintf( xx, "_%d_%d", crateMapp->handle, islot );
			boardMap.name = crateMapp->boardModels[ islot ] + string( xx ); // I don't get the name from CAEN, this is set by user

			// board properties (and channels) in detail, mode and type
			for ( uint32_t p = 0; p < boardMap.boardPropertyNames.size(); p++ ){
				PROPERTY_t boardProperty;
				boardProperty.name = boardMap.boardPropertyNames[ p ];
				result = CAENHV_GetBdParamProp( crateMapp->handle, boardMap.slot, boardProperty.name.c_str(), "Mode", &boardProperty.mode );
				result = CAENHV_GetBdParamProp( crateMapp->handle, boardMap.slot, boardProperty.name.c_str(), "Type", &boardProperty.type );
				boardMap.boardPropertiesv.push_back( boardProperty );

			}

			/** values: board firmware
			 * for a several boards which are indicated by slotList and it's dimension, slotNum
			 * the values for one parameter are returned.
			 *		CAENHVLIB_API CAENHVRESULT  _CAENHV_GetBdParam(int handle,
			 *		 ushort slotNum, const ushort *slotList, const char *ParName, void *ParValList){
			 *
			 */
			int slotNum = 1; // only this slot for a start
			unsigned short slotList[ 1 ];
			slotList[ 0 ] = boardMap.slot;
			void *ParValList;
			result = CAENHV_GetBdParam( crateMapp->handle, slotNum, slotList, "firmwareRelease", &ParValList );
			cout << "firmwareRelease= " << (char *) ParValList << endl;




			// all channels of normal boards are identical, so it would be enough
			// to interrogate the first channel only. In fact for a branch controller, this is not true
			// so we go through all channels individually. We assume implicitly that channel numbers
			// are monotone and consecutive 0...N, and that therefore all existing channels are populated
			// and numbered like this. This is different from crate slots, which can be left empty as well.
			char *channelParNameList;
			int ParNumber;
			for ( uint32_t ichannel = 0; ichannel < boardMap.nbChannels; ichannel ++ ) {
				result = CAENHV_GetChParamInfo( crateMapp->handle, boardMap.slot, ichannel, &channelParNameList, &ParNumber );
				vector<string> channelParameterNamesv = parseFixedWidthArrayOfCharBuffers( ( const char * ) channelParNameList, PARAM_NAME_WIDTH );
				CAENHV_Free( channelParNameList );

				CHANNEL_DISCOVERED_MAP_t channelMap;
				for ( uint32_t p = 0; p < channelParameterNamesv.size(); p++ ){
					PROPERTY_t channelProperty;
					channelProperty.name = channelParameterNamesv[ p ];

					result = CAENHV_GetChParamProp( crateMapp->handle, boardMap.slot, ichannel, channelProperty.name.c_str(), "Mode", &channelProperty.mode );
					result = CAENHV_GetChParamProp( crateMapp->handle, boardMap.slot, ichannel, channelProperty.name.c_str(), "Type", &channelProperty.type );

					channelMap.channelPropsv.push_back( channelProperty );
				}
				boardMap.channelsv.push_back( channelMap );
			}
			crateMapp->boardv.push_back( boardMap );
		}
	}

	// deinint crate to be clean
	result = CAENHV_DeinitSystem( crateMapp->handle );
	if ( result ){
		cout << "error deinit crate " << crateMapp->handle << " at " << crateMapp->crateip << " exiting. " << endl;
		return(-1);
	} else {
		cout << "deinit crate " << crateMapp->handle << " at " << crateMapp->crateip << " OK " << endl;
	}
	return(0);
}

int main( int argc, char **argv ){
	cout << endl << argv[ 0 ] << " program version: " << __DATE__ << " " << __TIME__ << endl;
	vector<string> crateipv;
	string fdefault = "CaenDiscoveredConfig.";
	const int IP_SIZE = 32;
	vector<CAENHV_SYSTEM_TYPE_t> cratesysv;
	vector<string> cratemodelv;

	CAENHV_SYSTEM_TYPE_t systemType = SY4527;
	string systemModel = "SY4527";

	int i = 0;
	for ( i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "-help")){
			usage( argv[0] );
		}
		if ( 0 == strcmp( argv[i], "-ip") && argc > i ){
			char ip[IP_SIZE];
			sscanf( argv[i+1], "%s", &ip[ 0 ] );
			crateipv.push_back( string( ip ));
		}
		if ( 0 == strcmp( argv[i], "-cfg") && argc > i ){
			char hh[ 256 ];
			sscanf( argv[i+1], "%s",  &hh[ 0 ] );
			fdefault = string( hh );
		}
		if ( 0 == strcmp( argv[i], "-sys") && argc > i ){
			char hh[ 256 ];
			sscanf( argv[i+1], "%s",  &hh[ 0 ] );
			systemModel = string( hh );
			if ( strcmp( systemModel.c_str(), "SY1527") == 0 ) systemType = SY1527;
			else if ( strcmp( systemModel.c_str(), "SY2527") == 0 ) systemType = SY2527;
			else if ( strcmp( systemModel.c_str(), "SY4527") == 0 ) systemType = SY4527;
			else if ( strcmp( systemModel.c_str(), "SY5527") == 0 ) systemType = SY5527;
			else if ( strcmp( systemModel.c_str(), "N568") == 0 ) systemType = N568;
			else if ( strcmp( systemModel.c_str(), "V65XX") == 0 ) systemType = V65XX;
			else if ( strcmp( systemModel.c_str(), "N1470") == 0 ) systemType = N1470;
			else if ( strcmp( systemModel.c_str(), "V8100") == 0 ) systemType = V8100;
			cratesysv.push_back( systemType );
			cratemodelv.push_back( systemModel );
		}
	}
	if ( crateipv.size() == 0 ) usage( argv[0] );
	if ( cratesysv.size() == 0 ){
		cout << "no crate types given, assuming SY4527 for all crate(s)" << endl;
	} else if ( cratesysv.size() != crateipv.size() ) {
		cout << "==>> not all crates have types. Re-think your command-line options, please ;-) <<==" << endl;
		usage( argv[0] );
	}
	vector<CRATE_DISCOVERED_MAP_t> mapv;
	string filename = fdefault + crateipv[ 0 ] + ".xml";

	// discover crates
	for ( uint32_t ic = 0; ic < crateipv.size(); ic++ ){
		CRATE_DISCOVERED_MAP_t crateMap;
		if ( cratesysv.size() == 0 ){
			crateMap.sytype = systemType;
			crateMap.crateModel = systemModel;
		} else {
			crateMap.sytype = cratesysv[ ic ];
			crateMap.crateModel = cratemodelv[ ic ];
		}
		// avoid two crates having the same name
		char xx[ 16 ];
		sprintf( xx, "_%d", ic );
		crateMap.crateName = systemModel + string(xx);
		crateMap.crateip = crateipv[ ic ];

		int ret = discoverCAENcrate( &crateMap );
		if ( ret ) {
			cout << "something went wrong with crate discovery. " << endl;
			return(-1);
		}
		mapv.push_back( crateMap );
	}

	// right. Lets write out an xml, plain strings, no checking, no boost
	// http://www.technical-recipes.com/2014/using-boostproperty_tree/
	writeMap( mapv, filename );

	// free allocated pointers in the server
	for ( uint32_t ic = 0; ic < crateipv.size(); ic++ ){
		CRATE_DISCOVERED_MAP_t map = mapv[ ic ];
		CAENHV_Free( map.NrofChList );
		CAENHV_Free( map.SerNumList );
		CAENHV_Free( map.FmwRelMinList );
		CAENHV_Free( map.FmwRelMaxList );
	}

#if 0
	// now, we want to have the settings: V0Set, V1Set, I0Set, I1Set, RDwn, RUp
	// from one crate
	CRATE_DISCOVERED_MAP_t crateMap = mapv[ 0 ];
	readParamsCAENcrate( &crateMap );

	// a pair of VMon and IMon to get the load
#endif

	return( 0 );
}

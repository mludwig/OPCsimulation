/*
 * Channel.h
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */

#ifndef SRC_CHANNEL_H_
#define SRC_CHANNEL_H_


#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <map>

#include <stdint.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>



#include "API.h"
using namespace ns_HAL_API;

using namespace std;

namespace ns_HAL_Channel {

#define CHANNEL_STATUS_BIT0 0x1
#define CHANNEL_STATUS_BIT1 0x2
#define CHANNEL_STATUS_BIT2 0x4
#define CHANNEL_STATUS_BIT3 0x8
#define CHANNEL_STATUS_BIT4 0x10
#define CHANNEL_STATUS_BIT5 0x20
#define CHANNEL_STATUS_BIT6 0x40
#define CHANNEL_STATUS_BIT7 0x80
#define CHANNEL_STATUS_BIT8 0x100
#define CHANNEL_STATUS_BIT9 0x200
#define CHANNEL_STATUS_BIT10 0x400
#define CHANNEL_STATUS_BIT11 0x800
#define CHANNEL_STATUS_BIT12 0x1000
#define CHANNEL_STATUS_BIT13 0x2000
#define CHANNEL_STATUS_BIT14 0x4000
#define CHANNEL_STATUS_BIT15 0x8000
#define CHANNEL_STATUS_BITXX 0x8fff // unused bits 16..31


#define CAEN_MAX_RAMP_SPEED 500.0

class Channel {
public:
	Channel( uint32_t ch, float vmax, float imax, string name ); // normal p.s. channel, generic
	Channel( uint32_t ch, string name );                // branch controller
	virtual ~Channel();
	string key( void ){ return(_key);}


	typedef enum { state_OFF, state_rampingUp, state_rampingDown, state_ON, state_ConstantCurrent, state_PendingTrip } SM_states_t;
	void sm( void );

	typedef enum { V_NOMINAL, OVERVOLTAGE, UNDERVOLTAGE } ChannelVoltageCondition_t;
	typedef enum { I_NOMINAL, OVERCURRENT } ChannelCurrentCondition_t;

	typedef enum { simpleSM, complexSM } ChannelStateMachineConfig_t;
	void configureSM ( ChannelStateMachineConfig_t c );

	// pilot API, simulation control, and also some simulated front panel inputs
	void pilot_setNoiseLevelVmon( float s ){ if ( s > 0 ) _noiseLevelVmon = s; }
	void pilot_setNoiseLevelImon( float s ){ if ( s > 0 ) _noiseLevelImon = s; }
	void pilot_setsimpleStateMachine( bool s ) { _lowVoltageStateMachine = s; }
	void pilot_setVtargetFromVSEL( bool f );
	void pilot_setImaxFromISEL( bool f );
	void pilot_setLoad( float res, float capa );
	void pilot_getLoad( float *res, float *capa );

	// API with some logic behind
	void set_Vramp_up( float s );
	void set_Vramp_down( float s );
	void set_TripTime( int32_t s );

	// standard API without logic, just containers
	void setName( string s );
	string name( void ){ return( _name ); }
	void setMyCrate( uint32_t i );
	uint32_t myCrate( void ){ return ( _my_icrate ); }
	void setMySlot( uint32_t i ) { _my_islot = i; }
	uint32_t mySlot( void ){ return ( _my_islot ); }
	uint32_t channel( void ){ return( _channel ); }
	// map<string,API::HAL_PROPERTY_t> allChangedItems( void );
	void showProperties( void );
	void subscribeParameter( string param, bool flag );

	map<string,API::HAL_PROPERTY_t> allChangedItems( void ){ return ( _allChangedItems() );	}

	// caen API
	vector<string> parameterList( void );
	uint32_t parameterAccessMode( string p ){ return( ( uint32_t ) _prop_map.at( p ).accessMode ); }
	uint32_t parameterDataType( string p ){ return( ( uint32_t ) _prop_map.at( p ).dataType ); }
	// overloaded version is clearer & simpler: first get the type, than the value.

	void getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type );
	void getParameterValue( string p, string *retval );
	void getParameterValue( string p, int8_t *retval );
	void getParameterValue( string p, int16_t *retval );
	void getParameterValue( string p, int32_t *retval );
	void getParameterValue( string p, uint8_t *retval );
	void getParameterValue( string p, uint16_t *retval );
	void getParameterValue( string p, uint32_t *retval );
	void getParameterValue( string p, float *retval );
	void getParameterValue( string p, bool *retval );

	float parameterMinval( string p );
	float parameterMaxval( string p );
	uint16_t parameterUnit( string p );
	int16_t parameterExp( string p );
	uint16_t parameterDecimal( string p );

	void setParameterValue( string p, string val );
	void setParameterValue( string p, int8_t val );
	void setParameterValue( string p, int16_t val );
	void setParameterValue( string p, int32_t val );
	void setParameterValue( string p, uint8_t val );
	void setParameterValue( string p, uint16_t val );
	void setParameterValue( string p, uint32_t val );
	void setParameterValue( string p, float val );

	void setDebug( bool f ){ _debug = f; };
	bool isBCchannel00( void) { return( _bcChannel00 );}

private:
	bool _debug;
	string _key;
	uint32_t _my_icrate;
	uint32_t _my_islot;
	// generic power supply API covering 95% of cases
	uint32_t _channel;
	float _HWVmax;   // hardware limits by design
	float _HWImax;
	string _name;    // symbolic name of the channel
	bool _bcChannel00;
	float _V0set;
	float _V1set;
	float _I0set; // in mA
	float _I1set;
	/// \todo implement tripping propagation on channel/crate...
	uint32_t _TripInt;
	uint32_t _TripExt;

	float _SVmax;    // programmable limit, by SW
	float _Vramp_up; // ramp up speed V/s
	float _Vramp_down;
	ChannelVoltageCondition_t _vcondition;
	ChannelCurrentCondition_t _icondition;
	int32_t _Pw;    // onoff = bool
	int32_t _Pon;   // onoff = bool
	int32_t _PDwn;  // onoff = bool
	float _Vmon;    // monitor output voltage
	float _Imon;
	int32_t _tripIntegrationTime;

	// internal object control
	float __resistance;        // in Ohm, can be set
	float __capacitance; // in Farad: 1e-12 = 1pF
	float __tripInfinite; // can hardcode this
	float __precisionVmonRamp;                 // ramp hysteresis
	float __precisionVmonTargetOn;             // plateau stickiness
	float __precisionVtargetOffsetFactor;      // V target is always 0.8% lower than specified: real electronics
	float __precisionVtargetOffsetLowValue;    // V target is always at least 2V lower than specified: real electronics
	float __precisionVtargetZeroValue;         // V target with Pw==off
	bool __powerOnOption; // persistency
	float __Vtarget;    // the setting stems from either V0 or V1 (I0 and I1), depending on _FP_VSEL of the crate,
	float __Imax;	 // but this is the actual current setting which is used in the state machine
	bool __VSEL;
	bool __ISEL;
	float __currentFactor; // uA or A

	float _tempChannel00branchController;

	// typedef enum { OFF, RUP, RDWN, OVC, OVV, UNV, EXTTRIP, INTTRIP, EXT_DIS } ChannelMessages_t; // returned by controller, depending on channel status
	// uint32_t _status;
	int32_t _status;

	// hand written state machine
	SM_states_t _SMstate;
	SM_states_t _SMstate_previous;
	struct timeval _now;
	struct timeval _tramp;
	struct timeval _ttrip1;
	struct timeval _ttrip2;
	struct timezone _tz;
	bool __pendingTripStartTime;
	bool _tripped;
	bool __constantCurrent;
	float _noiseLevelVmon;
	float _noiseLevelImon;
	int _symmetricNoiseCounter;
	float _noiseHalfAmplitudeImon;
	float _noiseHalfAmplitudeVmon;
	bool _lowVoltageStateMachine;

	void _refreshStatus( void );
	void _refreshPower( void );
	void _currentTrip( void );
	void _refreshVoltageCondition( void );
	void _refreshCurrentCondition( void );
	void _refresh_VSEL( void ){ this->pilot_setVtargetFromVSEL( __VSEL );}
	void _refresh_ISEL( void ){ this->pilot_setImaxFromISEL( __ISEL );}
	void _refresh_SVMax( void );

	void _updateControlInterface( void );
	void _updateResultInterface( void );
	void _updateChannel00branchController( void );


	map<string,API::CAEN_PROPERTY_t> _caenProp_map;
	map<string,API::HAL_PROPERTY_t> _prop_map;
	void _propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t );
	void _propinsert( API::HAL_PROPERTY_t h ) { _prop_map.insert( std::pair<string,API::HAL_PROPERTY_t>( h.name, h ));}
	void _testPropExist( string p );
	void _testPropNotExist( string p );

	bool _changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it );
	map<string,API::HAL_PROPERTY_t> _allChangedItems( void );

	// void _allChangedItems( void );
	// map<string,API::HAL_PROPERTY_t> _itemsv; // event mode changed items

};

} /* namespace ns_HAL_Channel */

#endif /* SRC_CHANNEL_H_ */

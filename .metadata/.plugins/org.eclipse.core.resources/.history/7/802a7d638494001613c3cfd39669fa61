/*
 * Distributor.cpp
 *
 *  Created on: Aug 24, 2016
 *      Author: mludwig
 * http://czmq.zeromq.org/manual:czmq
 */

#include "Distributor.h"


// C includes needed to make C-API happy
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

using namespace std;




namespace simulationHAL {

Distributor::Distributor() {
	_versions();
	_debug = false;
	_debugMessageRequests = false;
	_debugCZ = false;
}



Distributor::~Distributor() {
	// TODO Auto-generated destructor stub
}

void Distributor::_versions ( void ) {

	version_program	=  string(__DATE__) + string(" ") + string(__TIME__);

	int czmq_major = CZMQ_VERSION_MAJOR;
	int czmq_minor = CZMQ_VERSION_MINOR;
	int czmq_patch = CZMQ_VERSION_PATCH;

	std::ostringstream ostr;
	ostr << "major= " << czmq_major << " minor= " << czmq_minor << " patch= " << czmq_patch << endl;
	version_czeromq = ostr.str();  ostr.clear();

	ostr << PROTOBUF_C_VERSION << endl;
	version_protocol = ostr.str(); ostr.clear();

	ostr << PROTOBUF_C_VERSION_NUMBER << endl;
	version_protocol_c = ostr.str(); ostr.clear();

	const int size = 256;
	char sysrelease[ size ];
	FILE *fp1 = fopen("/etc/system-release","r");
	std::size_t ret_code = fread( sysrelease, sizeof *sysrelease, size, fp1 );
	fclose( fp1 );
	ostr << sysrelease << endl;
	version_ossystem =  ostr.str(); ostr.clear();

	char sysversion[ size ];
	FILE *fp2 = fopen("/proc/version","r");
	std::size_t ret2_code = fread( sysversion, sizeof *sysversion, size, fp2 );
	fclose( fp2 );
	ostr << sysversion << endl;
	version_osrelease =  ostr.str(); ostr.clear();
}

void Distributor::_caenCZreply( SCaen__CaenMessage *gReply ){
	uint32_t len = s_caen__caen_message__get_packed_size( gReply );
	if ( _debugCZ ) printf( "%s %d generic message packed len= %d\n", __FILE__, __LINE__, len );
	uint8_t buf_uint8[ len ];
	memset( buf_uint8, 0, len);
	uint32_t len2 = s_caen__caen_message__pack( gReply, buf_uint8 );
	if ( len - len2 ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _caenCZreply: packed message len= " << len << " and len2= "
				<< len2 << " sizes don't correspond. protocol buffer has a problem." << endl;
		throw std::runtime_error( os.str() );
	}

	// convert uint8 array into char* and send
	int char_sz = ar_uint8_to_char_sz( len2 );
	char buf_char[ char_sz ];
	ar_uint8_to_char( buf_uint8, buf_char, len2 );
	if(_debugCZ) printf( "%s %d sending %s char_sz= %d\n", __FILE__, __LINE__, buf_char, strlen(buf_char) );
	zstr_send ( sockGlue, (const char *) buf_char );
	if(_debugCZ) printf("%s %d ...sent OK\n", __FILE__ , __LINE__ );
}

void Distributor::_pilotCZreply( SCaen__PilotMessage *pReply ){
	uint32_t len = s_caen__pilot_message__get_packed_size( pReply );
	if ( _debugCZ ) printf( "%s %d pilot message packed len= %d\n", __FILE__, __LINE__, len );
	uint8_t buf_uint8[ len ];
	memset( buf_uint8, 0, len);
	uint32_t len2 = s_caen__pilot_message__pack( pReply, buf_uint8 );
	if ( len - len2 ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _pilotCZreply: packed message len= " << len << " and len2= "
				<< len2 << " sizes don't correspond. protocol buffer has a problem." << endl;
		throw std::runtime_error( os.str() );
	}

	// convert uint8 array into char* and send
	int char_sz = ar_uint8_to_char_sz( len2 );
	char buf_char[ char_sz ];
	ar_uint8_to_char( buf_uint8, buf_char, len2 );
	if( _debugCZ ) printf( "%s %d sending %s len= %d\n", __FILE__, __LINE__, buf_char, len );
	zstr_send ( sockPilot, (const char *) buf_char );
	if( _debugCZ ) printf("%s %d ...sent OK\n", __FILE__ , __LINE__ );
}



// distribute all glue messages to their HAL method as requests. The HAL
// organizes a zmq answer
void Distributor::glueDispatch( SCaen__CaenMessage *msg ){
	switch( msg->type ){
	case S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_request:{
		int ret = _CAENHV_InitSystem_request( msg->caenhv_initsystemrequest );
		if(_debug) cout << __FILE__ << " " << __LINE__ << " _CAENHV_InitSystem_request ret= " <<  ret << endl;
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_DeinitSystem_request:{
		int ret = _CAENHV_DeinitSystem_request( msg->caenhv_deinitsystemrequest );
		if(_debug) cout << __FILE__ << " " << __LINE__ << " _CAENHV_DeinitSystem_request ret= " <<  ret << endl;
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_request:{

		cout << __FILE__ << " " << __LINE__ << endl;
		cout << __FILE__ << " " << __LINE__ << " _CAENHV_GetCrateMap_request handle= " <<  msg->caenhv_getcratemaprequest->handle << endl;
		cout << __FILE__ << " " << __LINE__ << endl;


		int ret = _CAENHV_GetCrateMap_request( msg->caenhv_getcratemaprequest );
		if(_debug) cout << __FILE__ << " " << __LINE__ << " _CAENHV_GetCrateMap_request ret= " <<  ret << endl;
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropList_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropInfo_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamProp_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_SetChName_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_request:{
		break;
	}

	case S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_request:{
		break;
	}

	case   S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_request:{
		break;
	}

	case   S_CAEN__MESSAGE_TYPE__CAENHV_GetEventData_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_FreeEventData_request:{
		break;
	}

	case  S_CAEN__MESSAGE_TYPE__CAENHV_Free_request:{
		break;
	}

	default:{
		printf("unknown request message type, exiting\n");
		exit(-1);
		break;
	}
	} // switch
}

// specific request-reply
int Distributor::_CAENHV_InitSystem_request( SCaen__CAENHVInitSystemRequestM *request ){

	if ( _debugMessageRequests ){
		cout << __FILE__ << " " << __LINE__ << " _CAENHV_InitSystem_request " << endl;
		cout << __FILE__ << " " << __LINE__ << " arg= " <<  request->arg << endl;
		cout << __FILE__ << " " << __LINE__ << " linktype= " << request->linktype << endl;
		cout << __FILE__ << " " << __LINE__ << " passwd= " << request->passwd << endl;
		cout << __FILE__ << " " << __LINE__ << " username= " << request->username << endl;
	}

	// come up with a reply by interrogating the HAL: identify the object(s) to talk to
	// called once for each controller. The controller should exists already,
	// but if it does not we create a default controller from nothing,
	// just to keep code running. Defaults are 2 boards with 2 channels each
	uint32_t handle = 0;
	if ( ! Controller::have( handle ) ){
		Controller *c = new Controller( SY1527, handle );
	}

	// send back reply, pack it into a generic message
	SCaen__CaenMessage gReply;
	s_caen__caen_message__init( &gReply );
	gReply.type = S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_reply;
	SCaen__CAENHVInitSystemReplyM sReply;
	s_caen__caenhv__init_system_reply_m__init( &sReply );

	sReply.handle = handle; // copy the reply

	gReply.caenhv_initsystemreply = &sReply;
	_caenCZreply( &gReply );
	return( DISTRIBUTOR_OK );
}

int Distributor::_CAENHV_DeinitSystem_request( SCaen__CAENHVDeinitSystemRequestM *request ){
	if ( _debugMessageRequests ){
		cout << __FILE__ << " " << __LINE__ << " _CAENHV_DenitSystem_request " << endl;
		cout << __FILE__ << " " << __LINE__ << " handle= " <<  request->handle << endl;
	}
	uint32_t handle = request->handle;
	// stop communication on that handle:
	// tell the controller and all it's boards and channels to "be dead"
	// they can keep on running, so that the OPC server can reconnect and
	// eventually even discover them dynamically.
	if ( Controller::have( handle ) ){
		Controller_map.at( handle )->deinit();
	} else {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _CAENHV_DeinitSystem_request: could not find Controller with handle= " << handle << endl;
		throw std::runtime_error( os.str() );
	}

	// send back reply, pack it into a generic message
	SCaen__CaenMessage gReply;
	s_caen__caen_message__init( &gReply );
	gReply.type = S_CAEN__MESSAGE_TYPE__CAENHV_DeinitSystem_reply;
	SCaen__CAENHVDeinitSystemReplyM sReply;
	s_caen__caenhv__deinit_system_reply_m__init( &sReply );

	sReply.ret = 0; // CAENHV_OK

	gReply.caenhv_deinitsystemreply = &sReply;
	_caenCZreply( &gReply );
	return( DISTRIBUTOR_OK );
}

int Distributor::_CAENHV_GetCrateMap_request( SCaen__CAENHVGetCrateMapRequestM *request ){
	if ( _debugMessageRequests ){
		cout << __FILE__ << " " << __LINE__ << " _CAENHV_GetCrateMap_request " << endl;
		cout << __FILE__ << " " << __LINE__ << " handle= " <<  request->handle << endl;
	}


	uint32_t handle = request->handle;
	if ( ! Controller::have( handle ) ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _CAENHV_GetCrateMap_request: could not find Controller with handle= " << handle << endl;
		throw std::runtime_error( os.str() );
	}

	// interrogate that Controller to see which boards and channels it has
	Controller *c = Controller_map.at( handle );

	// interrogate that Controller to see which boards and channels it has

	int NrOfSlot = c->nbOfBoards();
	int dim = NrOfSlot;
	int NrofChList[ dim ];
	int SerNumList[ dim ];
	int FmwRelMinList[ dim ];
	int FmwRelMaxList[ dim ];
	char ModelList[ dim ][ CAEN_NAME_SIZE10 ];
	char DescriptionList[ dim ][ CAEN_NAME_SIZE10 ]; // dimension

	// debug
	{
		for ( int i = 0; i < NrOfSlot; i++ ){
			NrofChList[ i ] = i + 2;
			SerNumList[ i ] = i + 99;
			FmwRelMinList[ i ] = 1;
			FmwRelMaxList[ i ] = 2;
			sprintf( ModelList[ i ], "A99%d", i );
			sprintf( DescriptionList[ i ], "des A99%d", i );
		}
	}
	// -------------------------------------


	// send back reply, pack it into a generic message
	SCaen__CaenMessage gReply;
	s_caen__caen_message__init( &gReply );
	gReply.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_reply;
	SCaen__CAENHVGetCrateMapReplyM sReply;
	s_caen__caenhv__get_crate_map_reply_m__init( &sReply );

	cout << __FILE__ << " " << __LINE__ << endl;

	sReply.nrofslot = NrOfSlot;
	cout << __FILE__ << " " << __LINE__ << endl;

	sReply.nrofchlist = NrofChList;
	sReply.n_nrofchlist = dim;
	cout << __FILE__ << " " << __LINE__ << endl;

	sReply.sernumlist = SerNumList;
	sReply.n_sernumlist = dim;

	sReply.fmwrelminlist = FmwRelMinList;
	sReply.n_fmwrelminlist = dim;

	sReply.fmwrelmaxlist = FmwRelMaxList;
	sReply.n_fmwrelmaxlist = dim;

	sReply.n_modellist = dim;
	sReply.modellist = Distributor::calloc_charchar( dim, CAEN_NAME_SIZE10 );
	sReply.modellist[ 0 ] = (char *) string("xxx").c_str();
	sReply.modellist[ 1 ] = (char *) string("xxx1").c_str();

	sReply.n_descriptionlist = dim;
	sReply.descriptionlist = Distributor::calloc_charchar( dim, CAEN_NAME_SIZE10 );
	sReply.descriptionlist[ 0 ] = (char *) string("dexxx").c_str();
	sReply.descriptionlist[ 1 ] = (char *) string("dexxx1").c_str();

	gReply.caenhv_getcratemapreply = &sReply;
	_caenCZreply( &gReply );

	Distributor::free_charchar( sReply.modellist );
	Distributor::free_charchar( sReply.descriptionlist );
	return( DISTRIBUTOR_OK );
}



// ===pilot stuff===

void Distributor::pilotDispatch( SCaen__PilotMessage *msg ){


	switch( msg->type ){
	case S_CAEN__PILOT_MESSAGE_TYPE__pilot_setNoise_request:{
		if(_debug){
			cout << __FILE__ << __LINE__ << endl;
			cout << __FILE__ << __LINE__ << " " << msg->pilotsetnoiserequest->noise << endl;
		}

		// HAL stuff, which eventually throws back an answer, even from another thread
		int ret = _pilot_setNoise_request( msg->pilotsetnoiserequest );
		cout << __FILE__ << __LINE__ << " _pilot_setNoise_request ret= " <<  ret << endl;
		break;
	}

	default:{
		printf("unknown pilot request message type, exiting\n");
		exit(-1);
		break;
	}
	} // switch
}


int Distributor::_pilot_setNoise_request( SCaen__PilotSetNoiseRequestMsg *pRequest ){

	if ( _debug ){
		cout << __FILE__ << " " << __LINE__ << " _pilot_setNoise_request " << endl;
		cout << __FILE__ << " " << __LINE__ << "  noise= " <<  pRequest->noise << endl;
	}

	// come up with a reply by interrogating the HAL: identify the object(s) to talk to
	int ret = 0;

	// send back reply, pack it into generic message
	SCaen__PilotMessage pReply;
	s_caen__pilot_message__init( &pReply );
	pReply.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setNoise_request;

	s_caen__pilot_set_noise_reply_msg__init( pReply.pilotsetnoisereply );
	assert( pReply.pilotsetnoisereply );
	pReply.pilotsetnoisereply->ret = ret;

	_pilotCZreply( &pReply );
	return( DISTRIBUTOR_OK );
}


// conversions between protocol messages (uint8) and czmq messages (char * appended with \0\0 )
// these are needed, and it is better to have them explicit. We send all packed messages
// as hex strings over czmq.

// convert an array of uint8 to a string = char *
// each number takes 2 characters plus a whitespace in hex
// nb is the number of elements in *in
// buf has to be allocated to 3 * nb, therefore call the _sz before
/* static */ int Distributor::ar_uint8_to_char_sz( uint32_t nb ){
	return( 3 * nb ); // 2 chars plus blank + 2 ending 0
}
/* static */ void Distributor::ar_uint8_to_char( uint8_t *in, char *buf, uint32_t nb ){
	uint32_t len2 = Distributor::ar_uint8_to_char_sz( nb );
	memset( buf, 0, len2 );
	char cnum[ 3 ];
	int i = 0;
	for ( i = 0; i < nb; i++){
		sprintf( cnum, "%2x ", in[ i ]); // always 2 digits
		strcat ( buf, cnum );
	}
}

// convert a string = char * to an array of uint8
// each number takes 2 characters plus a whitespace: "FF "
// buf has to be allocated, get the dimension from _sz before
/* static */ int Distributor::char_to_ar_uint8_sz( uint32_t nb ){
	return( nb / 3 );
}
/* static */ void Distributor::char_to_ar_uint8( char *in, uint8_t *buf ){
	uint32_t len = strlen( in );
	uint32_t nb = Distributor::char_to_ar_uint8_sz( len );
	memset( buf, 0, nb );
	char delimiter[] = " ";
	int i = 0;
	char *sub = strtok ( in, delimiter);
	while ( sub != NULL)
	{
		int d = 0;
		sscanf( sub, "%x", &d );
		buf[ i++ ] = d;
		sub = strtok ( NULL, delimiter );
	}
}

/* static */ char** Distributor::calloc_charchar( uint32_t dim1, uint32_t dim2 ){
	char **rr = (char **) calloc( dim1, (unsigned long int) sizeof( char * ) );
	for ( int i = 0; i < dim1; i++ ){
		rr[ i ] = (char *) calloc( dim2, (unsigned long int) sizeof( char ) );
	}
	return( rr );
}

/* static */ void Distributor::free_charchar( char **v ){
	free( v );
}


} /* namespace simulationHAL */

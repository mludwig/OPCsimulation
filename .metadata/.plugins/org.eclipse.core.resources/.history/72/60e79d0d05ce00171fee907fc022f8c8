# 1. update and setup the system
# following lines are not re-executed if nothing has changed: it refers to the last image
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
RUN yum -y update
RUN yum -y install wget which protobuf-c-devel xerces-c-devel czmq-devel openssh-askpass git \
           gcc gcc-c++ cmake extra-cmake-modules boost boost-devel mc libxml2 libxml2-devel 
#
# 2. pull the VENUS_CAEN/OPC combo from nexus which has all the bins and configs needed
# this VENUS artifact is kept, since it was promoted to QA. This is generic
#ENV JENKINS_VENUS_BUILD 366
#RUN wget https://icejenkins.cern.ch/view/VENUS/job/VenusCAEN/$JENKINS_VENUS_BUILD/artifact/VENUS_CAEN.TEST.tar.gz -O VENUS_CAEN.TEST.tar.gz
# take venus/opc combo from nexus
ENV VENUS_VERSION 0.9
RUN wget https://repository.cern.ch/nexus/content/repositories/cern-venus/cc7/VENUS_CAEN.TEST/$VENUS_VERSION/VENUS_CAEN.TEST-$VENUS_VERSION.tar.gz
#RUN tar xvfz VENUS_CAEN.TEST.tar.gz
RUN tar xvzf VENUS_CAEN.TEST-$VENUS_VERSION.tar.gz

# the OPC server binaries for UA and open6 are included in the archive
# they are taken from explicit builds of jenkins, by the script OPCsimulation/make_jenkins_release.sh
# caen-opcua-server.ua/bin/OpcUaCaenServer.ua
# caen-opcua-server.open6/bin/OpcUaCaenServer.open6

# 3. set up the runtime environment. This is specific for one VENUS instance, We take Atlas as example.
# That specific set of config files is overwritten by the swarm config, for each (swarm) instance, in the
# docker_compose.yml
ADD entry-point.sh /
# select a config to run on that instance: use env vars as jenkins IDs, pointing to the reference build
ENV JENKINS_ROOT https://icejenkins.cern.ch/view/VENUS/job/VenusCAEN/$JENKINS_VENUS_BUILD/artifact
#
#ENV GLUECONFIG "caenGlueConfig.xml"
#ENV SIMCONFIG "sim_ATL.MDT.PS3.xml"
#ENV OPCCONFIG "opc_ATL.MDT.PS3.xml"
#
ENV GLUECONFIG "caenGlueConfig.xml"
ENV SIMCONFIG "sim_ALICE.ITS.xml"
ENV OPCCONFIG "opc_ALICE.ITS.xml"
#
# 4. setup runtime conditions
# expose docker ports: they need to be mapped
EXPOSE 8880 8881 4840 4841
#
# entry_point.sh is called from here onwards. Execute first the s.engine, then the OPC-UA server
ENTRYPOINT ["/entry-point.sh"]



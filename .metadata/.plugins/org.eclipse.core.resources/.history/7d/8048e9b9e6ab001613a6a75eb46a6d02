/*
 * Distributor.h
 *
 *  Created on: Aug 24, 2016
 *      Author: mludwig
 */

#ifndef SRC_DISTRIBUTOR_H_
#define SRC_DISTRIBUTOR_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <stdexcept>

#include <czmq.h>

#include "../../simulationOPCglue/protocolBuffers-c/simulationGlueCaen.pb-c.h"

// #include "Crate.h"
#include "Configuration.h"

using namespace std;
using namespace ns_HAL_Crate;
using namespace ns_HAL_Configuration;

namespace simulationHAL {


// caen api specific stuff... unavoidable
#define CAEN_NAME_SIZE10 10
#define CAEN_PARAM_SIZE256 256

#define DISTRIBUTOR_OK 0
#define DISTRIBUTOR_UNKNOWN_MSG_TYPE -1
#define DISTRIBUTOR_UNKNOWN_PILOT_MSG_TYPE -1000

#define DISTRIBUTOR_caenInitReply_ERROR -110
#define DISTRIBUTOR_caenDeinitReply_ERROR -120

class Distributor {
public:

	Distributor();
	virtual ~Distributor();

	static int ar_uint8_to_char_sz( uint32_t nb );
	static void ar_uint8_to_char( uint8_t *in, char *buf, uint32_t nb );
	static int char_to_ar_uint8_sz( uint32_t nb );
	static void char_to_ar_uint8( char *in, uint8_t *buf );
	static char** calloc_charchar( uint32_t dim1, uint32_t dim2 );
	// static void free_charchar( char **v );
	static void free_charchar( char **v, uint32_t dim1 );

	void glueDispatch( SCaen__CaenMessage *gRequest );
	void pilotDispatch( SCaen__PilotMessage *pRequest );

	void setDebug( bool d ){ _debug = d; }
	void setDebugMessageRequests( bool d ){ _debugMessageRequests = d; }
	void setDebugMessageReplies( bool d ){ _debugMessageReplies = d; }
	void setDebugCZ( bool d ){ _debugCZ = d; }
	void setCZsockets( zsock_t *sg, zsock_t *sp ){
		sockGlue = sg;
		sockPilot = sp;
	}

private:
	string version_program;
	string version_czeromq;
	string version_protocol;
	string version_protocol_c;
	string version_ossystem;
	string version_osrelease;

	bool _debug;
	bool _debugMessageRequests;
	bool _debugMessageReplies;
	bool _debugCZ;

	zsock_t *sockGlue;
	zsock_t *sockPilot;

	void _versions ( void );
	void _caenCZreply( SCaen__CaenMessage *gReply );

	int _CAENHV_InitSystem_request( SCaen__CAENHVInitSystemRequestM *request );
	int _CAENHV_DeinitSystem_request( SCaen__CAENHVDeinitSystemRequestM *request );
	int _CAENHV_GetCrateMap_request( SCaen__CAENHVGetCrateMapRequestM *request );
	int _CAENHV_GetSysPropList_request( SCaen__CAENHVGetSysPropListRequestM *request );
	int _CAENHV_GetSysPropInfo_request( SCaen__CAENHVGetSysPropInfoRequestM *request );
	int _CAENHV_GetSysProp_request( SCaen__CAENHVGetSysPropRequestM *request );
	int _CAENHV_GetBdParamInfo_request( SCaen__CAENHVGetBdParamInfoRequestM *request );
	int _CAENHV_GetBdParamProp_request( SCaen__CAENHVGetBdParamPropRequestM *request );
	int _CAENHV_GetBdParam_request( SCaen__CAENHVGetBdParamRequestM *request );
	int _CAENHV_GetChParamInfo_request( SCaen__CAENHVGetChParamInfoRequestM *request );

	int _pilot_setNoise_request( SCaen__PilotSetNoiseRequestM *pRequest );
	void _pilotCZreply( SCaen__PilotMessage *pReply );

	ns_HAL_Configuration::Configuration *cfg;
};

} /* namespace simulationHAL */

#endif /* SRC_DISTRIBUTOR_H_ */

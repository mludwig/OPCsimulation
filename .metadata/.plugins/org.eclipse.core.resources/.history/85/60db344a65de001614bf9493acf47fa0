/*
 * Channel.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */


/// \todo implement Vsel V0set and V1set correctly
/// \todo check state machine specs and detailled behavior: ramping, noise
/// \todo verify tripping behavior, and proliferation to crate
/// \todo channel/crate status, what is the semantics in reality, where is it reported
/// \todo implement persistency for settings depending on _powerOnOption
/// \todo do we really NOT need a standby state in the state machine ?
/// \todo hysteresis and precision of the ramping: does it do what it should ?
/// \todo pilot API implementation: noise level, load, trip external
/// \todo event mode, frequency
/// \todo grouped ramping between channels: if one ramps, all in the group ramp (command from crate)

#include "Channel.h"

namespace ns_HAL_Channel {

Channel::Channel( uint32_t ch, float hwvmax, float hwimax, string name  ) {
	_my_islot = 0;
	_channel = ch;
	_HWVmax = hwvmax;
	_HWImax = hwimax;
	_name = name;
	_V0set = 0;
	_I0set = 0;
	_V1set = 0;
	_I1set = 0;
	_SVmax = _HWVmax;
	_Vramp_up = 0.0;
	_Vramp_down = 0.0;
	_vcondition = V_NOMINAL;
	_icondition = I_NOMINAL;
	__powerOnOption = false;
	_Pw = false;
	_Pon = false; // disabled: loose settings

	/// \todo implement PWDwn correctly in the state machine
	_PDwn = false; // go down within 1 sec after current trip, no ramp down
	_Vmon = 0.0;
	_Imon = 0.0;
	_tripIntegrationTime = 5.0; // seconds, assume default
	__tripInfinite = 1000.0;     // according to specs
	_pendingTrip =  false;
	_tripped =  false;
	__load = 1.0e6;
	__precisionVmonRamp = 1.01;  // ramp hysteresis at 1%
	__precisionVmonTargetOn = 0.5;    // 0.5 V deviation allowed on target
	__precisionVtargetOffsetFactor = 0.95;  // V target is always 5% lower than specified: real electronics
	_noiseLevelVmon = 0.002;    // 0.2% typical
	_noiseLevelImon = 0.02;     // 2% typical
	_symmetricNoiseCounter = 0;
	_noiseHalfAmplitudeImon = 0;
	_noiseHalfAmplitudeVmon = 0;
	_lowVoltageStateMachine = false;
	// typedef enum { OFF, RUP, RDWN, OVC, OVV, UNV, EXTTRIP, INTTRIP, EXT_DIS } ChannelMessages_t; // returned by controller, depending on channel status
	_status = 0x0;

	__VSEL = false; // V0set and I0set, or V1 and I1
	__Vtarget = 0;
	__Iset = 0;
	_TripInt = 0x0;
	_TripExt = 0x0;

	// state machine
	_SMstate = state_OFF;


	// register the generic CAEN properties for all channels
    // API::CAEN_PROPERTY_t p;
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("V1Set",  API::ReadWriteProperty, API::F );
	_propinsert("I1Set",  API::ReadWriteProperty, API::F );
	_propinsert("RUp",    API::ReadWriteProperty, API::F );
	_propinsert("RDWn",   API::ReadWriteProperty, API::F );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("TripInt",  API::ReadWriteProperty, API::F );
	_propinsert("TripExt",  API::ReadWriteProperty, API::F );

	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("POn",  API::ReadWriteProperty, API::I );
	_propinsert("PDwn",  API::ReadWriteProperty, API::I );

	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );

	_propinsert("Status",  API::ReadOnlyProperty, API::I );

	// create the (still without values) HAL properties
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
	}

	_debug = false;
}

Channel::~Channel() {}

void Channel::setMyCrate( uint32_t i ) {
	_my_icrate = i;
	char buf[ 64 ];
	sprintf( buf, "crate%d_slot%d_channel%d", _my_icrate, _my_islot, _channel );
	_key = string( buf );
}

void Channel::_propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t ){
	_testPropNotExist( n );
	API::CAEN_PROPERTY_t p;
	p.name = n;	p.pAccessType = a;	p.pDataType = t;
	_caenProp_map.insert( std::pair<string,API::CAEN_PROPERTY_t>( p.name, p ));
}
void Channel::_testPropExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it == _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found";
		throw std::runtime_error( os.str() );
	}
}
void Channel::_testPropNotExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it != _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already";
		throw std::runtime_error( os.str() );
	}
}
void Channel::configureSM ( ChannelStateMachineConfig_t c ){

}

void Channel::_refreshStatus( void ){
	_status = 0x0;
	switch ( _SMstate ){
	// typedef enum { state_OFF, state_rampingUp, state_rampingDown, state_ON } SM_states_t;
	case state_OFF: { break; }
	case state_rampingUp: {
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT1;
		break;
	}
	case state_rampingDown: {
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT2;
		break;
	}
	case state_ON: {
		_status = _status | CHANNEL_STATUS_BIT0;
		break;
	}
	}

	if ( _icondition == OVERCURRENT ) _status = _status | CHANNEL_STATUS_BIT3;
	if ( _vcondition == OVERVOLTAGE ) _status = _status | CHANNEL_STATUS_BIT4;
	if ( _vcondition == UNDERVOLTAGE ) _status = _status | CHANNEL_STATUS_BIT5;

	/// \todo _status for external trip: don't know how to implement this BIT6

	if ( _Vmon >= _SVmax )  _status = _status | CHANNEL_STATUS_BIT7;
	if ( _Vmon >= _HWVmax ) _status = _status | CHANNEL_STATUS_BIT7;

	/// \todo _status for external disable: don't know how to implement this BIT8
	/// \todo _status for internal trip: don't know how to implement this BIT9
	/// \todo _status for calibration error: don't know how to implement this BIT10
	/// \todo _status for unplugged: don't know how to implement this BIT11

	// reserved BIT12

	// this is operating as a current source I guess... to be verified
	/// \todo _status for overvoltage protection: don't know how to implement this BIT13

	/// _status for power fail: don't know how to implement this BIT14
	/// _status for temperature error: don't know how to implement this BIT15, Temp comes from the board !?

	_status = _status & CHANNEL_STATUS_BITXX; // set unused bits to 0

	if ( _key ==  string("crate0_slot0_channel0") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " status= 0x" << hex << _status << dec << endl;
	}
}

void Channel::_refreshPower( void ){
	if ( __load > 0 ) {
		_Imon = _Vmon / __load;
	} else {
		_Imon = _HWImax;
	}
	// add symmetric noise
	switch ( _symmetricNoiseCounter){
	case 0: {
		_noiseHalfAmplitudeImon = _noiseLevelImon * (float) rand()/(float) RAND_MAX;
		_noiseHalfAmplitudeVmon = _noiseLevelVmon * (float) rand()/(float) RAND_MAX;
		_Imon += _noiseHalfAmplitudeImon;
		_Vmon += _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 1: {
		_Imon -= 2 * _noiseHalfAmplitudeImon;
		_Vmon -= 2 * _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 2: {
		_Imon += _noiseHalfAmplitudeImon;
		_Vmon += _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter = 0;
		break;
	}
	} // switch
}

void Channel::_refreshVoltageCondition( void ){

	// __precisionVmonRamp = 1.01;  // ramp hysteresis at 1%
	// __precisionVmonTargetOn = 0.5;    // 0.5 V deviation allowed on target
	// __precisionVtargetOffsetFactor = 0.95;  // V target is always 5% lower than specified: real electronics

	_vcondition = V_NOMINAL;
	if ( _Vmon > __Vtarget + __precisionVmonTargetOn ) _vcondition = OVERVOLTAGE;
	if ( _Vmon < __Vtarget - __precisionVmonTargetOn ) _vcondition = UNDERVOLTAGE;
}
void Channel::_refreshCurrentCondition( void ){
	_icondition = I_NOMINAL;
	if ( _pendingTrip ) _icondition = OVERCURRENT;
}

void Channel::_refresh_SVMax( void ){
	if ( __VSEL ){
		if ( _SVmax < _V1set ) _V1set = _SVmax;
	} else {
		if ( _SVmax < _V0set ) _V0set = _SVmax;
	}
}

// trip on current: must probably wait a bit
bool Channel::_currentTrip( void ){
	// constant current source, no tripping at all
	if ( _tripIntegrationTime >= __tripInfinite ) {
		_pendingTrip = false;
		_tripped = false;
	} else {
		if ( _Imon > __Iset ) {
			// overcurrent condition
			gettimeofday( &_now, &_tz );
			if ( !_pendingTrip ){
				gettimeofday( &_ttrip, &_tz );
				_pendingTrip = true;
			}
			double delta_ms = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
			if ( delta_ms > _tripIntegrationTime * 1000 ){
				// the trip stays active until reset
				_tripped = true;
			} else {
				_tripped = false;
			}
		} else {
			_pendingTrip = false;
			_tripped = false;
		}
	}
	return( _tripped );
}

// load api values into objects
void Channel::_updateControlInterface( void ){

	//cout << __FILE__ << " " << __LINE__ << " V0set= " << _V0set << endl;
	getParameterValue( "V0Set", &_V0set );
	//cout << __FILE__ << " " << __LINE__ << " V0set= " << _V0set << endl;
	getParameterValue( "V1Set", &_V1set );
	getParameterValue( "I0Set", &_I0set );
	getParameterValue( "I1Set", &_I1set );
	getParameterValue( "Pw", &_Pw );
	getParameterValue( "SVMax", &_SVmax );
	getParameterValue( "POn", &_Pon );
	getParameterValue( "PDwn", &_PDwn );

	float ff = 0;
	getParameterValue( "RUp", &ff ); set_Vramp_up( ff );
	getParameterValue( "RDWn", &ff ); set_Vramp_down( ff );
	getParameterValue( "Trip", &ff ); set_TripTime( ff );
}

// load object values into api
void Channel::_updateResultInterface( void ){
	setParameterValue( "VMon", _Vmon );
	setParameterValue( "IMon", _Imon );
	setParameterValue( "Status", _status );
//	ch->setParameterValue("TripInt", ch->TripInt() );
//	ch->setParameterValue("TripExt", ch->TripExt() );
}

// invoked any time the channel is read, or fires an event,
// but no private thread. The more frequent the higher the resolution...
/// \todo implement pilot interface or config for _lowVoltageStateMachine: a simplification of the state machine for PS without ramps (low voltage)
/// \todo V0Set (and V1Set) have to be lowered and published if they exceed limits SWVmax or HWVmax. Limits are respected,
/// but publishing does not yet work (event mode?).
void Channel::sm( void ){
	gettimeofday( &_now, &_tz );
	_updateControlInterface();

	// internal processing
	_refresh_SVMax();
	_refresh_VSEL();
	_refreshVoltageCondition();
	_refreshCurrentCondition();

	// can provoke ramping down by switching it off
	if ( _Pw == 0 ) {
		__Vtarget = 0;
		if ( _SMstate != state_OFF ){
			_SMstate = state_rampingDown;
		}
	}

#if 0
	// example for specific debugging before sm is executed
	if ( _key ==  string("crate0_slot0_channel0") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " sm state= " << (int) _SMstate
				<< " Vset= " << __Vtarget
				<< " V0set= " << _V0set
				<< " VMon= " << _Vmon
				<< " Pw= " << _Pw << endl;
	}
#endif

	if (( _SMstate != state_rampingUp ) && ( _SMstate != state_rampingDown )) {
		gettimeofday( &_tramp, &_tz );
	}


	// low voltage PS don't have ramps, we can easily eliminate them from the state
	// machine like this:
	if ( _lowVoltageStateMachine ){
		if ( _SMstate == state_rampingUp ) {
			_Vmon = __Vtarget;
			_SMstate = state_ON;
		}
		if ( _SMstate == state_rampingDown ) {
			_SMstate = state_OFF;
		}
	}

	switch ( _SMstate ) {
	case state_OFF:{
		_Vmon = 0.0;
		_Imon = 0.0;
		if ( _Vmon < __Vtarget * __precisionVmonRamp ){
			_SMstate = state_rampingUp;
		}
		break;
	}
	case state_rampingUp:{
		if ( _Vmon < __Vtarget * __precisionVmonRamp ){
			double delta = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
			double vdiff = _Vramp_up * delta / 1000.0;
			if ( _Vmon + vdiff < __Vtarget ) {
				_Vmon += vdiff;
			} else {
				_Vmon = __Vtarget;
				_SMstate = state_ON;
			}
			gettimeofday( &_tramp, &_tz );
		} else {
			_Vmon = __Vtarget;
			_SMstate = state_ON;
		}
		break;
	}
	case state_rampingDown:{
		if ( _Vmon * __precisionVmonRamp > __Vtarget ){
			// we are too high, even including some hysteresis
			double delta_ms = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
			double vdiff = _Vramp_down * delta_ms / 1000.0;
			if ( _Vmon - vdiff > __Vtarget ) {
				// we should make a step down, but will not arrive target yet
				_Vmon -= vdiff;
			} else {
				// we arrive target with less than one step, and should stay ON with 0 volts (=standby), no overshoot please
				_Vmon = __Vtarget;
				_SMstate = state_ON;
			}
			gettimeofday( &_tramp, &_tz );
		} else {
			// we have reached target
			_Vmon = __Vtarget;
			_SMstate = state_ON;
		}
		break;
	}
	case state_ON:{
		if ( _Vmon > ( __Vtarget + __precisionVmonTargetOn )) {
			_SMstate = state_rampingDown;
		} else if ( _Vmon < ( __Vtarget - __precisionVmonTargetOn )) {
			_SMstate = state_rampingUp;
		}
		break;
	}
	default: {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Channel:sm unknown state " << _SMstate
				<< " switching OFF as emergency. This is a bug." << endl;
		throw std::runtime_error( os.str() );
		break;
	}
	} // switch

	/// \todo implement again current trip with integration window
#if 0
	if ( _currentTrip() ) {
		if ( _key ==  string("crate0_slot0_channel0") ){
			cout << __FILE__ << " " << __LINE__ << " STATE => OFF after trip " << endl;
		}
		_SMstate = state_OFF;
		__Vset = 0;
		_Pw = false;
		_Vmon = 0.0;
		_Imon = 0.0;
	}
#endif

	_refreshPower();
	_refreshStatus();
	_updateResultInterface();	// read in api
}



// depends on _FP_VSEL from the crate. Default is 0=false=V0set
void Channel::pilot_setVSEL( bool f ){
	__VSEL = f;
	if ( f ) {
		__Vtarget = _V1set * __precisionVtargetOffsetFactor;
	} else {
		__Vtarget = _V0set * __precisionVtargetOffsetFactor;
	}
	if ( __Vtarget < 0 ) __Vtarget = 0;
}
void Channel::pilot_setISEL( bool f ){
	__ISEL = f;
	if ( f ) {
		__Iset = _I1set;
	} else {
		__Iset = _I0set;
	}
}


void Channel::set_TripTime( float s )
{
	if (( s > 0 ) && ( s < __tripInfinite )){
		_tripIntegrationTime = s;
	}
}
void Channel::set_Vramp_up( float s ){
	const float rmax = 20.0;
	if (( s > 0 ) && ( s <= rmax ))_Vramp_up = s;
}
void Channel::set_Vramp_down( float s )
{
	const float rmax = 20.0;
	if (( s > 0 ) && ( s <= rmax )) _Vramp_down = s;
}

void Channel::setName( string s ){
	const int size = 10;
	char hh[ size ];
	strncpy( hh, _name.c_str(), size );
}

vector<string> Channel::parameterList( void ){
	vector<string> v;
	v.clear();
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		v.push_back( _caenProp_map.at( it->first ).name );
	}
	return( v );
}

void Channel::getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	*type = prop.dataType;
}


void Channel::getParameterValue( string p, string *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, int8_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, int16_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, int32_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, uint8_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, uint16_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, uint32_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, float *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}


float Channel::parameterMinval( string p ){
	_testPropExist( p );
	return ( API::getPropAuxMinvalue( _prop_map.at( p ) ));
}
float Channel::parameterMaxval( string p ){
	_testPropExist( p );
	return ( API::getPropAuxMaxvalue( _prop_map.at( p ) ));
}
uint16_t Channel::parameterUnit( string p ){
	_testPropExist( p );
	return ( API::getPropAuxUnit( _prop_map.at( p ) ));
}
int16_t Channel::parameterExp( string p ){
	_testPropExist( p );
	return ( API::getPropAuxExp( _prop_map.at( p ) ));
}
uint16_t Channel::parameterDecimal( string p ){
	_testPropExist( p );
	return ( API::getPropAuxDecimal( _prop_map.at( p ) ));
}

void Channel::setParameterValue( string p, string val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_string = _prop_map.at( p ).value_string;
	_prop_map.at( p ).value_string = val;
}
void Channel::setParameterValue( string p, int8_t val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_int8 = _prop_map.at( p ).value_int8;
	_prop_map.at( p ).value_int8 = val;
}
void Channel::setParameterValue( string p, int16_t val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_int16 = _prop_map.at( p ).value_int16;
	_prop_map.at( p ).value_int16 = val;
}
void Channel::setParameterValue( string p, int32_t val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_int32 = _prop_map.at( p ).value_int32;
	_prop_map.at( p ).value_int32 = val;
}
void Channel::setParameterValue( string p, uint8_t val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_uint8 = _prop_map.at( p ).value_uint8;
	_prop_map.at( p ).value_uint8 = val;
}
void Channel::setParameterValue( string p, uint16_t val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_uint16 = _prop_map.at( p ).value_uint16;
	_prop_map.at( p ).value_uint16 = val;
}
void Channel::setParameterValue( string p, uint32_t val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_uint32 = _prop_map.at( p ).value_uint32;
	_prop_map.at( p ).value_uint32 = val;
}
void Channel::setParameterValue( string p, float val ){
	_testPropExist( p );
	_prop_map.at( p ).previous_float = _prop_map.at( p ).value_float;
	_prop_map.at( p ).value_float = val;
}

// returns a map with all properties which have changed since the last call
#if 0
map<string,API::HAL_PROPERTY_t> Channel::allChangedItems( void ){
	map<string,API::HAL_PROPERTY_t> itemsv;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if ( this->_changedItem( it ) ){
			itemsv.insert( std::pair<string,API::HAL_PROPERTY_t>(it->first, it->second) );
		}
	}
	return( itemsv );
}
#endif
void Channel::allChangedItems( void ){
	// map<string,API::HAL_PROPERTY_t> itemsv;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if ( this->_changedItem( it ) ){
			_itemsv.insert( std::pair<string,API::HAL_PROPERTY_t>(it->first, it->second) );
		}
	}
}


bool Channel::_changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it ){

	// any string change
	if ( it->second.value_string != it->second.previous_string ) return( true );

	// any channel status change
	if ( it->first == string("Status") ){

#if 0
		// this is how it should be
		if ( it->second.value_int32 != it->second.previous_int32 ) return( true );
		if ( it->second.value_uint32 != it->second.previous_uint32 ) return( true );
#endif

		// === for debugging only
		bool xx = false;
		if ( it->second.value_int32 != it->second.previous_int32 ) xx = true;
		if ( it->second.value_uint32 != it->second.previous_uint32 ) xx = true;
		if ( _key ==  string("crate0_slot0_channel0") && ( xx )) {
			cout << __FILE__ << " " << __LINE__ << " _changedItem status0 key= " << _key << " channel status= " << hex << it->second.value_uint32
					<< " previous status= " << it->second.previous_uint32 << dec << endl;
		}
		return( xx );
		// ====

	}

	// numeric parameters
	if ( ! it->second.subscribed ) return( false );
	const float limitFloat = 0.01; // 1%
	if ( pow( it->second.value_float - it->second.previous_float, 2 ) > limitFloat ) return( true );
	if ( pow( it->second.value_int8 -  it->second.previous_int8, 2  ) > limitFloat ) return( true );
	if ( pow( it->second.value_int16 - it->second.previous_int16, 2 ) > limitFloat ) return( true );
	if ( pow( it->second.value_int32 - it->second.previous_int32, 2 ) > limitFloat ) return( true );
	if ( pow( it->second.value_uint8 -  it->second.previous_uint8, 2  ) > limitFloat ) return( true );
	if ( pow( it->second.value_uint16 - it->second.previous_uint16, 2 ) > limitFloat ) return( true );
	if ( pow( it->second.value_uint32 - it->second.previous_uint32, 2 ) > limitFloat ) return( true );

	return( false );
}


} /* namespace ns_HAL_Channel */

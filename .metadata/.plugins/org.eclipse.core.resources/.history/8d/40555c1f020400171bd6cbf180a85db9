/*
 * Channel.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */


/// \todo implement Vsel V0set and V1set correctly
/// \todo check state machine specs and detailled behavior: ramping, noise
/// \todo verify tripping behavior, and proliferation to crate
/// \todo channel/crate status, what is the semantics in reality, where is it reported
/// \todo implement persistency for settings depending on _powerOnOption
/// \todo do we really NOT need a standby state in the state machine ?
/// \todo hysteresis and precision of the ramping: does it do what it should ?
/// \todo pilot API implementation: noise level, load, trip external
/// \todo event mode, frequency
/// \todo grouped ramping between channels: if one ramps, all in the group ramp (command from crate)

#include "Channel.h"

namespace ns_HAL_Channel {

Channel::Channel( uint32_t ch, float hwvmax, float hwimax, string name  ) {
	_bcChannel00 = false;
	_my_islot = 0;
	_my_icrate = 0;
	_channel = ch;
	_HWVmax = hwvmax;
	_HWImax = hwimax;
	_name = name;
	_V0set = 0;
	_I0set = 2; // seems to be mA
	_V1set = 0;
	_I1set = 2;
	_SVmax = _HWVmax * 0.9;
	_Vramp_up = 5.0;
	_Vramp_down = 5.0;
	_vcondition = V_NOMINAL;
	_icondition = I_NOMINAL;
	__powerOnOption = false;
	_Pw = false;
	_Pon = false; // disabled: loose settings


	/// \todo implement PWDwn correctly in the state machine
	_PDwn = false; // go down within 1 sec after current trip, no ramp down
	_Vmon = 0.0;
	_Imon = 0.0;
	_tripIntegrationTime = 5.0; // seconds, assume default
	__tripInfinite = 1000;     // according to specs
	__pendingTrip =  false;
	_tripped =  false;
	__resistance = 1.0e6;
	__capacitance = 1e-12; // pF
	__precisionVmonRamp = 1.01;              // ramp hysteresis at 1%
	__precisionVmonTargetOn = 0.5;           // 0.5 V deviation allowed on target, plateau stickiness
	__precisionVtargetOffsetFactor = 0.999;  // V target is always 0.1% lower than specified: real electronics
	__precisionVtargetOffsetLowValue = -1.7; // V target is always at least 1.7V lower than specified: real electronics
	__precisionVtargetZeroValue = 0.0;      // V target pW=0
	_noiseLevelVmon = 0.001;    // 0.1% typical
	_noiseLevelImon = 0.01;     // 1% typical
	_symmetricNoiseCounter = 0;
	_noiseHalfAmplitudeImon = 0;
	_noiseHalfAmplitudeVmon = 0;
	_lowVoltageStateMachine = false;
	// typedef enum { OFF, RUP, RDWN, OVC, OVV, UNV, EXTTRIP, INTTRIP, EXT_DIS } ChannelMessages_t; // returned by controller, depending on channel status
	_status = 0x0;

	__VSEL = false; // V0set
	__ISEL = false; // I0set
	__currentFactor = 1e6; // uA
	__Vtarget = 0;
	__Imax = 0;
	_TripInt = 0x0;
	_TripExt = 0x0;

	// state machine
	_SMstate = state_OFF;

	_tempChannel00branchController = 0;


	// register the generic CAEN properties for all channels
    // API::CAEN_PROPERTY_t p;
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("V1Set",  API::ReadWriteProperty, API::F );
	_propinsert("I1Set",  API::ReadWriteProperty, API::F );
	_propinsert("RUp",    API::ReadWriteProperty, API::F );
	_propinsert("RDWn",   API::ReadWriteProperty, API::F );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("TripInt",  API::ReadWriteProperty, API::F );
	_propinsert("TripExt",  API::ReadWriteProperty, API::F );

	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("POn",  API::ReadWriteProperty, API::I );
	_propinsert("PDwn",  API::ReadWriteProperty, API::I );

	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );

	_propinsert("Status",  API::ReadOnlyProperty, API::I );

	// create the (still without values) HAL properties
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
	}

	// set the write properties to their starting values as specified in the object
	// this is where the persistency should be implemented
	setParameterValue("V0Set", _V0set );
	setParameterValue("I0Set", _I0set );
	setParameterValue("V1Set", _V1set );
	setParameterValue("I1Set", _I1set );
	setParameterValue("RUp", _Vramp_up );
	setParameterValue("RDWn", _Vramp_down );
	setParameterValue("Trip", _tripIntegrationTime );
	setParameterValue("SVMax", _SVmax );
	setParameterValue("TripInt", _TripInt );
	setParameterValue("TripExt", _TripExt );
	setParameterValue("Pw", _Pw );
	setParameterValue("POn", _Pon );
	setParameterValue("PDwn", _PDwn );

	_debug = false;
}

// channel00 of a branch controller: it acts like a board with some special properties and executes no state machine
// but pops up in address space like a channel00. crappy logic, but thats how it is.
Channel::Channel( uint32_t ch, string name ) {
	cout << __FILE__ << " " << __LINE__ << " Channel::Channel: branch controller channel00 " << endl;

	_bcChannel00 = true;
	_my_islot = 0;
	_my_icrate = 0;
	_channel = ch;
	_HWVmax = 0;
	_HWImax = 0;
	_name = name;
	_V0set = 0;
	_I0set = 2; // seems to be mA
	_V1set = 0;
	_I1set = 2;
	_SVmax = _HWVmax * 0.9;
	_Vramp_up = 5.0;
	_Vramp_down = 5.0;
	_vcondition = V_NOMINAL;
	_icondition = I_NOMINAL;
	__powerOnOption = false;
	_Pw = false;
	_Pon = false; // disabled: loose settings

	/// \todo implement PWDwn correctly in the state machine
	_PDwn = false; // go down within 1 sec after current trip, no ramp down
	_Vmon = 0.0;
	_Imon = 0.0;
	_tripIntegrationTime = 0; // seconds, assume default
	__tripInfinite = 1000;     // according to specs
	__pendingTrip =  false;
	_tripped =  false;
	__resistance = 0;
	__capacitance = 0;
	__precisionVmonRamp = 0;
	__precisionVmonTargetOn = 0;
	__precisionVtargetOffsetFactor = 0;
	__precisionVtargetOffsetLowValue = 0;
	__currentFactor = 1e6; // uA
	//__precisionVtargetZeroValue = 0;
	_noiseLevelVmon = 0.0;
	_noiseLevelImon = 0.0;
	_symmetricNoiseCounter = 0;
	_noiseHalfAmplitudeImon = 0;
	_noiseHalfAmplitudeVmon = 0;
	_lowVoltageStateMachine = false;
	// typedef enum { OFF, RUP, RDWN, OVC, OVV, UNV, EXTTRIP, INTTRIP, EXT_DIS } ChannelMessages_t; // returned by controller, depending on channel status
	_status = 0x0;
	_tempChannel00branchController = 0;
	__VSEL = false; // V0set
	__ISEL = false; // I0set
	__Vtarget = 0;
	__Imax = 0;
	_TripInt = 0x0;
	_TripExt = 0x0;

	// state machine
	_SMstate = state_OFF;

	cout << __FILE__ << " " << __LINE__ << " Channel::Channel: branch controller channel00 inserting properties..." << endl;

	// dynamic values
	_propinsert("Temp",  API::ReadOnlyProperty, API::F );

	// static values
	_propinsert("12VPwS",  API::ReadOnlyProperty, API::I );// probably dynamic as well
	_propinsert("48VPwS",  API::ReadOnlyProperty, API::I );// probably dynamic as well
	_propinsert("Rel",  API::ReadOnlyProperty, API::F );
	_propinsert("SerNum",  API::ReadOnlyProperty, API::F );
	_propinsert("Sync",  API::ReadOnlyProperty, API::I );
	_propinsert("HVSync",  API::ReadOnlyProperty, API::I );
	_propinsert("RemIlk",  API::ReadOnlyProperty, API::I );
	_propinsert("MainPwS",  API::ReadOnlyProperty, API::I );
	_propinsert("RemBdName",  API::ReadOnlyProperty, API::I ); // shouldn't this be a string ?

	// create the (still without values) HAL properties
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
	}

	cout << __FILE__ << " " << __LINE__ << " Channel::Channel: branch controller channel00 setting fixed defaults..." << endl;
	// set static RO params to fixed values
	setParameterValue("Rel", (float) 9.99 );
	setParameterValue("SerNum", (float) 9.99 );
	setParameterValue("12VPwS", (int32_t) 12 );
	setParameterValue("48VPwS", (int32_t) 48 );
	setParameterValue("Sync", (int32_t) 1 );
	setParameterValue("HVSync", (int32_t) 1 );
	setParameterValue("RemIlk", (int32_t) 1 );
	setParameterValue("MainPwS", (int32_t) 220 );
	setParameterValue("RemBdName", (int32_t) 0 );

	_debug = true;
}

Channel::~Channel() {}

void Channel::setMyCrate( uint32_t i ) {
	_my_icrate = i;
	char buf[ 64 ];
	sprintf( buf, "crate%d_slot%d_channel%d", _my_icrate, _my_islot, _channel );
	_key = string( buf );
}

void Channel::_propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t ){
	_testPropNotExist( n );
	API::CAEN_PROPERTY_t p;
	p.name = n;	p.pAccessType = a;	p.pDataType = t;
	_caenProp_map.insert( std::pair<string,API::CAEN_PROPERTY_t>( p.name, p ));
}
void Channel::_testPropExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it == _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= " << _key;
		throw std::runtime_error( os.str() );
	}
}
void Channel::_testPropNotExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it != _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= " << _key;
		throw std::runtime_error( os.str() );
	}
}
void Channel::configureSM ( ChannelStateMachineConfig_t c ){

}

/// \todo to avoid status flipping check the current and voltage conditions and the ramping in sm
///       it should not flip around when at 0
void Channel::_refreshStatus( void ){
	_status = 0x0;
	switch ( _SMstate ){
	// typedef enum { state_OFF, state_rampingUp, state_rampingDown, state_ON } SM_states_t;
	case state_OFF: { break; }
	case state_rampingUp: {
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT1;
		break;
	}
	case state_rampingDown: {
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT2;
		break;
	}
	case state_ON: {
		_status = _status | CHANNEL_STATUS_BIT0;
		break;
	}
	}

	// in reality, most boards do not show these bits when ramping, but according to specs
	// they should...
	// let's fake the real electronics for now
#if 0
	if ( _icondition == OVERCURRENT ) _status = _status | CHANNEL_STATUS_BIT3;
	if ( _vcondition == OVERVOLTAGE ) _status = _status | CHANNEL_STATUS_BIT4;
	if ( _vcondition == UNDERVOLTAGE ) _status = _status | CHANNEL_STATUS_BIT5;
#endif

	/// \todo _status for external trip: don't know how to implement this on BIT6

	// overcurrent, but no trip
	if ( __constantCurrent ) _status = _status | CHANNEL_STATUS_BIT3; // OVERCURRENT bin1001=dec9



	if ( _Vmon >= _SVmax )  _status = _status | CHANNEL_STATUS_BIT7;
	if ( _Vmon >= _HWVmax ) _status = _status | CHANNEL_STATUS_BIT7;

	/// \todo _status for external disable: don't know how to implement this BIT8
	/// \todo _status for internal trip: don't know how to implement this BIT9
	/// \todo _status for calibration error: don't know how to implement this BIT10
	/// \todo _status for unplugged: don't know how to implement this BIT11

	// reserved BIT12

	// this is operating as a current source I guess... to be verified
	/// \todo _status for overvoltage protection: don't know how to implement this BIT13

	/// _status for power fail: don't know how to implement this BIT14
	/// _status for temperature error: don't know how to implement this BIT15, Temp comes from the board !?

	_status = _status & CHANNEL_STATUS_BITXX; // set unused bits to 0

#if 0
	if ( _key ==  string("crate0_slot0_channel0") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " status= 0x" << hex << _status << dec << endl;
	}
#endif
}

void Channel::_refreshPower( void ){
	if ( _SMstate == state_OFF ){
		_Vmon = 0.0;
		_Imon = 0.0;
		return;
	}
	if ( __resistance > 0 ) {
		_Imon = _Vmon / __resistance; // in uA most of the time
		if ( _SMstate == state_rampingUp )
			_Imon += __capacitance * _Vramp_up;
		else if ( _SMstate == state_rampingDown )
			_Imon += __capacitance * _Vramp_down;
	} else {
		// current source
		_Imon = _HWImax;
	}

	// add symmetric noise
	switch ( _symmetricNoiseCounter){
	case 0: {
		_noiseHalfAmplitudeImon = _noiseLevelImon * _Imon * (float) rand()/(float) RAND_MAX;
		_noiseHalfAmplitudeVmon = _noiseLevelVmon * _Vmon * (float) rand()/(float) RAND_MAX;
		_Imon += _noiseHalfAmplitudeImon;
		_Vmon += _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 1: {
		_Imon -= 2 * _noiseHalfAmplitudeImon;
		_Vmon -= 2 * _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 2: {
		_Imon += _noiseHalfAmplitudeImon;
		_Vmon += _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter = 0;
		break;
	}
	} // switch
}

void Channel::_refreshVoltageCondition( void ){
	_vcondition = V_NOMINAL;
	if ( _Vmon > __Vtarget + __precisionVmonTargetOn ) _vcondition = OVERVOLTAGE;
	if ( _Vmon < __Vtarget - __precisionVmonTargetOn ) _vcondition = UNDERVOLTAGE;
}
void Channel::_refreshCurrentCondition( void ){
	_icondition = I_NOMINAL;
	if ( __pendingTrip ) _icondition = OVERCURRENT;
}

void Channel::_refresh_SVMax( void ){
	if ( __VSEL ){
		if ( _SVmax < _V1set ) _V1set = _SVmax;
	} else {
		if ( _SVmax < _V0set ) _V0set = _SVmax;
	}
}

// evaluate current related behavior: either constant current or trip after waiting for tripping-delay
// returns true if the channel has physically tripped and goes off, after the tripping-delay
bool Channel::_currentTrip( void ){

	if ( _tripIntegrationTime >= __tripInfinite ) {
		// constant current source, no tripping at all, but adjust VMon and OVERCURRENT status
		__pendingTrip = false;
		_tripped = false;
		__constantCurrent = true;

	} else {
		if ( _Imon > __Imax ) {
			// overcurrent condition, waiting for trip= dec520
			gettimeofday( &_now, &_tz );
			if ( !__pendingTrip ){
				gettimeofday( &_ttrip, &_tz );
				__pendingTrip = true;
			}
			double delta_ms = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
			if ( delta_ms > _tripIntegrationTime * 1000 ){
				// the trip stays active until reset
				_tripped = true; // dec512
			} else {
				_tripped = false;
			}
		} else {
			__pendingTrip = false;
			_tripped = false;
		}
	}
	return( _tripped );
}

// load api values into objects
void Channel::_updateControlInterface( void ){

	//cout << __FILE__ << " " << __LINE__ << " V0set= " << _V0set << endl;
	getParameterValue( "V0Set", &_V0set );
	//cout << __FILE__ << " " << __LINE__ << " V0set= " << _V0set << endl;
	getParameterValue( "V1Set", &_V1set );
	getParameterValue( "I0Set", &_I0set );
	getParameterValue( "I1Set", &_I1set );
	getParameterValue( "Pw", &_Pw );
	getParameterValue( "SVMax", &_SVmax );
	getParameterValue( "POn", &_Pon );
	getParameterValue( "PDwn", &_PDwn );

	// settings with some limits
	float ff = 0;
	getParameterValue( "RUp", &ff ); set_Vramp_up( ff );
	getParameterValue( "RDWn", &ff ); set_Vramp_down( ff );

	/// \todo channel trip is not tested nor understood...
	getParameterValue( "Trip", &ff ); set_TripTime( ff );
}

// load object values into api
void Channel::_updateResultInterface( void ){
	setParameterValue( "VMon", _Vmon );
	setParameterValue( "IMon", _Imon * __currentFactor );
	setParameterValue( "Status", _status );
//	ch->setParameterValue("TripInt", ch->TripInt() );
//	ch->setParameterValue("TripExt", ch->TripExt() );
}

void Channel::_updateChannel00branchController( void ){
	static int counter = 0;
	if ( counter++ > 20 ){
		_tempChannel00branchController = 22.0 + ( (double) rand()/RAND_MAX - 0.5 ) * 1.8;
		// if ( _debug ) cout << __FILE__ << " " << __LINE__ << " board slot = " << _slot << " Temp= " << _Temp << endl;
		counter = 0;
	}
	setParameterValue( "Temp", _tempChannel00branchController );
}

// invoked any time the channel is read, or fires an event,
// but no private thread. The more frequent the higher the resolution...
/// \todo implement pilot interface or config for _lowVoltageStateMachine: a simplification of the state machine for PS without ramps (low voltage)
/// \todo V0Set (and V1Set) have to be lowered and published if they exceed limits SWVmax or HWVmax. Limits are respected,
/// but publishing does not yet work (event mode?).
void Channel::sm( void ){
	if ( _bcChannel00 ) {
		_updateChannel00branchController();
		return;
	}
	gettimeofday( &_now, &_tz );

	// internal processing, getting new control values
	_updateControlInterface();
	_refresh_SVMax();
	_refresh_VSEL();
	_refresh_ISEL();
	_refreshVoltageCondition();
	_refreshCurrentCondition();
	_SMstate_previous = _SMstate;

	// 1. determine the state we should be in (conditions), which are in fact transitions
	// 1.a can provoke ramping down by switching it off, and ramping up by switching it on: overwrite __Vtarget
	if ( _Pw == 0 ) {
		__Vtarget = __precisionVtargetZeroValue;
		if ( _SMstate != state_OFF ){
			_SMstate = state_rampingDown;
		}
	} else {
		if ( _SMstate != state_OFF ){
			_SMstate = state_rampingUp;
		}
	}

	// 1.b low voltage PS don't have ramps, we can easily eliminate them from the state machine like this:
	if ( _lowVoltageStateMachine ){
		if ( _SMstate == state_rampingUp ) {
			_Vmon = __Vtarget;
			_SMstate = state_ON;
		}
		if ( _SMstate == state_rampingDown ) {
			_SMstate = state_OFF;
		}
	}

	// 1.c ramp up
	if ( _Vmon < __Vtarget * __precisionVmonRamp ) _SMstate = state_rampingUp;

	// 1.d ramp down
	if ( _Vmon * __precisionVmonRamp > __Vtarget ) _SMstate = state_rampingDown;

	// 1.e on, steady
	if (( _Vmon < ( __Vtarget + __precisionVmonTargetOn )) && ( _Vmon > ( __Vtarget - __precisionVmonTargetOn )) ) _SMstate = state_ON;

	// 1.f off
	if ( ( __Vtarget == 0 ) && ( _Vmon <= 0.5 )) _SMstate = state_OFF;


	// take ramping times
	if (( _SMstate != state_rampingUp ) && ( _SMstate != state_rampingDown )) {
		gettimeofday( &_tramp, &_tz );
	}


#if 0
	// example for specific debugging before sm is executed
	if ( _key ==  string("crate0_slot0_channel0") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " sm state= " << (int) _SMstate
				<< " Vtarget= " << __Vtarget
				<< " V0set= " << _V0set
				<< " V1set= " << _V1set
				<< " VMon= " << _Vmon
				<< " Pw= " << _Pw << endl;
	}
#endif

	// 2. execute states
	switch ( _SMstate ) {
	case state_OFF:{
		_Vmon = 0.0;
		_Imon = 0.0;
		break;
	}
	case state_rampingUp:{
		double delta = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
		double vdiff = _Vramp_up * delta / 1000.0;
		if ( _Vmon + vdiff < __Vtarget ) {
			_Vmon += vdiff;
		} else {
			_Vmon = __Vtarget; // last step is smaller
		}
		gettimeofday( &_tramp, &_tz );
		break;
	}
	case state_rampingDown:{
		double delta_ms = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
		double vdiff = _Vramp_down * delta_ms / 1000.0;
		if ( _Vmon - vdiff > __Vtarget ) {
			_Vmon -= vdiff;
		} else {
			_Vmon = __Vtarget;// last step is smaller
		}
		gettimeofday( &_tramp, &_tz );
		break;
	}
	case state_ON:{
		_Vmon = __Vtarget;
		break;
	}
	default: {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Channel:sm unknown state " << _SMstate
				<< " switching OFF as emergency. This is a bug." << endl;
		throw std::runtime_error( os.str() );
		break;
	}
	} // switch


	_refreshPower();
	_currentTrip();
	_refreshStatus();
	_updateResultInterface();	// read in api
}


void Channel::pilot_setLoad( float res, float capa ){
	if ( res >= 0 ) __resistance = res;
	if ( capa >= 0 ) __capacitance = capa;
}
void Channel::pilot_getLoad( float *res, float *capa ){
	*res  = __resistance;
	*capa = __capacitance;
}


// depends on _FP_VSEL from the crate. Default is 0=false=V0set
void Channel::pilot_setVtargetFromVSEL( bool f ){
	__VSEL = f;
	if ( f ) {
		__Vtarget = _V1set * __precisionVtargetOffsetFactor + __precisionVtargetOffsetLowValue;
	} else {
		__Vtarget = _V0set * __precisionVtargetOffsetFactor + __precisionVtargetOffsetLowValue;
	}
	if ( __Vtarget < 0 ) __Vtarget = 0.0;
}

void Channel::pilot_setImaxFromISEL( bool f ){
	__ISEL = f;
	if ( f ) {
		__Imax = _I1set;
	} else {
		__Imax = _I0set;
	}
	if ( __Imax < 0 ) __Imax = 0;
}



void Channel::set_TripTime( int32_t s )
{
	if (( s > 0 ) && ( s < __tripInfinite )){
		_tripIntegrationTime = s;
	}
}
void Channel::set_Vramp_up( float s ){
	if (( s > 0 ) && ( s <= CAEN_MAX_RAMP_SPEED ))_Vramp_up = s;
}
void Channel::set_Vramp_down( float s )
{
	if (( s > 0 ) && ( s <= CAEN_MAX_RAMP_SPEED )) _Vramp_down = s;
}

void Channel::setName( string s ){
	const int size = 10;
	char hh[ size ];
	strncpy( hh, _name.c_str(), size );
}

vector<string> Channel::parameterList( void ){
	vector<string> v;
	v.clear();
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		v.push_back( _caenProp_map.at( it->first ).name );
	}
	return( v );
}

void Channel::getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	*type = prop.dataType;
}


void Channel::getParameterValue( string p, string *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, int8_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, int16_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, int32_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, uint8_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, uint16_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, uint32_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Channel::getParameterValue( string p, float *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}


float Channel::parameterMinval( string p ){
	_testPropExist( p );
	return ( API::getPropAuxMinvalue( _prop_map.at( p ) ));
}
float Channel::parameterMaxval( string p ){
	_testPropExist( p );
	return ( API::getPropAuxMaxvalue( _prop_map.at( p ) ));
}
uint16_t Channel::parameterUnit( string p ){
	_testPropExist( p );
	return ( API::getPropAuxUnit( _prop_map.at( p ) ));
}
int16_t Channel::parameterExp( string p ){
	_testPropExist( p );
	return ( API::getPropAuxExp( _prop_map.at( p ) ));
}
uint16_t Channel::parameterDecimal( string p ){
	_testPropExist( p );
	return ( API::getPropAuxDecimal( _prop_map.at( p ) ));
}

void Channel::setParameterValue( string p, string val ){
	_testPropExist( p );
	_prop_map.at( p ).value_string = val;
}
void Channel::setParameterValue( string p, int8_t val ){
	_testPropExist( p );
	_prop_map.at( p ).value_int8 = val;
}
void Channel::setParameterValue( string p, int16_t val ){
	_testPropExist( p );
	_prop_map.at( p ).value_int16 = val;
}
void Channel::setParameterValue( string p, int32_t val ){
	_testPropExist( p );
	_prop_map.at( p ).value_int32 = val;
}
void Channel::setParameterValue( string p, uint8_t val ){
	_testPropExist( p );
	_prop_map.at( p ).value_uint8 = val;
}
void Channel::setParameterValue( string p, uint16_t val ){
	_testPropExist( p );
	_prop_map.at( p ).value_uint16 = val;
}
void Channel::setParameterValue( string p, uint32_t val ){
	_testPropExist( p );
	_prop_map.at( p ).value_uint32 = val;
}
void Channel::setParameterValue( string p, float val ){
	_testPropExist( p );
	_prop_map.at( p ).value_float = val;
}

// returns a map with all properties which have changed since the last call
map<string,API::HAL_PROPERTY_t> Channel::_allChangedItems( void ){
	map<string,API::HAL_PROPERTY_t> itemsv; itemsv.clear();
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if ( this->_changedItem( it ) ){
			itemsv.insert( std::pair<string,API::HAL_PROPERTY_t>(it->first, it->second) );
			// cout << __FILE__ << " " << __LINE__ << " found changed item= " << it->first << endl;
		}
	}
	return( itemsv );
}

/// property value is checked if it has changed. If no: false
/// if comparison to previous shows difference, update previous and report true.
/// this works for all properties set and RW and RO alike, not just for sets.
bool Channel::_changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it ){
	/// \todo make numerical change limit float adjustable through pilot API
	const float limitFloat = 0.02; // 2%


#if 0
	// debugging
	cout << __FILE__ << " " << __LINE__ << " _changedItem ? key= " << _key
			<< " item= " << it->first << " type= " << it->second.dataType << endl;
	if ( _key ==  string("crate0_slot0_channel0") && ( it->first == "VMon" )) {
		bool xx = false;
		if ( pow( it->second.value_float - it->second.previous_float, 2 ) > limitFloat ) xx = true;
		cout << __FILE__ << " " << __LINE__ << " _changedItem status0 key= " << _key << " VMon= " << it->second.value_float
				<< " pVMon= " << it->second.previous_float << " pow= "
				<< pow( it->second.value_float - it->second.previous_float, 2 ) << " xx= " << xx << endl;
	}
#endif

	if ( ! it->second.subscribed ) return( false );

	// any channel status change
	if ( it->first == string("Status") ){
		if ( it->second.value_int32 != it->second.previous_int32 ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
#if 0
		if ( it->second.value_uint32 != it->second.previous_uint32 ) {
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
#endif
	}

	// any channel state machine change
	if ( _SMstate != _SMstate_previous ) return( true );


	// numeric parameters
	switch ( it->second.dataType ){
	case API::treal: {
		if ( pow( it->second.value_float - it->second.previous_float, 2 ) > limitFloat ) {
			it->second.previous_float = it->second.value_float;
			return( true );
		}
		break;
	}
	case API::tint8: {
		if ( pow( it->second.value_int8 - it->second.previous_int8, 2 ) > limitFloat ) {
			it->second.previous_int8 = it->second.value_int8;
			return( true );
		}
		break;
	}
	case API::tint16: {
		if ( pow( it->second.value_int16 - it->second.previous_int16, 2 ) > limitFloat ) {
			it->second.previous_int16 = it->second.value_int16;
			return( true );
		}
		break;
	}
	case API::tint32: {
		if ( pow( it->second.value_int32 - it->second.previous_int32, 2 ) > limitFloat ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
		break;
	}
	case API::tuint8: {
		if ( pow( it->second.value_uint8 - it->second.previous_uint8, 2 ) > limitFloat ) {
			it->second.previous_uint8 = it->second.value_uint8;
			return( true );
		}
		break;
	}
	case API::tuint16: {
		if ( pow( it->second.value_uint16 - it->second.previous_uint16, 2 ) > limitFloat ) {
			it->second.previous_uint16 = it->second.value_uint16;
			return( true );
		}
		break;
	}
	case API::tuint32: {
		if ( pow( it->second.value_uint32 - it->second.previous_uint32, 2 ) > limitFloat ) {
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
		break;
	}
	case API::tstr: {
		// any string change
		if (( it->second.dataType == API::tstr ) && ( it->second.value_string != it->second.previous_string )) {
			it->second.previous_string = it->second.value_string;
			return( true );
		}
		break;
	}
	}
	return( false );
}

void Channel::showProperties( void ){
	cout << endl << __FILE__ << " " << __LINE__ << " showProperties channel " << endl;
	cout << "==============" << endl;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		cout << __FILE__ << " " << __LINE__ << " channel property= " << it->first << endl;
	}
}
void Channel::subscribeParameter( string param, bool flag ){
		_testPropExist( param );
		_prop_map.at( param ).subscribed = flag;
}

} /* namespace ns_HAL_Channel */

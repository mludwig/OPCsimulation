// simulationHAL main.cpp
// the HAL server
// protoc-c: google protocol buffers C version
// czmq or zmq

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <exception>

#include <stdlib.h>
#include <stdio.h>

#include <bitset>

#include <boost/thread.hpp>


// using the C-only version of this,
// even if we are C++ here, to avoid any version/compatibility problems
// with the C-only glue code
#include <czmq.h>
#include <google/protobuf-c/protobuf-c.h>

#include "main.h"
#include "../../simulationOPCglue/protocolBuffers-c/simulationGlueCaen.pb-c.h"

#include "Distributor.h"
#include "Configuration.h"


using namespace std;

boost::mutex update_bmutex;

class Distributor;

void usage( char *p ){
	cout << "usage: %s -cfg <configfile> (required)" << endl;
	cout << "usage: %s -vendor [ caen | iseg | wiener ] (optional, default= caen)" << endl;
	cout << "usage: %s [ -help | -host_opc <h1> | -port_opc <p1> | -host_pilot <h2> | -port_pilot <p2> ] (optional)" << endl;
	cout << "usage: %s [ -pad | -debug ] (optional)" << endl;
	cout << "  -cfg: config file to populate the simulation engine server" << endl;
	cout << "  -host_opc, -port_opc: defaults: h1=* (=accept all incoming connections) p1=8880 h2=* p2=8881" << endl;
	cout << "  -debug: turn on very detailed debugging traces" << endl;
	cout << "  -pad: fill in missing channels of easy boards after configuration (patch holes from winccoa dpl dumps)" << endl;
	exit(0);
}


void versions( void ){
	cout<< "program version: " << __DATE__ << " " << __TIME__ << endl;

	//int zmq_major, zmq_minor, zmq_patch;
	//zmq_version(&zmq_major, &zmq_minor, &zmq_patch );
	//cout << "zeromq version: major= " << zmq_major << " minor= " << zmq_minor << " patch= " << zmq_patch << endl;
	cout << "protobuf-c version " << PROTOBUF_C_VERSION << " " << PROTOBUF_C_VERSION_NUMBER << endl;

	const int size = 256;
	char sysrelease[ size ];
	memset( sysrelease, 0, size );
	FILE *fp1 = fopen("/etc/system-release","r");
	fread( sysrelease, sizeof *sysrelease, size, fp1 );
	fclose( fp1 );
	cout << "system release: " <<  sysrelease;

	char sysversion[ size ];
	memset( sysversion, 0, size );
	FILE *fp2 = fopen("/proc/version","r");
	fread( sysversion, sizeof( *sysversion ), size, fp2 );
	fclose( fp2 );
	cout << "system version: " <<  string( sysversion );
}


void hexprint( const char *data, int size ) {
	// look into hex representation of message data
	int i = 0;
	printf("hexprint size= %d hex= ", size );
	for ( i = 0; i < size; i++ ){
		printf("%02x ", data[ i ]);
	}
	printf("\n");
}

void binprint( const char *data, int size ) {
	// look into binary representation of gbuf
	int i = 0;
	printf("binprint size= %d bit= ", size );
	for ( i = 0; i < size; i++ ){
		uint8_t ch = data[ i ];
		std::bitset<8> by( ch );
		cout << by << " ";
	}
	cout << endl;
}

void crateUpdate_method( Crate *cr )
{
	uint32_t delay_ms = cr->pilot_getUpdateDelay();
	struct timeval t0;
	struct timeval t1;
	struct timezone tz;
	double delta_ms = 0;
	while(true) {
		// time_t now = time(NULL);
		//cout << __FILE__ << " " << __LINE__ << " crateUpdate_method: updating crate key= "
		//	<< cr->key() << " model= " << cr->ModelName() << ctime(&now);
		update_bmutex.lock();
		gettimeofday( &t1, &tz );
		cr->update();
		gettimeofday( &t0, &tz );
		delay_ms = cr->pilot_getUpdateDelay();
		update_bmutex.unlock();

		delta_ms = (double) ( t0.tv_sec - t1.tv_sec) * 1000 + (double) ( t0.tv_usec - t1.tv_usec) / 1000.0;
		cr->setUpdateTime( delta_ms );

		boost::posix_time::milliseconds workTime( delay_ms ); // theoretically 2Hz
		boost::this_thread::sleep(workTime);
	}
}

void launchUpdateThreadsForControllers( void ){
	ns_HAL_Configuration::Configuration *cfg = NULL;
	cfg = ns_HAL_Configuration::Configuration::Instance();

	for ( uint32_t icrate = 0; icrate < cfg->totalNbCrates(); icrate++ ){
		Crate *cr = cfg->crate( icrate );
		// all the rest: launch timer threads for each crate
		cout << __FILE__ << " " << __LINE__ << " starting update thread on crate " << icrate << endl;
		boost::thread crateUpdate_thread( crateUpdate_method, cr );
	}
}

int main( int argc, char **argv ){
	versions();

	/// \todo main.cpp: set debuglevel via pilot during runtime
	bool _debug = true;
	bool _debugMessageRequests = false;
	bool _debugMessageReplies = false;
	bool _debugCZ = false;
	bool _debugConfig = true;
	bool _debugCrate = false;
	bool _debugBoard = false;
	bool _debugChannel = false;
	bool _padMissingChannels = false;
	bool _intrinsicUpdates = true;
	int vendor = VENDOR_CAEN;

	string host_opc = "*"; // accept any
	int port_opc = SIMULATION_GLUE_PORT;
	string host_pilot = "*"; // accept any
	int port_pilot = SIMULATION_PILOT_PORT;
	string configfile = "./configSimulation.xml";

	int i = 0;
	for ( i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "-pad")){
			_padMissingChannels = true;;
		}
		if ( 0 == strcmp( argv[i], "-debug")){
			_debug = true;
			_debugMessageRequests = true;
			_debugMessageReplies = true;
			_debugCZ = true;
			_debugConfig = true;
			_debugCrate = true;
			_debugBoard = true;
			_debugChannel = true;
		}
		if ( 0 == strcmp( argv[i], "-help")){
			usage( argv[0] );
		}
		if ( 0 == strcmp( argv[i], "-h")){
			usage( argv[0] );
		}
		if ( 0 == strcmp( argv[i], "-host_opc") && argc > i ){
			char hh[NAME_SIZE];
			sscanf( argv[i+1], "%s", &hh[ 0 ] );
			host_opc = string( hh );
		}
		if ( 0 == strcmp( argv[i], "-host_pilot") && argc > i ){
			char hh[NAME_SIZE];
			sscanf( argv[i+1], "%s",  &hh[ 0 ] );
			host_pilot = string( hh );
		}
		if ( 0 == strcmp(argv[i], "-port_opc" ) && argc > i){
			sscanf( argv[i+1], "%d", &port_opc );
		}
		if ( 0 == strcmp(argv[i], "-port_pilot" ) && argc > i){
			sscanf( argv[i+1], "%d", &port_pilot );
		}
		if ( 0 == strcmp( argv[i], "-cfg") && argc > i ){
			char hh[NAME_SIZE];
			sscanf( argv[i+1], "%s",  &hh[ 0 ] );
			configfile = string( hh );
		}
		if ( 0 == strcmp( argv[i], "-vendor") && argc > i ){
			char hh[NAME_SIZE];
			sscanf( argv[i+1], "%s",  &hh[ 0 ] );
			string vv = string(hh);
			if ( vv.find("caen") != std::string::npos ) vendor = VENDOR_CAEN;
			if ( vv.find("iseg") != std::string::npos ) vendor = VENDOR_ISEG;
			if ( vv.find("wiener") != std::string::npos )	vendor = VENDOR_WIENER;
		}
		if ( 0 == strcmp( argv[i], "-noIntrinsicUpdates")){
			_intrinsicUpdates = false;
		}
	}

	switch ( vendor ){
	case VENDOR_CAEN:{
		cout << argv[0] << " running a vendor= CAEN simulation" << endl;
		break;
	}
	case VENDOR_ISEG:{
		cout << argv[0] << " running a vendor= ISEG simulation" << endl;
		break;
	}
	case VENDOR_WIENER:{
		cout << argv[0] << " running a vendor= WIENER simulation" << endl;
		cout << "this is not implemented yet, forget it" << endl;
		exit(0);
		break;
	}
	}

	// configuration of HAL. We create API objects from the DOM tree,
	// which then know the hierarchy and each of them knows the interface.
	// Once this is done, the HAL needs to be populated and configured
	// and that's a complicated step. This is why configuration objects and HAL objects
	// are separated and don't see each others worlds (namespace, lifetime)
	ns_HAL_Configuration::Configuration *cfg = NULL;
	try {

		cfg = ns_HAL_Configuration::Configuration::Instance();
		cfg->debug( _debugConfig );
		cfg->padMissingChannels( _padMissingChannels );
		int ret = cfg->read( configfile );
		if ( ret ) {
			cout << argv[0] << " xerces-xml: problem reading configuration, exiting..." << endl;
			return( ret );
		}
	}
	catch ( std::runtime_error &e ){
		cout << endl << __FILE__ << " " << __LINE__ << " xerces-xml: runtime error: " << e.what() << endl;
		cout << __FILE__ << " " << __LINE__ << " " << argv[0] << " xerces-xml: this was a specific exception. Trying to cleanup..." << endl;
		exit(0);
	}
	catch(std::exception& e)	{
		cout << __FILE__ << " " << __LINE__ << " xerces-xml: standard exception: " << e.what() << endl;
		exit(0);
	}
	catch ( ... ){
		cout << __FILE__ << " " << __LINE__ << " xerces-xml: other exception" << endl;
		exit(0);
	}

	try {
		// now, lets populate the HAL and configure the behavior of the crates, boards and channels
		cfg->setIntrinsicUpdates( _intrinsicUpdates );
		cfg->populate( _debugCrate, _debugBoard, _debugChannel );
		cout << "===configuration===========================" << endl;
		cout << "total #crates:   " << cfg->totalNbCrates() << endl;
		cout << "total #boards:   " << cfg->totalNbBoards() << endl;
		cout << "total #channels: " << cfg->totalNbChannels() << endl;
		cout << "===========================================" << endl;

		struct timeval t1, t2;
		struct timezone tz;
		gettimeofday( &t1, &tz );


		// lets update each of the crates once before we go into communication, to have
		// a proper starting condition and see if all is well

		cfg->firstGlobalUpdate();
		gettimeofday( &t2, &tz );
		double delta_ms = (double) ( t2.tv_sec - t1.tv_sec) * 1000 + (double) ( t2.tv_usec - t1.tv_usec) / 1000.0;
		cout << "===first update & population===============" << endl;
		cout << "total #boards:      " << cfg->totalNbBoards2() << endl;
		cout << "total #easy boards: " << cfg->totalNbEasyBoards2() << endl;
		cout << "total #ps channels:" << cfg->totalNbPsChannels2() << endl;
		cout << "===========================================" << endl;
		cout << "initial global update done, took " << delta_ms << " ms" << endl;
		cout << "===========================================" << endl;
	}
	catch ( std::runtime_error &e ){
		cout << endl << __FILE__ << " " << __LINE__ << " runtime error: " << e.what() << endl;
		cout << __FILE__ << " " << __LINE__ << " " << argv[0] << " this was a specific exception. Trying to cleanup..." << endl;
		exit(0);
	}
	catch(std::exception& e)
	{
		cout << __FILE__ << " " << __LINE__ << " standard exception: " << e.what() << endl;
		exit(0);
	}
	catch ( ... ){
		cout << __FILE__ << " " << __LINE__ << " other exception" << endl;
		exit(0);
	}


	// if the controller is a SY3527 or SY5527 the global updates are handled by the event mode: the OPC server
	// continuously requests updated items and the GetEventModeData can therefore run (and delay) updates for
	// everything is a mono-threaded design. For any controllers who DO NOT run event mode, and rely only
	// on polling, this way of updating will not work because GetEventData is never invoked. Instead of that,
	// all data is requested explicity (GET) by polling. In order to produce these updates we therefore need
	// an extra timer thread which invokes these global updates. Each crate receives it's own timer-thread.
	launchUpdateThreadsForControllers();

	char hn[ 127 ];
	int ret = gethostname( hn, 127 );
	cout << "simulation engine running on " << hn << " ret= " << ret << endl;
	cout << "setting up networking/zmq" << endl;

	//  server: bind OPC simulation glue
	string tcpipport1;
	{
		ostringstream convert;
		convert << port_opc;
		tcpipport1 = "tcp://" + host_opc + ":" + convert.str();
	}
	zsock_t *sockGlue = zsock_new_rep( tcpipport1.c_str() ); // server: recv-send pattern
	int bind_ret = zsock_bind ( sockGlue, tcpipport1.c_str() );
	cout << argv[0] << " bind simulation HAL to OPC glue: " << tcpipport1 << " bind_ret= " << bind_ret << endl;

#if 0
	// handshake with glue
	{
		char *request = zstr_recv ( sockGlue );
		printf("%s %d HAL received: %s length= %d\n", __FILE__ , __LINE__, request, strlen( request ) );
		zstr_send ( sockGlue, "HAL is starting" );
	}
#endif

	//  server: bind simulation pilot
	string tcpipport2;
	{
		ostringstream convert;
		convert << port_pilot;
		tcpipport2 = "tcp://" + host_pilot + ":" + convert.str();
	}
	zsock_t *sockPilot = zsock_new_rep( tcpipport2.c_str() ); // server: recv-send pattern
	bind_ret = zsock_bind ( sockPilot, tcpipport2.c_str() );
	cout << argv[0] << " bind simulation HAL to pilot: " << tcpipport2 << " bind_ret= " << bind_ret << endl;

	zpoller_t *poller = zpoller_new( sockGlue, sockPilot );


	cout << "connecting objects and configuration" << endl;
	simulationHAL::Distributor *distributor = new simulationHAL::Distributor();
	distributor->setDebug( _debug );
	distributor->setDebugMessageRequests( _debugMessageRequests );
	distributor->setDebugMessageReplies( _debugMessageReplies );
	distributor->setDebugCZ( _debugCZ );
	distributor->setCZsockets( sockGlue, sockPilot );

	// sends back czmq, must be inside the same thread. If
	// multithreads needed, each thread must bind it's own server socket

	// set distributor configuration with all populated engine objects
	try {
		// listen to three zmq sockets (caen, iseg, pilot) by polling: whenever one socket fires the poller is woken up
		// This saves us from a multithreaded OO design in the HAL.
		// http://zguide.zeromq.org/cpp:mspoller
		int timeout = -1; // forever, in ms
		zsock_t *which = NULL;
		while (1) {

			which = (zsock_t *) zpoller_wait( poller, timeout );
			time_t now = time( NULL );
			if ( _debug ) cout << endl << __FILE__ << " " << __LINE__ << " poll: new message arrived "<< ctime( &now ) ;

			if ( which == sockGlue ) {
				// process messages from Glue
				char *request = zstr_recv ( sockGlue );
				int zrec_size = strlen( request ); // zstr_recv adds a null terminator, two bytes 00 00
				// if (_debug) printf("%s %d received: %s size= %d\n", __FILE__, __LINE__, request, zrec_size );

				// reformat the chars to uint8
				int uint8_sz = simulationHAL::Distributor::char_to_ar_uint8_sz( zrec_size );
				uint8_t buf_uint8[ uint8_sz ];
				simulationHAL::Distributor::char_to_ar_uint8( request, buf_uint8 );
				zstr_free( &request );

				// decode and feed it into the distributor for type dependent decoding
				SCaen__CaenMessage *gRequest = s_caen__caen_message__unpack( NULL, uint8_sz, (const uint8_t *) buf_uint8 );
				if ( _debug ) cout << __FILE__ << " " << __LINE__  << " GLUE gRequest->type= " << gRequest->type << endl;

				update_bmutex.lock();
				distributor->glueDispatch( gRequest );
				update_bmutex.unlock();

				s_caen__caen_message__free_unpacked( gRequest, NULL );
			}
			else if ( which == sockPilot ) {
				// process messages from Pilot: same structure as for glue, but different message formats,
				// since these are not vendor dependent
				// ... well don't know yet
				char *request = zstr_recv ( sockPilot );
				int zrec_size = strlen( request );
				if (_debug) printf("%s %d received: %s size= %d\n", __FILE__, __LINE__, request, zrec_size );

				// reformat the chars to uint8
				int uint8_sz = simulationHAL::Distributor::char_to_ar_uint8_sz( zrec_size );
				uint8_t buf_uint8[ uint8_sz ];
				simulationHAL::Distributor::char_to_ar_uint8( request, buf_uint8 );
				zstr_free( &request );

				// decode and feed it into the distributor for type dependent decoding
				SCaen__PilotMessage *pRequest = s_caen__pilot_message__unpack( NULL, uint8_sz, (const uint8_t *) buf_uint8 );
				if ( _debug ) cout << endl << __FILE__ << " " << __LINE__  << " PILOT pRequest->type= " << pRequest->type << endl;

				update_bmutex.lock();
				distributor->pilotDispatch( pRequest );
				update_bmutex.unlock();

				s_caen__pilot_message__free_unpacked( pRequest, NULL );
			} else {
				cout << argv[0] << " czmq poller problem: unknown socket/poller release, trying to exit properly." << endl;
				zpoller_destroy( &poller );
				zsock_destroy( &sockGlue );
				zsock_destroy( &sockPilot );
				cout << argv[0] << " looks good, exiting." << endl;
				exit(-1);
			}
		} // while.. polling
		zpoller_destroy( &poller );
		zsock_destroy( &sockGlue );
		zsock_destroy( &sockPilot );
	}
	catch ( std::runtime_error &e ){
		cout << endl << __FILE__ << " " << __LINE__ << " runtime error: " << e.what() << endl;
		cout << __FILE__ << " " << __LINE__ << " " << argv[0] << " this was a specific exception. Trying to cleanup..." << endl;
	}
	catch(std::exception& e)
	{
		cout << __FILE__ << " " << __LINE__ << " standard exception: " << e.what() << endl;
	}
	catch ( ... ){
		cout << __FILE__ << " " << __LINE__ << " other exception, that is bad." << endl;
	}
	zpoller_destroy( &poller );
	zsock_destroy( &sockGlue );
	zsock_destroy( &sockPilot );
	cout << argv[0] << " exiting after an exception occured, cleaned up zmq at least. Not too bad." << endl;
	// shutting down protobuf as well ? njet.. this is just the C api
	return( 0 );
}


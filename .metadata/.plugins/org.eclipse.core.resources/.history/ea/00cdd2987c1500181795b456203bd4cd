/*
 * Board.h
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */

#ifndef SRC_BOARD_H_
#define SRC_BOARD_H_

#include <stdint.h>
#include <math.h>
// #include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <fstream>
#include <string>
#include <vector>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Channel.h"

#include "API.h"
#include "caenAPIdefs.h"

using namespace ns_HAL_API;

using namespace std;
using namespace ns_HAL_Channel;


namespace ns_HAL_Board {


// we have one generic class for all boards, which has:
// * a configurable set of properties and their behavior
// * a common set of functionality
//
// the channels of that board have
// * support event mode
// * support timed behavior
// * ramps and trips, like most HV boards
// * common state machine
// the boards which behave really different from this will probably have their own class
//
// the event mode composes events with all channel items from one board which have changed within
// the last 500ms. When the sm of a channel is in ramping state, or has freshly changed state, the
// according parameters of that channel are indicated "as changed", and the according event items
// will have to be present. These updated channel-parameters are flagged, and when the events are assembled,
// after the blocking wait in the EventMode API has expired, all flagged channel-parameters are freshly read out,
// for all channels (straight sequence, but could be randomized as well). We have therefore some 3.4ms
// of CPU per channel (144channels at max, 500ms update) in a single thread design.
// This is all done inside the processing loop:
// 1. read in any updated values controller, all boards, all channels: empty protocol message queues
// 2. update all state machines of all channels
// 3. assemble event for all boards which have changed channels
//
// a multi-threaded design would have:
// one thread for a few crate boards plus their controller
// one thread per A1676A (Easy Crate CAEN) board
//
// each board and controller would have it's own zmq socket, and thread affinity is determined
// during initialization of the HAL, at configuration. The aim would be to always keep 2Hz event
// updates seen from each sending thread, which is achieved by an intelligent self-adjusting timeout
// to always be in the vicinity of 500ms (actually, between 1900ms and 100ms, as seen in the lab crate!).

typedef enum { none, unknown,
	// standard high volt
	A1519,  // 12 Channel 250 V, 0.1/1 mA Individual Floating Channel Dual Range Board
	A1535,  // 12/24 Channel 3.5 kV/3 mA (8 W) Common Floating Return Boards
	A1540,  // 12/24/32 Channel 100 V/1 mA Common Floating Return Boards
	A1590,  // 16 Channel 9 kV / 50 µA Common Floating Return Board
	A1734,  // some HV, old
	A1735,  // 12 Channel 1.5 kV/7 mA Board
	A1821,  // 12 Channel 3 kV, 200/20 µA Common Ground Dual Range Board
	A1821HN,
	A1832,  // 12 Channel 6 kV, 1 mA/200 µA Common Ground Dual Range Board
	A1833,  // 12 Channel 3 kV/3mA, 4 kV/2 mA, 4 kV/0.2 mA Common Ground Dual Range Board

	// low voltage
	A1513B,
	A1516B,
	A1517B,
	A1518B,
	A2517,
	A2518,
	A2519,

	// branch controllers for EASY crates
	A1676,
	A1676A,

	// default
	GENERIC // assumed by simulation
} BOARDTYPE_t;

#define CAEN_NAME_MAX12 12
#define CAEN_DESC_MAX 28

class Board {
public:
	Board( uint32_t sl, BOARDTYPE_t ty );
	virtual ~Board();

	string key( void ){ return(_key);}
	static BOARDTYPE_t convert_model2type( string name );
	static string convert_type2model( BOARDTYPE_t type );
	Channel * channel( uint32_t ich ){ return _channel_map.at( ich ); };
	void addPopulatedChannel( uint32_t pch ){ _populatedChannelsv.push_back( pch ); }

	uint32_t nbPopulatedChannels( void ){ return( _populatedChannelsv.size() ); }
	uint32_t nbChannelsFromModel( void ) { return( _nbChannels );	} // from constructor board type
	uint32_t nbChannelObjects( void ) { _nbChannels = _channel_map.size(); return( _nbChannels );	} // existing objects

	uint32_t populatedChannel( uint32_t i );
	bool isBranchController( void );
	uint32_t slot( void ){ return( _slot ); }
	bool channelExists( uint32_t index );

	uint32_t BdStatus( void );
	float HVMax( void );
	// float SVMax( void );
	float Temp( void );

	// branch controller specific
	float x1676Ilk( void ) { return( _x1676Ilk ); }
	float Vsel( void ) { return( _Vsel); }
	uint32_t HWReset0( void ) { return( _HWReset0 ); }
	uint32_t Recovery0( void ) { return( _Recovery0 ); }
	uint32_t HWReset1( void ) { return( _HWReset0 ); }
	uint32_t Recovery1( void ) { return( _Recovery0 ); }
	uint32_t HWReset2( void ) { return( _HWReset0 ); }
	uint32_t Recovery2( void ) { return( _Recovery0 ); }
	uint32_t HWReset3( void ) { return( _HWReset0 ); }
	uint32_t Recovery3( void ) { return( _Recovery0 ); }
	uint32_t HWReset4( void ) { return( _HWReset0 ); }
	uint32_t Recovery4( void ) { return( _Recovery0 ); }
	uint32_t HWReset5( void ) { return( _HWReset0 ); }
	uint32_t Recovery5( void ) { return( _Recovery0 ); }
	uint32_t GlobalOn( void ) { return( _GlobalOn ); }
	uint32_t GlobalOff( void ) { return( _GlobalOff ); }
	uint32_t x48VA1676( void ) { return( _48VA1676 ); }
	uint32_t VA1676( void ) { return( _VA1676 ); }

	void configureNbChannels( int n ){ _nbChannels = n; } // branch controller and generic board have configurable nb channels
	float ilimit( void ){ return ( _Ilimit ); }
	void setMyCrate( uint32_t i );
	uint32_t myCrate( void ){ return ( _my_icrate ); }
	void showProperties( void );
	string model( void ) { return( _model ); }
	string description( void ) { _description.resize( CAEN_DESC_MAX - 1 ); return( _description );}
	void setDescription( string d ) { _description = d; }
	uint16_t serNumber( void ) { return( _serNumber ); }
	string fwRelMin( void ) { return( _fwRelMin ); }
	string fwRelMax( void ) { return( _fwRelMax ); }
	uint32_t parameterAccessMode( string p ){ return( ( uint32_t ) _prop_map.at( p ).accessMode ); }
	uint32_t parameterDataType( string p ){ return( ( uint32_t ) _prop_map.at( p ).dataType ); }

	vector<string> parameterList( void );
	void insertChannel( uint32_t ichannel, Channel *ch );

	float parameterMinval( string p );
	float parameterMaxval( string p );
	uint16_t parameterUnit( string p );
	int16_t parameterExp( string p );
	void parameterOnstate( string p, string *os );
	void parameterOffstate( string p, string *os );

	// overloaded
	void getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type );
	void getParameterValue( string p, string *retval );
	void getParameterValue( string p, int8_t *retval );
	void getParameterValue( string p, int16_t *retval );
	void getParameterValue( string p, int32_t *retval );
	void getParameterValue( string p, uint8_t *retval );
	void getParameterValue( string p, uint16_t *retval );
	void getParameterValue( string p, uint32_t *retval );
	void getParameterValue( string p, float *retval );

	void setParameterValue( string p, string val );
	void setParameterValue( string p, int8_t val );
	void setParameterValue( string p, int16_t val );
	void setParameterValue( string p, int32_t val );
	void setParameterValue( string p, uint8_t val );
	void setParameterValue( string p, uint16_t val );
	void setParameterValue( string p, uint32_t val );
	void setParameterValue( string p, float val );

	map<string,API::HAL_PROPERTY_t> allChangedItems( void );
	void setDebug( bool f ){ _debug = f; };
	void subscribeParameter( string param, bool flag );
	void setIntrinsicUpdates( bool f ) { _intrinsicUpdates = f; }
	bool intrinsicUpdates( void ) { return(_intrinsicUpdates); }

private:
	bool _debug;
	bool _intrinsicUpdates; // show temp and fan also when "off"
	string _key;
	uint32_t _my_icrate;
	uint32_t _slot;
	BOARDTYPE_t _type;
	uint32_t _nbChannels;
	string _model;
	string _description;
	uint16_t _serNumber;
	string _fwRelMin;
	string _fwRelMax;
	float _Ilimit;

	// typedef enum { UNDER_TEMP, OVER_TEMP } BoardMessages_t; // returned by controller, depending on board status
	uint32_t _status;
	float _HVMax;
	float _Temp;

	// properties of a branch controller
	float _x1676Ilk;
	float _Vsel; /// \todo _Vsel for branch controller should be bool, and how is this suppsed to work actually ?
	/// \todo functionality for these extra properties for branch controllers are just empty containers, check semantic
	uint32_t _HWReset0;
	uint32_t _Recovery0;
	uint32_t _HWReset1;
	uint32_t _Recovery1;
	uint32_t _HWReset2;
	uint32_t _Recovery2;
	uint32_t _HWReset3;
	uint32_t _Recovery3;
	uint32_t _HWReset4;
	uint32_t _Recovery4;
	uint32_t _HWReset5;
	uint32_t _Recovery5;
	uint32_t _GlobalOn;
	uint32_t _GlobalOff;
	uint32_t _48VA1676;
	uint32_t _VA1676;

	std::map<uint32_t,Channel *> _channel_map; // key=channel
	vector<uint32_t> _populatedChannelsv;
	map<string,API::CAEN_PROPERTY_t> _caenProp_map;
	map<string,API::HAL_PROPERTY_t> _prop_map;
	void _insertStandardBoardProperties( void );
	void _setValuesStandardBoardProperties( void );
	void _propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t ); // caen
	void _propinsert( API::HAL_PROPERTY_t h ); // generic format
	bool _testPropExist( string p );
	bool _testPropNotExist( string p );
	bool _changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it );
};

} /* namespace ns_HAL_Board */

#endif /* SRC_BOARD_H_ */

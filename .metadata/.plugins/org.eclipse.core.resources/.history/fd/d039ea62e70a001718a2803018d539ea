/*
 * Configuration.h
 *
 *  Created on: Oct 21, 2016
 *      Author: mludwig
 */

#ifndef SRC_CONFIGURATION_H_
#define SRC_CONFIGURATION_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <map>
#include <vector>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "Crate.h"
#include "Board.h"
#include "Channel.h"

using namespace xercesc;
using namespace std;
using namespace ns_HAL_Crate;
using namespace ns_HAL_Board;
using namespace ns_HAL_Channel;

namespace ns_HAL_Configuration {


class APIConfig {
public:
	APIConfig(){};
	virtual ~APIConfig(){};

	typedef enum { ReadOnlyProperty, ReadWriteProperty, WriteOnlyProperty } PACCESS_TYPE_t;
	typedef enum { S, I, F } PDATA_TYPE_t;
	typedef struct {
		PACCESS_TYPE_t pAccessType;
		PDATA_TYPE_t pDataType;
		string name;
		string pname;
#if 0
		// storage of the values as well
		string value_string;
		// anonymous union for numerical types: can access union members more comfortable:
		// crateAPI.at(i).value_float =
		union {
		    int8_t value_int8;
		    uint8_t value_uint8;
		    int16_t value_int16;
		    uint16_t value_uint16;
		    int32_t value_int32;
		    uint32_t value_uint32;
		    float value_float;
		    double value_double;
		    // no arrays needed as it seems
		  };
#endif
	} PROPERTY_t;

private:

};

class ChannelConfig : APIConfig {
public:
	ChannelConfig( uint32_t i, string n, string k ){
		_index = i;
		_name = n;
		_key = k;
	};
	virtual ~ChannelConfig(){};

	uint32_t index( void ){ return( _index ); }
	uint32_t channel( void ){ return( _index ); }
	void setChannel( uint32_t cc ){ _index = cc; }
	string name( void ){ return( _name ); }
	string key( void ){ return( _key ); } // crate=X slot=Y channel=Z. This is an alternate key for the map
	bool propertyExists( string pname );

	std::map<string,PROPERTY_t> channelAPI_map;

private:
	uint32_t _index;
	string _name;
	string _key; // crate=X slot=Y channel=Z. This is an alternate key for the map,
	// and it is unique with global scope
};

class BoardConfig : APIConfig {
public:
	BoardConfig( string n, string m, uint32_t s, string k ){
		_slot = s;
		_model = m;
		_name = n;
		_key = k;
	};
	virtual ~BoardConfig(){};

	uint32_t slot( void ){ return( _slot ); }
	string name( void ){ return( _name ); }
	string model( void ){ return( _model ); }
	string description( void ){ return( _description ); }
	void setDescription( string d ){ _description = d; }
	string key( void ){ return( _key ); }

	std::map<string,PROPERTY_t> boardAPI_map;
	std::map<uint32_t,ChannelConfig *> channelConfig_map; // slot, object

private:
	uint32_t _slot;
	string _model;
	string _name;
	string _description;
	string _key; // crate=X slot=Y. This is an alternate key for the map,
	// and it is unique with global scope

};


class CrateConfig : APIConfig {
public:
	CrateConfig(){
		_ipAddress = "";
		_model = "";
		_name = "";
	};
	virtual ~CrateConfig(){};

	string ipAddress( void ){ return( _ipAddress ); }
	string model( void ){ return( _model ); };
	string name( void ){ return( _name ); };
	void setipAddress( string s ){ _ipAddress = s; }
	void setmodel( string s ){ _model = s; }
	void setname( string s ){ _name = s; }

	uint32_t nbPopulatedSlots( void ) { return ( populatedSlot_map.size() ); }
	void addPopulatedSlot( uint32_t ps ){ populatedSlot_map.insert( std::pair<uint32_t, uint32_t>( nbPopulatedSlots(), ps )); }
	bool isSlotPopulated( uint32_t slot ){
		std::map<uint32_t, uint32_t>::iterator it = populatedSlot_map.find( slot );
		if ( it == populatedSlot_map.end() ) return( false );
		else return( true );
	}
	uint32_t populatedSlot( uint32_t islot ){ return( populatedSlot_map.at( islot )); }

	std::map<uint32_t,uint32_t> populatedSlot_map; // count=key, populated slot
	std::map<string,PROPERTY_t> crateAPI_map;
	std::map<uint32_t,BoardConfig *> boardConfig_map; // slot, object

private:
	string _ipAddress;
	string _model;
	string _name;

};

// singleton
class Configuration {
public:
	//	Configuration();
	virtual ~Configuration();
	Configuration( void );	// singleton
	static Configuration* Instance( void );
	static string getConfigfile( void ){ return( Configuration::_configfile ); }

	int read( string filename );
	void populate( bool debugCrate, bool debugBoard, bool debugChannel );
	void firstGlobalUpdate( void );
	Crate *crate( uint32_t icrate ); // icrate same as handle
	Board *board( uint32_t icrate, uint32_t islot );
	Channel *channel( uint32_t icrate, uint32_t islot, uint32_t ich );
	void launchPollerThreadsForNonEventModeControllers( void );

	void debug( bool f ) { _debug = f; };
	void padMissingChannels( bool f ) { _padMissingChannels = f; };

	// object stats for bling bling
	uint32_t totalNbCrates( void );
	uint32_t totalNbBoard( void );
	uint32_t totalNbChannels( void );


private:
	static Configuration* _pInstance;
	static string _configfile;
	static int _totalNbBoards;

	Configuration( Configuration const&);                            // copy constructor absent (i.e. cannot call it - linker will fail).
	Configuration& operator=(Configuration const&);  		// assignment operator absent (i.e. cannot call it - linker will fail).

	bool _debug;

	// if we get a config which is derived from the winnccoa world, and not directly from the electronics
	// we might have not used/declared all channels of that board which physically exist.
	// These "missing channels" are a problem for the OPC server, so lets detect them and fill them in.
	bool _padMissingChannels;


	inline bool _fileExists( const std::string& name );
	void _configureSYCrate( DOMNode  *crate );
	void _configureSYCrateProperty( DOMNode  *syprop );
	void _configureBoard( DOMNode *board );
	void _configureBoardProperty( DOMNode *boardprop_node );
	void _configureChannel( DOMNode *channel_node );
	void _configureChannelProperty( DOMNode *channelprop_node );

	std::map<uint32_t,CrateConfig *> _crateConfig_map;
	uint32_t _currentIndexCrate;
	uint32_t _currentIndexCrateSlot;
	uint32_t _currentIndexCrateSlotChannel;

	typedef struct {
		string smodel;
		CAENHV_SYSTEM_TYPE_t imodel;
	} GENERIC_CRATES_t;
	vector<GENERIC_CRATES_t> _genericCrateModels;
	int _createSYCrate( CrateConfig *cfg );
	std::map<uint32_t,ns_HAL_Crate::Crate *> _crate_map;

	bool _createBoard( Crate *cr, BoardConfig *cfgboard );
	bool _createChannel( Board *bd, ChannelConfig *cfgchannel );
	bool _createEasyBoardHeaderChannel( Board *bd, ChannelConfig *cfgchannel );


};

} /* namespace ns_HAL_Configuration */

#endif /* SRC_CONFIGURATION_H_ */

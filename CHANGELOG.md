# Change Log
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).

## [devel]
### Added
### Changed
### Fixed

## [0.9.9] - 7 feb 2019
### Added
- simulation engine: in the configuration/xml, added the firmware as well. But this is still
  dead code, we need the reference board dumps before.

### Changed
- simulation engine: configuration, xml decoding, the whole lot, for those
  EASY board models.

### Fixed
- simulation engine & glue code: corected the EASY board models, which are published
  in each marker channel (Chan000) property:RemBdName attribute:Onstate. This is
  cumbersome and indirect, but that is what CAEN implemented and the s.engine has to
  remodel it. Seems OK now, is reported properly in the OPC server as well, as of the
  lastest version.

## [0.9.8] - 26 sept 2018
### Added

### Changed
- venus pilot server: health is in class VENUSCAENPILOT and updated each 2 secs
- venus pilot server: config is in class GENERICPILOT and updated once at init

### Fixed
- venus pilot server: bug fixed in tripChannelByVoltage method

## [0.9.7] - 28 aug 2018
### Added
- OPC-UA Venus Pilot server
- pilot server mutex protection against async user zmq messages
- proper noise level control for VMon and IMon 
- VenusCombo config files in a separate directory

### Changed
- suppressed old pilotUI from the combo for now (must update before)
- only .open6 version available for OPC-UA Caen and OPC-UA pilot

### Fixed


## [0.9.6] - 28 may 2018
### Added
- Board support A1515QG

### Changed
- combo against newest caen open6 and ua artefacts from jenkins

### Fixed
- bug fixed: channels got a wrong board slot sometimes


## [0.9.4, 0.9.5 dropped ]

## [0.9.3] - 27-Feb-2018
### Added
- CHANGELOG.md
- discoveryCAEN: upgraded so that it dumps a board footprint including all 
  channels for every board it finds, also EASY boards and branch controllers.
  Like this we can build up a database. Added some protection against
  empty EASY crates
  of known boards and their foortprints. Each footprint comes with the
  board firmware version as well. 
- discoveryCAEN: restrict crate types to SYXXXX, improved messages a bit more
- simulation engine: added a shorthand notation which permits to fully populate
  the engine by just naming the boards with their precise types. This is much 
  faster and cleaner. Still relies on an artificial classification of channels 
  into 4 types, but is good enough for now. Can run the OPC server in
  --hardwareDiscoveryMode at all times to get a clean Address Space.
- added A3486 EASY boards
- alice.its.short corrected, added A3486A boards at the proper places

### Changed
- README.md updated
- code updated
- cleaned up some project dirs in git
- renamed discoveryCAEN.cc7 to discoveryCAEN, dropped slc6 version (make_jenkins_release.sh)
- keepalive period set to 30seconds: empty keepalive tagged events are issued if nothing else is 
  updated, in event mode
- Dockerfile OPC-UA server jenkins builds: ua=1102 open6=521
- discovery config for OPC-UA with named crate for alice, updated entry_point.sh OPC-UA startup options

### Fixed
- EASY board channel numbering for discovery
- extension xml.txt for reference hardware structure dumps per board, firmware
- added nexus repo https://repository.cern.ch/nexus/content/repositories/cern-venus/caen/discoveredCAENHardware/
### Removed


## [0.9.2] - 01-Nov-2017
### Changed
- initial version, runs

### Fixed

### Removed



these are various configuration files which are needed.

caenGlueConfig.xml
	is read by the glue code (inside theOPC server's task) to basically know the IP of the s.engine.
	The IP does NOT need to be on the localhost.
	
sim_nnnn.xml
	genuine config file for the s.engine, containing as many details as available. They are tested. 
	They can come from hw-discovery OPC server, hw-discovery standalone tool, WinCCOA dump decoding or also
	fully synthetic. The simulator can also be fed with opc_nnnn.xml.
	
opc_nnnn.xml
	if the OPC server is NOT run in --hardwareDiscoveryMode, it will need a config which corresponds
	to what the simulator is playing, or at least a subset to what the simulator is doing. These have
	a few less attributes and slightly different syntax (more simple). The Opc server
	can NOT be fed with sim_nnnn.xml .
	
config.xml
	when OPC server runs in --hardwareDiscoveryMode it needs to know which crate type and where it 
	is supposed to discover the magic. The IP must be localhost always, the glue code does the cheating.
	
	
have fun ;-)
ml
#!/bin/csh
# cleaning atlas configs to have
# BoardXX
# ChanXXX
rm -f ./x1.tmp; rm -f ./x2.tmp; rm -f $1.tmp; rm -f $1.putz
cat -n $1 | grep  "name="\""Chan0[[:digit:]]\{3\}" | sed "s/Chan0/Chan/g"  > x1.tmp
cat -n $1 | grep  -v "name="\""Chan0[[:digit:]]\{3\}" > x2.tmp
cat x1.tmp x2.tmp | sort -n > $1.tmp
# take out line numbers again
cat $1.tmp | awk '{ printf("%s %s %s %s %s %s %s\n", $2, $3, $4, $5, $6, $7, $8 ) }' > $1.putz
echo $1".putz contains ChanXXX"


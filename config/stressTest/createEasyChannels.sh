#!/bin/csh
# just produce plain channels with correct numbering as specified, to fill up EASY crates
# $1 = output file 
# $2=crate nb
# $3=board nb
# $4=index of first channel
# $5=nb channels needed
if ( $1 == "" ) then
	set out="./stressTest.xml" 
	set out2="./stressTest.uap" 
else
	set out=$1.xml
endif

rm -f ${out}
touch ${out}


set nbCrates = 1
set nbBoards = 1 # per crate
set nbChannelsPerBoard = $5 # just channels in a bc

set G='"'



# set r = 0
@ r = 0
@ startcrate = $2
while ( ${r} < ${nbCrates} )
	
	@ b = $3
	@ c = 0
	while ( ${c} < ${nbChannelsPerBoard}  )
		@ channelIndex = $c + $4
#		echo "	<Channel index=${G}${channelIndex}${G} name=${G}CHANNEL.${r}.${b}.${channelIndex}${G}>">>  ${out}
		echo "	<Channel index=${G}${channelIndex}${G} name=${G}Chan000${channelIndex}${G}>">>  ${out}
		cat "tstressTest.channel.xml" >>  ${out}
		echo "	</Channel>" >>  ${out}
  		@ c++
	end




echo "==> OPC server config file= "${out}
echo "fix ChanXXXzzz digits by hand"


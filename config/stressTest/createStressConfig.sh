#!/bin/csh

if ( $1 == "" ) then
	set out="./stressTest.xml" 
	set out2="./stressTest.uap" 
else
	set out=$1.xml
	set out2=$1.uap
endif

rm -f ${out}
touch ${out}


set nbCrates = 1
set nbBoards = 7 # per crate
set nbChannelsPerBoard = 1 # per board

set G='"'

cat "tstressTest.header.xml" >>  ${out}


# set r = 0
@ r = 0
@ startcrate = 10
while ( ${r} < ${nbCrates} )
	# can have up to 16million crates: 127.0.0.1 .. 127.255.255.255
	# don't overdo it for the moment and stick to ipgroupfour only.. ;-)
	@ ipgroupfour = $r + 1 + $startcrate
	
	echo "<SYCrate ipAddress=${G}127.0.0.${ipgroupfour}${G} model=${G}SY4527${G} name=${G}CRATE${r}${G}>" >>  ${out}
	# echo "<SYCrate ipAddress=${G}192.168.88.${ipgroupfour}${G} model=${G}SY4527${G} name=${G}CRATE${r}${G}>" >>  ${out}
	cat "tstressTest.crateHeader.xml" >>  ${out}


	# set b = 0
	@ b = 0
	while ( ${b} < ${nbBoards} )
#
# normal board HV with a type
#       		echo "   <Board name=${G}BOARD.${r}.${b}${G} slot=${G}${b}${G} type=${G}A1832${G} description=${G}test${G}> " >>  ${out}

#
# branch controller with easy boards
#		echo "   <Board name=${G}board.${r}.${b}${G} slot=${G}${b}${G} type=${G}A1676${G} description=${G}test${G}> " >>  ${out}
#
# generic simulation boards
		echo "   <Board name=${G}BOARD.${r}.${b}${G} slot=${G}${b}${G} type=${G}GENERIC${G} description=${G}gdescription${G}> " >>  ${out}

# the OPC does not decode type and description. All boards are generic therefore. But we can let the OPC discover the "hardware"
#		echo "   <Board name=${G}BOARD.${r}.${b}${G} slot=${G}${b}${G}> " >>  ${out}

		echo "        <ReadOnlyProperty dataType=${G}I${G} name=${G}BdStatus${G} propertyName=${G}BdStatus${G}/> " >>  ${out}
		echo "        <ReadOnlyProperty dataType=${G}I${G} name=${G}HVMax${G} propertyName=${G}HVMax${G}/>" >>  ${out}
		echo "        <ReadOnlyProperty dataType=${G}F${G} name=${G}Temp${G} propertyName=${G}Temp${G}/>" >>  ${out}
		
		# set c = 0
		@ c = 0
		while ( ${c} < ${nbChannelsPerBoard} )
		    	echo "	<Channel index=${G}${c}${G} name=${G}CHANNEL.${r}.${b}.${c}${G}>">>  ${out}
			cat "tstressTest.channel.xml" >>  ${out}
		     	echo "	</Channel>" >>  ${out}
  			@ c++
		end

    		echo "   </Board>" >>  ${out}
		#echo "OPC added board "${b}" with "${nbChannelsPerBoard}" channels(nodes)"
  		@ b++
	end

	echo "     </SYCrate>" >>  ${out}
	echo "config added crate "${r}" with "${nbChannelsPerBoard}" X  "${nbBoards}" channels(nodes)"
	@ r++
end 

echo "   </configuration>" >>  ${out}
echo "==> OPC server config file= "${out}


# as a bonus, create also a uaexpert-gui config
set header = "./tstressTest.header.uap"
set footer = "./tstressTest.footer.uap"

rm -f ${out2}
touch ${out2}

cat ${header} >> ${out2}

@ node = 15
@ r = 0
while ( ${r} < ${nbCrates} )

	@ b = 0
	while ( ${b} < ${nbBoards} )
	
		@ c = 0
		while ( ${c} < ${nbChannelsPerBoard} )
			if ( ${node} < 100 ) then		
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\0${node}\DisplayName=VMon" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\0${node}\NodeId=${G}ns=2;s=CRATE${r}.BOARD.${r}.${b}.CHANNEL.${r}.${b}.${c}.VMon${G}" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\0${node}\Status=2" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\0${node}\MonitoringParameters\DiscardOldest=1" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\0${node}\MonitoringParameters\QueueSize=1" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\0${node}\MonitoringParameters\SamplingInterval=100" >>  ${out2}
			else
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\${node}\DisplayName=VMon" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\${node}\NodeId=${G}ns=2;s=CRATE${r}.BOARD.${r}.${b}.CHANNEL.${r}.${b}.${c}.VMon${G}" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\${node}\Status=2" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\${node}\MonitoringParameters\DiscardOldest=1" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\${node}\MonitoringParameters\QueueSize=1" >>  ${out2}
				echo "Data%20Access%20View\DocumentSettings\Items\\008\\${node}\MonitoringParameters\SamplingInterval=100" >>  ${out2}
			endif


			# echo "added channel "${c}" node "${node}
			@ c++
			@ node += 2

		end
		#echo "uaexpert added board "${b}" with "${nbChannelsPerBoard}" channels(nodes)"
		@ b++
	end
	echo "uaexpert added crate "${r}" with "${nbChannelsPerBoard}" X  "${nbBoards}" channels(nodes)"
	@ r++
end
cat ${footer} >> ${out2}

echo "==> uaexpert config file= "${out2}



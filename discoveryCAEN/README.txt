Wed Nov  2 15:15:45 CET 2016

this command-line utility takes an IP address (or a list of IPs for multi-crate)  of a CAEN crate,
runs completely through discovery mode and dumps the complete discovered config and all available details into an
xml. 

Specificaly it dumps:
crate model, firmware
board model
firmware version
all properties
property values from some properties
a shorthand notation for EASY architecture
a generic file for each discovered board model,
	 including firmware and all properties and channels with their properties as well 

That xml is used then to configure a simulation HAL as a snaphot of a real situation.


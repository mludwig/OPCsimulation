# CAEN discovery run at Mon Nov 12 14:21:38 2018

   <Board name="EASY.A3009B" slot="0" type="EASY.A3009B" description="">
      <Channel index="0" name="Chan000">
         <ReadOnlyProperty dataType="F" name="Temp" propertyName="Temp"/>
         <ReadOnlyProperty dataType="F" name="Rel" propertyName="Rel"/>
         <ReadOnlyProperty dataType="I" name="12VPwS" propertyName="12VPwS"/>
         <ReadOnlyProperty dataType="I" name="48VPwS" propertyName="48VPwS"/>
         <ReadOnlyProperty dataType="I" name="Sync" propertyName="Sync"/>
         <ReadOnlyProperty dataType="I" name="HVSync" propertyName="HVSync"/>
         <ReadOnlyProperty dataType="I" name="RemIlk" propertyName="RemIlk"/>
         <ReadOnlyProperty dataType="I" name="MainPwS" propertyName="MainPwS"/>
         <ReadOnlyProperty dataType="F" name="SerNum" propertyName="SerNum"/>
         <ReadOnlyProperty dataType="I" name="RemBdName" propertyName="RemBdName"/>
      </Channel>
      <Channel index="1" name="Chan001">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="2" name="Chan002">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="3" name="Chan003">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="4" name="Chan004">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="5" name="Chan005">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="6" name="Chan006">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="7" name="Chan007">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="8" name="Chan008">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="9" name="Chan009">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="10" name="Chan010">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="11" name="Chan011">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
      <Channel index="12" name="Chan012">
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadOnlyProperty dataType="F" name="VCon" propertyName="VCon"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadWriteProperty dataType="I" name="GlbOffEn" propertyName="GlbOffEn"/>
         <ReadWriteProperty dataType="I" name="GlbOnEn" propertyName="GlbOnEn"/>
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
      </Channel>
   </Board>

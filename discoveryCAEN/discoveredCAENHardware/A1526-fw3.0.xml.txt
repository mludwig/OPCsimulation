# CAEN discovery run at Tue Mar 20 11:10:41 2018

   <Board name="A1526" slot="0" type="A1526" description=" 6 Ch Pos. 15KV 1mA  ">
      <ReadOnlyProperty dataType="I" name="BdStatus" propertyName="BdStatus"/>
      <ReadOnlyProperty dataType="F" name="HVMax" propertyName="HVMax"/>
      <ReadOnlyProperty dataType="F" name="Temp" propertyName="Temp"/>
      <Channel index="0" name="Chan000">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="1" name="Chan001">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="2" name="Chan002">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="3" name="Chan003">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="4" name="Chan004">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="5" name="Chan005">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
   </Board>

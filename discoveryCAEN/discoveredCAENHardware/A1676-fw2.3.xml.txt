# CAEN discovery run at Tue Feb 27 18:23:11 2018

   <Board name="A1676" slot="0" type="A1676" description="CAEN Branch Controller     ">
      <ReadOnlyProperty dataType="F" name="A1676Ilk" propertyName="A1676Ilk"/>
      <ReadWriteProperty dataType="F" name="Vsel" propertyName="Vsel"/>
      <ReadWriteProperty dataType="I" name="HW Reset0" propertyName="HW Reset0"/>
      <ReadWriteProperty dataType="I" name="Recovery0" propertyName="Recovery0"/>
      <ReadWriteProperty dataType="I" name="HW Reset1" propertyName="HW Reset1"/>
      <ReadWriteProperty dataType="I" name="Recovery1" propertyName="Recovery1"/>
      <ReadWriteProperty dataType="I" name="HW Reset2" propertyName="HW Reset2"/>
      <ReadWriteProperty dataType="I" name="Recovery2" propertyName="Recovery2"/>
      <ReadWriteProperty dataType="I" name="HW Reset3" propertyName="HW Reset3"/>
      <ReadWriteProperty dataType="I" name="Recovery3" propertyName="Recovery3"/>
      <ReadWriteProperty dataType="I" name="HW Reset4" propertyName="HW Reset4"/>
      <ReadWriteProperty dataType="I" name="Recovery4" propertyName="Recovery4"/>
      <ReadWriteProperty dataType="I" name="HW Reset5" propertyName="HW Reset5"/>
      <ReadWriteProperty dataType="I" name="Recovery5" propertyName="Recovery5"/>
      <ReadWriteProperty dataType="I" name="GlobalOn" propertyName="GlobalOn"/>
      <ReadWriteProperty dataType="I" name="GlobalOff" propertyName="GlobalOff"/>
      <ReadWriteProperty dataType="I" name="48VA1676" propertyName="48VA1676"/>
   </Board>

<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<configuration xmlns="http://cern.ch/quasar/Configuration" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://cern.ch/quasar/Configuration ../Configuration/Configuration.xsd">
# CAEN discovery run at Mon Mar 19 14:21:22 2018

<StandardMetaData>
   <Log>
     <GeneralLogLevel logLevel="TRC"/> 
     <ComponentLogLevels>
        <ComponentLogLevel componentName="STATES" logLevel="TRC"/>
        <ComponentLogLevel componentName="CAEN_API_CALLS" logLevel="TRC"/>
        <ComponentLogLevel componentName="SESSION_SOCKETS" logLevel="TRC"/>
        <ComponentLogLevel componentName="AS_UPDATES" logLevel="TRC"/>
        <ComponentLogLevel componentName="AS_WRITES" logLevel="TRC"/>
        <ComponentLogLevel componentName="AS_DISCO" logLevel="TRC"/>
        <ComponentLogLevel componentName="HW_POLLER" logLevel="TRC"/>
      </ComponentLogLevels> 
   </Log>
</StandardMetaData>
<SYCrate ipAddress="172.26.54.162" model="SY2527" name="SY2527">
   <ReadOnlyProperty dataType="S" name="Sessions" propertyName="Sessions"/>
   <ReadOnlyProperty dataType="S" name="ModelName" propertyName="ModelName"/>
   <ReadOnlyProperty dataType="S" name="SwRelease" propertyName="SwRelease"/>
   <ReadWriteProperty dataType="I" name="GenSignCfg" propertyName="GenSignCfg"/>
   <ReadOnlyProperty dataType="I" name="FrontPanIn" propertyName="FrontPanIn"/>
   <ReadOnlyProperty dataType="I" name="FrontPanOut" propertyName="FrontPanOut"/>
   <ReadWriteProperty dataType="I" name="ResFlagCfg" propertyName="ResFlagCfg"/>
   <ReadOnlyProperty dataType="I" name="ResFlag" propertyName="ResFlag"/>
   <ReadOnlyProperty dataType="S" name="HvPwSM" propertyName="HvPwSM"/>
   <ReadOnlyProperty dataType="S" name="FanStat" propertyName="FanStat"/>
   <ReadOnlyProperty dataType="I" name="ClkFreq" propertyName="ClkFreq"/>
   <ReadWriteProperty dataType="S" name="HVClkConf" propertyName="HVClkConf"/>
   <ReadWriteProperty dataType="S" name="IPAddr" propertyName="IPAddr"/>
   <ReadWriteProperty dataType="S" name="IPNetMsk" propertyName="IPNetMsk"/>
   <ReadWriteProperty dataType="S" name="IPGw" propertyName="IPGw"/>
   <ReadWriteProperty dataType="S" name="RS232Par" propertyName="RS232Par"/>
   <ReadWriteProperty dataType="I" name="CnetCrNum" propertyName="CnetCrNum"/>
   <ReadWriteProperty dataType="S" name="SymbolicName" propertyName="SymbolicName"/>
   <ReadOnlyProperty dataType="I" name="CmdQueueStatus" propertyName="CmdQueueStatus"/>
   <ReadOnlyProperty dataType="S" name="CPULoad" propertyName="CPULoad"/>
   <ReadOnlyProperty dataType="S" name="MemoryStatus" propertyName="MemoryStatus"/>
   <Board name="Board01" slot="1" type="A1735" description=" 12 Ch Pos. 1.5KV 7mA  ">
      <ReadOnlyProperty dataType="I" name="BdStatus" propertyName="BdStatus"/>
      <ReadOnlyProperty dataType="F" name="HVMax" propertyName="HVMax"/>
      <ReadOnlyProperty dataType="F" name="Temp" propertyName="Temp"/>
      <Channel index="0" name="Chan000">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="1" name="Chan001">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="2" name="Chan002">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="3" name="Chan003">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="4" name="Chan004">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="5" name="Chan005">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="6" name="Chan006">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="7" name="Chan007">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="8" name="Chan008">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="9" name="Chan009">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="10" name="Chan010">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="11" name="Chan011">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
   </Board>
   <Board name="Board03" slot="3" type="A1516B" description=" 6 Ch Float 15V 1.5A">
      <ReadOnlyProperty dataType="I" name="BdStatus" propertyName="BdStatus"/>
      <ReadOnlyProperty dataType="F" name="HVMax" propertyName="HVMax"/>
      <ReadOnlyProperty dataType="F" name="Temp" propertyName="Temp"/>
      <Channel index="0" name="Chan000">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="1" name="Chan001">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="2" name="Chan002">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="3" name="Chan003">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="4" name="Chan004">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="5" name="Chan005">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
   </Board>
</SYCrate>
</configuration>

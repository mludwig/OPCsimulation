#!/bin/csh
# upload discovered files in this directory to nexus
# but only the files which are not existing yet and are new.
# check existing files for content.
# https://support.sonatype.com/hc/en-us/articles/213465818-How-can-I-programatically-upload-an-artifact-into-Nexus-
# upload the resulting discovery files to nexus, but only if the 
# file does not yet exists there. If it already exists, check if it has
# identical content (except the # line with the timestamp). 
# If yes, OK, if not identical, issue an error
#
stty -echo 
echo -n "Enter "`whoami`" password: "
set password = $<
stty echo
#
set VENDOR="caen"
set NREPO="cern-venus"
set SDIR="~/OPCsimulation/discoveryCAEN/discoveredCAENHardware"
set REPO="https://repository.cern.ch/nexus/content/repositories/"${NREPO}"/"${VENDOR}"/discoveredCAENHardware"
set REPOTMP="repository.cern.ch/nexus/content/repositories/"${NREPO}"/"${VENDOR}"/discoveredCAENHardware"
#cd ${SDIR}
#foreach s ( `ls *.xml.txt` ) 
#	echo "uploading  "${s}" to " ${REPO}
#	xterm -e "curl -v -u mludwig:${password} --upload-file ${s} ${REPO}/${s}; "
#end;
#
#
# dl from REPO tmp 
set CDIR=`pwd`
rm -rf ./tmp
mkdir -p ./tmp
cd ./tmp
wget -r -np ${REPO}
echo "==============="
echo "already discovered files are in "`pwd`/${REPOTMP}" : "
echo "==============="
ls ${REPOTMP}
cd ${CDIR}
echo "checking which files are new in "${CDIR}
#
foreach ff ( `ls -1 *.xml.txt | grep -v "CaenDiscoveredConfig" `)
	echo "checking "`pwd`/${ff}" against "./tmp/${REPOTMP}/${ff}
    if ( -f "./tmp/${REPOTMP}/${ff}" ) then
		echo "${ff} found. comparing content."
        rm -rf ./new.txt;
        rm -rf ./old.txt
        cat ${ff} | grep -v "#" > ./new.txt
        cat ./tmp/${REPOTMP}/${ff} | grep -v "#" > ./old.txt
        if ( `diff -q ./new.txt ./old.txt` ) then
            echo "=== ERROR !!!! ==="
            echo "=== ERROR !!!! ==="
          	echo "ERROR: discovered and archived files are different but should be identical for same board and firmware!"
            echo "ERROR: We have a problem with discovered file ${ff}"
          	echo "ERROR: either the discovery went wrong, the archived file is wrong, "
            echo "ERROR: or the firmware of the CAEN board got changed without "
            echo "ERROR: updating/incrementing the firmware version. We have found a mess."
            # remove from artifacts and therefore force ERROR on completion
            echo "=== ERROR !!!! ==="
            echo "=== ERROR !!!! ==="
            
 		else
        	# remove from artifacts
          	echo "=== OK: keep ${ff} ==="
            # rm -f ${ff} 
        endif
	else
		echo "=== OK : new board found ! ./tmp/${REPOTMP}/${ff} is not yet archived in nexus. PUSH NEW FILE ${ff} to nexus therefore ==="
        curl -u mludwig:${password} --upload-file "${ff}" "${REPO}/${ff}"
	endif
end

# CAEN discovery run at Wed Mar 14 11:02:26 2018

   <Board name="A1832" slot="0" type="A1832" description=" 12 Ch Pos. 6KV 200uA">
      <ReadOnlyProperty dataType="I" name="BdStatus" propertyName="BdStatus"/>
      <ReadOnlyProperty dataType="F" name="HVMax" propertyName="HVMax"/>
      <ReadOnlyProperty dataType="F" name="Temp" propertyName="Temp"/>
      <Channel index="0" name="Chan000">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="1" name="Chan001">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="2" name="Chan002">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="3" name="Chan003">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="4" name="Chan004">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="5" name="Chan005">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="6" name="Chan006">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="7" name="Chan007">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="8" name="Chan008">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="9" name="Chan009">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="10" name="Chan010">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
      <Channel index="11" name="Chan011">
         <ReadWriteProperty dataType="F" name="V0Set" propertyName="V0Set"/>
         <ReadWriteProperty dataType="F" name="I0Set" propertyName="I0Set"/>
         <ReadWriteProperty dataType="F" name="V1Set" propertyName="V1Set"/>
         <ReadWriteProperty dataType="F" name="I1Set" propertyName="I1Set"/>
         <ReadWriteProperty dataType="F" name="RUp" propertyName="RUp"/>
         <ReadWriteProperty dataType="F" name="RDWn" propertyName="RDWn"/>
         <ReadWriteProperty dataType="F" name="Trip" propertyName="Trip"/>
         <ReadWriteProperty dataType="F" name="SVMax" propertyName="SVMax"/>
         <ReadOnlyProperty dataType="F" name="VMon" propertyName="VMon"/>
         <ReadOnlyProperty dataType="F" name="IMon" propertyName="IMon"/>
         <ReadOnlyProperty dataType="I" name="Status" propertyName="Status"/>
         <ReadWriteProperty dataType="I" name="Pw" propertyName="Pw"/>
         <ReadWriteProperty dataType="I" name="POn" propertyName="POn"/>
         <ReadWriteProperty dataType="I" name="PDwn" propertyName="PDwn"/>
         <ReadWriteProperty dataType="F" name="TripInt" propertyName="TripInt"/>
         <ReadWriteProperty dataType="F" name="TripExt" propertyName="TripExt"/>
      </Channel>
   </Board>

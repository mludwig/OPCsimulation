this directory contains CAEN discovered hardware format footprints
==================================================================

one file per board model and firmware release: <model>-fw<major>.<minor>.xml.txt
all properties of the board and their channels dumped
EASY boards have one leading marker channel followed by power-suppply channels

==> the hardware structure is dumped
==> NOT the values of the properties

contact: michael.ludwig at cern.ch

/**
 * discoveryCAEN
 * main.h
 */

#ifndef _include_discoveryCAEN_main_h
#define _include_discoveryCAEN_main_h

#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <CAENHVWrapper.h>

#define CAEN_PARAM_MAX10 10
using namespace std;

// one property of a crate, board, or channel
typedef struct {
	string name;
	uint32_t mode;
	uint32_t type;
} PROPERTY_t;

// one channel has a couple of properties
typedef struct {
	vector<PROPERTY_t> channelPropsv; // each channel can have different properties
	int easyBoardCount;
} CHANNEL_DISCOVERED_MAP_t;


// one board. primary key = { model, firmwareRelease }
typedef struct {
	uint32_t slot;
	// string parameterName;
	string firmwareRelease;
	string model;
	vector<string> easyModels;
	vector<float> easyFirmwareRelease;
	vector<int> easyChannelCounts;
	string name; // from the user
	string description;
	uint32_t nbChannels;
	vector<string> channelNames;
	vector<string> boardPropertyNames;
	vector<PROPERTY_t> boardPropertiesv;
	vector<CHANNEL_DISCOVERED_MAP_t> channelsv; // channels can be different for a board
} BOARD_DISCOVERED_MAP_t;

// one crate
typedef struct {
	string crateip;
	string crateModel;
	string crateName;
	CAENHV_SYSTEM_TYPE_t sytype;
	int handle;
	string libswrel;
	uint16_t slotsCount;             // NrOfSlot crate slots available in total
	uint16_t discoveredBoardsCount;  // boards present with a type, from discovery
	uint16_t *NrofChList;
	uint16_t nbCrateProperties;
	uint16_t *SerNumList;
	vector<int> FmwRelMinList;
	vector<int> FmwRelMaxList;
	vector<string> boardFirmwareRelease;
	vector<string>boardModels;
	vector<string>boardDescriptions;
	vector<PROPERTY_t> cratePropertiesv;
	vector<BOARD_DISCOVERED_MAP_t> boardv;
} CRATE_DISCOVERED_MAP_t;



#endif

/* © Copyright CERN, 2018.  All rights not expressly granted are reserved.
 *
 *  This file is part of the BE-ICS VENUS project.
 *
 * discoveryCAEN main.cpp
 *
 *  Created on: 27.Feb.2017
 *      Author: mludwig
 *      */
/**
 * standalone tool with minimal dependencies to discover the complete structure of one CAEN crate (ip address).
 * Only reading calls to the CAEN low-level hardware library are made. We discover
 *
 * - all crate properties
 * - all standard boards which are in primary crate slots, including their board properties
 * -- all channels of standard boards
 * - branch controllers which are also standard boards (but without generic channels), including their board properties
 * -- all the branch controller channels (there can be MANY), and we extract the EASY substructure of EASY boards and channels
 *
 * All this is dumped into xml files: first the whole crate discovery in one global file, ready to feed into VENUS.
 * Also the structure of each board, also for EASY boards, is dumped in a separate file for each board model
 * board and firmware. These boards are dumped as if they would sit in slot0 all alone in a crate.
 * These "board reference dumps" are used as modules for VENUS and elsewhere.
 *
 */
#include "../include/main.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <string.h>
#include <vector>
#include <iomanip>

using namespace std;

bool debug = false;

string standardMeta( void ) {
	string x = "\
<StandardMetaData>\n \
  <Log>\n \
    <GeneralLogLevel logLevel=\"TRC\"/> \n \
    <ComponentLogLevels>\n  \
      <ComponentLogLevel componentName=\"STATES\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"CAEN_API_CALLS\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"SESSION_SOCKETS\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"AS_UPDATES\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"AS_WRITES\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"AS_DISCO\" logLevel=\"TRC\"/>\n  \
      <ComponentLogLevel componentName=\"HW_POLLER\" logLevel=\"TRC\"/>\n  \
    </ComponentLogLevels> \n \
  </Log>\n\
</StandardMetaData>";
	return( x );
}

string serializeSystemProperty( PROPERTY_t mm, uint32_t nbTabs ){
	string xml;
	string tab = "   ";
	switch( nbTabs ){
	case 0: tab = ""; break;
	case 1: tab = "   "; break;
	case 2: tab = "      "; break;
	case 3: tab = "         "; break;
	default: tab = "   "; break;
	}
	switch( mm.mode ) {
	case SYSPROP_MODE_RDONLY:{
		xml = tab + "<ReadOnlyProperty ";
		break;
	}
	case SYSPROP_MODE_WRONLY:{
		xml = tab + "<WriteOnlyProperty ";
		break;
	}
	case SYSPROP_MODE_RDWR:{
		xml = tab + "<ReadWriteProperty ";
		break;
	}
	} // switch mode
	switch( mm.type ) {
	case SYSPROP_TYPE_STR:{
		xml = xml + "dataType=\"S\" ";
		break;
	}
	case SYSPROP_TYPE_REAL: {
		xml = xml + "dataType=\"F\" ";
		break;
	}
	case SYSPROP_TYPE_UINT2:
	case SYSPROP_TYPE_UINT4:
	case SYSPROP_TYPE_INT2:
	case SYSPROP_TYPE_INT4:
	case SYSPROP_TYPE_BOOLEAN:{
		xml = xml + "dataType=\"I\" ";
		break;
	}
	} // switch type
	xml = xml + "name=\"" + mm.name + "\" propertyName=\"" + mm.name + "\"/>";
	return( xml );
}

string serializeBdChProperty( PROPERTY_t mm, uint32_t nbTabs ){
	string xml;
	string tab = "   ";
	switch( nbTabs ){
	case 0: tab = ""; break;
	case 1: tab = "   "; break;
	case 2: tab = "      "; break;
	case 3: tab = "         "; break;
	default: tab = "   "; break;
	}
	switch( mm.mode ) {
	case PARAM_MODE_RDONLY:{
		xml = tab + "<ReadOnlyProperty ";
		break;
	}
	case PARAM_MODE_WRONLY:{
		xml = tab + "<WriteOnlyProperty ";
		break;
	}
	case PARAM_MODE_RDWR:{
		xml = tab + "<ReadWriteProperty ";
		break;
	}
	} // switch mode

	switch( mm.type ) {
	case PARAM_TYPE_STRING:{
		xml = xml + "dataType=\"S\" ";
		break;
	}
	case PARAM_TYPE_NUMERIC: {
		xml = xml + "dataType=\"F\" ";
		break;
	}
	case PARAM_TYPE_CHSTATUS:
	case PARAM_TYPE_BDSTATUS:
	case PARAM_TYPE_BINARY:
	case PARAM_TYPE_ONOFF: {
		xml = xml + "dataType=\"I\" ";
	}
	} // switch type
	xml = xml + "name=\"" + mm.name + "\" propertyName=\"" + mm.name + "\"/>";
	return( xml );
}


string xmlTimeStamp( void ){
	time_t now = time( NULL );
	string tt = ctime(&now);
	string tt2 = tt.substr(0, tt.length() - 1 );
	return( "# CAEN discovery run at " + tt2 + "\n" );
}

void writeMap( CRATE_DISCOVERED_MAP_t crateMap, string filename ){

	vector<string> xmlpartsv;
	xmlpartsv.push_back("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>");
	xmlpartsv.push_back("<configuration xmlns=\"http://cern.ch/quasar/Configuration\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://cern.ch/quasar/Configuration ../Configuration/Configuration.xsd\">");
	xmlpartsv.push_back( xmlTimeStamp() );
	xmlpartsv.push_back( standardMeta() );
	string tab = "   ";
	string xml = "<SYCrate ipAddress=\"" + crateMap.crateip +
			"\" model=\"" + crateMap.crateModel +
			"\" name=\"" + crateMap.crateName + "\">";
	xmlpartsv.push_back( xml );

	for ( uint32_t p = 0; p < crateMap.cratePropertiesv.size(); p++ ){
		xmlpartsv.push_back( serializeSystemProperty( crateMap.cratePropertiesv[ p ], 1 ));
	}

	// the boards
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " discovered primary boards count= " << crateMap.discoveredBoardsCount << endl;
	for ( uint16_t ib = 0; ib < crateMap.discoveredBoardsCount; ib++ ){

		BOARD_DISCOVERED_MAP_t boardMap = crateMap.boardv[ ib ];

		if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ib= " << ib << " primary board model= " << boardMap.model << endl;

		char sslot[ 32 ];
		sprintf( sslot, "%d", boardMap.slot );
		xml = tab + "<Board name=\"" + boardMap.name + "\" slot=\"" + sslot +
				"\" type=\"" + boardMap.model + "\" description=\"" + boardMap.description + "\">";
		xmlpartsv.push_back( xml );

		for ( uint16_t iboardProp = 0; iboardProp < boardMap.boardPropertiesv.size(); iboardProp++ ){
			PROPERTY_t prop = boardMap.boardPropertiesv[ iboardProp ];
			xmlpartsv.push_back( serializeBdChProperty( prop, 2 ));
		}

		// the channels: normally they are all identical except the channel names. We make up the channel names:
		// like the board names they come from the user and not from CAEN.
		// But in fact for branch controllers they are NOT identical
		boardMap.channelNames.clear();
		for ( uint32_t ichannel = 0; ichannel < boardMap.nbChannels; ichannel++ ){
			stringstream ss;
			ss << "Chan" << setfill('0') << setw(3) << ichannel; // follow OPCUA caen server conventions

			boardMap.channelNames.push_back( ss.str() );
			char sich[ 32 ];
			sprintf( sich, "%d", ichannel );

			xml = tab + tab + "<Channel index=\"" + sich + "\" name=\"" + boardMap.channelNames[ ichannel ] + "\">";
			xmlpartsv.push_back( xml );

			if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " boardMap.model= " << boardMap.model << " ichannel= " << ichannel << endl;

			for ( uint16_t channelPropertycount = 0; channelPropertycount < boardMap.channelsv[ ichannel ].channelPropsv.size(); channelPropertycount++ ){
				PROPERTY_t channelProperty = boardMap.channelsv[ ichannel].channelPropsv[ channelPropertycount ];
				xmlpartsv.push_back( serializeBdChProperty( channelProperty, 3 ));
			}
			xmlpartsv.push_back( tab + tab + "</Channel>" );
		}
		xmlpartsv.push_back( tab + "</Board>" );
	}
	xmlpartsv.push_back("</SYCrate>");

	xmlpartsv.push_back("</configuration>");

	std::ofstream ofs;
	ofs.open( filename.c_str(), std::ofstream::out | std::ofstream::out );
	for ( uint32_t i = 0; i < xmlpartsv.size(); i++ ){
		ofs << xmlpartsv[ i ] << endl;
	}
	ofs.close();
	cout << "written crate discovery xml to " << filename << endl;
}

/**
 * write an xml fragment for all properties of the board, all channels,
 * into a file named <boardtype>.<firmwareversion>.xml
 * for generic use.
 */
void writeBoardMap( BOARD_DISCOVERED_MAP_t boardMap, string fw ){

	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " writeBoardMap model= " << boardMap.model << endl;

	string filename = boardMap.model + "-fw" + fw + ".xml.txt";
	vector<string> xmlpartsv;
	xmlpartsv.clear();
	string tab = "   ";
	string xml;
	xmlpartsv.push_back( xmlTimeStamp() );

	char sslot[ 32 ] = "0"; // generic slot 0
	xml = tab + "<Board name=\"" + boardMap.model + "\" slot=\"" + sslot +
			"\" type=\"" + boardMap.model + "\" description=\"" + boardMap.description + "\">";
	xmlpartsv.push_back( xml );

	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " writeBoardMap boardMap.boardPropertiesv.size()= " << boardMap.boardPropertiesv.size() << endl;

	for ( uint16_t iboardProp = 0; iboardProp < boardMap.boardPropertiesv.size(); iboardProp++ ){
		PROPERTY_t prop = boardMap.boardPropertiesv[ iboardProp ];
		xmlpartsv.push_back( serializeBdChProperty( prop, 2 ));
	}

	// the channels: normally they are all identical except the channel names
	// But in fact for branch controllers they are NOT identical.
	if ( boardMap.model == "A1676"){
		xmlpartsv.push_back( tab + "</Board>" );
		if ( debug ) cout << "found branch controller " << boardMap.model << ": this has no generic channels, skipping" << endl;
	} else {
		boardMap.channelNames.clear();
		if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " generic boardMap.nbChannels = " << boardMap.nbChannels << endl;
		for ( uint32_t ichannel = 0; ichannel < boardMap.nbChannels; ichannel++ ){

			if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " writeBoardMap boardMap.channelsv.size()= " << boardMap.channelsv.size() << endl;

			stringstream ss;
			ss << "Chan" << setfill('0') << setw(3) << ichannel; // follow OPCUA caen server conventions

			boardMap.channelNames.push_back( ss.str() );
			char sich[ 32 ];
			sprintf( sich, "%d", ichannel );

			xml = tab + tab + "<Channel index=\"" + sich + "\" name=\"" + boardMap.channelNames[ ichannel ] + "\">";
			xmlpartsv.push_back( xml );

			for ( uint16_t channelPropertycount = 0; channelPropertycount < boardMap.channelsv[ ichannel ].channelPropsv.size(); channelPropertycount++ ){
				PROPERTY_t channelProperty = boardMap.channelsv[ ichannel].channelPropsv[ channelPropertycount ];
				xmlpartsv.push_back( serializeBdChProperty( channelProperty, 3 ));
			}
			xmlpartsv.push_back( tab + tab + "</Channel>" );
		}
		xmlpartsv.push_back( tab + "</Board>" );
	}
	std::ofstream ofs;
	ofs.open( filename.c_str(), std::ofstream::out | std::ofstream::out );
	for ( uint32_t i = 0; i < xmlpartsv.size(); i++ ){
		ofs << xmlpartsv[ i ] << endl;
	}
	ofs.close();
	cout << "written generic board xml to " << filename << endl;
}



vector<string> parseCharBuffArray(char* const* charBuffArray, int boardCount) {
	vector<string> result;
	const char* currentChar = *charBuffArray;
	for(unsigned short j=0; j<boardCount;  j++)	{
		if(*currentChar) {
			ostringstream name;
			while(*currentChar)
			{
				name << *currentChar;
				currentChar++;
			}
			result.push_back(name.str());
		} else {
			result.push_back("NA"); // not available
		}
		currentChar++;
	}
	return result;
}

vector<string> boardFirmware(unsigned char* msb, unsigned char *lsb, int boardCount) {
	vector<string> result;
	for(unsigned short j=0; j<boardCount;  j++)	{
		stringstream ss;
		ss << int(msb[j]) << "." << int(lsb[j]);
		result.push_back( ss.str());
	}
	return result;
}

vector<string> parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth) {
	vector<string> result;
	for(char* i = const_cast<char*>(buffer); i[0]; i+= fixedWidth) {
		result.push_back(i);
	}
	return vector<string>(result);
}

void usage( char *p ){
	cout << "full dump of CAEN P.S. crate(s) to xml config, including board models, straight from the CAEN hw lib." << endl;
	cout << "used mainly to create a detailed configuration, clone systems and store board types for simulation." << endl;
	cout << "usage: " << p << " -ip <ip0>  [ -help | -sys <ty0> | -debug ] " << endl;
	cout << "  where <ip0> is the CAEN crate to discover, together with it's system-type <ty0>" << endl;
	cout << "     if you don't specify a type the default SY4527 will be used" << endl;
	cout << "  where <ty0> = crate system name as a string, i.e. \"SY4527\" (which is also the default)" << endl;
	cout << "example: " << p << " -ip \"137.138.9.19\" -sys \"SY4527\"" << endl;
	cout << "  will write the config of one crate at 137.138.9.19 to file CaenFullCfgSim.137.138.9.19.xml" << endl << endl;
	cout << "known crate types: -sys SY1527 SY2527 SY4527 SY5527"
			<< " (types are downward compatible, i.e. you can use a SY1527 for an existing SY4527)"
			<< " (no idea yet for NXXXX and VXXXX types)"
			<< endl;
	exit(0);
}


/** the EASY boards are marker channels followed by power supply channels, and from the marker channel
 * we can find out the EASY board model. It has to have the Property "RemBdName" and we need the Onstate
 * of that property. This will be actually a string. OMG. argh.
 */
string getEASYBoardModel( int chandle, int bdslot, int imarker ){
	char xxx[ CAEN_PARAM_MAX10 ];
	CAENHVRESULT result = CAENHV_GetChParamProp( chandle, bdslot, imarker, "RemBdName", "Onstate", &xxx );
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__  << " getEASYBoardModel= " << xxx << endl;
	if ( result == CAENHV_OK ) return( string(xxx));
	else return( "(not found)" );
}

/**
 * read property Rel from the marker channel
 */
float getEASYBoardFirmwareRelease( int chandle, int bdslot, int imarker ){
	unsigned short chNum = 1;
	unsigned short chList[ chNum ];
	chList[ 0 ] = imarker;
	float parValList[ chNum ];
	CAENHVRESULT result = CAENHV_GetChParam( chandle, bdslot, "Rel", chNum, chList, parValList );
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__  << " getEASYBoardFirmwareRelease " << parValList[ 0 ] << endl;
	if ( result == CAENHV_OK ) return( parValList[ 0 ] );
	else return( -99.9 );
}

int discoverCAENcrate( CRATE_DISCOVERED_MAP_t *crateMapp, vector<BOARD_DISCOVERED_MAP_t> *easyBoardMapv ){
	const size_t PARAM_NAME_WIDTH = 10;
	crateMapp->libswrel = CAENHVLibSwRel();
	cout << "CAENHVLibSwRel library SW release: " << crateMapp->libswrel << endl;

	if ( debug ) cout << " sytype= " << crateMapp->sytype
			<< " ip= " << crateMapp->crateip.c_str()
			<< endl;

	string myuser = "admin";
	string mypasswd = "admin";
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " user= " << myuser << " passwd= " << mypasswd << endl;
	CAENHVRESULT result = CAENHV_InitSystem( crateMapp->sytype, LINKTYPE_TCPIP, ( char *) crateMapp->crateip.c_str(), myuser.c_str(), mypasswd.c_str(), &crateMapp->handle);
	if ( debug )  cout << "init crate result= " << result << endl;
	if ( result != CAENHV_OK ){
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " error init crate at " << crateMapp->crateip << " type= " << crateMapp->sytype << ": check init parameters. exiting. " << endl;
		return(-1);
	} else {
		if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " init crate at " << crateMapp->crateip << " OK " << endl;
	}
	char *ModelList = 0;
	char *DescriptionList = 0;
	short unsigned int *SerNumList = 0;
	unsigned char *FmwRelMinList = 0;
	unsigned char *FmwRelMaxList = 0;

	result = CAENHV_GetCrateMap( crateMapp->handle, &crateMapp->slotsCount, &crateMapp->NrofChList,
			&ModelList, &DescriptionList,
			&SerNumList,
			&FmwRelMinList,
			&FmwRelMaxList );

	if ( debug ) cout << crateMapp->crateip << " handle= " << crateMapp->handle << " queryCrateMap: found " << crateMapp->slotsCount << " slots" << endl;
	crateMapp->boardModels = parseCharBuffArray( ( char * const * ) &ModelList, crateMapp->slotsCount );
	crateMapp->boardDescriptions = parseCharBuffArray( ( char * const * ) &DescriptionList, crateMapp->slotsCount );
	crateMapp->boardFirmwareRelease = boardFirmware( FmwRelMaxList, FmwRelMinList, crateMapp->slotsCount );

	CAENHV_Free(ModelList);
	CAENHV_Free(DescriptionList);
	CAENHV_Free(SerNumList);
	CAENHV_Free(FmwRelMinList);
	CAENHV_Free(FmwRelMaxList);

	crateMapp->discoveredBoardsCount = 0;
	for ( int i = 0; i < crateMapp->slotsCount; i++ ){
		if ( crateMapp->NrofChList[ i ] > 0 ){
			if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " slot" << i << " board model= " << crateMapp->boardModels[ i ] <<
					" boardDescription= " << crateMapp->boardDescriptions[ i ] <<
					" SerNumList= " << SerNumList[ i ] <<
					" firmwareRelease= " << crateMapp->boardFirmwareRelease[ i ] <<
					endl;
			crateMapp->discoveredBoardsCount++;
		}
	}
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " " << crateMapp->crateip
			<< " handle= " << crateMapp->handle << " queryCrateMap: discoveredBoardsCount= " << crateMapp->discoveredBoardsCount << endl;

	// crate properties
	char *PropNameList = 0;
	result =  CAENHV_GetSysPropList( crateMapp->handle, &crateMapp->nbCrateProperties, &PropNameList );
	vector<string> propNames = parseCharBuffArray( ( char * const * ) &PropNameList, crateMapp->nbCrateProperties );
	CAENHV_Free(PropNameList);
	for ( int i = 0; i < crateMapp->nbCrateProperties; i++ ){
		PROPERTY_t property;
		property.name = propNames[ i ];
		if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " crate property= " << property.name << endl;
		result = CAENHV_GetSysPropInfo( crateMapp->handle, propNames[ i ] .c_str(), &property.mode, &property.type );
		crateMapp->cratePropertiesv.push_back( property );
	}

	// discover boards in crates, populated slots
	for ( uint32_t islot = 0; islot < crateMapp->slotsCount; islot++ ){
		if ( crateMapp->NrofChList[ islot ] > 0 ){

			uint32_t nbChannels = crateMapp->NrofChList[ islot ];
			if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " " << crateMapp->crateip << " handle= " << crateMapp->handle <<
					" slot= " << islot << " discover board type= "<< crateMapp->boardModels[ islot ] << " nbChannels= " << nbChannels << endl;
			char *boardParameterList = 0;
			result = CAENHV_GetBdParamInfo( crateMapp->handle, islot, &boardParameterList);


			// if the board is a BC we already know how many channels in total it has, and by looking at the marker channels we
			// find out which easy boards are there.
			bool boardIsBrancController = false;
			if ( crateMapp->boardModels[ islot ] == "A1676"){
				boardIsBrancController = true;
				if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " " << crateMapp->crateip << " handle= " << crateMapp->handle <<
						" slot= " << islot << " is a branch controller with channels= " << nbChannels << endl;
			} else {
				boardIsBrancController = false;
			}

			BOARD_DISCOVERED_MAP_t boardMap;
			boardMap.boardPropertyNames = parseFixedWidthArrayOfCharBuffers( ( const char * ) boardParameterList, PARAM_NAME_WIDTH );
			CAENHV_Free( boardParameterList );

			for ( unsigned int i  = 0; i < boardMap.boardPropertyNames.size(); i++ )
				if ( debug ) {
					cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
							<< " board property= " << boardMap.boardPropertyNames[ i ] << endl;
				}

			boardMap.slot = islot;
			boardMap.description = crateMapp->boardDescriptions[ islot ];
			boardMap.nbChannels = nbChannels;
			boardMap.model = crateMapp->boardModels[ islot ];
			stringstream ss;
			ss << "Board" << setfill('0') << setw(2) << islot;;
			boardMap.name = ss.str(); // follow OPCUA caen server conventions

			// primary board properties (and channels) in detail, mode and type
			for ( uint32_t p = 0; p < boardMap.boardPropertyNames.size(); p++ ){
				PROPERTY_t boardProperty;
				boardProperty.name = boardMap.boardPropertyNames[ p ];

				if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " islot= " << islot
						<< " board property= " << boardProperty.name << endl;

				result = CAENHV_GetBdParamProp( crateMapp->handle, boardMap.slot, boardProperty.name.c_str(), "Mode", &boardProperty.mode );
				result = CAENHV_GetBdParamProp( crateMapp->handle, boardMap.slot, boardProperty.name.c_str(), "Type", &boardProperty.type );
				boardMap.boardPropertiesv.push_back( boardProperty );
			}


			/**
			 * All channels of normal primary boards are identical, so it would be enough
			 * to interrogate the first channel only. For a branch controller, this is not true
			 * so we go through all channels individually for all boards.
			 * We assume that channel numbers are monotone and consecutive 0...N, and that therefore all existing channels are populated
			 * and numbered like this. This is different from crate slots, which can be left empty as well.
			 */
			if ( boardIsBrancController) {
				BOARD_DISCOVERED_MAP_t easyBoardMap;
				if ( debug ) cout <<__FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " islot= " << islot << " boardIsBranchController " <<  endl;

				// all discovered channel properties for this channel are in fact board or channel properties of some easy board.
				// lets just discover all of them again to have them neatly in the easy board map. This is the
				// first channel =  easy board marker channel, and all following channels belong to this easy
				// board as well, until the next board or marker channel is detected.
				int easyBoardCount = -1;
				int easyChannelCount = 0;
				easyBoardMap.easyChannelCounts.push_back( easyChannelCount );
				for ( unsigned int iEASYchannel = 0; iEASYchannel < nbChannels; iEASYchannel ++){
					easyChannelCount++;

					char *channelParNameList2;
					int ParNumber2;
					// get all the properties of that EASY channel, marker or not...
					result = CAENHV_GetChParamInfo( crateMapp->handle, boardMap.slot, iEASYchannel, &channelParNameList2, &ParNumber2 );
					vector<string> EasyChannelPropertyNames = parseFixedWidthArrayOfCharBuffers( ( const char * ) channelParNameList2, PARAM_NAME_WIDTH );
					CAENHV_Free( channelParNameList2 );

					if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " islot= " << islot
							<< " channel= " << iEASYchannel
							<< " EASY board has nb of properties= " << EasyChannelPropertyNames.size() << endl;

					/**
					 * go through the properties and distinguish the separate EASY boards. Consecutive easy boards
					 * are separated by channel markers: channels which have Temp and no VMon property
					 */
					CHANNEL_DISCOVERED_MAP_t channelPropertiesMap;
					for ( uint32_t iprop = 0; iprop < EasyChannelPropertyNames.size(); iprop++ ){
						PROPERTY_t prop;
						prop.name = EasyChannelPropertyNames[ iprop ];

						result = CAENHV_GetChParamProp( crateMapp->handle, boardMap.slot, iEASYchannel, prop.name.c_str(), "Mode", &prop.mode );
						result = CAENHV_GetChParamProp( crateMapp->handle, boardMap.slot, iEASYchannel, prop.name.c_str(), "Type", &prop.type );
						if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " islot= " << islot << " EASY board iBCchannel= " << iEASYchannel
								<< " property= " << prop.name
								<< " mode= " << prop.mode
								<< " type= " << prop.type
								<< endl;

						/**
						 * an EASY board marker channel has a property "Temp". We can get the model of the easy board.
						 * The marker channel always comes first in the discovery sequence, and these are the "board properties"
						 * of the new easy board=channel00
						 */
						if( prop.name == "Temp" ) {
							easyBoardCount++;
							easyChannelCount = 0;
							string easyBoardModel = getEASYBoardModel( crateMapp->handle, boardMap.slot, iEASYchannel );
							float easyFirmwareRelease = getEASYBoardFirmwareRelease( crateMapp->handle, boardMap.slot, iEASYchannel );
							if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " found easyBoardModel= " << easyBoardModel
									<< " easy firmware rel= " << easyFirmwareRelease
									<< " easyBoardCount= " << easyBoardCount
									<< " islot= " << islot << " found EASY board marker iBCchannel= " << iEASYchannel
									<< endl;
							easyBoardMap.easyModels.push_back( easyBoardModel );
							easyBoardMap.easyFirmwareRelease.push_back( easyFirmwareRelease );
							// easyBoardMap.easyChannelCounts.push_back( easyChannelCount );
						}
						channelPropertiesMap.channelPropsv.push_back( prop );
						channelPropertiesMap.easyBoardCount = easyBoardCount;
					}
					easyBoardMap.channelsv.push_back( channelPropertiesMap );
					easyBoardMap.easyChannelCounts[ easyBoardCount ] = easyChannelCount + 1; // including marker
					if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " easyBoardCount= " << easyBoardCount << " easyChannelCounts= " << easyChannelCount << endl;
				}

				// lets check if the map for easy boards is complete and OK and sort it out properly, ready
				// for file dump
				for ( unsigned int i = 0; i < easyBoardMap.easyModels.size(); i++ ){
					BOARD_DISCOVERED_MAP_t splitMap;
					splitMap.model = "EASY." + easyBoardMap.easyModels[ i ];

					std::ostringstream ss;
					ss << easyBoardMap.easyFirmwareRelease[ i ];

					splitMap.firmwareRelease = ss.str(); // easyBoardMap.easyFirmwareRelease[ i ];

					// splitMap.nbChannels = easyBoardMap.channelsv.size();
					splitMap.nbChannels = easyBoardMap.easyChannelCounts[ i ];
					splitMap.slot = -1;

					if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " easyModels[ slot= " << i << " ]= " << easyBoardMap.easyModels[ i ]
							<< " easyFWReleases[ " << i << " ]= " << easyBoardMap.easyFirmwareRelease[ i ]
							<< " easyChannelCount= " << easyBoardMap.easyChannelCounts[ i ]
							<< endl;

					for ( unsigned int k = 0; k < easyBoardMap.channelsv.size(); k++ ){
						CHANNEL_DISCOVERED_MAP_t dchan;
						for ( unsigned int ip = 0; ip < easyBoardMap.channelsv[ k ].channelPropsv.size(); ip++ ){
							PROPERTY_t prop = easyBoardMap.channelsv[ k ].channelPropsv[ ip ];
							dchan.channelPropsv.push_back( prop );
							if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " channelsv[ " << k
									<< " ] property name= " << easyBoardMap.channelsv[ k ].channelPropsv[ ip ].name << endl;
						}
						splitMap.channelsv.push_back( dchan );
					}
					easyBoardMapv->push_back( splitMap );
				}


				// lets check the final maps for all easy boards
				if ( debug ) {
					for ( unsigned int i = 0; i < easyBoardMapv->size(); i++ ){
						BOARD_DISCOVERED_MAP_t m = (*easyBoardMapv)[ i ];
						for ( unsigned int k = 0; k < m.channelsv.size(); k++ ){
							for ( unsigned int ip = 0; ip < m.channelsv[ k ].channelPropsv.size(); ip++ ){
								cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " EASY channel= " << k
										<< " property name= " << m.channelsv[ k ].channelPropsv[ ip ].name << endl;
							}
						}
					}
				}

				// what do we do with empty branch controllers ?? Lets ignore that for now ;-(
				cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " found a branch controller in slot= " << islot
						<< " which routes " << easyBoardMapv->size()+1 << " EASY boards" << endl;
			} // if.. branch controller

			// standard board, and the BC is in fact also a standard board
			cout << "found islot= " << islot << " with standard board " <<  crateMapp->boardModels[ islot ] << endl;
			char *channelParNameList;
			int ParNumber;
			for ( uint32_t ichannel = 0; ichannel < boardMap.nbChannels; ichannel ++ ) {
				result = CAENHV_GetChParamInfo( crateMapp->handle, boardMap.slot, ichannel, &channelParNameList, &ParNumber );
				vector<string> channelParameterNamesv = parseFixedWidthArrayOfCharBuffers( ( const char * ) channelParNameList, PARAM_NAME_WIDTH );
				CAENHV_Free( channelParNameList );

				if ( debug ) cout << endl << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " islot= " << islot << " channel= " << ichannel	<< endl;

				CHANNEL_DISCOVERED_MAP_t channelMap;
				for ( uint32_t p = 0; p < channelParameterNamesv.size(); p++ ){
					PROPERTY_t channelProperty;
					channelProperty.name = channelParameterNamesv[ p ];

					result = CAENHV_GetChParamProp( crateMapp->handle, boardMap.slot, ichannel, channelProperty.name.c_str(), "Mode", &channelProperty.mode );
					result = CAENHV_GetChParamProp( crateMapp->handle, boardMap.slot, ichannel, channelProperty.name.c_str(), "Type", &channelProperty.type );

					if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " islot= " << islot << " channel= " << ichannel
							<< " property= " << channelProperty.name
							<< " mode= " << channelProperty.mode
							<< " type= " << channelProperty.type
							<< endl;

					channelMap.channelPropsv.push_back( channelProperty );
				}
				boardMap.channelsv.push_back( channelMap );
			}
			crateMapp->boardv.push_back( boardMap );
		} // loop over populated primary crate slots
	}

	// deinint crate to be clean
	result = CAENHV_DeinitSystem( crateMapp->handle );
	if ( result ){
		cout << "error deinit crate " << crateMapp->handle << " at " << crateMapp->crateip << " exiting. " << endl;
		return(-1);
	} else {
		cout << "deinit crate " << crateMapp->handle << " at " << crateMapp->crateip << " OK " << endl;
	}
	return(0);
}



int main( int argc, char **argv ){
	cout << endl << argv[ 0 ] << " program version: " << __DATE__ << " " << __TIME__ << endl;
	string crateip;
	string fdefault = "CaenDiscoveredConfig.";
	const int IP_SIZE = 32;
	CAENHV_SYSTEM_TYPE_t systemType = SY4527;
	string systemModel = "SY4527";

	int i = 0;
	for ( i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "-help")){
			usage( argv[0] );
		}
		if ( 0 == strcmp( argv[i], "-debug")){
			debug = true;
		}
		if ( 0 == strcmp( argv[i], "-ip") && argc > i ){
			char ip[IP_SIZE];
			sscanf( argv[i+1], "%s", &ip[ 0 ] );
			crateip = string( ip );
		}
		if ( 0 == strcmp( argv[i], "-sys") && argc > i ){
			char hh[ 256 ];
			sscanf( argv[i+1], "%s",  &hh[ 0 ] );
			systemModel = string( hh );
			if ( strcmp( systemModel.c_str(), "SY1527") == 0 ) systemType = SY1527;
			else if ( strcmp( systemModel.c_str(), "SY2527") == 0 ) systemType = SY2527;
			else if ( strcmp( systemModel.c_str(), "SY4527") == 0 ) systemType = SY4527;
			else if ( strcmp( systemModel.c_str(), "SY5527") == 0 ) systemType = SY5527;
			// else if ( strcmp( systemModel.c_str(), "N568") == 0 ) systemType = N568;
			// else if ( strcmp( systemModel.c_str(), "V65XX") == 0 ) systemType = V65XX;
			// else if ( strcmp( systemModel.c_str(), "N1470") == 0 ) systemType = N1470;
			// else if ( strcmp( systemModel.c_str(), "V8100") == 0 ) systemType = V8100;
			else {
				cout << "ERROR: unknown -sys option: " << systemModel << ", exiting " << endl << endl;
				usage( argv[0] );
			}
		}
	}
	if ( crateip.size() == 0 ) usage( argv[0] );
	vector<BOARD_DISCOVERED_MAP_t> easyBoardMapv;
	string filename = fdefault + crateip + ".xml.txt";

	CRATE_DISCOVERED_MAP_t crateMap;
	crateMap.sytype = systemType;
	crateMap.crateModel = systemModel;
	crateMap.crateName = systemModel;
	crateMap.crateip = crateip;

	int ret = discoverCAENcrate( &crateMap, &easyBoardMapv );
	if ( ret ) {
		cout << "something went wrong with crate discovery. Check the ip " << crateMap.crateip << " and the crate model " << systemModel << endl;
		return(-1);
	}

	// right. Lets write out an xml, plain strings, no checking, no boost
	// http://www.technical-recipes.com/2014/using-boostproperty_tree/
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " write crate map to " << filename << endl;
	writeMap( crateMap, filename );

	// free allocated pointers in the server
	CAENHV_Free( crateMap.NrofChList );
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " CAENHV_ deallocate pointers"<<  endl;


	// write out the boards footprints into each specific file
	if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " writing specific board footprints per model per firmware..." << endl;
	CRATE_DISCOVERED_MAP_t crate_map = crateMap;
	for ( unsigned int iboard = 0; iboard < crate_map.boardv.size(); iboard++ ){
		BOARD_DISCOVERED_MAP_t board_map  = crate_map.boardv[ iboard ];
		string fw = crate_map.boardFirmwareRelease[ iboard ];
		if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " write board footprint model= " << board_map.model << endl;
		writeBoardMap( board_map, fw );
	}

	// write out the discovered easy boards if any
	if ( easyBoardMapv.size() ){
		if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " writing specific footprints for " << easyBoardMapv.size() << " EASY boards"<< endl;
		for ( unsigned int iEasyBoard = 0; iEasyBoard < easyBoardMapv.size(); iEasyBoard++ ){
			BOARD_DISCOVERED_MAP_t board_map  = easyBoardMapv[ iEasyBoard ];
			string fw = board_map.firmwareRelease;
			if ( debug ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " write board footprint easy model= " << board_map.model << " firmwareRelease= " << fw << endl;
			writeBoardMap( board_map, fw );
		}
	}
	// we would like to have a pair of VMon and IMon to get the load of that channel, but that is another story
	// and we don't want to cover this functionality with this program. This is a hardware snapshot only.

	return( 0 );
}

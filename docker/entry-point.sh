#!/bin/bash
# these environment vars are inherited to the docker via the Dockerfile
# the Dockerfile points to the VENUS jenkins artifact. In order to
# change the config for different docker instances, it is enough to
# inject the configs through the environment. The JENKINS_VENUS_BUILD
# is defined in the Dockerfile 
#
# must not exit this script, otherwise slave dies.
#
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/venus/lib
env
#
# s. engine
#ldd /venus/sim/simulationHAL
echo "===starting simulation engine==="
cd /venus/sim && ./simulationHAL $SIMCONFIG &
echo "Now sleeping five seconds to give the s.engine time enough to start up fully"
sleep 5
#
# run the Unified Automation sdk-based flavour of the Opc_Ua CAEN server
echo "runtime dependencies OpcUaCaenServer.ua, hopefully they are all there"
ldd OpcUaCaenServer.ua
echo "===starting OPC .ua server==="
cp ./libcaenhvwrapper.so /lib64
cd /venus/opc/bin && ./OpcUaCaenServer.ua $OPCCONFIG &
#
# 
# run the open6 sdk-based flavour of the Opc_Ua CAEN server
#ldd /opc/bin/OpcUaCaenServer.open6
#ls -l /opc/bin/CAEN/hwAccess/lib
#echo "===starting OPC CAEN server with $OPCCONFIG==="
#cd /venus/opc/bin && ./OpcUaCaenServer.open6 --hardwareDiscoveryMode --config_file ./$OPCCONFIG &
#echo "===starting OPC CAEN server in HW discovery mode==="
#cd /venus/opc/bin && ./OpcUaCaenServer.open6 --hardwareDiscoveryMode &

echo "Now sleeping five seconds to give the Caen OPC server time enough to start up fully"
sleep 5

# run the OPC Venus pilot server, open6
echo "runtime dependencies OpcVenusCaenPilotServer.open6, hopefully they are all there"
ldd /venus/pilot/OpcVenusCaenPilotServer.open6
echo "===starting OPC Venus Pilot server==="
cd /venus/pilot/bin && ./OpcUaVenusCaenPilotServer.open6
# never exits


#echo "keeping docker alive "
#while true
#do
#    sleep 10
#done

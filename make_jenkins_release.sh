#!/bin/csh
#
# create a tarball with a given name, script is called by jenkins to provide a jenkins artifact
# for public use. Version info etc is just the tarball argument
# run inside ~/OPCsimulation
#
# script to run from OPCsimulation root (git) dir to make tarball containing
# -a simulation engine, (current venus build)
# -glue libs, (current venus build)
# -an OPC-UA.ua and OPC-UA.open6 server, (pulled in from jenkins builds)
# -a pilot, (current venus build)
# -a simple caen hw discovery tool
# - all configurations neccessary. (current configs dir)
# Also all runnable configs for ATLAS, ALICE... are included.
#
# dir structure:
# 	./release/sim
# 	./release/opc
#	./release/lib
#	./release/tools
#
# we pull everything together into the subdir ./release and make a tgz from that
#
set PROJDIR=`pwd`
set RELEASEDIR=${PROJDIR}/release
set DIR_OPC=${RELEASEDIR}/opc
set DIR_SIM=${RELEASEDIR}/sim
set DIR_LIB=${RELEASEDIR}/lib
set DIR_TOOLS=${RELEASEDIR}/tools

rm -rf ${RELEASEDIR}; mkdir -p ${RELEASEDIR}
# rm -rf ${DIR_OPC}; mkdir -p ${DIR_OPC}
rm -rf ${DIR_SIM}; mkdir -p ${DIR_SIM}
rm -rf ${DIR_LIB}; mkdir -p ${DIR_LIB}
rm -rf ${DIR_TOOLS}; mkdir -p ${DIR_TOOLS}

# 1. copy libs
# lib: system libs needed for glue and engine . Take them from the devel system
set LIBS_GLUE="libprotobuf.so.8 libprotoc.so.8 libczmq.so.3 liblzma.so.5 libxml2.so.2 libzmq.so.5 libxerces-c-3.1.so libprotobuf-c.so libczmq.so.3 libsodium.so.13 libpgm-5.2.so.0 libcrypto.so.10"
foreach ff ( ${LIBS_GLUE} )
	cp -v /lib64/${ff} ${DIR_LIB}
end
# lib: glue & config
set SRCDIR=${PROJDIR}/simulationOPCglue/src
set FILES="libsimcaen.so libgluecaen.so README.txt"
foreach ff ( ${FILES} )
	cp -v ${SRCDIR}/${ff} ${DIR_LIB}
end
# opc glue config
set SRCDIR=${PROJDIR}/config
set FILES="caenGlueConfig.xml"
foreach ff ( ${FILES} )
	cp -v ${SRCDIR}/${ff} ${DIR_OPC}
end
# set symlinks for lib versions
cd ${DIR_LIB}
ln -s libprotobuf.so.8 libprotobuf.so
ln -s libprotoc.so.8 libprotoc.so
ln -s libczmq.so.3 libczmq.so
ln -s liblzma.so.5 liblzma.so
ln -s libxml2.so.2 libxml2.so
ln -s libzmq.so.5 libzmq.so
ln -s libxerces-c-3.1.so libxerces-c.so
ln -s libprotobuf-c.so.1 libprotobuf-c.so


#
# 2. pull OPC-UA.ua CAEN server Unfied Automation cc7-binary from jenkins
set BUILD_UA="1190"
set OPC_JENKINS_TGZ="caen-opcua-server.TEST.tar.gz"
set OPC_SERVER_UA_BIN="OpcUaCaenServer.ua"
cd ${RELEASEDIR}
wget https://icejenkins.cern.ch/view/OPC-UA/job/caen-opcua-server/${BUILD_UA}/artifact/${OPC_JENKINS_TGZ}
tar -xvzf ${OPC_JENKINS_TGZ}
rm -f ${OPC_JENKINS_TGZ}
rm -rf ${DIR_OPC} # dir need to be non existant
mv ${RELEASEDIR}/caen-opcua-server.TEST ${DIR_OPC}
mv ${DIR_OPC}/bin/OpcUaCaenServer ${DIR_OPC}/bin/${OPC_SERVER_UA_BIN}
# add the needed configs for the .ua server
cp ${PROJDIR}/server-config/ServerConfig.xml ${DIR_OPC}/bin/
mkdir -p ${DIR_OPC}/Configuration/
cp ${PROJDIR}/server-config/Configuration/Configuration.xsd ${DIR_OPC}/Configuration
# set symlink opc/bin to lib
cd ${DIR_OPC}/bin
ln -s ../../lib ./
echo "---pulled OPC-UA.ua from jenkins "/${BUILD_UA}"---"


#
# 3. pull in OPC-UA.open6 server CAEN server open62541 cc7-binary from jenkins
cd ${DIR_OPC}
set BUILD_OPEN6="604"
set OPC_JENKINS_TGZ="caen-opcua-server.TEST.tar.gz"
set OPC_SERVER_OPEN6_BIN="OpcUaCaenServer.open6"
wget https://icejenkins.cern.ch/view/OPC-UA/job/caen-opcua-server-open62541/${BUILD_OPEN6}/artifact/${OPC_JENKINS_TGZ}
tar -xvzf ${OPC_JENKINS_TGZ}
rm ${OPC_JENKINS_TGZ}
mv caen-opcua-server.TEST/bin/OpcUaCaenServer ${DIR_OPC}/bin/${OPC_SERVER_OPEN6_BIN}
rm -rf ./caen-opcua-server.TEST
echo "---pulled OPC-UA.open6 from jenkins "/${BUILD_OPEN6}"---"

#
# 4. setup symlink for simulation .ua and .open6 server. Relative links please
cd ${DIR_OPC}/bin/CAEN/hwAccess/lib/
rm -f ./libcaenhvwrapper.so.*
#mv ./libcaenhvwrapper.so.5.84 ./libcaenhvwrapper.so.5.84.org
ln -s ../../../../lib/libgluecaen.so ./
ln -s libgluecaen.so ./libcaenhvwrapper.so.5.84
ln -s libgluecaen.so ./libcaenhvwrapper.so.5.83
ln -s libgluecaen.so ./libcaenhvwrapper.so.5.82

#
# 5. copy sim: engine
set SRCDIR=${PROJDIR}/simulationHAL/src
set FILES="simulationHAL README.txt"
foreach ff ( ${FILES} )
	cp -v ${SRCDIR}/${ff} ${DIR_SIM}
end

#
# 6. configs: common server, glue and s.engine configs, all in the good places please
mkdir -p ${DIR_OPC}/Configuration
cp -v ${PROJDIR}/server-config/Configuration/Configuration.xsd ${DIR_OPC}/Configuration
cp -v ${PROJDIR}/server-config/config.127.0.0.1.xml ${DIR_OPC}/bin/config.xml
cp -v ${PROJDIR}/server-config/ServerConfig.xml ${DIR_OPC}/bin
cp -v ${PROJDIR}/scripts/killopc.sh ${DIR_OPC}/bin
cp -v ${PROJDIR}/config/caenGlueConfig.127.0.0.1.xml ${DIR_OPC}/bin/caenGlueConfig.xml
cp -v ${PROJDIR}/config/README.txt ${DIR_OPC}
cp -rv ${PROJDIR}/server-config/PKI ${DIR_OPC}
# configs for simulation opc and s.engine
cp -v ${PROJDIR}/config/opc_*.xml ${DIR_OPC}/bin
cp -v ${PROJDIR}/config/config.xml ${DIR_OPC}/bin
cp -v ${PROJDIR}/config/sim_*.xml ${DIR_SIM}

#
# 7. tools: pilot and caen discovery
set SRCDIR=${PROJDIR}/simulationPilotUI/src
set FILES="simulationPilotUI"
foreach ff ( ${FILES} )
	cp -v ${SRCDIR}/${ff} ${DIR_TOOLS}
end
set SRCDIR=${PROJDIR}/discoveryCAEN/src
set FILES="discoveryCAEN"
foreach ff ( ${FILES} )
	cp -v ${SRCDIR}/${ff} ${DIR_TOOLS}
end
# CAEN hw lib as well
mkdir -p ${DIR_TOOLS}/CAEN/hwAccess/lib/
cp -rv ${SRCDIR}/CAEN/hwAccess/lib/libcaenhvwrapper.* ${DIR_TOOLS}/CAEN/hwAccess/lib/

# at last. archive & compress, including the server bin, configs, sim libs, certificates, keys
echo "===creating project bin tarball==="
cd ${PROJDIR}/release
set tarball=$1
rm -f ${tarball}
/bin/tar -cvzf ${tarball} ./*

echo "created "`pwd`/${tarball}" OK"




#!/bin/csh
#
# create a tarball VENUS.<version>.tar.gz which has everything inside needed to be run on a CCC7/scada vm standalone
# optionally: push this tarball to gitlab/VENUS and to nexus (curl rest api nexus)
#
# containing a simulation engine, glue libs, an OPC-UA.ua and OPC-UA.open6 server and
# all confiurations neccessary. Also all runnable configs for ATLAS, ALICE... are included.
# NOT called from jenkins since it pulls in artifacts from several jenkins projects
#
# pulls in jenkins tagged builds for OPC-UA.ua and OPC-UA.open6, and takes the current VENUS source tree
#
set ROOTDIR="/home/mludwig/OPCsimulation"
# set ROOTDIR="./"
set VERSION="0.9"
set TGZ="CaenVENUS."${VERSION}.tar.gz
rm -rf ${ROOTDIR}/tmp

# sim: engine
set SRCDIR="simulationHAL/src"
set FILES="simulationHAL README.txt"
set DEST=${ROOTDIR}/tmp/${VERSION}/sim
mkdir -p ${DEST}
foreach ff ( ${FILES} )
	cp -v ${ROOTDIR}/${SRCDIR}/${ff} ${DEST}
end

# lib: system libs needed for glue and engine 
set DEST=${ROOTDIR}/tmp/${VERSION}/lib
set LIBS_GLUE="/lib64/libprotobuf.so.8 /lib64/libprotoc.so.8 /lib64/libczmq.so.3 /lib64/liblzma.so.5 /lib64/libxml2.so.2 /lib64/libzmq.so.5 /lib64/libxerces-c-3.0.so  /lib64/libxerces-c-3.1.so /lib64/libprotobuf-c.so"
mkdir -p ${DEST}
foreach ff ( ${LIBS_GLUE} )
	cp -v ${ff} ${DEST}
end
set LIBS_ENGINE="/lib64/libprotobuf-c.so.1 /lib64/libczmq.so.3 /usr/local/lib/libxerces-c-3.0.so /lib64/libdl.so.2 /lib64/libstdc++.so.6 /lib64/libm.so.6 /lib64/libgcc_s.so.1 /lib64/libc.so.6 /lib64/libzmq.so.5 /lib64/libnsl.so.1 /lib64/libpthread.so.0 /lib64/libcurl.so.4 /lib64/libicuuc.so.50 /lib64/libicudata.so.50 /lib64/libsodium.so.13 /lib64/libpgm-5.2.so.0 /lib64/libgssapi_krb5.so.2 /lib64/librt.so.1 /lib64/libidn.so.11 /lib64/libssh2.so.1 /lib64/libssl3.so /lib64/libsmime3.so /lib64/libnss3.so /lib64/libnssutil3.so /lib64/libplds4.so /lib64/libplc4.so /lib64/libnspr4.so /lib64/libkrb5.so.3 /lib64/libk5crypto.so.3 /lib64/libcom_err.so.2 /lib64/liblber-2.4.so.2 /lib64/libldap-2.4.so.2 /lib64/libz.so.1 /lib64/libkrb5support.so.0 /lib64/libkeyutils.so.1 /lib64/libresolv.so.2 /lib64/libssl.so.10 /lib64/libcrypto.so.10 /lib64/libsasl2.so.3 /lib64/libselinux.so.1 /lib64/libcrypt.so.1 /lib64/libpcre.so.1 /lib64/libfreebl3.so"
mkdir -p ${DEST}
foreach ff ( ${LIBS_ENGINE} )
	cp -v ${ff} ${DEST}
end

# set symlinks for lib versions
cd ${DEST}
ln -s libprotobuf.so.8 libprotobuf.so
ln -s libprotoc.so.8 libprotoc.so
ln -s libczmq.so.3 libczmq.so
ln -s liblzma.so.5 liblzma.so
ln -s libxml2.so.2 libxml2.so
ln -s libzmq.so.5 libzmq.so
ln -s libxerces-c-3.0.so libxerces-c.so
ln -s libprotobuf-c.so.1 libprotobuf-c.so

# lib: glue & config
set SRCDIR="simulationOPCglue/src"
set DEST=${ROOTDIR}/tmp/${VERSION}/lib
set FILES="libsimcaen.so libgluecaen.so caenGlueConfig.xml README.txt"
mkdir -p ${DEST}
foreach ff ( ${FILES} )
	cp -v ${ROOTDIR}/${SRCDIR}/${ff} ${DEST}
end

#
# OPC server open6
#
set ROOTDIR2="/home/mludwig/jenkins_downloaded"
more ${ROOTDIR2}/latest.txt
set SRCDIR="caen-opcua-open6-server.TEST"
set DEST=${ROOTDIR}/tmp/${VERSION}/opc
mkdir -p ${DEST}/bin/CAEN/hwAccess/lib
mkdir -p ${DEST}/Configuration
cp -v ${ROOTDIR2}/${SRCDIR}/bin/OpcUaCaenServer ${DEST}/bin
mv ${DEST}/bin/OpcUaCaenServer ${DEST}/bin/OpcUaCaenServer.open6
# copy open6
cp -v ${ROOTDIR2}/${SRCDIR}/open62541-compat/open62541/build/libopen62541.so ${ROOTDIR}/tmp/${VERSION}/lib
# copy HW lib for real access from caen
cp -v ${ROOTDIR2}/${SRCDIR}/bin/CAEN/hwAccess/lib/libcaenhvwrapper.so.5.56 ${DEST}/bin/CAEN/hwAccess/lib/libcaenhvwrapper.so.5.56.org
#

#
# OPC server UA (only bins)
#
set ROOTDIR2="/home/mludwig/jenkins_downloaded"
set SRCDIR="caen-opcua-UA-server.TEST"
set DEST=${ROOTDIR}/tmp/${VERSION}/opc
mkdir -p ${DEST}/bin/CAEN/hwAccess/lib
mkdir -p ${DEST}/Configuration
cp -v ${ROOTDIR2}/${SRCDIR}/bin/OpcUaCaenServer ${DEST}/bin
mv ${DEST}/bin/OpcUaCaenServer ${DEST}/bin/OpcUaCaenServer.ua

#
# configs: common server, glue and s.engine configs, authentication (for docker)
#
set ROOTDIR2="/home/mludwig/OPCsimulation"
set DEST=${ROOTDIR}/tmp/${VERSION}/opc
mkdir -p ${DEST}/config
cp -v ${ROOTDIR2}/server-config/Configuration/Configuration.xsd ${DEST}/Configuration
cp -v ${ROOTDIR2}/server-config/config.127.0.0.1.xml ${DEST}/bin/config.xml
cp -v ${ROOTDIR2}/server-config/ServerConfig.xml ${DEST}/bin
cp -v ${ROOTDIR2}/scripts/killopc.sh ${DEST}/bin
cp -v ${ROOTDIR2}/simulationOPCglue/config/caenGlueConfig.127.0.0.1.xml ${DEST}/bin/caenGlueConfig.xml
cp -v ${ROOTDIR2}/config/*.xml ${DEST}/config
cp -v ${ROOTDIR2}/config/README.txt ${DEST}/config
cp -rv ${ROOTDIR2}/server-config/PKI ${DEST}/config


# copy simulation lib loader to opc
rm -f ${DEST}/bin/CAEN/hwAccess/lib/libgluecaen.so
cp -v ${ROOTDIR}/tmp/${VERSION}/lib/libgluecaen.so ${DEST}/bin/CAEN/hwAccess/lib/

# set symlink to simulation
cd ${DEST}/bin/CAEN/hwAccess/lib/
rm -f libcaenhvwrapper.so.5.56
rm -f libcaenhvwrapper.so.5.82
ln -s libgluecaen.so libcaenhvwrapper.so.5.56
ln -s libgluecaen.so libcaenhvwrapper.so.5.82 

# set symlink to make bin find the libs
cd ${DEST}/bin
ln -s ../../lib ./

# archive
mkdir -p ${ROOTDIR}/tgz
cd ${ROOTDIR}/tgz
rm -f ${TGZ}
tar -czvf ${TGZ} ${ROOTDIR}/tmp/${VERSION}

# cleanup
# rm -rf ${ROOTDIR}/tmp

echo "=== summary ==="
more /home/mludwig/jenkins_downloaded/latest.txt
echo "unpack like: tar -xhzvf "${TGZ}" --strip-components=4" 
echo "created "${ROOTDIR}/tgz/${TGZ}" OK"



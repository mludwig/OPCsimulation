#!/bin/csh
# deliver simulation framework and tools, into a public package version. 
# This script is not called by jenkins because it pulls in artifacts
# from other jenkins jobs: the OPC-UA server binaries. We dont want 
# jenkins jobs to depend between them
#
# s.engine
# glue code
# pilot UI (command line)
# OPC-UA Caen Server .ua (with license, but not delivered because of OPCUA#159)
# OPC-UA Caen Server .open6 (modified workaround to OPCUA#159)
# OPC-UA Venus Pilot Server .ua (demo license only)
# OPC-UA Venus Pilot Server .open6
# all needed config files and libraries
# 

set ROOTDEST="~/venus-combos"
set VERSION="1.0.0"

# ----
# jenkins build numbers
# simulation  engine and glue code 
# from https://ics-fd-cpp-master.web.cern.ch/view/VENUS/
set JENKINS_ARTIFACT_COUNTER_SIM=267

# CAEN OPC UA open6
set ARTIFACT_DIR_OPCCAEN_OPEN6=/home/mludwig/OPCserver/CAEN/caen-opcua-open6.11feb2019/caen-opcua-server.TEST/bin

# CAEN OPC-UA ua
set ARTIFACT_DIR_OPCCAEN_UA=/home/mludwig/OPCserver/CAEN/caen-opcua-ua.11feb2019/caen-opcua-server.PKG/bin

# VENUS OPC UA pilot server from ics-fd-cpp-master / jenkins both toolkits
set JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_OPEN6=229
set JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_UA=282

# authentication
set user=`whoami`
stty -echo 
echo -n "Enter password for user ** ${user} ** : "
set password = $<
stty echo
echo ""
echo "===================================================="
echo "enter version, directories and jenkins build numbers"
echo "===================================================="
#
echo -n "VENUSCombo version to produce [${VERSION}]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default"
else
	set VERSION = ${IS}	
	echo "VERSION=${VERSION}"
endif


#
echo -n "simulation engine & glue from https://ics-fd-cpp-master.web.cern.ch/view/VENUS/ [${JENKINS_ARTIFACT_COUNTER_SIM}]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default"
else
	set JENKINS_ARTIFACT_COUNTER_SIM = ${IS}	
	echo "JENKINS_ARTIFACT_COUNTER_SIM=${JENKINS_ARTIFACT_COUNTER_SIM}"
endif

#
echo -n "OPC-UA Caen Server .open6 [${ARTIFACT_DIR_OPCCAEN_OPEN6}]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default"
else
	set ARTIFACT_DIR_OPCCAEN_OPEN6 = ${IS}	
	echo "ARTIFACT_DIR_OPCCAEN_OPEN6=${ARTIFACT_DIR_OPCCAEN_OPEN6}"
endif

#
echo -n "OPC-UA Caen Server .ua, licensed [${ARTIFACT_DIR_OPCCAEN_UA}]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default"
else
	set ARTIFACT_DIR_OPCCAEN_UA = ${IS}	
	echo "ARTIFACT_DIR_OPCCAEN_UA=${ARTIFACT_DIR_OPCCAEN_UA}"
endif

#
echo -n "OPC-UA Venus Pilot Server (.open6) from https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/VenusOpcPilotCaen-open6.cc7/ [${JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_OPEN6}]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default"
else
	set JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_OPEN6 = ${IS}	
	echo "JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_OPEN6=${JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_OPEN6}"
endif

#
echo -n "OPC-UA Venus Pilot Server (.ua, demo) from https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/VenusOpcPilotCaen-ua.cc7/ [${JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_UA}]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default"
else
	set JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_UA = ${IS}	
	echo "JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_UA=${JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_UA}"
endif


# prepare
set MYWGET="wget --http-user=${user} --http-password=${password} --auth-no-challenge "
rm -rf ${ROOTDEST}/tmp


# =================
# simulation engine
# =================
set URL="https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/VenusCAEN/${JENKINS_ARTIFACT_COUNTER_SIM}/artifact/simulationHAL/src/simulationHAL"
set DEST=${ROOTDEST}/tmp/${VERSION}
mkdir -p ${DEST}/sim
cd ${DEST}/sim
echo "==> s.engine from ${URL} to "`pwd`
${MYWGET} ${URL}
chmod u+x ./simulationHAL


# =================
# system libs needed, take them from devel ws 
# =================
set DEST=${ROOTDEST}/tmp/${VERSION}/lib
set LIBS_SYSTEM="/lib64/libxerces-c-3.1.so /lib64/libprotobuf.so.8 /lib64/libczmq.so.3 /lib64/liblzma.so.5 /lib64/libpgm-5.2.so.0 /lib64/libprotobuf-c.so.1 /lib64/libprotoc.so.8 /lib64/libsodium.so.23 /lib64/libxml2.so.2 /lib64/libzmq.so.5"
mkdir -p ${DEST}
echo "==> set of system libs  to "`pwd`
foreach ff ( ${LIBS_SYSTEM} ) 
	cp -v ${ff} ${DEST}
end

# set symlinks for lib versions
cd ${DEST}
echo "==> setting symlinks inside "`pwd`
ln -s libprotobuf.so.8 libprotobuf.so
ln -s libprotoc.so.8 libprotoc.so
ln -s libczmq.so.3 libczmq.so
ln -s liblzma.so.5 liblzma.so
ln -s libxml2.so.2 libxml2.so
ln -s libzmq.so.5 libzmq.so
ln -s libxerces-c-3.1.so libxerces-c.so
ln -s libprotobuf-c.so.1 libprotobuf-c.so


# ========================
# libs: glue code & config
# ========================
set DEST=${ROOTDEST}/tmp/${VERSION}
cd ${DEST}/lib
set URL="https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/VenusCAEN/${JENKINS_ARTIFACT_COUNTER_SIM}/artifact/simulationOPCglue/src"
set FILES_GLUE="libgluecaen.so libsimcaen.so"
echo "==> glue code & libs to "`pwd`
foreach ff ( ${FILES_GLUE} ) 
	${MYWGET} ${URL}/${ff}
end


# =================================
# OPC-UA Caen Server .ua (licensed)
# =================================
# copy the archive/artifact by hand from Ben's gitlab pipeline/CI
# at https://gitlab.cern.ch/caen/caen-opcua-server 
# then copy the bins
set ORIGIN=${ARTIFACT_DIR_OPCCAEN_UA}
set DEST=${ROOTDEST}/tmp/${VERSION}/opc
mkdir -p ${DEST}/bin/

cd ${DEST}/bin
echo "==> OPC-UA Caen Server .ua from ${ORIGIN} to "`pwd`
cp -v ${ORIGIN}/OpcUaCaenServer ${DEST}/bin/OpcUaCaenServer.ua
cp -v ${ORIGIN}/ServerConfig.xml ${DEST}/bin/ServerConfig.xml
chmod u+x ./OpcUaCaenServer.ua
mkdir -p ${DEST}/Configuration
cp -v ${ORIGIN}/../Configuration/Configuration.xsd ${DEST}/Configuration/Configuration.xsd


# =========================
# OPC-UA Caen server .open6
# =========================
# copy the archive/artifact by hand from Ben's jenkins/gitlab
# at https://icejenkins.cern.ch/view/OPC-UA/job/caen-opcua-server-open62541/ 
# then copy the bins
#set ORIGIN=${ARTIFACT_DIR_OPCCAEN_OPEN6}
#set DEST=${ROOTDEST}/tmp/${VERSION}/opc
#cp ${ORIGIN}/OpcUaCaenServer ${DEST}/bin/OpcUaCaenServer.open6
#cd ${DEST}/lib
#set URL13=https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/OPC-UACaenServerModified.open6/${JENKINS_ARTIFACT_COUNTER_OPCCAEN_OPEN6}/artifact/open62541-compat/open62541/libopen62541.a
#echo "==> compat .open6 from ${URL13} to "`pwd`
#${MYWGET} ${URL13}


# ================================
# OPC-UA Venus Pilot Server .open6
# ================================
set DEST=${ROOTDEST}/tmp/${VERSION}/pilot/bin
set URL=https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/VenusOpcPilotCaen-open6.cc7/${JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_OPEN6}/artifact/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/bin/OpcUaVenusCaenPilotServer.open6
mkdir -p ${DEST}
cd ${DEST}
echo "==> OPC-UA Venus Pilot Server .open6 from ${URL} to "`pwd`
${MYWGET} ${URL}
chmod u+x ./OpcUaVenusCaenPilotServer.open6

# ====================================
# OPC-UA Venus Pilot Server .ua (demo)
# ====================================
set DEST=${ROOTDEST}/tmp/${VERSION}/pilot/bin
set URL=https://ics-fd-cpp-master.web.cern.ch/view/VENUS/job/VenusOpcPilotCaen-ua.cc7/${JENKINS_ARTIFACT_COUNTER_OPCVENUSPILOT_UA}/artifact/OPCsimulation/simulationPilotOpcCaen/venusCaenPilot/bin/OpcUaVenusCaenPilotServer.ua
cd ${DEST}
mkdir -p ${DEST}
echo "==> OPC-UA Venus Pilot Server .ua (demo) from ${URL} to "`pwd`
${MYWGET} ${URL}
chmod u+x ./OpcUaVenusCaenPilotServer.ua


# =========================
# configs: server .ua and .open6, glue and s.engine, authentication (for docker)
# =========================
set ORIGIN="/home/mludwig/OPCsimulation/server-config/VENUSComboConfigs"
echo "==> copy server, glue and s.engine configs"
#
# make caen opc happy
set DEST=${ROOTDEST}/tmp/${VERSION}/opc
cp -v ${ORIGIN}/Configuration.xsd ${DEST}/Configuration/Configuration.xsd
cp -v ${ORIGIN}/config.opc.xml ${DEST}/bin/config.xml
cp -v ${ORIGIN}/caenGlueConfig.127.0.0.1.xml ${DEST}/bin/caenGlueConfig.xml
cp -v ${ORIGIN}/ServerConfig.xml ${DEST}/bin/ServerConfig.xml

# make pilot opc happy
set DEST=${ROOTDEST}/tmp/${VERSION}/pilot
mkdir -p ${DEST}/Configuration
mkdir -p ${DEST}/bin
cp -v ${ORIGIN}/Configuration.pilot.xsd ${DEST}/Configuration/Configuration.xsd
cp -v ${ORIGIN}/config.pilot.xml ${DEST}/bin/config.xml
cp -v ${ORIGIN}/ServerConfig.pilot.xml ${DEST}/bin/ServerConfig.xml

# copy a few more example configs s.engine
set DEST=${ROOTDEST}/tmp/${VERSION}
cp -v ${ORIGIN}/killopc.sh ${DEST}/opc/bin
cp -v ${ORIGIN}/sim_*.xml ${DEST}/sim/
cp -v ${ORIGIN}/README.txt ${DEST}/sim/config

# copy the glue lib 
# and set symlink the aux libs needed for the bin. 
set DEST=${ROOTDEST}/tmp/${VERSION}/opc/bin
echo "==> move glue lib into ${DEST}"
cd ${DEST}
cp ../../lib/libgluecaen.so ./libcaenhvwrapper.so

echo "==> set symlink to lib into ${DEST}"
ln -s ../../lib ./

# archive the whole dir tree
set TGZ="CaenVENUS."${VERSION}.tar.gz
echo "==> packing the archive ${ROOTDEST}/tgz/${TGZ}"
mkdir -p ${ROOTDEST}/tgz
rm -f ${ROOTDEST}/tgz/${TGZ}
cd ${ROOTDEST}/tmp
tar  -czf ${ROOTDEST}/tgz/${TGZ} ./${VERSION}

# cleanup
# rm -rf ${ROOTDEST}/tmp

echo "==> OK: delivered simulation framework into "${ROOTDEST}/tgz/${TGZ}
echo "unpack like: tar -xhzvf ${TGZ}"

echo "==="
set ARCH="cc7"
set NREPO="cern-venus"
set REPO="https://repository.cern.ch/nexus/content/repositories/${NREPO}/${ARCH}/VENUSCombo/${VERSION}"
echo -n "upload combo to nexus to ${REPO} ? (y/n): "
set ANSWER = $<
if ( ${ANSWER} == "y") then
	echo "uploading to nexus"
	echo "uploading ${ROOTDEST}/tgz/${TGZ} to ${REPO}/${TGZ} (authenticate in separate xterm)"
	xterm -bg orange -e "curl -v -u mludwig --upload-file ${ROOTDEST}/tgz/${TGZ} ${REPO}/${TGZ}; sleep 10; "
else
	echo "doing nothing"
endif



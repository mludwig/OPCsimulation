#!/bin/csh
set hosts="aliceits venusaliceits venusatlasps3-0 venustrain0 venustrain1 venustrain2"
cd ~/OPCsimulation/simulationPilotUI/src
foreach h ( ${hosts} )
	echo "checking "${h}
#	./simulationPilotUI -host_engine ${h}.cern.ch getConfig
	./simulationPilotUI -host_engine ${h}.cern.ch getHealth
#	./simulationPilotUI -host_engine ${h}.cern.ch rampUpAllChannels 0 300 20 5 5 
end;

# ./simulationPilotUI -host_engine aliceits.cern.ch getConfig


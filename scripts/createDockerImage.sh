#!/bin/csh
# creating a parametrizable docker image for VENUS, which has a combo
# of simulation engine and OPC server
# from Dockerfile
echo "===first, deploy simulation to git and do a jenkins build==="
echo "===second, build a new VENUSCombo version, using assembleCombo.sh==="
echo "===third, dont forget to make Dockerfile take the latest Combo version from nexus==="
echo "===fourth, check entry-point.sh for the docker runtime==="
echo "========================================================="
echo "following commands must be executed in /home/mludwig/OPCsimulation/docker: "
echo "touch ./Dockerfile"
echo "docker build ."
echo "# when finished, tag the new image i.e. <abcde>"
echo "docker tag <abcde> gitlab-registry.cern.ch/mludwig/opcsimulation"
echo "# then push tag to repo: [ login before: docker login gitlab-registry.cern.ch ]"
echo "docker push gitlab-registry.cern.ch/mludwig/opcsimulation"
echo " and also for keeping the versions:"
echo "  docker push gitlab-registry.cern.ch/mludwig/opcsimulation:0.9.4"
echo " " 
echo "then restart the VNUSJenkins job accordingly, and connect a client"

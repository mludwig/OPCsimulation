#!/bin/csh
# run a VENUS combo locally in a docker:
#    OPC-UA CAEN .ua server
#    s.engine with a configured electronic tree
#    OPC-UA VENUS pilot server
# we need:
#    a docker image/tag
#    a electronic tree config file for the s.engine
#
#
# --------
# defaults
# --------
set DOCKER_TAG="1.0.0"
# possible values are all delivered configs inside the combo, we'll take a simple one
# set SIMTREE="sim_CAENREF.short.xml"
set SIMTREE="sim_BASIC.short.xml"
#
#

# ----------
# user input
# ----------
echo "run a VENUS combo in a local docker - having fun with simulated CAEN power supplies"
echo -n "[ gitlab-registry.cern.ch/mludwig/opcsimulation ] DockerTag [ ${DOCKER_TAG} ]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default "${DOCKER_TAG}
else
	set DOCKER_TAG = ${IS}	
	echo "DOCKER_TAG=${DOCKER_TAG}"
endif
echo -n "electronic tree configuration file [ ${SIMTREE} ]: "
set IS=$<
if ( ${IS} == "" ) then
	echo "keep default "${SIMTREE}
else
	set SIMTREE = ${IS}	
	echo "SIMTREE=${SIMTREE}"
endif
#
#
# internal defaults for running the venus combo
set OPC_CONFIG="--hardwareDiscoveryMode"
set SIM_CONFIG="-shorthand -cfg ${SIMTREE}"
set GLUE_CONFIG="caenGlueConfig.xml"
#
echo "pulling docker image..."
docker pull gitlab-registry.cern.ch/mludwig/opcsimulation:${DOCKER_TAG}
echo "OK: pulled docker image. Stopping any running opcsimulation containers if there are..."
docker rm -f opcsimulation || true
#echo "OK: running containers are stopped. Removing dangling images..."
#docker rmi $( docker images -q -f dangling=true) || true
#echo "OK: removed dangling images. Starting docker..." 
echo "OK: Starting docker..." 
echo "docker run -d --name opcsimulation --net=host --expose=4840-4940 --expose=8800-8900 --env OPCCONFIG="${OPC_CONFIG}" --env SIMCONFIG="${SIM_CONFIG}" --env GLUECONFIG="${GLUE_CONFIG}" gitlab-registry.cern.ch/mludwig/opcsimulation:${DOCKER_TAG}"
docker run -d --name opcsimulation --net=host --expose=4840-4940 --expose=8800-8900 --env OPCCONFIG="${OPC_CONFIG}" --env SIMCONFIG="${SIM_CONFIG}" --env GLUECONFIG="${GLUE_CONFIG}" gitlab-registry.cern.ch/mludwig/opcsimulation:${DOCKER_TAG}



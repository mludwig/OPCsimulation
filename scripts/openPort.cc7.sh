#!/bin/csh
# for running the simulation engine we need glue ports and pilot ports open
# on that host. This is the communication between the glue/OPC server and 
# the simulation engine, and s.engine talking to pilot UI.
# cern@cc7 does not have iptables, there is a specific firewall tool
 
# on the station running the OPC server the official OPC port 4841
# must be open to get client connections
firewall-cmd --get-active-zones
sudo firewall-cmd --zone=public --permanent --add-port=4841/tcp
sudo firewall-cmd --zone=public --permanent --add-port=4901/tcp
#
# 8880 communicates between the OPCserver&glue code and the s.engine
# the s.engine can be on a different host if 8880 is open
sudo firewall-cmd --zone=public --permanent --add-port=8880/tcp
#
# 8881 communicates between the s.engine and the pilot UI
# if it is open the polit is a client which can connect from anywhere, 
# as long as the IP of the s.engine is specified
sudo firewall-cmd --zone=public --permanent --add-port=8881/tcp
sudo firewall-cmd --reload

# 8890 for the cenus OPC caen pilot server, which cominucates directly with the s.engine
sudo firewall-cmd --zone=public --permanent --add-port=8890/tcp
sudo firewall-cmd --reload


#and also
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/mludwig/simulation/caen/v0.4/lib

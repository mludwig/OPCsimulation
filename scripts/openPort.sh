#!/bin/csh
# 1. for running the simulation engine we need glue ports and pilot ports open
# on that host. This is the communication between the glue/OPC server and the simulation engine.
# 
# 2. on the station running the OPC server the official OPC port 4841
# must be open
rpm -q iptables
set GLUE_PORTS="50000 50010 50020 50030 50040 50050"
set PILOT_PORTS="50001 50011 50021 50031 50041 50051"
foreach p ( ${GLUE_PORTS} )
	echo "testing if GLUE port "${p}" is open"
	sudo netstat -tulpn | grep ${p}
end
foreach p ( ${PILOT_PORTS} )
	echo "testing if PILOT port "${p}" is open"
	sudo netstat -tulpn | grep ${p}
end
set OPC_PORT="4841"
echo "testing if OPC port "${OPC_PORT}" is open"
sudo netstat -tulpn | grep ${OPC_PORT}


# cc7
echo "to open a port in CC7:"
echo "sudo iptables --append INPUT --source 0/0 -p tcp --dport <port-to-open> --jump ACCEPT"

# slc6
echo "to open a port in SLC6:"
echo "sudo iptables --append INPUT --source 0/0 --protocol tcp --destination-port <port-to-open> --jump ACCEPT"


#!/bin/csh
# upload CAEN discovered hardware xml files from directory discoveryCAEN/discoveredCAENHardware to nexus
# https://support.sonatype.com/hc/en-us/articles/213465818-How-can-I-programatically-upload-an-artifact-into-Nexus-
stty -echo 
echo -n "Enter "`whoami`" password: "
set password = $<
stty echo
#
set VENDOR="caen"
set NREPO="cern-venus"
set SDIR="~/OPCsimulation/discoveryCAEN/discoveredCAENHardware"
set REPO="https://repository.cern.ch/nexus/content/repositories/"${NREPO}"/"${VENDOR}"/discoveredCAENHardware"
cd ${SDIR}
foreach s ( `ls *.xml.txt` ) 
	echo "uploading  "${s}" to " ${REPO}
	xterm -e "curl -v -u mludwig:${password} --upload-file ${s} ${REPO}/${s}; "
end;




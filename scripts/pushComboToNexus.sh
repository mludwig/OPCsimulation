#!/bin/csh
# upload venus tarball to nexus
# https://support.sonatype.com/hc/en-us/articles/213465818-How-can-I-programatically-upload-an-artifact-into-Nexus-
set VERSION="test"
set ARCH="cc7"
set NREPO="cern-venus"
set TGZ="CaenVENUS."${VERSION}".tar.gz"
set INSTALLER="installCaenVENUS."${VERSION}".sh"
#
set SDIR="~/OPCsimulation/tgz"
set REPO="https://repository.cern.ch/nexus/content/repositories/"${NREPO}"/"${ARCH}"/VENUS_CAEN.TEST/"${VERSION}
set SOURCE=${SDIR}/${TGZ}
set TARGET=${REPO}/${TGZ}
set SDIR2="~/OPCsimulation/scripts"
set SOURCE2=${SDIR2}/${INSTALLER}
set TARGET2=${REPO}/${INSTALLER}
xterm -e "curl -v -u mludwig --upload-file ${SOURCE} ${TARGET}; curl -v -u mludwig --upload-file ${SOURCE2} ${TARGET2}; sleep 10; "
#
#curl -v -u mludwig --upload-file /home/mludwig/OPCsimulation/tgz/CaenVENUS.0.9.tar.gz https://repository.cern.ch/nexus/content/repositories/cern-venus/cc7/VENUS_CAEN.TEST/0.9/CaenVENUS.0.9.tar.gz



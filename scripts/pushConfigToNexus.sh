#!/bin/csh
# upload venus configurations sim_* and opc_* to nexus
# https://support.sonatype.com/hc/en-us/articles/213465818-How-can-I-programatically-upload-an-artifact-into-Nexus-
set VERSION="devel"
set ARCH="cc7"
set NREPO="cern-venus"
#
set SDIR="~/OPCsimulation/config"
set REPO="https://repository.cern.ch/nexus/content/repositories/"${NREPO}"/"${ARCH}"/VENUSCombo/"${VERSION}
set SOURCES="sim_ALICE.ITS5.xml opc_ALICE.ITS5.xml"

foreach s ( ${SOURCES} ) 
	echo "sources "${SDIR}/${s}
	xterm -e "curl -v -u mludwig --upload-file ${SDIR}/${s} ${REPO}/${s}; "
end;
#
#curl -v -u mludwig --upload-file /home/mludwig/OPCsimulation/tgz/CaenVENUS.0.9.tar.gz https://repository.cern.ch/nexus/content/repositories/cern-venus/cc7/VENUS_CAEN.TEST/0.9/CaenVENUS.0.9.tar.gz



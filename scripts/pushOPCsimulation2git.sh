#!/bin/csh
# git push project OPCsimulation (from root dir) to private gitlab
pwd
cd $1
# make clean

git status
find ./ -name "CMakeCache.txt" -exec rm {} \;
find ./ -name "CMakeFiles" -exec rm -rf {} \;
find ./ -name "doc-html" -exec rm -rvf {} \;
find ./ -name "html" -exec rm -rvf {} \;
rm -rf CMakeFiles
rm -rf latex
#
# force CHANGELOG.md editing
gedit ./CHANGELOG.md
#
# come up with a message for git
set MSG1=`date`" eclipse-push from "`whoami`"@"`hostname`
echo ${MSG1}
rm -f ./tmp_msg.txt
echo ${MSG1}"\n" > ./tmp_msg.txt
gedit ./tmp_msg.txt
set MSG=`cat ./tmp_msg.txt`

git add --all
git commit -m "${MSG}"
git push origin master




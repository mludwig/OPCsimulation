#!/bin/csh
# strip an xml config file for the simulation engine, which contains 
# * board type and board description
# * channel model
# down to a format the opc will handle: drop type, description and model attributes.
#
# before:
#   <Board name="sim_A1676" slot="11" type="A1676" description="CAEN Branch Controller     ">
#	<Channel name="Chan104" index="104" model="CAEN Easy Channel A3535AP">
# after:
#   <Board name="sim_A1676" slot="11">
#	<Channel name="Chan104" index="104">
set out=$1.opc.xml
set tmp1=$1.tmp1
set tmp2=$1.tmp2
set tmp3=$1.tmp3
set tmp4=$1.tmp4

rm -f ${out}; touch ${out}
rm -f ${tmp1}; touch ${tmp1}
rm -f ${tmp2}; touch ${tmp2}
rm -f ${tmp3}; touch ${tmp3}
rm -f ${tmp4}; touch ${tmp4}

cat -n $1.xml > ${tmp1}
cat -n $1.xml | grep "<Channel" | awk '{ printf("%s %s %s %s>\n", $1, $2, $3, $4) }' > ${tmp2}

echo "tmp "${tmp1}" "${tmp2}
# sift out the line numbers
cat ${tmp2} | awk '{ printf( "%6.6s\n", $1) }' > ${tmp3}

# eliminate these lines from tmp1, therefore removing the Channel xml opening tags
foreach li ( `cat ${tmp3}` )
    echo "cleaning up line= "${li} # " from "${tmp1}
    cat ${tmp1} | grep -v " ${li}" > ${tmp4}
    mv ${tmp4} ${tmp1}
end

# concatenate together, sort by line numbers and eliminate line numbers: drop first column (which was the sort key)
#cat ${tmp1} ${tmp2} | sort -n --key=1 > ${out}
cat ${tmp1} ${tmp2} | sort -n --key=1 | awk '{$1=""}1' > ${out}


rm -f ${tmp1}; touch ${tmp1}
rm -f ${tmp2}; touch ${tmp2}

# must remove leading blank otherwise xerces is unhappy
cat ${out} | grep -v "xml version" > ${tmp1}
head -1 $1.xml > ${tmp2}
cat  ${tmp2}  ${tmp1} > ${out}
echo "written stripped config for OPC server to "${out}


# cleanup
rm -f ${tmp1}
rm -f ${tmp2}
rm -f ${tmp3}
rm -f ${tmp4}

#!/bin/csh
# strip an xml config file for the simulation engine, which contains 
# * board type and board description
# * channel model
# down to a format the opc will handle: drop type, description and model attributes.
#
# before:
#   <Board name="sim_A1676" slot="11" type="A1676" description="CAEN Branch Controller     ">
#	<Channel name="Chan104" index="104" model="CAEN Easy Channel A3535AP">
# after:
#   <Board name="sim_A1676" slot="11">
#	<Channel name="Chan104" index="104">
# 
# atlas dpl has many boards on the same slot, this might be a pb from the winnccoa dump
# anyway this is ignored by the simulator: they are treated as one single board.
# In the lab CAEN discovery, BCs show up as boards with many channels, with channel00 special.
# The winccoa dump shows the BC as many channels of the same board, in a "channel orientated" 
# manner. Both descriptions are not compatible...we have a mess...
# 
# We adopt the electronic hierarchical description like in the lab.
# The temporary fix is to make sure that no two boards are on the same channel
# by correcting the slots from the atlas dpl config.


rm -f ./x1.xml
cat $1  | sed 's/model=\"CAEN Easy Channel A3100\"//g' \
	| sed 's/model=\"CAEN Easy Channel A3050D\"//g' \
	| sed 's/model=\"CAEN Easy Channel A3025D\"//g' \
	| sed 's/model=\"CAEN Easy Channel A3050D\"//g' \
	| sed 's/model=\"CAEN Easy Channel A3035AP\"//g' \
	| sed 's/model=\"CAEN Easy Channel A3535AP\"//g' \
	| sed 's/model=\"CAEN Easy Channel A3100B\"//g' \
	| sed 's/description=\"N\/A\"//g' \
	| sed 's/type=\"\"//g' \
	> x1.xml


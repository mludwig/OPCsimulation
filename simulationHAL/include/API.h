/*
 * API.h
 *
 *  Created on: Nov 11, 2016
 *      Author: mludwig
 *
 *      define formats for CAEN properties, other vendor data formats etc etc and HAL formats (which are
 *      more general. And of course the neccessary converters both ways. Does not store data itself.
 */

#ifndef SRC_API_H_
#define SRC_API_H_

#include <stdint.h>
#include <math.h>
#include <string>
#include <iostream>
#include <cstdlib>
#include <map>


using namespace std;


namespace ns_HAL_API {

class API {
public:
	API(){
		cout << __FILE__ << " " << __LINE__ << " don't call this constructor... it is not needed" << endl;
		exit(0);
	};
	virtual ~API(){};
	/**
#define SYSPROP_TYPE_STR            0
#define SYSPROP_TYPE_REAL           1
#define SYSPROP_TYPE_UINT2          2
#define SYSPROP_TYPE_UINT4          3
#define SYSPROP_TYPE_INT2           4
#define SYSPROP_TYPE_INT4           5
#define SYSPROP_TYPE_BOOLEAN        6

#define SYSPROP_MODE_RDONLY         0
#define SYSPROP_MODE_WRONLY         1
#define SYSPROP_MODE_RDWR           2
**/

	// CAEN specific
	//#define PARAM_MODE_RDONLY           0
	//#define PARAM_MODE_WRONLY           1
	//#define PARAM_MODE_RDWR             2
	typedef enum { ReadOnlyProperty=0, WriteOnlyProperty,  ReadWriteProperty } PACCESS_TYPE_t;
	typedef enum { S=0, F, U2, U4, I2, I, B, unused } PDATA_TYPE_t;
	typedef struct {
		PACCESS_TYPE_t pAccessType;
		PDATA_TYPE_t pDataType;
		string name;
		string pname;
	} CAEN_PROPERTY_t;


	// HAL general
	// attributed properties for CAEN data types numeric, onoff... etc
	typedef struct {
		// numeric
		float minval; // also for enum
		float maxval; // also for enum
		uint16_t unit;
		int16_t exp;
		uint16_t decimal; // for channels
		// onoff
		string onstate;   // for easy boards (which are channels in the BC)  this is the board model
		string offstate;  // for easy boards (which are channels in the BC)  this is the firmware version hopefully
		/// \todo CAEN weirdo syntax for EASY, discover {board, firmware} properly
	} ATTRIBUTE_PROPERTIES_t;


	// typedef enum { tstr=0, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t; // same order as PDATA_TYPE_t !
	// 15.aug.2018 ml
	typedef enum { tstr=0, treal, tuint16, tuint32, tint16, tint32, tuint8, tint8, unknown } HAL_DATA_TYPE_t; // same order as PDATA_TYPE_t !
	typedef PACCESS_TYPE_t HAL_ACCESS_MODE_t ;
	typedef struct {
		HAL_ACCESS_MODE_t accessMode;
		HAL_DATA_TYPE_t dataType;
		string description;
		string name; // of the property
		bool subscribed;

		// storage of the values as well
		string value_string;
	    int8_t value_int8;
	    uint8_t value_uint8;
	    int16_t value_int16;
	    uint16_t value_uint16;
	    int32_t value_int32;
	    uint32_t value_uint32;
	    float value_float;

		// storage of all previous values, in order to decide
	    // if they have changed enough since the last call to trigger an update.
	    // this permits to implement event mode items "whenever sth has changed", in a slightly
	    // more sophisticated manner. By primitive default, every call to set/get a parameter changes something
	    // of course, because if the call isn't even made nothing has the possibility to change.
	    // Right. this eats quite some extra mem and CPU, but I am also convinced that this
	    // luxurious way of deciding "when values have changed enough" is needed here, since the
	    // true behavior of the electronics is sometimes quite far from their specification. There
	    // are hidden hysteresis, extra noise etc etc, which creates a difference between the behavior of the
	    // real electronics and the "programming==mathematical model".
		string previous_string;
	    int8_t previous_int8;
	    uint8_t previous_uint8;
	    int16_t previous_int16;
	    uint16_t previous_uint16;
	    int32_t previous_int32;
	    uint32_t previous_uint32;
	    float previous_float;

	    ATTRIBUTE_PROPERTIES_t attributeProperties;
	} HAL_PROPERTY_t;

	static HAL_PROPERTY_t convertPropCAEN2HAL( CAEN_PROPERTY_t c );
	static bool changedValue( std::map<string,API::HAL_PROPERTY_t>::iterator it );

	static void getPropValue( HAL_PROPERTY_t h, string *v ){ *v = h.value_string; }
	static void getPropValue( HAL_PROPERTY_t h, int8_t *v ){ *v = h.value_int8; }
	static void getPropValue( HAL_PROPERTY_t h, int16_t *v ){ *v = h.value_int16; }
	static void getPropValue( HAL_PROPERTY_t h, int32_t *v ){ *v = h.value_int32; }
	static void getPropValue( HAL_PROPERTY_t h, uint8_t *v ){ *v = h.value_uint8; }
	static void getPropValue( HAL_PROPERTY_t h, uint16_t *v ){ *v = h.value_uint16; }
	static void getPropValue( HAL_PROPERTY_t h, uint32_t *v ){ *v = h.value_uint32; }
	static void getPropValue( HAL_PROPERTY_t h, float *v ){ *v = h.value_float;	}

	// how do they get [populated ? quite unclear at the moment.
	// Start populating the Onstate = board model for easy boards, which are channels of BCs
	static string getPropAuxOnstate( HAL_PROPERTY_t h ){ return( h.attributeProperties.onstate ); }
	static string getPropAuxOffstate( HAL_PROPERTY_t h ){ return( h.attributeProperties.offstate ); }
	static float getPropAuxMinvalue( HAL_PROPERTY_t h ){ return( h.attributeProperties.minval ); }
	static float getPropAuxMaxvalue( HAL_PROPERTY_t h ){ return( h.attributeProperties.maxval ); }
	static uint16_t getPropAuxUnit( HAL_PROPERTY_t h ){ return( h.attributeProperties.unit ); }
	static int16_t getPropAuxExp( HAL_PROPERTY_t h ){ return( h.attributeProperties.exp ); }
	static uint16_t getPropAuxDecimal( HAL_PROPERTY_t h ){ return( h.attributeProperties.decimal ); }
};
} // ns_HAL_API

#endif /* SRC_API_H_ */

/*
 * Channel.h
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */

#ifndef SRC_CHANNEL_H_
#define SRC_CHANNEL_H_

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <map>

#include <stdint.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
// #include <string>
#include <string.h>
#include <stdio.h>

#include "API.h"

using namespace ns_HAL_API;

using namespace std;


namespace ns_HAL_Channel {
#define CHANNEL_STATUS_BIT0 0x1
#define CHANNEL_STATUS_BIT1 0x2
#define CHANNEL_STATUS_BIT2 0x4
#define CHANNEL_STATUS_BIT3 0x8
#define CHANNEL_STATUS_BIT4 0x10
#define CHANNEL_STATUS_BIT5 0x20
#define CHANNEL_STATUS_BIT6 0x40
#define CHANNEL_STATUS_BIT7 0x80
#define CHANNEL_STATUS_BIT8 0x100
#define CHANNEL_STATUS_BIT9 0x200
#define CHANNEL_STATUS_BIT10 0x400
#define CHANNEL_STATUS_BIT11 0x800
#define CHANNEL_STATUS_BIT12 0x1000
#define CHANNEL_STATUS_BIT13 0x2000
#define CHANNEL_STATUS_BIT14 0x4000
#define CHANNEL_STATUS_BIT15 0x8000
#define CHANNEL_STATUS_BITXX 0x8fff // unused bits 16..31


#define CAEN_MAX_RAMP_SPEED 500.0

/// \todo implement tripping propagation on channel/crate...
/// \todo merge constructors for channels/b.c agian
class Channel {
public:
	typedef enum { primaryPSchannelWithVSELandISEL, primaryPSchannelNoVSEL, easyBoardMarkerChannel,
		easyPSchannelNoVSELv1, easyPSchannelNoVSELv2, easyPSchannelWithVSELnoISEL  } CHANNEL_TYPES_t;

	// Channel( CHANNEL_TYPES_t type, uint32_t ch, float hwvmax, float hwimax, string name  );
	Channel( uint32_t icrate, uint32_t islot, CHANNEL_TYPES_t type, uint32_t ch, float hwvmax, float hwimax, string name  );

	virtual ~Channel();
	string key( void ){ return(_key);}
	static string convert_type2model( CHANNEL_TYPES_t type );


	typedef enum { state_OFF, state_rampingUp, state_rampingDown, state_ON, state_ConstantCurrent, state_PendingTrip } SM_states_t;
	void sm( void );
	SM_states_t smState( void ){	return( _SMstate );	}

	typedef enum { V_NOMINAL, OVERVOLTAGE, UNDERVOLTAGE } ChannelVoltageCondition_t;
	typedef enum { I_NOMINAL, OVERCURRENT } ChannelCurrentCondition_t;

	// easyBoardChanel000: channel does not have a state machine (easy00),
	// simple: no ramps no trips,
	// complex: with ramps and trips
	typedef enum { easyBoardMarkerChannelSM, simpleSM, complexSM, noSM } ChannelStateMachineConfig_t;
	void configureSM ( ChannelStateMachineConfig_t c ){	_smBehaviour = c;}

	// pilot API, simulation control, and also some simulated front panel inputs
	void pilot_setNoiseLevelVmon( float s ){ if ( s > 0 ) _noiseLevelVmon = s; } // V
	void pilot_setNoiseLevelImon( float s ){ if ( s > 0 ) _noiseLevelImon = s; } // uA
	void pilot_setsimpleStateMachine( bool s ) { configureSM( simpleSM ); }
	void pilot_setVtargetFromVSEL( bool f );
	void pilot_setImaxFromISEL( bool f );
	void pilot_setLoad( float res, float capa );
	void pilot_getLoad( float *res, float *capa );

	// API with some logic behind
	void set_Vramp_up( float s );
	void set_Vramp_down( float s );
	void set_TripTime( int32_t s );
	map<string,API::HAL_PROPERTY_t> allChangedItems( void ){ return ( _allChangedItems() );	}

	// standard API without logic, just containers
	void setName( string s );
	string name( void ){ return( _name ); }
	uint32_t myCrate( void ){ return ( _my_icrate ); }
	uint32_t mySlot( void ){ return ( _my_islot ); }
	uint32_t channel( void ){ return( _channel ); }
	vector<std::string> showProperties( void );
	void subscribeParameter( string param, bool flag );


	// caen API
	vector<string> parameterList( void );
	uint32_t parameterAccessMode( string p ){ return( ( uint32_t ) _prop_map.at( p ).accessMode ); }
	uint32_t parameterDataType( string p ){ return( ( uint32_t ) _prop_map.at( p ).dataType ); }
	void getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type );
	void getParameterValue( string p, string *retval );
	void getParameterValue( string p, int8_t *retval );
	void getParameterValue( string p, int16_t *retval );
	void getParameterValue( string p, int32_t *retval );
	void getParameterValue( string p, uint8_t *retval );
	void getParameterValue( string p, uint16_t *retval );
	void getParameterValue( string p, uint32_t *retval );
	void getParameterValue( string p, float *retval );
	void getParameterValue( string p, bool *retval );

	string parameterOnstate( string p );
	string parameterOffstate( string p );
	float parameterMinval( string p );
	float parameterMaxval( string p );
	uint16_t parameterUnit( string p );
	int16_t parameterExp( string p );
	uint16_t parameterDecimal( string p );

	void setParameterValue( string p, string val );
	void setParameterValue( string p, int8_t val );
	void setParameterValue( string p, int16_t val );
	void setParameterValue( string p, int32_t val );
	void setParameterValue( string p, uint8_t val );
	void setParameterValue( string p, uint16_t val );
	void setParameterValue( string p, uint32_t val );
	void setParameterValue( string p, float val );

	void setParameterOnstate( string p, string val );
	void setParameterOffstate( string p, string val );

	void setDebug( bool f ){ _debug = f; };
	bool isEasyBoardMarkerChannel( void) { return( _isEasyBoardMarkerChannel );}
	bool isEasyPowerConverterChannel( void) { return( _isEasyPowerConverterChannel );}
	int channelType( void ) { return( _channelType ); }

private:
	bool _debug;
	string _key;
	string _description;
	uint32_t _my_icrate;
	uint32_t _my_islot;
	uint32_t _channel;
	float _HWVmax;   // hardware limits by design
	float _HWImax;
	string _name;
	bool _isEasyBoardMarkerChannel;
	bool _isEasyChannel;
	bool _isEasyPowerConverterChannel;
	CHANNEL_TYPES_t _channelType;
	float _V0set;
	float _V1set;
	float _I0set;
	float _I1set;
	uint32_t _TripInt;
	uint32_t _TripExt;
	uint32_t _Trip;

	float _SVmax;    // programmable limit, by SW
	float _Vramp_up; // ramp speed V/s
	float _Vramp_down;
	ChannelVoltageCondition_t _vcondition;
	ChannelCurrentCondition_t _icondition;
	int32_t _Pw;    // onoff = bool
	int32_t _Pon;   // onoff = bool
	int32_t _PDwn;  // onoff = bool
	int32_t _GlbOffEn;
	int32_t _GlbOnEn;
	float _VCon;
	int32_t _OutReg;
	float _Vmon;    // monitor output voltage
	float _Imon;    // monitor output current
	int32_t _tripIntegrationTime; // Trip

	// internal object control
	float __resistance;        // in Ohm, can be set
	float __capacitance;       // in Farad, can be set
	float __tripInfinite;      // can hardcode this
	float __precisionVmonRamp;                 // ramp hysteresis
	float __precisionVmonTargetOn;             // plateau stickiness
	float __precisionVtargetOffsetFactor;      // V target is always 0.8% lower than specified: real electronics
	float __precisionVtargetOffsetLowValue;    // V target is always at least 2V lower than specified: real electronics
	float __precisionVtargetZeroValue;         // V target with Pw==off
	bool __powerOnOption; /// \todo implement power-on persistency settings (complicated!)
	float __Vtarget;      // the setting stems from either V0 or V1 (I0 and I1), depending on _FP_VSEL of the crate,
	float __Imax;	      // but this is the actual current setting which is used in the state machine
	bool __VSEL;
	bool __ISEL;

	float _tempMarkerChannelEasyBoard; // temperature of th marker channel of an easy board

	// typedef enum { OFF, RUP, RDWN, OVC, OVV, UNV, EXTTRIP, INTTRIP, EXT_DIS } ChannelMessages_t; // returned by controller, depending on channel status
	int32_t _status;

	// channel state machine
	ChannelStateMachineConfig_t _smBehaviour;
	SM_states_t _SMstate;
	SM_states_t _SMstate_previous;
	struct timeval _now;
	struct timeval _tramp;
	struct timeval _ttrip1;
	struct timeval _ttrip2;
	struct timezone _tz;
	bool _pendingTripStarted;
	bool _tripTriggered;  // one time switch
	bool _hasTripped;     // status
	bool _constantCurrent;
	float _noiseLevelVmon;
	float _noiseLevelImon;
	int _symmetricNoiseCounter;
	float _noiseHalfAmplitudeImon;
	float _noiseHalfAmplitudeVmon;

	void _refreshStatus( void );
	void _refreshPower( void );
	void _refreshVoltageCondition( void );
	void _refreshCurrentCondition( void );
	void _refresh_VSEL( void ){ this->pilot_setVtargetFromVSEL( __VSEL );}
	void _refresh_ISEL( void ){ this->pilot_setImaxFromISEL( __ISEL );}
	void _refresh_SVMax( void );

	void _updateControlInterface( void );
	void _updateResultInterface( void );
	void _updateChannel00easyBoard( void );

	map<string,API::CAEN_PROPERTY_t> _caenProp_map;
	map<string,API::HAL_PROPERTY_t> _prop_map;
	void _propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t );
	void _propinsert( API::HAL_PROPERTY_t h ) { _prop_map.insert( std::pair<string,API::HAL_PROPERTY_t>( h.name, h ));}
	bool _testPropExist( string p );
	bool _testPropNotExist( string p );

	void _insertProperties_primaryPSchannelWithVSELandISEL( void );
	void _setValues_primaryPSchannelWithVSELandISEL( void );

	void _insertProperties_primaryPSchannelNoVSEL( void );
	void _setValues_primaryPSchannelNoVSEL( void );

	void _insertProperties_easyPSchannelNoVSELv1( void );
	void _setValues_easyPSchannelNoVSELv1( void );

	void _insertProperties_easyPSchannelNoVSELv2( void );
	void _setValues_easyPSchannelNoVSELv2( void );

	void _insertProperties_easyPSchannelWithVSELnoISEL( void );
	void _setValues_easyPSchannelWithVSELnoISEL( void );

	void _insertProperties_easyBoardMarkerChannel( void );
	void _setValues_easyBoardMarkerChannel( void );

	bool _changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it );
	map<string,API::HAL_PROPERTY_t> _allChangedItems( void );
};

} /* namespace ns_HAL_Channel */

#endif /* SRC_CHANNEL_H_ */

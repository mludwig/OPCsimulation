/*
 * Configuration.h
 *
 *  Created on: Oct 21, 2016
 *      Author: mludwig
 */

#ifndef SRC_CONFIGURATION_H_
#define SRC_CONFIGURATION_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdexcept>
#include <map>
#include <vector>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "Crate.h"
#include "Board.h"
#include "Channel.h"

using namespace xercesc;
using namespace std;
using namespace ns_HAL_Crate;
using namespace ns_HAL_Board;
using namespace ns_HAL_Channel;

namespace ns_HAL_Configuration {

class Configuration;

class APIConfig {
public:
	APIConfig(){};
	virtual ~APIConfig(){};

	typedef enum { ReadOnlyProperty, ReadWriteProperty, WriteOnlyProperty } PACCESS_TYPE_t;
	typedef enum { S, I, F } PDATA_TYPE_t;
	typedef struct {
		PACCESS_TYPE_t pAccessType;
		PDATA_TYPE_t pDataType;
		string name;
		string pname;
	} PROPERTY_t;

private:

};





class ChannelConfig : APIConfig {
public:
	typedef enum {
		generic = 0, // parametrizable to fit board specs, used for normal channels
		A1676A00,    // branchController channel 00, looks like a board A1676A=BC , for BW compatibility in the code.
		/// \todo cleanup A1676A00

		A3484,  // EASY 3 phase power supply 48V
		A3485,  // EASY 3 phase power supply 48V
		A3486,  // EASY 3 phase power supply 48V
		A3486A, // EASY 2 channel 3 phase power supply 48V
		A3006,  // EASY  power
		A3009,  // EASY  power
		A3016,  // EASY  power
		A3050,  // EASY  power
		A3100,  // EASY  power
		A3501,  // EASY  power
		A3512,  // EASY  power
		A3535,  // EASY  power
		A3540,  // EASY  power

		A3801,  // EASY ADC not implemented
		A3801A, // EASY temp not implemented
		A3802, // EASY DAC not implemented

		/**shorthand support, done in a weirdo way. This is because
		 * presently I want to support both shorthand and explicit OPC-UA configs
		 * typedef enum { ...  } CHANNEL_TYPES_t;
		 */
		config_easyBoardMarkerChannel,
		config_standardHighVoltChannel,
		config_standardLowVoltChannel,
		// config_easyLowVoltChannel, some easy boards low volt are in fact different !! w.t.f.
		config_easyLowVoltChannelv1,
		config_easyLowVoltChannelv2,
		config_easyHighVoltChannel
	} ConfigChannelType_t;

	ChannelConfig( uint32_t i, string n, string k, string m ){
		_index = i;
		_name = n;
		_key = k;
		_model = m;
		_type = _convertChModel2ChType( _model );
		_easyBoardModel = "(not an easy channel)";
	};
	ChannelConfig( uint32_t i, string n, string k, string m, string easyBoardModel ){
		_index = i;
		_name = n;
		_key = k;
		_model = m;
		_type = _convertChModel2ChType( _model );
		_easyBoardModel = easyBoardModel;
	};
	virtual ~ChannelConfig(){};
	uint32_t index( void ){ return( _index ); }
	uint32_t channel( void ){ return( _index ); }
	void setChannel( uint32_t cc ){ _index = cc; }
	string name( void ){ return( _name ); }
	string model( void ){ return( _model ); }
	int type( void ){ return( _type ); }
	string key( void ){ return( _key ); } // crate=X slot=Y channel=Z. This is an alternate key for the map
	bool propertyExists( string pname );
	std::map<string,PROPERTY_t> channelAPI_map;
	string easyBoardModel( void ){ return( _easyBoardModel ); };

private:
	uint32_t _index;
	string _name;
	string _key; // unique with global scope
	string _model;
	int _type;
	string _easyBoardModel;

	// channel model string to channel enum type conversion
	static int _convertChModel2ChType( string model ){
		if ( model == "generic") return ChannelConfig::generic;
		if ( model == "A1676A00") return ChannelConfig::A1676A00;
		if ( model == "A3006") return ChannelConfig::A3006;
		if ( model == "A3009") return ChannelConfig::A3009;
		if ( model == "A3016") return ChannelConfig::A3016;
		if ( model == "A3050") return ChannelConfig::A3050;
		if ( model == "A3100") return ChannelConfig::A3100;
		if ( model == "A3486") return ChannelConfig::A3486;
		if ( model == "A3486A") return ChannelConfig::A3486A;
		if ( model == "A3501") return ChannelConfig::A3501;
		if ( model == "A3512") return ChannelConfig::A3512;
		if ( model == "A3535") return ChannelConfig::A3535;
		if ( model == "A3540") return ChannelConfig::A3540;

		// shorthand support, done in a weirdo way. These are behaviour types of easy boards (=BC channels), not
		// easy board models. Many models can have the same behavior. This should go into a
		// switch for behaviour, or better even, a per-board config which includes this
		if ( model == "standardHighVoltChannel") return ChannelConfig::config_standardHighVoltChannel;
		if ( model == "easyBoardMarkerChannel") return ChannelConfig::config_easyBoardMarkerChannel;

		 //if ( model == "easyLowVoltChannel")   return ChannelConfig::config_easyLowVoltChannel;
		if ( model == "easyLowVoltChannelv1") return ChannelConfig::config_easyLowVoltChannelv1;
		if ( model == "easyLowVoltChannelv2") return ChannelConfig::config_easyLowVoltChannelv2;
		if ( model == "easyHighVoltChannel") return ChannelConfig::config_easyHighVoltChannel;
		return ChannelConfig::generic;
	}
};

class BoardConfig : APIConfig {
public:
	BoardConfig( string n, string m, uint32_t s, string k ){
		_slot = s;
		_model = m;
		_firmware = "unknown";
		_name = n;
		_key = k;
	};
	BoardConfig( string n, string m, string f, uint32_t s, string k ){
		_slot = s;
		_model = m;
		_firmware = f;
		_name = n;
		_key = k;
	};
	virtual ~BoardConfig(){};

	uint32_t slot( void ){ return( _slot ); }
	string name( void ){ return( _name ); }
	string model( void ){ return( _model ); }
	string firmware( void ){ return( _firmware ); }
	string description( void ){ return( _description ); }
	void setDescription( string d ){ _description = d; }
	string key( void ){ return( _key ); }

	std::map<string,PROPERTY_t> boardAPI_map;
	std::map<uint32_t,ChannelConfig *> channelConfig_map; // slot, object

private:
	uint32_t _slot;
	string _model;
	string _firmware;
	string _name;
	string _description;
	string _key; // unique with global scope
};


class CrateConfig : APIConfig {
public:
	CrateConfig(){
		_ipAddress = "";
		_model = "";
		_name = "";
		_pollCycleTime_ms = 1000;
	};
	virtual ~CrateConfig(){};

	string ipAddress( void ){ return( _ipAddress ); }
	string model( void ){ return( _model ); };
	string name( void ){ return( _name ); };
	void setipAddress( string s ){ _ipAddress = s; }
	void setmodel( string s ){ _model = s; }
	void setname( string s ){ _name = s; }
	void setPollCycleTimeMs( string s ){
		istringstream convert( s );
		if ( !(convert >> _pollCycleTime_ms ) )
			_pollCycleTime_ms = 1000;
	}
	int pollCycleTime_ms( void ){ return _pollCycleTime_ms; }
	uint32_t nbPopulatedSlots( void ) { return ( populatedSlot_map.size() ); }
	void addPopulatedSlot( uint32_t ps ){ populatedSlot_map.insert( std::pair<uint32_t, uint32_t>( nbPopulatedSlots(), ps )); }
	bool isSlotPopulated( uint32_t slot ){
		std::map<uint32_t, uint32_t>::iterator it = populatedSlot_map.find( slot );
		if ( it == populatedSlot_map.end() ) return( false );
		else return( true );
	}
	uint32_t populatedSlot( uint32_t islot ){ return( populatedSlot_map.at( islot )); }

	std::map<uint32_t,uint32_t> populatedSlot_map; // count=key, populated slot
	std::map<string,PROPERTY_t> crateAPI_map;
	std::map<uint32_t,BoardConfig *> boardConfig_map; // slot, object

private:
	string _ipAddress;
	string _model;
	string _name;
	int _pollCycleTime_ms;
};

// singleton
class Configuration {
public:

	virtual ~Configuration();
	Configuration( void );	// singleton
	static Configuration* Instance( void );
	static string getConfigfile( void ){ return( Configuration::_configfile ); }

	int read( string filename );
	void readShortHand( string filename );
	void readReferenceConfig( string filename );
	void populate( bool debug );
	void populateShortHand( bool debug );
	void initScadaCountersAllCrates( void );
	void firstGlobalUpdate( void );
	Crate *crate( uint32_t icrate ); // icrate same as handle
	Board *board( uint32_t icrate, uint32_t islot );
	Channel *channel( uint32_t icrate, uint32_t islot, uint32_t ich );
	void launchPollerThreadsForNonEventModeControllers( void );

	void debug( bool f ) { _debug = f; };
	void padMissingChannels( bool f1 ) { _padMissingChannels = f1; };

	// object stats for bling bling
	uint32_t totalNbCrates( void );
	uint32_t totalNbBoards( void );
	uint32_t totalNbChannels( void );    // from config
	uint32_t totalNbPsChannels2( void ); // from crate
	uint32_t totalNbBoards2( void );     // from crate
	uint32_t totalNbEasyBoards2( void );  // from crate
	void channelStates( int32_t *count_rup, int32_t *count_rdown,
			int32_t *count_on, int32_t *count_cc, int32_t *count_ptrip );
	void setIntrinsicUpdates( bool f ) { _intrinsicUpdates = f; }

private:
	static Configuration* _pInstance;
	static string _configfile;
	XercesDOMParser* _parser;

	Configuration( Configuration const&);                            // copy constructor absent (i.e. cannot call it - linker will fail).
	Configuration& operator=(Configuration const&);  		// assignment operator absent (i.e. cannot call it - linker will fail).

	bool _debug;
	bool _intrinsicUpdates;

	// if we get a config which is derived from the winnccoa world, and not directly from the electronics
	// we might have not used/declared all channels of that board which physically exist.
	// These "missing channels" are a problem for the OPC server, so lets detect them and fill them in.
	bool _padMissingChannels;
	inline bool _fileExists( const std::string& name );
	void _configureSYCrate( DOMNode  *crate );
	void _configureSYCrateProperty( DOMNode  *syprop );
	void _configureSYCrateShortHand( DOMNode  *crate );
	void _configureSYX527PropertiesShortHand( CrateConfig *crate );
	void _configureBoard( DOMNode *board );
	void _configureBoardProperty( DOMNode *boardprop_node );
	void _configureBoardShortHand( DOMNode *board );
	void _configureBoardStandardPropertiesShortHand( BoardConfig *board );
	void _configureBoardBranchControllerPropertiesShortHand( BoardConfig *board );
	void _configureChannel( DOMNode *channel_node );
	void _configureChannelProperty( DOMNode *channelprop_node );
	void _configureEasyBoard( DOMNode *channel_node );
	void _classifyEasyChannels( string bstr, int *count, string *classification );
	string _classifyBoardByProperties( DOMNode *board_node );
	string _classifyChannelByProperties( DOMNode *channel_node );
	string _padTo3DigitName( string oldname, int channelIndex );

	std::map<uint32_t,CrateConfig *> _crateConfig_map;
	uint32_t _currentIndexCrate;
	uint32_t _currentIndexCrateSlot;
	uint32_t _currentIndexCrateSlotChannel;

	typedef struct {
		string smodel;
		CAENHV_SYSTEM_TYPE_t imodel;
	} GENERIC_CRATES_t;
	vector<GENERIC_CRATES_t> _genericCrateModels;
	int _createSYCrate( CrateConfig *cfg );
	std::map<uint32_t,ns_HAL_Crate::Crate *> _crate_map;

	bool _createBoard( Crate *cr, BoardConfig *cfgboard );
	bool _createHighVoltChannel( Board *bd, ChannelConfig *cfgchannel );
	bool _createPaddedEasyChannel( Board *bd, uint32_t ichannel );
	bool _createEasyBoardMarkerChannel( Board *bd, ChannelConfig *cfgchannel );
	bool _createEASYLowVoltChannelv1( Board *bd, ChannelConfig *cfgchannel, float vlimit, float ilimit );
	bool _createEASYLowVoltChannelv2( Board *bd, ChannelConfig *cfgchannel, float vlimit, float ilimit );
	bool _createEASYHighVoltChannel( Board *bd, ChannelConfig *cfgchannel, float vlimit, float ilimit );
	bool _createLowVoltChannel( Board *bd, ChannelConfig *cfgchannel );
};

} /* namespace ns_HAL_Configuration */

#endif /* SRC_CONFIGURATION_H_ */

/*
 * Distributor.h
 *
 *  Created on: Aug 24, 2016
 *      Author: mludwig
 */

#ifndef SRC_DISTRIBUTOR_H_
#define SRC_DISTRIBUTOR_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <stdexcept>

#include <czmq.h>

#include "../../simulationOPCglue/protocolBuffers-c/simulationGlueCaen.pb-c.h"

#include "Configuration.h"

using namespace std;
using namespace ns_HAL_Crate;
using namespace ns_HAL_Configuration;

namespace simulationHAL {


// caen api specific stuff... unavoidable
#define CAEN_NAME_SIZE10 10
#define CAEN_PARAM_SIZE256 256
#define CAEN_EXECCOMM_MAX128 128
#define CAEN_STRINGPARAM_SIZE1024 1024
#define CAEN_MAX_NB_BOARDS 16

// definitions from wrapper
#define CAEN_WRAP_PARAM_TYPE_NUMERIC          0
#define CAEN_WRAP_PARAM_TYPE_ONOFF            1
#define CAEN_WRAP_PARAM_TYPE_CHSTATUS         2
#define CAEN_WRAP_PARAM_TYPE_BDSTATUS         3
#define CAEN_WRAP_PARAM_TYPE_BINARY			4			// Rel. 2.16
#define CAEN_WRAP_PARAM_TYPE_STRING			5

#define CAEN_WRAP_PARAM_MODE_RDONLY           0
#define CAEN_WRAP_PARAM_MODE_WRONLY           1
#define CAEN_WRAP_PARAM_MODE_RDWR             2


#define DISTRIBUTOR_OK 0
#define DISTRIBUTOR_UNKNOWN_MSG_TYPE -1
#define DISTRIBUTOR_UNKNOWN_PILOT_MSG_TYPE -1000
#define DISTRIBUTOR_caenInitReply_ERROR -110
#define DISTRIBUTOR_caenDeinitReply_ERROR -120

#define DISTRIBUTOR_MAX_DIM_COUNTER_NAME 128

#define SIM_MAX_NB_CRATES 50

// #define SIM_MAX_NB_CHANNELS 1000000 // in total ;-)

class Distributor {
public:

	Distributor();
	virtual ~Distributor();

	static int ar_uint8_to_char_sz( uint32_t nb );
	static void ar_uint8_to_char( uint8_t *in, char *buf, uint32_t nb );
	static int char_to_ar_uint8_sz( uint32_t nb );
	static void char_to_ar_uint8( char *in, uint8_t *buf );
	static char** calloc_charchar( uint32_t dim1, uint32_t dim2 );
	static void free_charchar( char **v, uint32_t dim1 );
	int32_t haltype2caentype( int32_t ht, char *parname );

	void glueDispatch( SCaen__CaenMessage *gRequest );
	void pilotDispatch( SCaen__PilotMessage *pRequest );

	void setDebug( bool d ){ _distributor_debug = d; }
	void setDebugMessageRequests( bool d ){ _distributor_debugMessageRequests = d; }
	void setDebugMessageReplies( bool d ){ _distributor_debugMessageReplies = d; }
	void setDebugCZ( bool d ){ _distributor_debugCZ = d; }
	void setCZsockets( zsock_t *sg, zsock_t *sp ){
		sockGlue = sg;
		sockPilot = sp;
	}

private:
	string version_program;
	string version_czeromq;
	string version_protocol;
	string version_protocol_c;
	string version_ossystem;
	string version_osrelease;

	bool _distributor_debug;
	bool _distributor_debugMessageRequests;
	bool _distributor_debugMessageReplies;
	bool _distributor_debugPilotMessageRequests;
	bool _distributor_debugPilotMessageReplies;
	bool _distributor_debugCZ;

	zsock_t *sockGlue;
	zsock_t *sockPilot;

	void _versions ( void );
	void _caenCZreply( SCaen__CaenMessage *gReply );

	int _CAENHV_InitSystem_request( SCaen__CAENHVInitSystemRequestM *request );
	int _CAENHV_DeinitSystem_request( SCaen__CAENHVDeinitSystemRequestM *request );
	int _CAENHV_GetCrateMap_request( SCaen__CAENHVGetCrateMapRequestM *request );
	int _CAENHV_GetSysPropList_request( SCaen__CAENHVGetSysPropListRequestM *request );
	int _CAENHV_GetSysPropInfo_request( SCaen__CAENHVGetSysPropInfoRequestM *request );
	int _CAENHV_GetSysProp_request( SCaen__CAENHVGetSysPropRequestM *request );
	int _CAENHV_SetSysProp_request( SCaen__CAENHVSetSysPropRequestM *request );
	int _CAENHV_GetBdParamInfo_request( SCaen__CAENHVGetBdParamInfoRequestM *request );
	int _CAENHV_TestBdPresence_request( SCaen__CAENHVTestBdPresenceRequestM *request );
	int _CAENHV_GetBdParamProp_request( SCaen__CAENHVGetBdParamPropRequestM *request );
	int _CAENHV_GetBdParam_request( SCaen__CAENHVGetBdParamRequestM *request );
	int _CAENHV_SetBdParam_request( SCaen__CAENHVSetBdParamRequestM *request );
	int _CAENHV_GetChParamInfo_request( SCaen__CAENHVGetChParamInfoRequestM *request );
	int _CAENHV_GetChName_request( SCaen__CAENHVGetChNameRequestM *request );
	int _CAENHV_SetChName_request( SCaen__CAENHVSetChNameRequestM *request );
	int _CAENHV_GetChParamProp_request( SCaen__CAENHVGetChParamPropRequestM *request );
	int _CAENHV_GetChParam_request( SCaen__CAENHVGetChParamRequestM *request );
	int _CAENHV_SetChParam_request( SCaen__CAENHVSetChParamRequestM *request );
	int _CAENHV_GetExecCommList_request( SCaen__CAENHVGetExecCommListRequestM *request );
	int _CAENHV_ExecComm_request( SCaen__CAENHVExecCommRequestM *request );
	int _CAENHV_GetError_request( SCaen__CAENHVGetErrorRequestM *request );
	int _CAENHV_GetEventData_request( SCaen__CAENHVGetEventDataRequestM *request ); // this one is performance critical
	int _CAENHV_SubscribeSystemParams_request( SCaen__CAENHVSubscribeSystemParamsRequestM *request );
	int _CAENHV_SubscribeBoardParams_request( SCaen__CAENHVSubscribeBoardParamsRequestM *request );
	int _CAENHV_SubscribeChannelParams_request( SCaen__CAENHVSubscribeChannelParamsRequestM *request );
	int _CAENHV_UnSubscribeSystemParams_request( SCaen__CAENHVUnSubscribeSystemParamsRequestM *request );
	int _CAENHV_UnSubscribeBoardParams_request( SCaen__CAENHVUnSubscribeBoardParamsRequestM *request );
	int _CAENHV_UnSubscribeChannelParams_request( SCaen__CAENHVUnSubscribeChannelParamsRequestM *request );
	vector<std::string> _decodeParameterList( char *parlist );

	int _pilot_setNoise_request( SCaen__PilotSetNoiseRequestM *pRequest );
	int _pilot_getCrateHandles_request( SCaen__PilotGetCrateHandlesRequestM *pRequest );
	int _pilot_setCrateVSEL_request( SCaen__PilotSetCrateVSELRequestM *pRequest );
	int _pilot_setLoad_request( SCaen__PilotSetLoadRequestM *pRequest );
	int _pilot_setAllLoads_request( SCaen__PilotSetAllLoadsRequestM *pRequest );
	int _pilot_rampAll_request( SCaen__PilotRampAllRequestM *pRequest );
	int _pilot_rampAllOnBoard_request( SCaen__PilotRampAllOnBoardRequestM *pRequest );
	int _pilot_switchPwAll_request( SCaen__PilotSwitchPwAllRequestM *pRequest );
	int _pilot_getLoad_request( SCaen__PilotGetLoadRequestM *pRequest );
	int _pilot_getUpdateDelay_request( SCaen__PilotGetUpdateDelayRequestM *pRequest );
	int _pilot_setUpdateDelay_request( SCaen__PilotSetUpdateDelayRequestM *pRequest );
	int _pilot_setCrateISEL_request( SCaen__PilotSetCrateISELRequestM *pRequest );
	int _pilot_tripChannelByVoltage_request( SCaen__PilotTripChannelByVoltageRequestM *pRequest );
	int _pilot_tripChannelByCurrent_request( SCaen__PilotTripChannelByCurrentRequestM *pRequest );
	int _pilot_tripChannelByLoad_request( SCaen__PilotTripChannelByLoadRequestM *pRequest );
	int _pilot_getConfiguration_request( SCaen__PilotGetConfigurationRequestM *pRequest );
	int _pilot_setConfiguration_request( SCaen__PilotSetConfigurationRequestM *pRequest );
	int _pilot_getHealthEngine_request( SCaen__PilotGetHealthEngineRequestM *pRequest );
	int _pilot_setDebuglevel_request( SCaen__PilotSetDebuglevelRequestM *pRequest );
	int _pilot_getCounters_request( SCaen__PilotGetCountersRequestM *pRequest );
	int _pilot_scada_request( SCaen__PilotScadaRequestM *pRequest );

	void _pilotCZreply( SCaen__PilotMessage *pReply );


	ns_HAL_Configuration::Configuration *cfg;
};

} /* namespace simulationHAL */

#endif /* SRC_DISTRIBUTOR_H_ */

//  simulationHAL main.h


#define SIMULATION_GLUE_PORT  8880
#define SIMULATION_PILOT_PORT 8881

#define NAME_SIZE 512

#define VENDOR_CAEN 0
#define VENDOR_ISEG 1
#define VENDOR_WIENER 2

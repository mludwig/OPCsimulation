/*
 * API.cpp
 *
 *  Created on: Nov 11, 2016
 *      Author: mludwig
 */

#include "../include/API.h"

using namespace ns_HAL_API;

API::HAL_PROPERTY_t API::convertPropCAEN2HAL( CAEN_PROPERTY_t c ){
	API::HAL_PROPERTY_t h;
	h.name = c.name;
	h.accessMode = ( HAL_ACCESS_MODE_t ) c.pAccessType; // same order or identical
	h.dataType = ( HAL_DATA_TYPE_t ) c.pDataType;       // same order
	h.value_string = "";
    h.value_int8 = 0;
    h.value_uint8 = 0;
    h.value_int16 = 0;
    h.value_uint16 = 0;
    h.value_int32 = 0;
    h.value_uint32 = 0;
    h.value_float = 0.0;
    return( h );
}



bool API::changedValue( std::map<string,API::HAL_PROPERTY_t>::iterator it ){

	if ( ! it->second.subscribed ) return( false );

	// any board status change
	if ( it->first == string("BdStatus") ){
		if ( it->second.value_int32 != it->second.previous_int32 ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
		if ( it->second.value_uint32 != it->second.previous_uint32 ) {
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
	}

#if 0
	// debugging
	cout << __FILE__ << " " << __LINE__ << " changedValue ? item= " << it->first << " type= " << it->second.dataType << endl;
	if ( it->first == "IPAddr" ){
		cout << __FILE__ << " " << __LINE__ << " changedValue ? val= " << it->second.value_string
				<< " pval= " << it->second.previous_string << endl;

	}
#endif
	// numeric parameters
	const float limitFloat = 0.02; // 2%
	switch ( it->second.dataType ){
	case API::treal: {
		if ( pow( it->second.value_float - it->second.previous_float, 2 ) > limitFloat ) {
			it->second.previous_float = it->second.value_float;
			return( true );
		}
		break;
	}
	case API::tint8: {
		if ( pow( it->second.value_int8 - it->second.previous_int8, 2 ) > limitFloat ) {
			it->second.previous_int8 = it->second.value_int8;
			return( true );
		}
		break;
	}
	case API::tint16: {
		if ( pow( it->second.value_int16 - it->second.previous_int16, 2 ) > limitFloat ) {
			it->second.previous_int16 = it->second.value_int16;
			return( true );
		}
		break;
	}
	case API::tint32: {
		if ( pow( it->second.value_int32 - it->second.previous_int32, 2 ) > limitFloat ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
		break;
	}
	case API::tuint8: {
		if ( pow( it->second.value_uint8 - it->second.previous_uint8, 2 ) > limitFloat ) {
			it->second.previous_uint8 = it->second.value_uint8;
			return( true );
		}
		break;
	}
	case API::tuint16: {
		if ( pow( it->second.value_uint16 - it->second.previous_uint16, 2 ) > limitFloat ) {
			it->second.previous_uint16 = it->second.value_uint16;
			return( true );
		}
		break;
	}
	case API::tuint32: {
		if ( pow( it->second.value_uint32 - it->second.previous_uint32, 2 ) > limitFloat ) {
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
		break;
	}
	case API::tstr: {
		// any string change
		if (( it->second.dataType == API::tstr ) && ( it->second.value_string != it->second.previous_string )) {
			it->second.previous_string = it->second.value_string;
			return( true );
		}
		break;
	}
	default:
		{
			cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property type not found" << endl;
			break;
		}
	}
	return( false );
}






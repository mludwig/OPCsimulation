/*
 * Board.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */

// hash example
#include <iostream>
#include <functional>
#include <string>

#include "../include/Board.h"

using namespace std;

namespace ns_HAL_Board {


Board::~Board() {}

// Board::Board( uint32_t sl, BOARDTYPE_t ty ) {
Board::Board( uint32_t sl, BOARDTYPE_t ty, string firmware ) {
	_debug = false;
	_intrinsicUpdates = true;
	_slot = sl;
	_type = ty;
	_firmware = firmware;
	_status = 0x0;
	_my_icrate = 0;
	_model = "unknown";

	// branch controller stuff, initialize to 0/neutral effect for everyone
	_x1676Ilk = 0;
	_Vsel = 0;
	_HWReset0 = 0;
	_Recovery0= 0;
	_HWReset1= 0;
	_Recovery1= 0;
	_HWReset2= 0;
	_Recovery2= 0;
	_HWReset3= 0;
	_Recovery3= 0;
	_HWReset4= 0;
	_Recovery4= 0;
	_HWReset5= 0;
	_Recovery5= 0;
	_GlobalOn= 0;
	_GlobalOff= 0;
	_48VA1676= 0;
	_HVMax = 10e6;
	_Ilimit = 10e3;
	_description = "";
	_serNumber = sl + 9000;
	_fwRelMin = "00.00";
	_fwRelMax = "99.99";

	// board characteristics
	switch ( _type ){
	case A1510: {
		_nbChannels = 12;
		_HVMax = 100.0;
		_Ilimit = 10.0e-3;
		_model = "A1510";
		__stateMachineType = Board::complexSM;
		_description = "12Ch 100V 10mA";
		_insertStandardBoardProperties();
		break;
	}
	case A1513B: {
		_nbChannels = 6;
		_HVMax = 10.0;
		_Ilimit = 3.7;
		_model = "A1513B";
		__stateMachineType = Board::complexSM;
		_description = "6Ch 10V 3.7A";
		_insertStandardBoardProperties();
		break;
	}
	case A1516B: {
		_nbChannels = 6;
		_HVMax = 15.0;
		_Ilimit = 1.5;
		_model = "A1516B";
		__stateMachineType = Board::complexSM;
		_description = "6Ch 15V 1.5A";
		_insertStandardBoardProperties();
		break;
	}
	case A1517B: {
		_nbChannels = 6;
		_HVMax = 7.0;
		_Ilimit = 4.0;
		_model = "A1517B";
		__stateMachineType = Board::complexSM;
		_description = "6Ch 7V 4.0A";
		_insertStandardBoardProperties();
		break;
	}
	case A1518B: {
		_nbChannels = 6;
		_HVMax = 4.5;
		_Ilimit = 6.0;
		_model = "A1518B";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A1515QG: {
		_nbChannels = 16;
		_HVMax = 1000.0;
		_Ilimit = 100.0e-3;
		_model = "A1515QG";
		__stateMachineType = Board::complexSM;
		_description = "16Ch 1kV 100mA GEM";
		_insertStandardBoardProperties();
		break;
	}
	case A1519: {
		_nbChannels = 12;
		_HVMax = 250.0;
		_Ilimit = 1.0e-3;
		_model = "A1519";
		__stateMachineType = Board::complexSM;
		_description = "12Ch 250kV 1mA";
		_insertStandardBoardProperties();
		break;
	}
	case A1535: {
		_nbChannels = 24;
		_HVMax = 3500.0;
		_Ilimit = 3.0e-3;
		_model = "A1535";
		__stateMachineType = Board::complexSM;
		_description = "24Ch 3.5kV 3mA";
		_insertStandardBoardProperties();
		break;
	}
	case A1540: {
		_nbChannels = 32;
		_HVMax = 100.0;
		_Ilimit = 1.0e-3;
		_model = "A1540";
		__stateMachineType = Board::complexSM;
		_description = "32Ch 100V 1mA";
		_insertStandardBoardProperties();
		break;
	}
	case A1590: {
		_nbChannels = 16;
		_HVMax = 9000.0;
		_Ilimit = 50.0e-6;
		_model = "A1590";
		__stateMachineType = Board::complexSM;
		_description = "16Ch 9kV 50uA";
		_insertStandardBoardProperties();
		break;
	}

	// case A1676A:
	case A1676: {
		// branch controller EASY3000, controls up to 6 EASY crates, and each of them can cave up to 10 boards.
		// Lets count max 32 channels per board, makes 60*32=1920 channels in total... maybe even more...
		_nbChannels = 1920;
		// _HVMax = 3500.0;  // global limit, each board/channel can be different though
		// _Ilimit = 3.0e-3; // global limit, each board/channel can be different though
		_model = "A1676";
		_description = "CAEN Branch Controller (simulated)";
		__stateMachineType = Board::undeterminedSM;

		// extra parameters for branch controller board:
		_propinsert("48VA1676",  API::ReadWriteProperty, API::I );
		_propinsert("A1676Ilk",  API::ReadOnlyProperty, API::F );
		_propinsert("GlobalOn",  API::ReadWriteProperty, API::I );
		_propinsert("GlobalOff", API::ReadWriteProperty, API::I );
		_propinsert("HW Reset0",  API::ReadWriteProperty, API::I );
		_propinsert("HW Reset1",  API::ReadWriteProperty, API::I );
		_propinsert("HW Reset2",  API::ReadWriteProperty, API::I );
		_propinsert("HW Reset3",  API::ReadWriteProperty, API::I );
		_propinsert("HW Reset4",  API::ReadWriteProperty, API::I );
		_propinsert("HW Reset5",  API::ReadWriteProperty, API::I );
		_propinsert("Recovery0",  API::ReadWriteProperty, API::I );
		_propinsert("Recovery1",  API::ReadWriteProperty, API::I );
		_propinsert("Recovery2",  API::ReadWriteProperty, API::I );
		_propinsert("Recovery3",  API::ReadWriteProperty, API::I );
		_propinsert("Recovery4",  API::ReadWriteProperty, API::I );
		_propinsert("Recovery5",  API::ReadWriteProperty, API::I );
		_propinsert("Vsel",  API::ReadWriteProperty, API::F );

		//_propinsert("VA1676",  API::ReadWriteProperty, API::I ); // watts ?
		//_propinsert("1676Ilk",  API::ReadOnlyProperty, API::F );

		break;
	}
	case A1734:  {
		_nbChannels = 12;
		_HVMax = 3000.0;
		_Ilimit = 3.0e-3;
		_model = "A1734";
		_description = "12Ch 3kV 3mA";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A1735:  {
		_nbChannels = 12;
		_HVMax = 1500.0;
		_Ilimit = 7.0e-3;
		_model = "A1735";
		_description = "12Ch 1.5kV 7mA";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A1821HN:  {
		_nbChannels = 12;
		_HVMax = 3000.0;
		_Ilimit = 200.0e-6;
		_model = "A1821HN";
		_description = "12Ch 3kV 200uA (neg)";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A1821:  {
		_nbChannels = 12;
		_HVMax = 3000.0;
		_Ilimit = 200.0e-6;
		_model = "A1821";
		_description = "12Ch 3kV 200uA";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A1832: {
		_nbChannels = 12;
		_HVMax = 6000.0;
		_Ilimit = 1.0e-3;
		_model = "A1832";
		_description = "12Ch 6kV 1mA";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A1833:  {
		_nbChannels = 12;
		_HVMax = 4000.0;
		_Ilimit = 2.0e-3;
		_model = "A1833";
		_description = "12Ch 4kV 2mA";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	case A2517: {
		_model = "A2517";
		_nbChannels = 8;
		_Ilimit = 15.0; // amps 50W in total
		_HVMax = 5.0; // low volt board
		_description = "8Ch 5V 15A";
		__stateMachineType = Board::simpleSM;
		_insertStandardBoardProperties();
		break;
	}
	case A2518: {
		_model = "A2518";
		_nbChannels = 8;
		_Ilimit = 10.0; // amps 50W in total
		_HVMax = 8.0; // low volt board
		_description = "8Ch 8V 10A";
		__stateMachineType = Board::simpleSM;
		_insertStandardBoardProperties();
		break;
	}
	case A2519: {
		_model = "A2519";
		_nbChannels = 8;
		_Ilimit = 5.0; // amps 50W in total
		_HVMax = 15.0; // low volt board
		_description = "8Ch 15V 5A";
		__stateMachineType = Board::simpleSM;
		_insertStandardBoardProperties();
		break;
	}
	case GENERIC:  {
		_nbChannels = 10000;
		_HVMax = 10e6;
		_Ilimit = 10e3;
		_model = "generic";
		_description = "superCow power board";
		__stateMachineType = Board::complexSM;
		_insertStandardBoardProperties();
		break;
	}
	default : {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Board: unknown type " << ty << endl;
		throw std::runtime_error( os.str() );
	}
	}
	_Temp = 99;

	// create the (still without values) HAL properties
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
	}

	// set BC special props
	if ( _type == A1676 ){
		setParameterValue("A1676Ilk", x1676Ilk() );
		setParameterValue("Vsel", _Vsel );
		setParameterValue("HW Reset0", HWReset0() );
		setParameterValue("Recovery0", Recovery0() );
		setParameterValue("HW Reset1", HWReset1() );
		setParameterValue("Recovery1", Recovery1() );
		setParameterValue("HW Reset2", HWReset2() );
		setParameterValue("Recovery2", Recovery2() );
		setParameterValue("HW Reset3", HWReset3() );
		setParameterValue("Recovery3", Recovery3() );
		setParameterValue("HW Reset4", HWReset4() );
		setParameterValue("Recovery4", Recovery4() );
		setParameterValue("HW Reset5", HWReset5() );
		setParameterValue("Recovery5", Recovery5() );
		setParameterValue("GlobalOn", GlobalOn() );
		setParameterValue("GlobalOff", GlobalOff() );
		setParameterValue("48VA1676", x48VA1676() );
	} else {
		// set normal boards RO
		_setValuesStandardBoardProperties();
	}

	// since boards do not have RW properties we do not need to set anything from persistency,
	// this is different in the channels
	srand( time( NULL ));
	cout << __FILE__ << " " << __LINE__ << " created Board slot= " << sl << " type= " << convert_type2model( _type ) << endl;
}

/**
 *  let's set values into this board's RO properties: all boards have that
 */
void Board::_setValuesStandardBoardProperties( void ){
	setParameterValue("BdStatus", _status );
	setParameterValue("HVMax", _HVMax );
	setParameterValue("Temp", _Temp );
}

/**
 *  register the generic CAEN properties for all boards
 */
void Board::_insertStandardBoardProperties( void ){
	_propinsert("BdStatus",  API::ReadOnlyProperty, API::I );
	_propinsert("HVMax", API::ReadOnlyProperty, API::F );
	_propinsert("Temp", API::ReadOnlyProperty, API::F );
}

void Board::setMyCrate( uint32_t i ) {
	_my_icrate = i;
	char buf[ 64 ];
	sprintf( buf, "crate%d_slot%d", _my_icrate, _slot );
	_key = string( buf );
}


bool Board::_testPropExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it == _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= "<< _key;
		cout << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= "<< _key << endl;
		// throw std::runtime_error( os.str() );
		return( false );
	}
	return( true );
}
bool Board::_testPropNotExist( string p ) {
	// cout << __FILE__ << " " << __LINE__ << " _testPropNotExist p= " << p << " slot= " << _slot << endl;
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it != _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= "<< _key;
		cout << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= "<< _key << endl;
		// throw std::runtime_error( os.str() );
		return( false );
	}
	return( true );
}

void Board::_propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t ){
	_testPropNotExist( n );
	API::CAEN_PROPERTY_t p;
	p.name = n;	p.pAccessType = a;	p.pDataType = t;
	_caenProp_map.insert( std::pair<string,API::CAEN_PROPERTY_t>( p.name, p ));
}
void Board::_propinsert( API::HAL_PROPERTY_t h ) {
	_prop_map.insert( std::pair<string,API::HAL_PROPERTY_t>( h.name, h ));
}

void Board::insertChannel( uint32_t ichannel, Channel *ch ){
	_channel_map.insert(std::pair<uint32_t,ns_HAL_Channel::Channel *>( ichannel, ch ));
}

/* static */ string Board::convert_type2model( BOARDTYPE_t type ){
	switch( type ){
	case none: return("none");
	case A1510: return("A1510");
	case A1515QG: return("A1515QG");

	case A1513B: return("A1513B");
	case A1516B: return("A1516B");
	case A1517B: return("A1517B");
	case A1518B: return("A1518B");

	case A1519: return("A1519");
	case A1535: return("A1535");
	case A1540: return("A1540");
	case A1590: return("A1590");
	case A1676: return("A1676");
	case A1734: return("A1734");
	case A1735: return("A1735");
	case A1821: return("A1821");
	case A1821HN: return("A1821HN");
	case A1832: return("A1832");
	case A1833: return("A1833");
	case A2518: return("A2518");
	case GENERIC: return("GENERIC");
	default: return("unknown");
	}
}


/* static */ BOARDTYPE_t Board::convert_model2type( string model ){
	if ( strcmp( model.c_str(), "none") == 0 ) return( none );
	if ( strcmp( model.c_str(), "") == 0 ) return( none );
	if ( strcmp( model.c_str(), "0") == 0 ) return( none );

	if ( strcmp( model.c_str(), "A1510") == 0 ) return( A1510 );
	if ( strcmp( model.c_str(), "A1515QG") == 0 ) return( A1515QG );
	if ( strcmp( model.c_str(), "A1513B") == 0 ) return( A1513B );
	if ( strcmp( model.c_str(), "A1516B") == 0 ) return( A1516B );
	if ( strcmp( model.c_str(), "A1517B") == 0 ) return( A1517B );
	if ( strcmp( model.c_str(), "A1518B") == 0 ) return( A1518B );
	if ( strcmp( model.c_str(), "A1519") == 0 ) return( A1519 );
	if ( strcmp( model.c_str(), "A1535") == 0 ) return( A1535 );
	if ( strcmp( model.c_str(), "A1540") == 0 ) return( A1540 );
	if ( strcmp( model.c_str(), "A1590") == 0 ) return( A1590 );

	if ( strcmp( model.c_str(), "A1676") == 0 ) return( A1676 ); // branch controller
	if ( strcmp( model.c_str(), "A1676A") == 0 ) return( A1676 ); // branch controller

	if ( strcmp( model.c_str(), "A1734") == 0 ) return( A1734 );
	if ( strcmp( model.c_str(), "A1735") == 0 ) return( A1735 );

	if ( strcmp( model.c_str(), "A1821") == 0 ) return( A1821 );
	if ( strcmp( model.c_str(), "A1821HN") == 0 ) return( A1821HN );
	if ( strcmp( model.c_str(), "A1832") == 0 ) return( A1832 );
	if ( strcmp( model.c_str(), "A1833") == 0 ) return( A1833 );

	if ( strcmp( model.c_str(), "A2517") == 0 ) return( A2517 );
	if ( strcmp( model.c_str(), "A2518") == 0 ) return( A2518 );
	if ( strcmp( model.c_str(), "A2519") == 0 ) return( A2519 );
	// ...

	if ( strcmp( model.c_str(), "generic") == 0 ) return( GENERIC );
	if ( strcmp( model.c_str(), "GENERIC") == 0 ) return( GENERIC );

	cout << __FILE__ << " " << __LINE__ << " Board::convert_model2type name " << model << " not found "<< endl;
	return( unknown );
}

bool Board::isBranchController( void ){
	if ( convert_model2type( _model ) == A1676 ) return true;
	return false;
}

/**
 * gives back the i-th populated channel.
 */
uint32_t Board::populatedChannel( uint32_t i ){
	if ( i < this->nbPopulatedChannels() ){
		return( _populatedChannelsv[ i ] );
	}
	std::ostringstream os;
	os << __FILE__ << " " << __LINE__ << " populatedChannel: channel " << i
			<< "of this board is not populate" << endl;
	throw std::runtime_error( os.str() );
}

uint32_t Board::BdStatus( void ){
	/// \todo BdStatus we need some status logic here and return the correct bits: it is not always SYNC
	_status = SYNC;
	return( _status );
}
float Board::HVMax( void ){
	// hardware max voltage not to exceed: fixed by design
	return( _HVMax );
}

/// just simulate temperature slowly floating around 22deg C for now.
float Board::Temp( void ){
	if ( !_intrinsicUpdates ) return( 22.3 );
	static int counter = 0;
	if ( counter++ > 20 ){
		_Temp = 22.0 + ( (double) rand()/RAND_MAX - 0.5 ) * 1.8;
		// if ( _debug ) cout << __FILE__ << " " << __LINE__ << " board slot = " << _slot << " Temp= " << _Temp << endl;
		counter = 0;
	}
	return( _Temp );
}

vector<string> Board::parameterList( void ){
	vector<string> v;
	v.clear();
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		v.push_back( _caenProp_map.at( it->first ).name );
	}
	return( v );
}

void Board::getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		*type = prop.dataType;
	}
}


void Board::getParameterValue( string p, string *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, int8_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, int16_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, int32_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, uint8_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, uint16_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, uint32_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Board::getParameterValue( string p, float *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}

float Board::parameterMinval( string p ){
	if (_testPropExist( p ) ){
		return ( API::getPropAuxMinvalue( _prop_map.at( p ) ));
	}
	return( 0.0 );
}
float Board::parameterMaxval( string p ){
	if (_testPropExist( p ) ){
		return ( API::getPropAuxMaxvalue( _prop_map.at( p ) ));
	}
	return( 0.0 );
}
uint16_t Board::parameterUnit( string p ){
	if (_testPropExist( p ) ){
		return ( API::getPropAuxUnit( _prop_map.at( p ) ));
	}
	return( 0 );
}
int16_t Board::parameterExp( string p ){
	if (_testPropExist( p ) ){
		return ( API::getPropAuxExp( _prop_map.at( p ) ));
	}
	return( 0 );
}
void Board::parameterOnstate( string p, string *os ){
	if (_testPropExist( p ) ){
		// API::getPropAuxOnstate( _prop_map.at( p ), os );
		*os = API::getPropAuxOnstate( _prop_map.at( p ));
	}
}
void Board::parameterOffstate( string p, string *os ){
	if (_testPropExist( p ) ){
		// API::getPropAuxOffstate( _prop_map.at( p ), os );
		*os = API::getPropAuxOffstate( _prop_map.at( p ));
	}
}


void Board::setParameterValue( string p, string val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_string = _prop_map.at( p ).value_string;
		_prop_map.at( p ).value_string = val;
	}
}
void Board::setParameterValue( string p, int8_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_int8 = _prop_map.at( p ).value_int8;
		_prop_map.at( p ).value_int8 = val;
	}
}
void Board::setParameterValue( string p, int16_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_int16 = _prop_map.at( p ).value_int16;
		_prop_map.at( p ).value_int16 = val;
	}
}
void Board::setParameterValue( string p, int32_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_int32 = _prop_map.at( p ).value_int32;
		_prop_map.at( p ).value_int32 = val;
	}
}
void Board::setParameterValue( string p, uint8_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_uint8 = _prop_map.at( p ).value_uint8;
		_prop_map.at( p ).value_uint8 = val;
	}
}
void Board::setParameterValue( string p, uint16_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_uint16 = _prop_map.at( p ).value_uint16;
		_prop_map.at( p ).value_uint16 = val;
	}
}
void Board::setParameterValue( string p, uint32_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_uint32 = _prop_map.at( p ).value_uint32;
		_prop_map.at( p ).value_uint32 = val;
	}
}
void Board::setParameterValue( string p, float val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_float = _prop_map.at( p ).value_float;
		_prop_map.at( p ).value_float = val;
	}
}

// returns a map with all properties which have changed since the last call
map<string,API::HAL_PROPERTY_t> Board::allChangedItems( void ){
	map<string,API::HAL_PROPERTY_t> itemsv;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if ( this->_changedItem( it ) ){
			itemsv.insert( std::pair<string,API::HAL_PROPERTY_t>(it->first, it->second) );
		}
	}
	return( itemsv );
}

bool Board::channelExists( uint32_t index ){
	std::map<uint32_t, Channel *>::iterator it;
	it = _channel_map.find( index );
	if ( it == _channel_map.end() ) return( false );
	else return( true );
};

/// property value is checked if it has changed. If no: false
/// if comparison to previous shows difference, update previous and report true.
/// this works for all properties set and RW and RO alike, not just for sets.
bool Board::_changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it ){
	if ( ! it->second.subscribed ) return( false );
	// any channel status change
	if ( it->first == string("Status") ){
		if ( it->second.value_int32 != it->second.previous_int32 ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
		if ( it->second.value_uint32 != it->second.previous_uint32 ) {
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
	}

	// numeric parameters
	/// \todo make numerical change limit float adjustable through pilot API
	const float limitFloat = 0.02; // 2%
	switch ( it->second.dataType ){
	case API::treal: {
		if ( pow( it->second.value_float - it->second.previous_float, 2 ) > limitFloat ) {
			it->second.previous_float = it->second.value_float;
			return( true );
		}
		break;
	}
	case API::tint8: {
		if ( pow( it->second.value_int8 - it->second.previous_int8, 2 ) > limitFloat ) {
			it->second.previous_int8 = it->second.value_int8;
			return( true );
		}
		break;
	}
	case API::tint16: {
		if ( pow( it->second.value_int16 - it->second.previous_int16, 2 ) > limitFloat ) {
			it->second.previous_int16 = it->second.value_int16;
			return( true );
		}
		break;
	}
	case API::tint32: {
		if ( pow( it->second.value_int32 - it->second.previous_int32, 2 ) > limitFloat ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
		break;
	}
	case API::tuint8: {
		if ( pow( it->second.value_uint8 - it->second.previous_uint8, 2 ) > limitFloat ) {
			it->second.previous_uint8 = it->second.value_uint8;
			return( true );
		}
		break;
	}
	case API::tuint16: {
		if ( pow( it->second.value_uint16 - it->second.previous_uint16, 2 ) > limitFloat ) {
			it->second.previous_uint16 = it->second.value_uint16;
			return( true );
		}
		break;
	}
	case API::tuint32: {
		if ( pow( it->second.value_uint32 - it->second.previous_uint32, 2 ) > limitFloat ) {
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
		break;
	}
	case API::tstr: {
		// any string change
		if (( it->second.dataType == API::tstr ) && ( it->second.value_string != it->second.previous_string )) {
			it->second.previous_string = it->second.value_string;
			return( true );
		}
		break;
	}
	default:
		{
			cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property type not found" << endl;
			break;
		}
	}
	return( false );
}


vector<std::string> Board::showProperties( void ){
	if (_debug) cout << endl << "=== " << __FILE__ << " " << __LINE__ << " showProperties board key= " << _key
			<< " model= " << _model
			<< " description= " << _description << " ===" << endl;
	vector<std::string> propv;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if (_debug) cout << __FILE__ << " " << __LINE__ << it->first << endl;
		propv.push_back( it->first );
	}
	return( propv );
}



void Board::subscribeParameter( string param, bool flag ){
	if (_debug) cout << __FILE__ << " " << __LINE__ << " Board::subscribeParameter " << flag << " " << param << endl;
	if ( _testPropExist( param ) ){
		if (_debug) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " " << flag << " " << param << " exists" << endl;
		_prop_map.at( param ).subscribed = flag;
	}
}


} /* namespace ns_HAL_Board */

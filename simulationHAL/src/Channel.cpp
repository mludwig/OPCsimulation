/*
 * Channel.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */


/// \todo channel/crate status, what is the semantics in reality, where is it reported
/// \todo implement persistency for settings depending on _powerOnOption
/// \todo do we really NOT need a standby state in the state machine ?
/// \todo hysteresis and precision of the ramping: does it do what it should ?
/// \todo event mode, frequency
/// \todo grouped ramping between channels: if one ramps, all in the group ramp (command from crate)
#include <cmath>
#include "../include/Channel.h"

namespace ns_HAL_Channel {
using namespace std;


Channel::~Channel() {}

Channel::Channel( uint32_t icrate, uint32_t islot, CHANNEL_TYPES_t type, uint32_t ch, float hwvmax, float hwimax, string description  ) {
	_channelType = type;
	_my_islot = islot;
	_my_icrate = icrate;
	_channel = ch;
	char buf[ 64 ];
	sprintf( buf, "crate%d_slot%d_channel%d", _my_icrate, _my_islot, _channel );
	_key = string( buf );
	_HWVmax = hwvmax;
	_HWImax = hwimax;

	/**
	 * channel naming. We always name them ChanXXX. More intelligence can go into the description.
	 * we discard the given name therefore.
	 */
	_description = description;
    std::ostringstream stm ;
	stm << setfill('0') << setw(3) << ch ;
	if ( type == easyBoardMarkerChannel ){
		_name = string("Marker") + stm.str();
	} else {
		_name = string("Chan") + stm.str();
	}

	_V0set = 0;
	_I0set = 2; // seems to be mA
	_V1set = 0;
	_I1set = 2;
	_SVmax = _HWVmax * 0.98;
	_Vramp_up = 5.0;
	_Vramp_down = 5.0;
	_vcondition = V_NOMINAL;
	_icondition = I_NOMINAL;
	__powerOnOption = false;
	_Pw = false;
	_Pon = false; // disabled: loose settings

	/// \todo implement PWDwn correctly in the state machine
	_PDwn = false; // go down within 1 sec after current trip, no ramp down
	_GlbOffEn = true; // only easy board channels
	_GlbOnEn = true;  // only easy board channels
	_Vmon = 0.0;
	_Imon = 0.0;
	_VCon = 0;
	_OutReg = 0;
	_tripIntegrationTime = 5.0; // seconds, assume default
	__tripInfinite = 1000;     // according to specs
	_pendingTripStarted =  false;
	_tripTriggered =  false;
	_hasTripped = false;
	__resistance = 20.0e6;
	__capacitance = 1e-12; // pF
	__precisionVmonRamp = 1.01;              // ramp hysteresis at 1%
	__precisionVmonTargetOn = 0.7;           // 0.7 V deviation allowed on target, plateau stickiness
	__precisionVtargetOffsetFactor = 0.999;  // V target is always 0.1% lower than specified: real electronics
	__precisionVtargetOffsetLowValue = -0.3; // V target is always lower than specified: real electronics
	__precisionVtargetZeroValue = 0.0;       // V target pW=0
	_noiseLevelVmon = 0.001;    // 10mV typical
	_noiseLevelImon = 1e-10;    // 0.1nA typical
	_symmetricNoiseCounter = 0;
	_noiseHalfAmplitudeImon = 0;
	_noiseHalfAmplitudeVmon = 0;
	//_lowVoltageStateMachine = false;
	// typedef enum { OFF, RUP, RDWN, OVC, OVV, UNV, EXTTRIP, INTTRIP, EXT_DIS } ChannelMessages_t; // returned by controller, depending on channel status
	_status = 0x0;

	__VSEL = false; // V0set
	__ISEL = false; // I0set
	__Vtarget = 0;
	__Imax = 0;
	_TripInt = 0x0;
	_TripExt = 0x0;

	// state machine
	_SMstate = state_OFF;
	_tempMarkerChannelEasyBoard = 0;
	_isEasyChannel = false;
	_isEasyBoardMarkerChannel = false;
	_isEasyPowerConverterChannel = false; // they are not even implemented, but pop up in certain configs

	switch ( type ){
	case primaryPSchannelWithVSELandISEL :{
		_insertProperties_primaryPSchannelWithVSELandISEL();
		for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
			_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
		}
		configureSM( Channel::complexSM );
		_setValues_primaryPSchannelWithVSELandISEL();
		break;
	}
	case primaryPSchannelNoVSEL :{
		_insertProperties_primaryPSchannelNoVSEL();
		for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
			_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
		}
		configureSM( Channel::simpleSM );
		_setValues_primaryPSchannelNoVSEL();
		break;
	}

	case easyBoardMarkerChannel :{
		_isEasyBoardMarkerChannel = true;
		_insertProperties_easyBoardMarkerChannel();
		for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
			_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
		}
		configureSM( Channel::easyBoardMarkerChannelSM );
		_setValues_easyBoardMarkerChannel();
		break;
	}
	case easyPSchannelNoVSELv1 :{
		_isEasyChannel = true;
		_insertProperties_easyPSchannelNoVSELv1();
		for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
			_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
		}
		configureSM( Channel::simpleSM );
		_setValues_easyPSchannelNoVSELv1();
		break;
	}
	case easyPSchannelNoVSELv2 :{
		_isEasyChannel = true;
		_insertProperties_easyPSchannelNoVSELv2();
		for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
			_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
		}
		configureSM( Channel::simpleSM );
		_setValues_easyPSchannelNoVSELv2();
		break;
	}
	case easyPSchannelWithVSELnoISEL :{
		_isEasyChannel = true;
		_insertProperties_easyPSchannelWithVSELnoISEL();
		for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
			_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
		}
		configureSM( Channel::complexSM );
		_setValues_easyPSchannelWithVSELnoISEL();
		break;
	}
	}
	_debug = false;
	//cout << __FILE__ << " " << __LINE__ << " created Channel " << ch << " _name= " << _name
	//		<< " _description= " << _description << " type= " << convert_type2model( type) << endl;
}

/* static */ string Channel::convert_type2model( CHANNEL_TYPES_t type ){
	switch( type ){
	case primaryPSchannelWithVSELandISEL: return("standardHighVoltChannel");
	case primaryPSchannelNoVSEL: return("standardLowVoltChannel");
	case easyBoardMarkerChannel: return("easyBoardMarkerChannel");
	case easyPSchannelNoVSELv1: return("easyLowVoltChannel");
	case easyPSchannelWithVSELnoISEL: return("easyHighVoltChannel");
	default: return("unknown");
	}
}

void Channel::_insertProperties_primaryPSchannelWithVSELandISEL( void ){
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("I1Set",  API::ReadWriteProperty, API::F );
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );
	_propinsert("PDwn",  API::ReadWriteProperty, API::I );
	_propinsert("POn",  API::ReadWriteProperty, API::I );
	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("RDWn",   API::ReadWriteProperty, API::F );
	_propinsert("RUp",    API::ReadWriteProperty, API::F );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("Status",  API::ReadOnlyProperty, API::I );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("TripExt",  API::ReadWriteProperty, API::F );
	_propinsert("TripInt",  API::ReadWriteProperty, API::F );
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("V1Set",  API::ReadWriteProperty, API::F );
	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
}

void Channel::_insertProperties_easyPSchannelWithVSELnoISEL( void ){
	_propinsert("GlbOffEn",  API::ReadWriteProperty, API::I );
	_propinsert("GlbOnEn",  API::ReadWriteProperty, API::I );
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );
	_propinsert("PDwn",  API::ReadWriteProperty, API::I );
	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("RDWn",   API::ReadWriteProperty, API::F );
	_propinsert("RUp",    API::ReadWriteProperty, API::F );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("Status",  API::ReadOnlyProperty, API::I );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("V1Set",  API::ReadWriteProperty, API::F );
	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
}
void Channel::_setValues_easyPSchannelWithVSELnoISEL( void ){
	setParameterValue("GlbOffEn", 0 );
	setParameterValue("GlbOnEn", 0 );
	setParameterValue("I0Set", _I0set );
	setParameterValue("RDWn", _Vramp_down );
	setParameterValue("RUp", _Vramp_up );

	setParameterValue("Pw", _Pw );
	setParameterValue("SVMax", _SVmax );
	setParameterValue("Status", _status );
	setParameterValue("Trip", _Trip );
	setParameterValue("V0Set", _V0set );
	setParameterValue("V1Set", _V1set );
}

// set the write properties to their starting values as specified in the object
// this is where the persistency should be implemented
void Channel::_setValues_primaryPSchannelWithVSELandISEL( void ){
	setParameterValue("V0Set", _V0set );
	setParameterValue("I0Set", _I0set );
	setParameterValue("V1Set", _V1set );
	setParameterValue("I1Set", _I1set );
	setParameterValue("RUp", _Vramp_up );
	setParameterValue("RDWn", _Vramp_down );
	setParameterValue("Trip", _tripIntegrationTime );
	setParameterValue("SVMax", _SVmax );
	setParameterValue("TripInt", _TripInt );
	setParameterValue("TripExt", _TripExt );
	setParameterValue("Pw", _Pw );
	setParameterValue("POn", _Pon );
	setParameterValue("PDwn", _PDwn );
	setParameterValue("Status", _status );
}

void Channel::_insertProperties_primaryPSchannelNoVSEL( void ){
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("I1Set",  API::ReadWriteProperty, API::F ); // hum..
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );
	_propinsert("PDwn",  API::ReadWriteProperty, API::I );
	_propinsert("POn",  API::ReadWriteProperty, API::I );
	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("Status",  API::ReadOnlyProperty, API::I );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("TripExt",  API::ReadWriteProperty, API::F );
	_propinsert("TripInt",  API::ReadWriteProperty, API::F );
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("V1Set",  API::ReadWriteProperty, API::F ); // hum ..
	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
}

// set the write properties to their starting values as specified in the object
// this is where the persistency should be implemented
void Channel::_setValues_primaryPSchannelNoVSEL( void ){
	setParameterValue("V0Set", _V0set );
	setParameterValue("I0Set", _I0set );
	setParameterValue("V1Set", _V1set ); // hum..
	setParameterValue("I1Set", _I1set ); // hum..
	setParameterValue("Trip", _tripIntegrationTime );
	setParameterValue("SVMax", _SVmax );
	setParameterValue("TripInt", _TripInt );
	setParameterValue("TripExt", _TripExt );
	setParameterValue("Pw", _Pw );
	setParameterValue("POn", _Pon );
	setParameterValue("PDwn", _PDwn );
	setParameterValue("Status", _status );
}

/**
 * in order to do this fine distinction we really need to discover the boards
 * from the electronics without any alterations, and also intruduce fully
 * the firmware versions. Same board models with different firmware versions
 * will typically have these small differences. To the simulation engine this does not matter yet,
 * but some WinCC-OA fwCAEN might be unhappy about this. We'll get there eventually
 */

/** right now all easy boards are v1. Change the
 *
 * void Configuration::_classifyEasyChannels( string bstr, int *count, string *classification ){
 * method or replace it. Currently we do "guess the behaviour from the API", which does the job, but is
 * not really the final good solution. We should have the {model, firmware} for ALL boards, primary and EASY boards,
 * plus the discovered API, to decide how to simulate it.
 */
void Channel::_insertProperties_easyPSchannelNoVSELv1( void ){
	// no ramping up, simple sm
	_propinsert("GlbOffEn",  API::ReadWriteProperty, API::I );
	_propinsert("GlbOnEn",  API::ReadWriteProperty, API::I );
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );
	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("Status",  API::ReadOnlyProperty, API::I );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("VCon",  API::ReadOnlyProperty, API::F );
	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
}
void Channel::_insertProperties_easyPSchannelNoVSELv2( void ){
	// no ramping up, simple sm
	_propinsert("GlbOffEn",  API::ReadWriteProperty, API::I );
	_propinsert("GlbOnEn",  API::ReadWriteProperty, API::I );
	_propinsert("I0Set",  API::ReadWriteProperty, API::F );
	_propinsert("IMon",  API::ReadOnlyProperty, API::F );
	_propinsert("Pw",   API::ReadWriteProperty, API::I );
	_propinsert("SVMax",  API::ReadWriteProperty, API::F );
	_propinsert("Status",  API::ReadOnlyProperty, API::I );
	_propinsert("Trip",   API::ReadWriteProperty, API::F );
	_propinsert("V0Set",  API::ReadWriteProperty, API::F );
	_propinsert("VCon",  API::ReadOnlyProperty, API::F );
	_propinsert("VMon",  API::ReadOnlyProperty, API::F );
	_propinsert("OutReg",  API::ReadOnlyProperty, API::I );
	//	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " board= " << this->mySlot() << " channel= " << _channel << endl;
}

void Channel::_setValues_easyPSchannelNoVSELv1( void ){
	setParameterValue("GlbOffEn", 0 );
	setParameterValue("GlbOnEn", 0 );
	setParameterValue("I0Set", _I0set );
	setParameterValue("Pw", _Pw );
	setParameterValue("SVMax", _SVmax );
	setParameterValue("Status", _status );
	setParameterValue("Trip", _Trip );
	setParameterValue("V0Set", _V0set );
}
void Channel::_setValues_easyPSchannelNoVSELv2( void ){
	setParameterValue("GlbOffEn", 0 );
	setParameterValue("GlbOnEn", 0 );
	setParameterValue("I0Set", _I0set );
	setParameterValue("Pw", _Pw );
	setParameterValue("SVMax", _SVmax );
	setParameterValue("Status", _status );
	setParameterValue("Trip", _Trip );
	setParameterValue("V0Set", _V0set );
}

void Channel::_insertProperties_easyBoardMarkerChannel( void ){
	_propinsert("12VPwS",  API::ReadOnlyProperty, API::I );// probably dynamic as well
	_propinsert("48VPwS",  API::ReadOnlyProperty, API::I );// probably dynamic as well
	_propinsert("HVSync",  API::ReadOnlyProperty, API::I );
	_propinsert("MainPwS",  API::ReadOnlyProperty, API::I );
	_propinsert("Rel",  API::ReadOnlyProperty, API::F );

	_propinsert("RemBdName",  API::ReadOnlyProperty, API::I );
	// shouldn't this be a string ? No.. this is how boards are discovered
	// this prop is using an attribute (aux property) canlled Onstate, which holds
	// the easy board  model in the marker channel

	_propinsert("RemIlk",  API::ReadOnlyProperty, API::I );
	_propinsert("SerNum",  API::ReadOnlyProperty, API::F );
	_propinsert("Sync",  API::ReadOnlyProperty, API::I );
	_propinsert("Temp",  API::ReadOnlyProperty, API::F );
}

// set static RO params to fixed values
void Channel::_setValues_easyBoardMarkerChannel( void ){
	setParameterValue("12VPwS", (int32_t) 12 );
	setParameterValue("48VPwS", (int32_t) 48 );
	setParameterValue("HVSync", (int32_t) 1 );
	setParameterValue("MainPwS", (int32_t) 220 );
	setParameterValue("Rel", (float) 9.99 );
	setParameterValue("RemBdName", (int32_t) 0 );
	setParameterValue("RemIlk", (int32_t) 1 );
	setParameterValue("SerNum", (float) 9.99 );
	setParameterValue("Sync", (int32_t) 1 );

	// set the aux value (Onstate) of parameter (RemBdName) to the board name
	string easyBoardModel = parameterOnstate("RemBdName");
	setParameterOnstate("RemBdName", easyBoardModel );

	// add also the firmware version: set the aux value (Offstate) of parameter (RemBdName) to the fw version
	string easyBoardFWversion = parameterOffstate("RemBdName");
	setParameterOffstate("RemBdName", easyBoardFWversion );
}

void Channel::_propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t ){
	_testPropNotExist( n );
	API::CAEN_PROPERTY_t p;
	p.name = n;	p.pAccessType = a;	p.pDataType = t;
	_caenProp_map.insert( std::pair<string,API::CAEN_PROPERTY_t>( p.name, p ));
}
bool Channel::_testPropExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it == _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= " << _key;
		cout << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= " << _key << endl;
		// throw std::runtime_error( os.str() );
		return( false );
	}
	return( true );
}
bool Channel::_testPropNotExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it != _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= " << _key;
		cout << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= " << _key << endl;
		// throw std::runtime_error( os.str() );
		return( false );
	}
	return( true );
}

void Channel::_refreshStatus( void ){
	_status = 0x0;
	switch ( _SMstate ){
	// typedef enum { state_OFF, state_rampingUp, state_rampingDown, state_ON } SM_states_t;
	case state_OFF: { break; }
	case state_rampingUp: {
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT1;
		break;
	}
	case state_rampingDown: {
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT2;
		break;
	}
	case state_ON: {
		_status = _status | CHANNEL_STATUS_BIT0;
		break;
	}
	case state_ConstantCurrent :{
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT3;
		break;
	}
	case state_PendingTrip: {
		// we are stuck in ramp up, status wise, if we have a pending trip
		_status = _status | CHANNEL_STATUS_BIT0;
		_status = _status | CHANNEL_STATUS_BIT1;
		break;
	}
	}

	// in reality, most boards do not show these bits when ramping, but according to specs
	// they should...
#if 0
	if ( _icondition == OVERCURRENT ) _status = _status | CHANNEL_STATUS_BIT3;
	if ( _vcondition == OVERVOLTAGE ) _status = _status | CHANNEL_STATUS_BIT4;
	if ( _vcondition == UNDERVOLTAGE ) _status = _status | CHANNEL_STATUS_BIT5;
#endif

	/// \todo _status for external trip: don't know how to implement this on BIT6
	if ( _icondition == OVERCURRENT )
		_status = _status | CHANNEL_STATUS_BIT3; // OVERCURRENT bin1001=dec9
	else
		_status = _status & ~CHANNEL_STATUS_BIT3; // NORMAL unset

	if ( _hasTripped ) _status = _status | CHANNEL_STATUS_BIT9; // internal trip dec512

	if ( _Vmon >= _SVmax )  _status = _status | CHANNEL_STATUS_BIT7;
	if ( _Vmon >= _HWVmax ) _status = _status | CHANNEL_STATUS_BIT7;

	/// \todo _status for external disable: don't know how to implement this BIT8
	/// \todo _status for calibration error: don't know how to implement this BIT10
	/// \todo _status for unplugged: don't know how to implement this BIT11

	// reserved BIT12

	/// \todo _status for overvoltage protection: don't know how to implement this BIT13
	/// _status for power fail: don't know how to implement this BIT14
	/// _status for temperature error: don't know how to implement this BIT15

	_status = _status & CHANNEL_STATUS_BITXX; // set unused bits to 0

#if 0
	if ( _key ==  string("crate0_slot9_channel1") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " status= 0x" << hex << _status << dec << endl;
	}
#endif
}

void Channel::_refreshPower( void ){
	if ( _SMstate == state_OFF ){
		_Vmon = 0.0;
		_Imon = 0.0;
		return;
	}

	if (( _SMstate == state_PendingTrip ) || ( _SMstate == state_ConstantCurrent )) {
		_Imon = __Imax;
		_Vmon = __resistance * _Imon;
	} else {
		if ( __resistance > 0 ) {
			_Imon = _Vmon / __resistance; // in uA most of the time
			if ( _SMstate == state_rampingUp )
				_Imon += __capacitance * _Vramp_up;
			else if ( _SMstate == state_rampingDown )
				_Imon += __capacitance * _Vramp_down;
		} else {
			// becoming a constant current source
			_Imon = __Imax;
			_Vmon = __resistance * _Imon; // 0 in this case
		}
	}

	// add symmetric noise
	switch ( _symmetricNoiseCounter){
	case 0: {
		_noiseHalfAmplitudeImon = _noiseLevelImon * ((float) rand()/RAND_MAX);
		_noiseHalfAmplitudeVmon = _noiseLevelVmon * ((float) rand()/RAND_MAX);
		_Imon += _noiseHalfAmplitudeImon;
		_Vmon += _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 1: {
		_Imon -= _noiseHalfAmplitudeImon;
		_Vmon -= _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 2: {
		_Imon -= _noiseHalfAmplitudeImon;
		_Vmon -= _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter++;
		break;
	}
	case 3: {
		_Imon += _noiseHalfAmplitudeImon;
		_Vmon += _noiseHalfAmplitudeVmon;
		_symmetricNoiseCounter = 0;
		break;
	}
	} // switch
	if (( _channel == 1 ) && ( _my_islot == 1 )){
		cout << __FILE__ << " " << __LINE__ << " _symmetricNoiseCounter= " << _symmetricNoiseCounter << endl;
		cout << __FILE__ << " " << __LINE__ << " _noiseHalfAmplitudeImon= " << _noiseHalfAmplitudeImon << endl;
		cout << __FILE__ << " " << __LINE__ << " _noiseHalfAmplitudeVmon= " << _noiseHalfAmplitudeVmon << endl;
		cout << __FILE__ << " " << __LINE__ << " Vmon= " << _Vmon << " Imon= " << _Imon << endl;
	}
}

void Channel::_refreshVoltageCondition( void ){
	_vcondition = V_NOMINAL;
	if ( _Vmon > __Vtarget + __precisionVmonTargetOn ) _vcondition = OVERVOLTAGE;
	if ( _Vmon < __Vtarget - __precisionVmonTargetOn ) _vcondition = UNDERVOLTAGE;
}
void Channel::_refreshCurrentCondition( void ){
	if ( _pendingTripStarted )
		_icondition = OVERCURRENT;
	else
		_icondition = I_NOMINAL;
}

void Channel::_refresh_SVMax( void ){
	if ( __VSEL ){
		if ( _SVmax < _V1set ) _V1set = _SVmax;
	} else {
		if ( _SVmax < _V0set ) _V0set = _SVmax;
	}
}



// load api values into objects
void Channel::_updateControlInterface( void ){
	if ( _channelType == Channel::easyBoardMarkerChannel ) return;

	// settings with some limits
	float ff = 0;
	getParameterValue( "Trip", &ff ); set_TripTime( ff );

	switch ( _channelType ){
	case Channel::primaryPSchannelWithVSELandISEL:{
		getParameterValue( "V0Set", &_V0set );
		getParameterValue( "I0Set", &_I0set );
		getParameterValue( "Pw", &_Pw );
		getParameterValue( "SVMax", &_SVmax );
		getParameterValue( "V1Set", &_V1set );
		getParameterValue( "I1Set", &_I1set );
		getParameterValue( "POn", &_Pon );
		getParameterValue( "PDwn", &_PDwn );
		getParameterValue( "RUp", &ff ); set_Vramp_up( ff );
		getParameterValue( "RDWn", &ff ); set_Vramp_down( ff );
		break;
	}
	case Channel::easyPSchannelWithVSELnoISEL:{
		getParameterValue( "V0Set", &_V0set );
		getParameterValue( "I0Set", &_I0set );
		getParameterValue( "Pw", &_Pw );
		getParameterValue( "SVMax", &_SVmax );
		getParameterValue( "V1Set", &_V1set );
		getParameterValue( "PDwn", &_PDwn );
		getParameterValue( "RUp", &ff ); set_Vramp_up( ff );
		getParameterValue( "RDWn", &ff ); set_Vramp_down( ff );
		break;
	}
	case Channel::primaryPSchannelNoVSEL:{
		getParameterValue( "V0Set", &_V0set );
		getParameterValue( "I0Set", &_I0set );
		getParameterValue( "Pw", &_Pw );
		getParameterValue( "SVMax", &_SVmax );
		break;
	}
	case Channel::easyPSchannelNoVSELv1:{
		getParameterValue( "V0Set", &_V0set );
		getParameterValue( "I0Set", &_I0set );
		getParameterValue( "Pw", &_Pw );
		getParameterValue( "SVMax", &_SVmax );
		break;
	}
	case Channel::easyPSchannelNoVSELv2:{
		getParameterValue( "V0Set", &_V0set );
		getParameterValue( "I0Set", &_I0set );
		getParameterValue( "Pw", &_Pw );
		getParameterValue( "SVMax", &_SVmax );
		break;
	}
	case Channel::easyBoardMarkerChannel:{
		// no control values to read
		break;
	}
	} // switch
}

// load object values into api
void Channel::_updateResultInterface( void ){
	setParameterValue( "VMon", _Vmon );
	setParameterValue( "IMon", _Imon );
	setParameterValue( "Status", _status );

	//	ch->setParameterValue("TripInt", ch->TripInt() );
	//	ch->setParameterValue("TripExt", ch->TripExt() );

	// easy board status
	//setParameterValue( "OutReg", _OutReg );
}


/// \todo cleanup _updateChannel00easyBoard
void Channel::_updateChannel00easyBoard( void ){
	static int counter = 0;
	if ( counter++ > 20 ){
		_tempMarkerChannelEasyBoard = 22.0 + ( (double) rand()/RAND_MAX - 0.5 ) * 1.8;
		// if ( _debug ) cout << __FILE__ << " " << __LINE__ << " board slot = " << _slot << " Temp= " << _Temp << endl;
		counter = 0;
	}
	setParameterValue( "Temp", _tempMarkerChannelEasyBoard );
}

// invoked any time the channel is read, or fires an event,
// but no private thread. The more frequent the higher the resolution...
/// \todo implement pilot interface or config for _lowVoltageStateMachine: a simplification of the state machine for PS without ramps (low voltage)
void Channel::sm( void ){

	if (_debug) cout << __FILE__ << " " << __LINE__ << " " << _key << " sm state= " << (int) _SMstate << endl;
#if 0
	if ( _key ==  string("crate0_slot1_channel0") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " sm state= " << (int) _SMstate
				<< " Vtarget= " << __Vtarget
				<< " V0set= " << _V0set
				<< " V1set= " << _V1set
				<< " VMon= " << _Vmon
				<< " Pw= " << _Pw
				<< " status= " << _status
				<< " my_islot= " << _my_islot
				<< " my_icrate= " << _my_icrate
				<< " _channel= " << _channel << endl;
	}
	if ( _key ==  string("crate0_slot5_channel1") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " sm state= " << (int) _SMstate
				<< " Vtarget= " << __Vtarget
				<< " V0set= " << _V0set
				<< " V1set= " << _V1set
				<< " VMon= " << _Vmon
				<< " Pw= " << _Pw
				<< " status= " << _status
				<< " my_islot= " << _my_islot
				<< " my_icrate= " << _my_icrate
				<< " _channel= " << _channel << endl;
	}
#endif
	if ( _smBehaviour == easyBoardMarkerChannelSM ) {
		_updateChannel00easyBoard();
		return;
	}
	gettimeofday( &_now, &_tz );

	// internal processing, getting new control values
	_updateControlInterface();
	_refresh_SVMax();

	// when a trip is pending we are stuck and we WILL trip. This is to avoid
	// ramping up again with a new target voltage
	if ( _SMstate != state_PendingTrip ) _refresh_VSEL();

	_refresh_ISEL();
	_refreshVoltageCondition();
	_refreshCurrentCondition();
	_SMstate_previous = _SMstate;

	// 1. determine the state we should be in (conditions)
	// 1.a can provoke ramping down by switching it off, and ramping up by switching it on: overwrite __Vtarget
	if ( _Pw == 0 ) {
		__Vtarget = __precisionVtargetZeroValue;
		if ( _SMstate != state_OFF ){
			_SMstate = state_rampingDown;
		}
	} else {
		if ( _SMstate != state_OFF ){
			if ( _SMstate != state_PendingTrip ) {
				_SMstate = state_rampingUp;
			}
			_hasTripped = false;
		}
	}

	//#if 0
	// 1.b low voltage PS don't have ramps, we can easily eliminate them from the state machine like this:
	if ( _smBehaviour == simpleSM ){
		if ( _SMstate == state_rampingUp ) {
			_Vmon = __Vtarget;
			_SMstate = state_ON;
		}
		if ( _SMstate == state_rampingDown ) {
			_SMstate = state_OFF;
		}
	}
	//#endif

	// 1.c ramp up, but must not have a pending trip.
	if ( _SMstate != state_PendingTrip ) {
		if ( _Vmon < __Vtarget * __precisionVmonRamp ) _SMstate = state_rampingUp;
	}

	// 1.d ramp down
	if ( _Vmon * __precisionVmonRamp > __Vtarget ) _SMstate = state_rampingDown;

	// 1.e on, steady, but must not have a pending trip.
	if ( _SMstate != state_PendingTrip ) {
		if (( _Vmon < ( __Vtarget + __precisionVmonTargetOn )) && ( _Vmon > ( __Vtarget - __precisionVmonTargetOn )) ) _SMstate = state_ON;
	}

	// 1.f off
	if ( ( __Vtarget == 0.0 ) && ( _Vmon <= 0.5 )) _SMstate = state_OFF;

	// 2.a constant current
	if (( _Imon >= __Imax ) && ( _tripIntegrationTime >= __tripInfinite )) _SMstate = state_ConstantCurrent;

	// 2.b pending trip
	if (( _Imon >= __Imax )
			&& ( _tripIntegrationTime < __tripInfinite )
			&& ( _SMstate != state_rampingDown )) _SMstate = state_PendingTrip;

	// 2.c tripped: no down ramp, one-time-condition
	if ( _tripTriggered ) {
		_Pw = 0;
		setParameterValue( "Pw", _Pw );// we have to overwrite the controls as well
		__Vtarget = 0.0;
		_Vmon = __Vtarget;
		_hasTripped = true;
		_tripTriggered = false;
		_SMstate = state_OFF;
	}

	// take ramping times
	if (( _SMstate != state_rampingUp ) && ( _SMstate != state_rampingDown )) {
		gettimeofday( &_tramp, &_tz );
	}


#if 0
	// example for specific debugging before sm is executed
	if ( _key ==  string("crate0_slot0_channel1") ){
		cout << __FILE__ << " " << __LINE__ << " " << _key << " sm state= " << (int) _SMstate
				<< " Vtarget= " << __Vtarget
				<< " V0set= " << _V0set
				<< " V1set= " << _V1set
				<< " VMon= " << _Vmon
				<< " Pw= " << _Pw
				<< " status= " << _status << endl;
	}
#endif

	// 2. execute states
	switch ( _SMstate ) {
	case state_OFF:{
		_Vmon = 0.0;
		_Imon = 0.0;
		_pendingTripStarted = false;
		break;
	}
	case state_rampingUp:{
		double delta = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
		double vdiff = _Vramp_up * delta / 1000.0;
		if ( _Vmon + vdiff < __Vtarget ) {
			_Vmon += vdiff;
		} else {
			_Vmon = __Vtarget; // last step is smaller
		}
		gettimeofday( &_tramp, &_tz );
		break;
	}
	case state_rampingDown:{
		double delta_ms = (double) ( _now.tv_sec - _tramp.tv_sec) * 1000 + (double) ( _now.tv_usec - _tramp.tv_usec) / 1000.0;
		double vdiff = _Vramp_down * delta_ms / 1000.0;
		if ( _Vmon - vdiff > __Vtarget ) {
			_Vmon -= vdiff;
		} else {
			_Vmon = __Vtarget;// last step is smaller
		}
		gettimeofday( &_tramp, &_tz );
		break;
	}
	case state_ON:{
		_Vmon = __Vtarget;
		break;
	}
	case state_ConstantCurrent:{
		_Imon = __Imax;
		_Vmon = __resistance * _Imon;
		break;
	}
	case state_PendingTrip:{
		_Imon = __Imax;
		_Vmon = __resistance * _Imon;
		__Vtarget = _Vmon; // prevent further ramping up

		// overcurrent condition, waiting for trip. Normally, the user can get out of a pending trip by
		// lowering the V0Set/V1Set or by toggling VSEL. We don't care here: we will always trip once it is pending.
		gettimeofday( &_ttrip1, &_tz );
		if ( !_pendingTripStarted ){
			gettimeofday( &_ttrip2, &_tz );
			_pendingTripStarted = true;
		}
		double delta_ms = (double) ( _ttrip1.tv_sec - _ttrip2.tv_sec) * 1000 + (double) ( _ttrip1.tv_usec - _ttrip2.tv_usec) / 1000.0;
#if 0
		if ( _key ==  string("crate0_slot0_channel0") ){
			cout << __FILE__ << " " << __LINE__ << " key= " << _key << " pending trip delta= "
					<< delta_ms << " Imon= " << _Imon << " tripIntegrationTime= " << _tripIntegrationTime  << endl;
		}
#endif
		if ( delta_ms > _tripIntegrationTime * 1000 ){
			// the trip stays active until Pw=1 again, we ramp down
			_tripTriggered = true;
#if 0
			if ( _key ==  string("crate0_slot0_channel0") ){
				cout << __FILE__ << " " << __LINE__ << " key= " << _key << " tripped Imon= " << _Imon << endl;
			}
#endif
		}
		break;
	}
	default: {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Channel:sm unknown state " << _SMstate
				<< " switching OFF as emergency. This is a bug." << endl;
		throw std::runtime_error( os.str() );
		break;
	}
	} // switch
	_refreshPower();
	_refreshStatus();
	_updateResultInterface();	// read in api
}


void Channel::pilot_setLoad( float res, float capa ){
	if ( res >= 0 ) __resistance = res;
	if ( capa >= 0 ) __capacitance = capa;
}
void Channel::pilot_getLoad( float *res, float *capa ){
	*res  = __resistance;
	*capa = __capacitance;
}


// depends on _FP_VSEL from the crate. Default is 0=false=V0set
void Channel::pilot_setVtargetFromVSEL( bool f ){
	__VSEL = f;
	if ( f ) {
		__Vtarget = _V1set * __precisionVtargetOffsetFactor + __precisionVtargetOffsetLowValue;
	} else {
		__Vtarget = _V0set * __precisionVtargetOffsetFactor + __precisionVtargetOffsetLowValue;
	}
	if ( __Vtarget < 0 ) __Vtarget = 0.0;
}

void Channel::pilot_setImaxFromISEL( bool f ){
	__ISEL = f;
	if ( f ) {
		__Imax = _I1set;
	} else {
		__Imax = _I0set;
	}
	if ( __Imax < 0 ) __Imax = 0;
}

void Channel::set_TripTime( int32_t s )
{
	if (( s > 0 ) && ( s < __tripInfinite )){
		_tripIntegrationTime = s;
	}
}
void Channel::set_Vramp_up( float s ){
	if (( s > 0 ) && ( s <= CAEN_MAX_RAMP_SPEED ))_Vramp_up = s;
}
void Channel::set_Vramp_down( float s )
{
	if (( s > 0 ) && ( s <= CAEN_MAX_RAMP_SPEED )) _Vramp_down = s;
}

void Channel::setName( string s ){
	const int size = 10;
	char hh[ size ];
	strncpy( hh, _name.c_str(), size );
}

vector<string> Channel::parameterList( void ){
	vector<string> v;
	v.clear();
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		v.push_back( _caenProp_map.at( it->first ).name );
	}
	return( v );
}

void Channel::getParameterHALtype( string p, API::HAL_DATA_TYPE_t *type ){
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		*type = prop.dataType;
	} else {
		*type = API::unknown;
	}
}


void Channel::getParameterValue( string p, string *retval ){
	*retval = " ";
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, int8_t *retval ){
	*retval = 0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, int16_t *retval ){
	*retval = 0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, int32_t *retval ){
	*retval = 0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, uint8_t *retval ){
	*retval = 0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, uint16_t *retval ){
	*retval = 0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, uint32_t *retval ){
	*retval = 0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Channel::getParameterValue( string p, float *retval ){
	*retval = 0.0;
	if ( _testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}

// EASY board model
string Channel::parameterOnstate( string p ){
	if ( _testPropExist( p ) ){
		return ( API::getPropAuxOnstate( _prop_map.at( p ) )); // the auxiliary value of that property (=RemBdName) is named Onstate
	}
	return( "parameter Onstate not found" );
}
// EASY board firmware version, hopefully
string Channel::parameterOffstate( string p ){
	if ( _testPropExist( p ) ){
		return ( API::getPropAuxOffstate( _prop_map.at( p ) ));
	}
	return( "parameter Offstate not found" );
}
float Channel::parameterMinval( string p ){
	if ( _testPropExist( p ) ){
		return ( API::getPropAuxMinvalue( _prop_map.at( p ) ));
	}
	return( 0.0 );
}
float Channel::parameterMaxval( string p ){
	if ( _testPropExist( p ) ){
		return ( API::getPropAuxMaxvalue( _prop_map.at( p ) ));
	}
	return( 0.0 );
}
uint16_t Channel::parameterUnit( string p ){
	_testPropExist( p );
	return ( API::getPropAuxUnit( _prop_map.at( p ) ));
}
int16_t Channel::parameterExp( string p ){
	if ( _testPropExist( p ) ){
		return ( API::getPropAuxExp( _prop_map.at( p ) ));
	}
	return ( 0 );
}
uint16_t Channel::parameterDecimal( string p ){
	if ( _testPropExist( p ) ){
		return ( API::getPropAuxDecimal( _prop_map.at( p ) ));
	}
	return( 0 );
}

void Channel::setParameterValue( string p, string val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_string = val;
	}
}
void Channel::setParameterValue( string p, int8_t val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_int8 = val;
	}
}

void Channel::setParameterOnstate( string p, string val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).attributeProperties.onstate = val;
	}
}
void Channel::setParameterOffstate( string p , string val){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).attributeProperties.offstate = val;
	}
}
void Channel::setParameterValue( string p, int16_t val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_int16 = val;
	}
}
void Channel::setParameterValue( string p, int32_t val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_int32 = val;
	}
}
void Channel::setParameterValue( string p, uint8_t val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_uint8 = val;
	}
}
void Channel::setParameterValue( string p, uint16_t val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_uint16 = val;
	}
}
void Channel::setParameterValue( string p, uint32_t val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_uint32 = val;
	}
}
void Channel::setParameterValue( string p, float val ){
	if ( _testPropExist( p ) ){
		_prop_map.at( p ).value_float = val;
	}
}

// returns a map with all properties which have changed since the last call
map<string,API::HAL_PROPERTY_t> Channel::_allChangedItems( void ){
	map<string,API::HAL_PROPERTY_t> itemsv; itemsv.clear();
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if ( this->_changedItem( it ) ){
			itemsv.insert( std::pair<string,API::HAL_PROPERTY_t>(it->first, it->second) );
			// cout << __FILE__ << " " << __LINE__ << " found changed item= " << it->first << endl;
		}
	}
	return( itemsv );
}

/**
 * the property value is checked if it has changed. If no: false
 * if comparison to previous shows difference, update previous and report true.
 * this works for all properties set and RW and RO alike, not just for sets.
 */

bool Channel::_changedItem( std::map<string,API::HAL_PROPERTY_t>::iterator it ){
	/// \todo make numerical change limit float adjustable through pilot API
	const float limitFloat = 0.01;

#if 0
	// debugging
	cout << __FILE__ << " " << __LINE__ << " _changedItem ? key= " << _key
			<< " item= " << it->first << " type= " << it->second.dataType << endl;
	if ( _key ==  string("crate0_slot0_channel0") && ( it->first == "VMon" )) {
		bool xx = false;
		if ( pow( it->second.value_float - it->second.previous_float, 2 ) > limitFloat ) xx = true;
		cout << __FILE__ << " " << __LINE__ << " _changedItem status0 key= " << _key << " VMon= " << it->second.value_float
				<< " pVMon= " << it->second.previous_float << " pow= "
				<< pow( it->second.value_float - it->second.previous_float, 2 ) << " xx= " << xx << endl;
	}
#endif

	if ( ! it->second.subscribed ) {
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property "
				<< it->first << " is not subscribed to. No change reported." << endl;
		return( false );
	}

	// any channel status change
	if ( it->first == string("Status") ){
		if ( it->second.value_int32 != it->second.previous_int32 ) {
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
	}

	// any channel state machine change
	if ( _SMstate != _SMstate_previous ) return( true );

	// numeric parameters
	switch ( it->second.dataType ){
	case API::treal: {
		if ( abs(it->second.value_float) > 1e-20 ){
			if ( abs(( it->second.value_float - it->second.previous_float )/it->second.value_float) > limitFloat ) {
				it->second.previous_float = it->second.value_float;
				return( true );
			}
		}
		break;
	}
	case API::tint8: {
		if ( it->second.value_int8 != it->second.previous_int8 ){
			it->second.previous_int8 = it->second.value_int8;
			return( true );
		}
		break;
	}
	case API::tint16: {
		if ( it->second.value_int16 != it->second.previous_int16 ){
			it->second.previous_int16 = it->second.value_int16;
			return( true );
		}
		break;
	}
	case API::tint32: {
		if ( it->second.value_int32 != it->second.previous_int32 ){
			it->second.previous_int32 = it->second.value_int32;
			return( true );
		}
		break;
	}
	case API::tuint8: {
		if ( it->second.value_uint8 != it->second.previous_uint8 ){
			it->second.previous_uint8 = it->second.value_uint8;
			return( true );
		}
		break;
	}
	case API::tuint16: {
		if ( it->second.value_uint16 != it->second.previous_uint16 ){
			it->second.previous_uint16 = it->second.value_uint16;
			return( true );
		}
		break;
	}
	case API::tuint32: {
		if ( it->second.value_uint32 != it->second.previous_uint32 ){
			it->second.previous_uint32 = it->second.value_uint32;
			return( true );
		}
		break;
	}
	case API::tstr: {
		// any string change
		if ( it->second.value_string != it->second.previous_string ) {
			it->second.previous_string = it->second.value_string;
			return( true );
		}
		break;
	}
	default:
	{
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property type not found" << endl;
		break;
	}
	}
	return( false );
}

vector<std::string> Channel::showProperties( void ){
	if ( _debug ){
		cout << endl << "=== "<< __FILE__ << " " << __LINE__ << " showProperties channel key= " << _key
		<< " name= "<< _name << " ===" << endl;
	}
	vector<std::string> propv;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		if ( _debug ){
			cout << __FILE__ << " " << __LINE__ << " icrate= " << _my_icrate
			<< " islot= " << _my_islot << " ichannel= " << _channel << " " << it->first << endl;
		}
		propv.push_back( it->first );
	}
	return( propv );
}

void Channel::subscribeParameter( string param, bool flag ){
	if ( _testPropExist( param ) ){
		_prop_map.at( param ).subscribed = flag;
	} else {

	}
}

} /* namespace ns_HAL_Channel */

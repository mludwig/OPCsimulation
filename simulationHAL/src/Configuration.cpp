/*
 * Configuration.cpp
 *
 *  Created on: Oct 21, 2016
 *      Author: mludwig
 */


#include "../include/Configuration.h"


using namespace std;
using namespace xercesc;

namespace ns_HAL_Configuration {

#define NODE_NAME_MAX 127
#define CAEN_NAME_MAX 10

// singleton
Configuration* Configuration::_pInstance = NULL;
/* static */ string Configuration::_configfile = "";

Configuration* Configuration::Instance( void )
{
	if (!_pInstance)   // Only allow one instance of class to be generated.
		_pInstance = new Configuration();
	return _pInstance;
}

Configuration::Configuration() {
	_debug = true;
	_intrinsicUpdates = true;
	_crateConfig_map.clear();
	_currentIndexCrate = 0;
	_currentIndexCrateSlot = 0;
	_currentIndexCrateSlotChannel = 0;
	_padMissingChannels = false;

	// these crates have the default simulated behaviour
	GENERIC_CRATES_t g;
	g.smodel = "SY4527";
	g.imodel = SY4527; // CAENHV_SYSTEM_TYPE_t
	_genericCrateModels.push_back( g );
	g.smodel = "SY5527";
	g.imodel = SY5527;
	_genericCrateModels.push_back( g );

	try {
		XMLPlatformUtils::Initialize();
		_parser = new XercesDOMParser();
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML init error: "	<< message << "\n";
		XMLString::release(&message);
		throw std::runtime_error( os.str() );
	}
}

Configuration::~Configuration() {
	XMLPlatformUtils::Terminate();
}

Crate * Configuration::crate( uint32_t icrate ){
	return ( _crate_map.at( icrate ));
}
Board * Configuration::board( uint32_t icrate, uint32_t islot ){
	return( crate( icrate )->board( islot ) );
}
Channel * Configuration::channel( uint32_t icrate, uint32_t islot, uint32_t ich ){
	return( crate( icrate )->board( islot )->channel( ich ) );
}

/**
 *  deserialize the DOM tree into a config and create the objects
 *  the format is the dump from the OPC-UA server: no board models, but the precise API.
 *  In order to find out what kind of behaviour the engine needs to mimick
 *  the properties per channel/board are analyzed.
 */
int Configuration::read( string filename ){
	Configuration::_configfile = filename;
	if ( ! _fileExists( filename )) {
		cout << __FILE__ << " " << __LINE__ << " file not found : " << filename << endl;
		return(-1);
	}

	_parser->setValidationScheme( XercesDOMParser::Val_Never );
	_parser->setDoNamespaces( false );
	_parser->setDoSchema( false );
	_parser->setLoadExternalDTD( false );

	try {
		cout << __FILE__ << " " << __LINE__ << " ...reading " << filename << endl;
		_parser->parse( filename.c_str() );
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		XMLString::release(&message);
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML parsing error: " << message << "\n";
		throw std::runtime_error( os.str() );
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		XMLString::release(&message);
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML DOM parsing error: " << message << "\n";
		throw std::runtime_error( os.str() );
	}
	catch (...) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces unexpected parsing error ";
		throw std::runtime_error( os.str() );
	}

	cout << __FILE__ << " " << __LINE__ << " ...de-serializing " << filename << endl;

	DOMNode* docRootNode;
	DOMDocument* doc;
	doc = _parser->getDocument();
	docRootNode = doc->getDocumentElement();
	DOMNodeList *rootlist = docRootNode->getChildNodes();

	for ( uint32_t n0 = 0; n0 < rootlist->getLength(); n0++ ){
		char ns0[ NODE_NAME_MAX ] = "";
		XMLString::transcode( rootlist->item( n0 )->getNodeName(), ns0, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " level0= " << ns0 << endl;

		if ( strcmp(ns0, "SYCrate") == 0 ){
			// -----
			// crate
			// -----
			_configureSYCrate( rootlist->item( n0 ) );

			// crate system properties
			DOMNodeList *cratelist = rootlist->item( n0 )->getChildNodes();
			for ( uint32_t n1 = 0; n1 < cratelist->getLength(); n1++ ){
				char ns1[ NODE_NAME_MAX ] = "";
				XMLString::transcode( cratelist->item( n1 )->getNodeName(), ns1, NODE_NAME_MAX );
				//cout << __FILE__ << " " << __LINE__ << " level1= " << ns1 << endl;

				if (( strcmp(ns1, "ReadOnlyProperty") == 0 ) ||
						( strcmp(ns1, "ReadWriteProperty") == 0 ) ||
						( strcmp(ns1, "WriteOnlyProperty") == 0 ) )
				{
					_configureSYCrateProperty( cratelist->item( n1 ) );
				} else if ( strcmp(ns1, "Board") == 0 ) {
					// -----
					// board
					// -----
					_configureBoard( cratelist->item( n1 ) );

					// board properties
					DOMNodeList *boardlist = cratelist->item( n1 )->getChildNodes();

					// cout << __FILE__ << " " << __LINE__ << " found #board-props= " << boardlist->getLength() << endl;

					for ( uint32_t n2 = 0; n2 < boardlist->getLength(); n2++ ){
						char ns2[ NODE_NAME_MAX ] = "";
						XMLString::transcode( boardlist->item( n2 )->getNodeName(), ns2, NODE_NAME_MAX );
						//cout << __FILE__ << " " << __LINE__ << " level2 = " << ns2 << endl;

						if (( strcmp(ns2, "ReadOnlyProperty") == 0 ) ||
								( strcmp(ns2, "ReadWriteProperty")== 0 ) ||
								( strcmp(ns2, "WriteOnlyProperty") == 0 ) )
						{
							_configureBoardProperty( boardlist->item( n2 ) );
						} else if ( strcmp(ns2, "Channel") == 0 ) {
							// -------
							// channel
							// -------
							_configureChannel( boardlist->item( n2 ) );

							// channel properties
							DOMNodeList *channellist = boardlist->item( n2 )->getChildNodes();

							// cout << __FILE__ << " " << __LINE__ << " found #channels= " << channellist->getLength() << endl;

							for ( uint32_t n3 = 0; n3 < channellist->getLength(); n3++ ){
								char ns3[ NODE_NAME_MAX ] = "";
								XMLString::transcode( channellist->item( n3 )->getNodeName(), ns3, NODE_NAME_MAX );
								//cout << __FILE__ << " " << __LINE__ << " n3= " << n3 << endl;
								//cout << __FILE__ << " " << __LINE__ << " level3= " << ns3 << endl;
								if (( strcmp(ns3, "ReadOnlyProperty") == 0 ) ||
										( strcmp(ns3, "ReadWriteProperty") == 0 ) ||
										( strcmp(ns3, "WriteOnlyProperty") == 0 ) )
								{
									_configureChannelProperty( channellist->item( n3 ) );
								}
							}
						}
					}
				}
			}
		}
	} // ...argh ;-)
	cout << __FILE__ << " " << __LINE__ << " configuration " << filename << " read OK" << endl;
	return( 0 );
}

/**
 * read the reference dumps of the SW APIs: need the board type and the firmware version
 * Board properties and channel properties for all existing channels repeated.
 * Therefore the xml format for configuration is different: 2 stages:
 * 1.stage: read an xml with < board-type, firmware >
 * 2.stage: for each of the < board-type, firmware > we read in the reference board dumps (txt files) and build the API
 * 		these reference board dumps have to be in a subdirectory ./referenceBoards
 * 		if the reference dump for the board does not exists, warn and continue
 */
void Configuration::readReferenceConfig( string filename ){

}

void Configuration::readShortHand( string filename ){
	Configuration::_configfile = filename;
	if ( ! _fileExists( filename )) {
		cout << __FILE__ << " " << __LINE__ << " file not found : " << filename << endl;
		return;
	}
	XercesDOMParser* parser = new XercesDOMParser();
	parser->setValidationScheme( XercesDOMParser::Val_Never );
	parser->setDoNamespaces( false );
	parser->setDoSchema( false );
	parser->setLoadExternalDTD( false );
	try {
		cout << __FILE__ << " " << __LINE__ << " ...reading " << filename << endl;
		parser->parse( filename.c_str() );
	}
	catch (const XMLException& toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		XMLString::release(&message);
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML parsing error: " << message << "\n";
		throw std::runtime_error( os.str() );
	}
	catch (const DOMException& toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		XMLString::release(&message);
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces XML DOM parsing error: " << message << "\n";
		throw std::runtime_error( os.str() );
	}
	catch (...) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " xerces unexpected parsing error ";
		throw std::runtime_error( os.str() );
	}

	cout << __FILE__ << " " << __LINE__ << " ...de-serializing shorthand " << filename << endl;

	DOMNode* docRootNode;
	DOMDocument* doc;
	doc = parser->getDocument();
	docRootNode = doc->getDocumentElement();
	DOMNodeList *rootlist = docRootNode->getChildNodes();

	for ( uint32_t n0 = 0; n0 < rootlist->getLength(); n0++ ){
		char ns0[ NODE_NAME_MAX ] = "";
		XMLString::transcode( rootlist->item( n0 )->getNodeName(), ns0, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " level0= " << ns0 << endl;

		if ( strcmp(ns0, "SYCrate") == 0 ){
			/**
			 * crate + the properties according to crate type
			 */
			_configureSYCrateShortHand( rootlist->item( n0 ) );

			// children of crate
			DOMNodeList *cratelist = rootlist->item( n0 )->getChildNodes();
			for ( uint32_t n1 = 0; n1 < cratelist->getLength(); n1++ ){
				char ns1[ NODE_NAME_MAX ] = "";
				XMLString::transcode( cratelist->item( n1 )->getNodeName(), ns1, NODE_NAME_MAX );
				//cout << __FILE__ << " " << __LINE__ << " level1= " << ns1 << endl;

				if ( strcmp(ns1, "Board") == 0 ) {

					/**
					 * board + the properties according to each board type.
					 * we broadly have 4 types: easy HV, easy LV, standard HV, standard LV
					 * since we know the type we also configure the channels for standard boards
					 */
					_configureBoardShortHand( cratelist->item( n1 ) );

					/**
					 * children of the board:
					 * we don't need properties because we have the board type already
					 * we don't need channels because the standard board types have also fixed nb and type of channels
					 * we might have EASYBoards for BC boards, and they have also types which determines their channels
					 *
					 * in fact you can only have:
					 *     <EasyBoard name="EasyBoard00" slot="0" type="A3100" firmware="1.0.1" description="A3100_e12_0"></EasyBoard>
					 */
					DOMNodeList *boardlist = cratelist->item( n1 )->getChildNodes();

					// cout << __FILE__ << " " << __LINE__ << " found #board-props= " << boardlist->getLength() << endl;

					for ( uint32_t n2 = 0; n2 < boardlist->getLength(); n2++ ){
						char ns2[ NODE_NAME_MAX ] = "";
						XMLString::transcode( boardlist->item( n2 )->getNodeName(), ns2, NODE_NAME_MAX );
						//cout << __FILE__ << " " << __LINE__ << " level2 = " << ns2 << endl;

						if ( strcmp(ns2, "EasyBoard") == 0 ) {
							_configureEasyBoard( boardlist->item( n2 ) );
						}
					}
				}
			}
		}
	}
	cout << __FILE__ << " " << __LINE__ << " shorthand configuration " << filename << " read OK" << endl;
}

inline bool Configuration::_fileExists( const std::string& name ) {
	if ( FILE *file = fopen(name.c_str(), "r")) {
		fclose(file);
		return true;
	} else {
		return false;
	}
}

void Configuration::_configureSYCrate( DOMNode  *crate_node ){
	// <SYCrate ipAddress="127.0.0.11" model="SY4527" name="CRATE0">
	char cname[ NODE_NAME_MAX ];
	XMLString::transcode( crate_node->getNodeName(), cname, NODE_NAME_MAX );
	if ( _debug ){
		cout << __FILE__ << " " << __LINE__ << " _configureSYCrate " << cname << endl;
		cout << __FILE__ << " " << __LINE__ << " type= " << crate_node->getNodeType() << endl;
	}

	// add another crate
	_currentIndexCrate = _crateConfig_map.size();
	_crateConfig_map.insert( std::pair<int, CrateConfig *>(_currentIndexCrate, new CrateConfig() ));
	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	// crate id
	DOMNamedNodeMap *attributes = crate_node->getAttributes();// 1 = ELEMENT_NODE
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char ns[ NODE_NAME_MAX ] = "";
		char sval[ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), ns, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl;
		if ( strcmp(ns, "address") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			crate->setipAddress( sval );
		} else if ( strcmp(ns, "model") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			crate->setmodel( sval );
		} else if ( strcmp(ns, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			crate->setname(  sval );
		}
	}
	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureSYCrate added crate "
				<< "{" <<	_currentIndexCrate << "} "
				<< crate->model()
				<< " " << crate->name()
				<< " " << crate->ipAddress()
				<< endl;
	}
	if ( crate->ipAddress().length() < 7 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Crate does not have IP-Address [" << crate->ipAddress() << "]: check config for field \"address\" ";
		throw std::runtime_error( os.str() );
	}
	if ( crate->model().length() < 1 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Crate does not have model [" << crate->model() << "]: check config for field \"model\" ";
		throw std::runtime_error( os.str() );
	}
	if ( crate->name().length() < 3 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Crate does not have namel [" << crate->name() << "]: check config for field \"name\" ";
		throw std::runtime_error( os.str() );
	}
}

void Configuration::_configureSYCrateShortHand( DOMNode  *crate_node ){
	// <SYCrate ipAddress="127.0.0.11" model="SY4527" name="CRATE0">
	char cname[ NODE_NAME_MAX ];
	XMLString::transcode( crate_node->getNodeName(), cname, NODE_NAME_MAX );
	if ( _debug ){
		cout << __FILE__ << " " << __LINE__ << " _configureSYCrate " << cname << endl;
		cout << __FILE__ << " " << __LINE__ << " type= " << crate_node->getNodeType() << endl;
	}

	// add another crate
	_currentIndexCrate = _crateConfig_map.size();
	_crateConfig_map.insert( std::pair<int, CrateConfig *>(_currentIndexCrate, new CrateConfig() ));
	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	// crate id
	DOMNamedNodeMap *attributes = crate_node->getAttributes();// 1 = ELEMENT_NODE
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char ns[ NODE_NAME_MAX ] = "";
		char sval[ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), ns, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl;
		if ( strcmp(ns, "address") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			crate->setipAddress( sval );
		} else if ( strcmp(ns, "model") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			crate->setmodel( sval );
		} else if ( strcmp(ns, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			crate->setname(  sval );
		}
	}
	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureSYCrate added crate "
				<< "{" <<	_currentIndexCrate << "} "
				<< crate->model()
				<< " " << crate->name()
				<< " " << crate->ipAddress()
				<< endl;
	}
	if ( crate->ipAddress().length() < 7 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Crate does not have IP-Address [" << crate->ipAddress() << "]: check config for field \"address\" ";
		throw std::runtime_error( os.str() );
	}
	if ( crate->model().length() < 1 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Crate does not have model [" << crate->model() << "]: check config for field \"model\" ";
		throw std::runtime_error( os.str() );
	}
	if ( crate->name().length() < 3 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " Crate does not have name [" << crate->name() << "]: check config for field \"name\" ";
		throw std::runtime_error( os.str() );
	}

	// add the crate properties according to the crate type
	// <ReadOnlyProperty dataType="S" name="CPULoad" propertyName="CPULoad"/>
	if (( crate->model() == "SY1527")
			|| ( crate->model() == "SY2527")
			|| ( crate->model() == "SY3527")
			|| ( crate->model() == "SY4527")
			|| ( crate->model() == "SY5527") ){
		_configureSYX527PropertiesShortHand( crate );
	} else {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " could not find and configure crate model " << crate->model();
		throw std::runtime_error( os.str() );
	}
}

void Configuration::_configureSYX527PropertiesShortHand( CrateConfig *crate ){
	APIConfig::PROPERTY_t p;
	p.pDataType = APIConfig::S;
	p.pAccessType = APIConfig::ReadOnlyProperty;
	vector<string> pn;
	pn.push_back( "CPULoad" );
	pn.push_back( "HVFanStat" );
	pn.push_back( "HvPwSM");
	pn.push_back( "MemoryStatus");
	pn.push_back( "ModelName");
	pn.push_back( "PWFanStat");
	pn.push_back( "PWVoltage");
	pn.push_back( "Sessions");
	pn.push_back( "SwRelease");
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		crate->crateAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	pn.clear();
	p.pDataType = APIConfig::I;
	p.pAccessType = APIConfig::ReadOnlyProperty;
	pn.push_back( "ClkFreq" );
	pn.push_back( "CmdQueueStatus" );
	pn.push_back( "FrontPanIn" );
	pn.push_back( "FrontPanOut" );
	pn.push_back( "NbSlots" );
	pn.push_back( "ResFlag" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		crate->crateAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	pn.clear();
	p.pAccessType = APIConfig::ReadWriteProperty;
	p.pDataType = APIConfig::I;
	pn.push_back( "CMDExecMode" );
	pn.push_back( "DummyReg" );
	pn.push_back( "GenSignCfg" );
	pn.push_back( "ResFlagCfg" );
	pn.push_back( "HVFanSpeed" );
	pn.push_back( "OutputLevel" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		crate->crateAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	pn.clear();
	p.pAccessType = APIConfig::ReadWriteProperty;
	p.pDataType = APIConfig::S;
	pn.push_back( "HVClkConf" );
	pn.push_back( "IPAddr" );
	pn.push_back( "IPGw" );
	pn.push_back( "IPNetMsk" );
	pn.push_back( "PWCurrent" );
	pn.push_back( "SymbolicName" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		crate->crateAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}
}


void Configuration::_configureSYCrateProperty( DOMNode *sysprop_node ){
	APIConfig::PROPERTY_t property;
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( sysprop_node->getNodeName(), ns, NODE_NAME_MAX );
	// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl;

	property.pAccessType = APIConfig::ReadOnlyProperty; // silly default
	if ( strcmp( ns, "ReadOnlyProperty") == 0 ){
		property.pAccessType = APIConfig::ReadOnlyProperty;
	} else if ( strcmp( ns, "ReadWriteProperty") == 0 ){
		property.pAccessType = APIConfig::ReadWriteProperty;
	} else if ( strcmp( ns, "WriteOnlyProperty") == 0 ){
		property.pAccessType = APIConfig::WriteOnlyProperty;
	}

	uint32_t icrate = _crateConfig_map.size() - 1; // already created
	CrateConfig *crate = _crateConfig_map.at( icrate );

	crate->crateAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>(ns, property ));
	APIConfig::PROPERTY_t prop = crate->crateAPI_map.at( ns );

	DOMNamedNodeMap *attributes = sysprop_node->getAttributes();
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char ns[ NODE_NAME_MAX ] = "";
		char sval[ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), ns, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " attr name= " << ns << endl;

		if ( strcmp(ns, "dataType") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			property.pDataType = APIConfig::S;
			if ( strcmp( sval, "S") == 0 ){
				property.pDataType = APIConfig::S;
			} else if ( strcmp( sval, "I") == 0 ){
				property.pDataType = APIConfig::I;
			} else if ( strcmp( sval, "F") == 0 ){
				property.pDataType = APIConfig::F;
			}
		} else if ( strcmp(ns, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			property.name = string( sval );
		} else if ( strcmp(ns, "propertyName") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			property.pname = string( sval );
		}
	}
	crate->crateAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( property.name, property ));
	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureSYCrateProperty added crate property: "
				<< "{ " <<	_currentIndexCrate << " } "
				<< " name= " << property.name << " access type= " << property.pAccessType << " data type= " << property.pDataType << endl;
	}
}


/**
 * configure board properties based on the type of the board only. Branch controllers are also boards.
 * board type == model == is optional and defaults to GENERIC
 * description is optional and defaults to "generic description"
 * slot is mandatory
 * name is mandatory
 */
void Configuration::_configureBoardShortHand( DOMNode *board_node ){
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( board_node->getNodeName(), ns, NODE_NAME_MAX );
	// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl; // Board

	bool haveType = false;
	bool haveDesc = false;
	bool haveName = false;
	bool haveSlot = false;
	bool haveFirmware = false;

	char bname[ NODE_NAME_MAX ] = "";
	char bmodel[ NODE_NAME_MAX ] = "";
	char bfirmware[ NODE_NAME_MAX ] = "";
	char bslot[ NODE_NAME_MAX ] = "";
	char bdescription[ CAEN_DESC_MAX ] = "";
	DOMNamedNodeMap *attributes = board_node->getAttributes();
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char bn [ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), bn, NODE_NAME_MAX );
		if ( strcmp(bn, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bname, NODE_NAME_MAX );
			haveName = true;
		} else if ( strcmp(bn, "slot") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bslot, NODE_NAME_MAX );
			haveSlot = true;
		} else if ( strcmp(bn, "type") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bmodel, NODE_NAME_MAX );
			haveType = true;
		} else if ( strcmp(bn, "firmware") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bfirmware, NODE_NAME_MAX );
			haveFirmware = true;
		} else if ( strcmp(bn, "description") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bdescription, NODE_NAME_MAX );
			haveDesc = true;
		}
	}
	if ( !haveType ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoardShortHand: name= " << bname
				<< " slot= " << bslot
				<< " bdescription= " << bdescription
				<< " no type found, assuming generic" << endl;
		sprintf( bmodel, "GENERIC" );
	}
	if ( !haveFirmware ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoardShortHand: no firmware version found, assuming unknown" << endl;
		sprintf( bfirmware, "unknown" );
	}
	if ( !haveDesc ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoardShortHand: no description found, assuming generic" << endl;
		sprintf( bdescription, "generic description" );
	}
	if ( !haveName ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _configureBoardShortHand: ERROR no board name found" << endl;
		throw std::runtime_error( os.str() );
	}
	if ( !haveSlot ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _configureBoardShortHand: ERROR no board slot found" << endl;
		throw std::runtime_error( os.str() );
	}

	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureBoardShortHand bname= " << bname
				<< " slot= " << bslot
				<< " model= " << bmodel
				<< " firmware= " << bfirmware
				<< " description= " << bdescription << endl;
	}

	// add another board to the current crate, at the populated slot
	sscanf( bslot, "%d", &_currentIndexCrateSlot );
	char key[ NODE_NAME_MAX ] = "";
	sprintf( key, "crate=%d slot=%d", _currentIndexCrate, _currentIndexCrateSlot );
	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	// crate->boardConfig_map.insert( std::pair<uint32_t, BoardConfig *>(_currentIndexCrateSlot, new BoardConfig( bname, bmodel, _currentIndexCrateSlot, key )));
	crate->boardConfig_map.insert( std::pair<uint32_t, BoardConfig *>(_currentIndexCrateSlot, new BoardConfig( bname, bmodel, bfirmware, _currentIndexCrateSlot, key )));
	crate->addPopulatedSlot( _currentIndexCrateSlot );

	BoardConfig *board = crate->boardConfig_map.at( _currentIndexCrateSlot );
	board->setDescription( bdescription );

	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureBoardShortHand added board: "
				<< " crate= " <<	_currentIndexCrate
				<< " slot= " << _currentIndexCrateSlot
				<< " name= " << board->name()
				<< " slot= " << board->slot()
				<< " model= " << board->model()
				<< " description= " << board->description()
				<< " key= " << board->key()
				<< endl;
	}

	// add the board properties according to the type
	//	   <Board name="Board00" slot="0" type="A1676" description="BC0">
	//	   <Board name="Board03" slot="3" type="A2518" description="A2518_0">

	/// \todo the SW API for each {board type, firmware version} should come from the reference dumps at
	///  https://repository.cern.ch/nexus/content/repositories/cern-venus/caen/discoveredCAENHardware/
	///  and take therefore also the firmware version into account
	if (
			   ( board->model() == "A1510")
			|| ( board->model() == "A1519")
			|| ( board->model() == "A1515QG")
			|| ( board->model() == "A1535")
			|| ( board->model() == "A1540")
			|| ( board->model() == "A1590")
			|| ( board->model() == "A1734")
			|| ( board->model() == "A1735")
			|| ( board->model() == "A1821")
			|| ( board->model() == "A1821HN")
			|| ( board->model() == "A1832")
			|| ( board->model() == "A1833")

			// low volt
			|| ( board->model() == "A1519")
			|| ( board->model() == "A1513B")
			|| ( board->model() == "A1516B")
			|| ( board->model() == "A1517B")
			|| ( board->model() == "A1518B")
			|| ( board->model() == "A2517")
			|| ( board->model() == "A2518")
			|| ( board->model() == "A2519")){
		// standard high volt and standard low voltage: the same
		// not entirely true
		_configureBoardStandardPropertiesShortHand( board );

	} else if (( board->model() == "A1676") || ( board->model() == "A1676A")){
		// branch controller for EASY crates
		_configureBoardBranchControllerPropertiesShortHand( board );
	} else {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " could not find and configure board model " << board->model();
		throw std::runtime_error( os.str() );
	}
}


void Configuration::_configureBoardStandardPropertiesShortHand( BoardConfig *board ){
	APIConfig::PROPERTY_t p;
	p.pDataType = APIConfig::I;
	p.pAccessType = APIConfig::ReadOnlyProperty;
	vector<string> pn;
	pn.push_back( "BdStatus" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		board->boardAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}
	p.pDataType = APIConfig::F;
	pn.clear();
	pn.push_back( "HVMax" );
	pn.push_back( "Temp" );
	pn.push_back( "VMax" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		board->boardAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	// standard boards also have a known number of standard channels according to board type,
	// but in fact the board constructor takes care of that when populating

}

void Configuration::_configureBoardBranchControllerPropertiesShortHand( BoardConfig *board ){
	vector<string> pn;
	APIConfig::PROPERTY_t p;

	p.pDataType = APIConfig::I;
	p.pAccessType = APIConfig::ReadOnlyProperty;
	pn.push_back( "BdStatus" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		board->boardAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	pn.clear();
	p.pDataType = APIConfig::F;
	p.pAccessType = APIConfig::ReadOnlyProperty;
	pn.push_back( "1676Ilk" );
	pn.push_back( "A1676Ilk" );
	pn.push_back( "HVMax" );
	pn.push_back( "Temp" );
	pn.push_back( "VMax" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		board->boardAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	pn.clear();
	p.pDataType = APIConfig::F;
	p.pAccessType = APIConfig::ReadWriteProperty;
	pn.push_back( "48VA1676" );
	pn.push_back( "GlobalOff" );
	pn.push_back( "GlobalOn" );
	pn.push_back( "HW Reset0" );
	pn.push_back( "HW Reset1" );
	pn.push_back( "HW Reset2" );
	pn.push_back( "HW Reset3" );
	pn.push_back( "HW Reset4" );
	pn.push_back( "HW Reset5" );
	pn.push_back( "Recovery0" );
	pn.push_back( "Recovery1" );
	pn.push_back( "Recovery2" );
	pn.push_back( "Recovery3" );
	pn.push_back( "Recovery4" );
	pn.push_back( "Recovery5" );
	pn.push_back( "VA1676" );
	pn.push_back( "Vsel" );
	for ( unsigned int i = 0; i < pn.size(); i++ ){
		p.pname = pn[ i ]; p.name = pn[ i ];
		board->boardAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( p.name, p ));
	}

	/**
	 * branch controllers have sub channels which are Easy boards of known type
	 * we are starting a new BC in a new slot here, so reatsrt counting at 0
	 */
	_currentIndexCrateSlotChannel = 0;
}

/**
 * try to find out the board model, or at least the supposed behaviour and structure,
 * by looking at the channels of that board
 * either "A1676" branch controller or "generic", meaning primary power supply board
 */
string Configuration::_classifyBoardByProperties( DOMNode *board_node ){
	bool found = false;
	string boardType = "generic";
	DOMNamedNodeMap *attributes = board_node->getAttributes();
	// if it has a type, return it
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char an [ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), an, NODE_NAME_MAX );
		//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " board attribute an = " << an << endl;

		if ( strcmp(an, "type") == 0 ){
			char aname[ NODE_NAME_MAX ] = "";
			XMLString::transcode( attributes->item( n )->getTextContent(), aname, NODE_NAME_MAX );
			//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " board type= " << aname << endl;
			boardType = aname;
			found = true;
		}
	}
	if ( found ) {
		cout << __FILE__ << " " << __LINE__ << " (attribute) boardType= " << boardType << endl;	exit(0);
		return( boardType );
	} else {
		cout << __FILE__ << " " << __LINE__ << " board type not specified as attribute, inspecting channels " << endl;
	}

	// board nodes
	DOMNodeList *boardlist = board_node->getChildNodes();
	for ( uint32_t n0 = 0; n0 < boardlist->getLength(); n0++ ){
		char ns0[ NODE_NAME_MAX ] = "";
		XMLString::transcode( boardlist->item( n0 )->getNodeName(), ns0, NODE_NAME_MAX );
		//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " level0 = " << ns0 << endl;

		if (( strcmp(ns0, "ReadOnlyProperty") == 0 ) ||
				( strcmp(ns0, "ReadWriteProperty")== 0 ) ||
				( strcmp(ns0, "WriteOnlyProperty") == 0 ) )
		{
			// cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " board property level0 = " << ns0 << endl;
		} else if ( strcmp(ns0, "Channel") == 0 ) {
			//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " channel level0 = " << ns0 << endl;

			DOMNodeList *channellist = boardlist->item( n0 )->getChildNodes();
			for ( uint32_t n1 = 0; n1 < channellist->getLength(); n1++ ){
				char ns1[ NODE_NAME_MAX ] = "";
				XMLString::transcode( channellist->item( n1 )->getNodeName(), ns1, NODE_NAME_MAX );
				//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " level1 = " << ns1 << endl;


				if (( strcmp(ns1, "ReadOnlyProperty") == 0 ) ||
						( strcmp(ns1, "ReadWriteProperty") == 0 ) ||
						( strcmp(ns1, "WriteOnlyProperty") == 0 )) {

					// cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property level1 = " << ns1 << endl;
					DOMNode *channel_property_node = channellist->item( n1 );
					DOMNamedNodeMap *attributes = channel_property_node->getAttributes();
					for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
						char an [ NODE_NAME_MAX ] = "";
						XMLString::transcode( attributes->item( n )->getNodeName(), an, NODE_NAME_MAX );
						// cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " level1 an = " << an << endl;

						if ( strcmp(an, "name") == 0 ){
							char aname[ NODE_NAME_MAX ] = "";
							XMLString::transcode( attributes->item( n )->getTextContent(), aname, NODE_NAME_MAX );
							//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " channel" << n0
							//		<< " property" << n1 << " name= " << aname << endl;

							/** now, just look at one single property of the first channel
							 * if it has "Temp" it is a marker channel and the board is a BC
							 * if it has V0Set this is a power supply channel and the board is primary
							 */
							if ( strcmp( aname, "Temp") == 0 ){
								cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
										<< " found channel property Temp, this is a marker channel. Therefore the board is a BC." << endl;
								cout << __FILE__ << " " << __LINE__ << " boardType= A1676" << endl;
								return( "A1676" );
							} else if( strcmp( aname, "V0Set") == 0 ){
								cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
										<< " found channel property V0Set, this is a power supply board." << endl;
								cout << __FILE__ << " " << __LINE__ << " boardType= generic" << endl;
								return( "generic" );
							}
						}
					}
				}
			}
		}
	}
	// board could not be identified as generic (ramping with VSEL/ISEL) or branch controller.
	cout << __FILE__ << " " << __LINE__ << " boardType= " << boardType << endl;
	return( boardType );
}


/**
 * by looking at the channel properties, find out how the channel should behave:
 * marker channel EASY
 * high voltage ramping VSEL/ISEL primary board
 * low voltage simple ramping only primary board
 * high voltage ramping VSEL/ISEL EASY
 * low voltage simple ramping only EASY
 * return:
 * typedef enum { ...  } CHANNEL_TYPES_t;
 */
string Configuration::_classifyChannelByProperties( DOMNode *channel_node ){
	string channelType = "standardHighVoltChannel";
	vector<string> propertiesv;

	DOMNodeList *channel_property_list = channel_node->getChildNodes();
	for ( uint32_t n0 = 0; n0 < channel_property_list->getLength(); n0++ ){
		char ns0[ NODE_NAME_MAX ] = "";
		XMLString::transcode( channel_property_list->item( n0 )->getNodeName(), ns0, NODE_NAME_MAX );
		//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " level0 = " << ns0 << endl;

		if (( strcmp(ns0, "ReadOnlyProperty") == 0 ) ||
				( strcmp(ns0, "ReadWriteProperty") == 0 ) ||
				( strcmp(ns0, "WriteOnlyProperty") == 0 )) {

			//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property level0 = " << ns0 << endl;

			DOMNode *property_node = channel_property_list->item( n0 );
			DOMNamedNodeMap *attributes = property_node->getAttributes();
			for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
				char an [ NODE_NAME_MAX ] = "";
				XMLString::transcode( attributes->item( n )->getNodeName(), an, NODE_NAME_MAX );
				//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " level1 an = " << an << endl;

				if ( strcmp(an, "name") == 0 ){
					char aname[ NODE_NAME_MAX ] = "";
					XMLString::transcode( attributes->item( n )->getTextContent(), aname, NODE_NAME_MAX );
					//cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
					//		<< " property" << n0 << " name= " << aname << endl;
					propertiesv.push_back( aname );
				}
			}
		}
	}

	/**
	 * now, look at properties of this channel
	 *  have collected all properties from this channel, should be able to decide now what it should be
	 */
	if ( std::find(propertiesv.begin(), propertiesv.end(), "Temp") != propertiesv.end() ){
		channelType = "easyBoardMarkerChannel";
	} else if (( std::find(propertiesv.begin(), propertiesv.end(), "V0Set")    != propertiesv.end() ) &&
			   ( std::find(propertiesv.begin(), propertiesv.end(), "V1Set")    != propertiesv.end() ) &&
			  !( std::find(propertiesv.begin(), propertiesv.end(), "GlbOffEn") != propertiesv.end())){
		channelType = "standardHighVoltChannel";
	} else if (( std::find(propertiesv.begin(), propertiesv.end(), "V0Set")    != propertiesv.end() ) &&
			  !( std::find(propertiesv.begin(), propertiesv.end(), "V1Set")    != propertiesv.end() ) &&
			  !( std::find(propertiesv.begin(), propertiesv.end(), "GlbOffEn") != propertiesv.end()) ){
		channelType = "standardLowVoltChannel";
	} else if (( std::find(propertiesv.begin(), propertiesv.end(), "V0Set")    != propertiesv.end() ) &&
			  !( std::find(propertiesv.begin(), propertiesv.end(), "V1Set")    != propertiesv.end() ) &&
			  !( std::find(propertiesv.begin(), propertiesv.end(), "OutReg")   != propertiesv.end() ) &&
			   ( std::find(propertiesv.begin(), propertiesv.end(), "GlbOffEn") != propertiesv.end() )){
		channelType = "easyLowVoltChannelv1";
	} else if (( std::find(propertiesv.begin(), propertiesv.end(), "V0Set")    != propertiesv.end() ) &&
			  !( std::find(propertiesv.begin(), propertiesv.end(), "V1Set")    != propertiesv.end() ) &&
			   ( std::find(propertiesv.begin(), propertiesv.end(), "GlbOffEn") != propertiesv.end() ) &&
		       ( std::find(propertiesv.begin(), propertiesv.end(), "OutReg") != propertiesv.end() )){
		channelType = "easyLowVoltChannelv2";
	} else if (( std::find(propertiesv.begin(), propertiesv.end(), "V0Set")    != propertiesv.end() ) &&
			   ( std::find(propertiesv.begin(), propertiesv.end(), "V1Set")    != propertiesv.end() ) &&
			   ( std::find(propertiesv.begin(), propertiesv.end(), "GlbOffEn") != propertiesv.end() )){
		channelType = "easyHighVoltChannel";
	}

	if (_debug ){
		for ( unsigned int i = 0; i < propertiesv.size(); i++ ){
			cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " i= " << i << " prop= " << propertiesv[ i ] << endl;
		}
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " channelType= " << channelType << endl;
	}
	return( channelType );
}

void Configuration::_configureBoard( DOMNode *board_node ){
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( board_node->getNodeName(), ns, NODE_NAME_MAX );
	// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl; // Board

	bool haveType = false;
	bool haveDesc = false;
	bool haveName = false;
	bool haveSlot = false;

	char bname[ NODE_NAME_MAX ] = "";
	char bmodel[ NODE_NAME_MAX ] = "";
	char bslot[ NODE_NAME_MAX ] = "";
	char bdescription[ CAEN_DESC_MAX ] = "";
	DOMNamedNodeMap *attributes = board_node->getAttributes();
	// <Board name="Board00" description="N/A" type="GENERIC" slot="0">
	// <Board name="sim_A1832" slot="0" type="A1832" description=" 12 Ch Pos. 6KV 1mA  ">
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char bn [ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), bn, NODE_NAME_MAX );
		if ( strcmp(bn, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bname, NODE_NAME_MAX );
			haveName = true;
		} else if ( strcmp(bn, "slot") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), bslot, NODE_NAME_MAX );
			haveSlot = true;
		} else if ( strcmp(bn, "type") == 0 ){
			// XMLString::transcode( attributes->item( n )->getTextContent(), bmodel, NAME_MAX );
			XMLString::transcode( attributes->item( n )->getTextContent(), bmodel, NODE_NAME_MAX );
			haveType = true;
		} else if ( strcmp(bn, "description") == 0 ){
			// XMLString::transcode( attributes->item( n )->getTextContent(), bdescription, CAEN_DESC_MAX );
			XMLString::transcode( attributes->item( n )->getTextContent(), bdescription, NODE_NAME_MAX );
			haveDesc = true;
		}
	}

	string foundModel = _classifyBoardByProperties( board_node );
	sprintf( bmodel, foundModel.c_str() );
	haveType = true;

	if ( !haveType ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoard: name= " << bname
				<< " slot= " << bslot
				<< " bdescription= " << bdescription
				<< " no type found, assuming generic" << endl;
		sprintf( bmodel, "generic" );
	}
	if ( !haveDesc ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoard: no description found, assuming generic" << endl;
		sprintf( bdescription, "generic description" );
	}
	if ( !haveName ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoard: ERROR no board name found" << endl;
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _configureBoard: ERROR no board name found" << endl;
		throw std::runtime_error( os.str() );
	}
	if ( !haveSlot ){
		cout << __FILE__ << " " << __LINE__ << " _configureBoard: ERROR no board slot found" << endl;
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _configureBoard: ERROR no board slot found" << endl;
		throw std::runtime_error( os.str() );
	}

	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureBoard bname= " << bname
				<< " slot= " << bslot
				<< " model= " << bmodel
				<< " description= " << bdescription << endl;
	}

	// add another board to the current crate, at the populated slot
	sscanf( bslot, "%d", &_currentIndexCrateSlot );
	char key[ NODE_NAME_MAX ] = "";
	sprintf( key, "crate=%d slot=%d", _currentIndexCrate, _currentIndexCrateSlot );
	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	crate->boardConfig_map.insert( std::pair<uint32_t, BoardConfig *>(_currentIndexCrateSlot, new BoardConfig( bname, bmodel, _currentIndexCrateSlot, key )));
	crate->addPopulatedSlot( _currentIndexCrateSlot );

	BoardConfig *board = crate->boardConfig_map.at( _currentIndexCrateSlot );
	board->setDescription( bdescription );


	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureBoard added board: "
				<< " crate= " <<	_currentIndexCrate
				<< " slot= " << _currentIndexCrateSlot
				<< " name= " << board->name()
				<< " slot= " << board->slot()
				<< " model= " << board->model()
				<< " description= " << board->description()
				<< " key= " << board->key()
				<< endl;
	}
}

void Configuration::_configureBoardProperty( DOMNode *boardprop_node ){
	APIConfig::PROPERTY_t cr;
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( boardprop_node->getNodeName(), ns, NODE_NAME_MAX );
	// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl;

	cr.pAccessType = APIConfig::ReadOnlyProperty; // silly default
	if ( strcmp( ns, "ReadOnlyProperty") == 0 ){
		cr.pAccessType = APIConfig::ReadOnlyProperty;
	} else if ( strcmp( ns, "ReadWriteProperty") == 0 ){
		cr.pAccessType = APIConfig::ReadWriteProperty;
	} else if ( strcmp( ns, "WriteOnlyProperty") == 0 ){
		cr.pAccessType = APIConfig::WriteOnlyProperty;
	}

	DOMNamedNodeMap *attributes = boardprop_node->getAttributes();
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char ns[ NAME_MAX ] = "";
		char sval[ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), ns, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " attr name= " << ns << endl;

		if ( strcmp(ns, "dataType") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			cr.pDataType = APIConfig::S;
			if ( strcmp( sval, "S") == 0 ){
				cr.pDataType = APIConfig::S;
			} else if ( strcmp( sval, "I") == 0 ){
				cr.pDataType = APIConfig::I;
			} else if ( strcmp( sval, "F") == 0 ){
				cr.pDataType = APIConfig::F;
			}
		} else if ( strcmp(ns, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, CAEN_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			cr.name = string( sval );
		} else if ( strcmp(ns, "propertyName") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, CAEN_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			cr.pname = string( sval );
		}
	}

	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	BoardConfig *board = crate->boardConfig_map.at( _currentIndexCrateSlot );
	board->boardAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>(ns, cr ));
	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureBoardProperty added board property "
				<< "{" <<	_currentIndexCrate << ", "
				<<	_currentIndexCrateSlot << "} "
				<< " name= " << cr.name << " access type= " << cr.pAccessType << " data type= " << cr.pDataType << endl;
	}
}

/**
 * we know the type of the easy board, therefore we can add all it's channels,
 * including the leading marker channel, to the config. The Easy board itself
 * does not show up as a board, it just shows up as the marker channel.
 * name and type are obligatory, slot and description not.
 *
 *   <EasyBoard name="EasyBoard03" slot="0" type="A3100" description="A3100_e11_3"></EasyBoard>
 */
void Configuration::_configureEasyBoard( DOMNode *channel_node ){
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( channel_node->getNodeName(), ns, NODE_NAME_MAX );
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " node= " << ns << endl; // // EasyBoard of type...

	bool haveSlot = false;
	bool haveName = false;
	bool haveType = false;
	bool haveDescription = false;

	char cslot[ NODE_NAME_MAX ] = "";
	char cname[ NODE_NAME_MAX ] = "";
	char ctype[ NODE_NAME_MAX ] = "";
	char cdescription[ NODE_NAME_MAX ] = "";
	DOMNamedNodeMap *attributes = channel_node->getAttributes();
	// cout << __FILE__ << " " << __LINE__ << " _configureChannel: #attributes= " << attributes->getLength() << endl;
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char bn [ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), bn, NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " _configureChannel: attribute= " << bn << endl;
		if ( strcmp(bn, "slot") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), cslot, NODE_NAME_MAX );
			haveSlot = true;
		} else if ( strcmp(bn, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), cname, CAEN_NAME_MAX );
			haveName = true;
		} else if ( strcmp(bn, "type") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), ctype, CAEN_NAME_MAX );
			haveType = true;
		} else if ( strcmp(bn, "description") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), cdescription, CAEN_NAME_MAX );
			haveDescription = true;
		}
	}

	//cout << __FILE__ << " " << __LINE__ << " cname= " << cname << " ctype= " << ctype << endl;

	if ( !haveName ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERROR no board name found" << endl;
		throw std::runtime_error( os.str() );
	}
	if ( !haveType ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ERROR no board type found" << endl;
		throw std::runtime_error( os.str() );
	}
	if ( !haveDescription && !haveSlot && _debug ){
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " no EASY board slot or description present, using generic"
				<< " type= " << ctype
				<< " cname= " << cname << endl;
	}

	// in fact we add up all easy channels to the same BC and do not
	// restart counting with each new EasyBoard
	char key[ NODE_NAME_MAX ] = "";
	sprintf( key, "crate=%d slot=%d channel=%d", _currentIndexCrate, _currentIndexCrateSlot, _currentIndexCrateSlotChannel );
	CrateConfig *cfgcrate = _crateConfig_map.at( _currentIndexCrate );
	BoardConfig *cfgboard = cfgcrate->boardConfig_map.at ( _currentIndexCrateSlot ); // that's the branch controller

	/**
	 * add EASY channels according to board type. We have to add all the channels explicitly this time.
	 */
	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureEasyBoard BC name= " << cfgboard->name()
		<< " type(model)= " << cfgboard->model()
		<< " board->description= " << cfgboard->description()
		<< ", adding EASY board type= " << ctype << " channels" << endl;
	}
	vector<ChannelConfig *> chCfg;

	// first, insert a marker channel, and it needs board type in RemBdName::Onstate
	string easyBoardModel = ctype;
	chCfg.push_back( new ChannelConfig( _currentIndexCrateSlotChannel, cname, key, "easyBoardMarkerChannel", easyBoardModel ) );
	if ( _debug )
		cout << __FILE__ << " " << __LINE__ << " _configureEasyBoard adding EASY marker channel " << endl;

	int nbEasyChannels = 0;
	string easyChannelType = "easyHighVoltChannel";
	_classifyEasyChannels( ctype, &nbEasyChannels, &easyChannelType );
	cout << __FILE__ << " " << __LINE__ << " _configureEasyBoard classified " << ctype << " as "
			<< easyChannelType << " with " << nbEasyChannels << " easy channels" << endl;

	for ( int i = 0; i < nbEasyChannels; i++ ){
		stringstream ss;
		ss << "easy_" << ctype << "_"<< i;
		string ename = ss.str();
		sprintf( key, "crate=%d slot=%d channel=%d", _currentIndexCrate, _currentIndexCrateSlot, _currentIndexCrateSlotChannel + i + 1);
		chCfg.push_back( new ChannelConfig( _currentIndexCrateSlotChannel, ename, key, easyChannelType ) );
		if ( _debug )
			cout << __FILE__ << " " << __LINE__ << " _configureEasyBoard adding EASY channel " << ename << endl;
	}

	// insert the channels into the BC == board. All these channels are at the same slot, where also the BC lives
	for ( unsigned k = 0; k < chCfg.size(); k++ ){
		cfgboard->channelConfig_map.insert( std::pair<uint32_t, ChannelConfig *>(_currentIndexCrateSlotChannel, chCfg[ k ]));
		ChannelConfig *channelCfg = cfgboard->channelConfig_map.at( _currentIndexCrateSlotChannel );
		if ( _debug ){
			cout << __FILE__ << " " << __LINE__ << " _configureEasyBoard inserting channel "
					<< "{" <<	_currentIndexCrate << ", "
					<<	_currentIndexCrateSlot << ", "
					<<	_currentIndexCrateSlotChannel << "} "
					<< " name= " << channelCfg->name()
					<< " model= " << channelCfg->model()
					<< " key= " << channelCfg->key()
					<< endl;
		}
		_currentIndexCrateSlotChannel++; // index is map key: must increase it therefore
	}
}

/**
 * identify the physical easy channels needed:
 * channel count and classification
 *   EasyBoard name="EasyBoard03" slot="0" type="A3100" description="A3100_e11_3" EasyBoard
 * 	typedef enum { standardHighVoltChannel, standardLowVoltChannel, easyBoardMarkerChannel, easyLowVoltChannel, easyHighVoltChannel  } CHANNEL_TYPES_t;
 * 	the marker channel is NOT counted here, since it is a logical addition, not a physical.
 */
/** right now all easy boards are v1. Change the
 *
 * void Configuration::_classifyEasyChannels( string bstr, int *count, string *classification ){
 * method or replace it. Currently we do "guess the behaviour from the API", which does the job, but is
 * not really the final good solution. We should have the {model, firmware} for ALL boards, primary and EASY boards,
 * plus the discovered API, to decide how to simulate it.
 */
void Configuration::_classifyEasyChannels( string bstr, int *count, string *classification ){
	*count = 0;
	*classification = "unknown easy channel";
	if ( bstr == "A3006"){ 	*count = 6;  *classification = "easyLowVoltChannelv1";	}
	if ( bstr == "A3009"){ 	*count = 12; *classification = "easyLowVoltChannelv1";	}
	if ( bstr == "A3016"){ 	*count = 6;  *classification = "easyLowVoltChannelv1";	}
	if ( bstr == "A3050"){ 	*count = 2;  *classification = "easyLowVoltChannelv1";	}
	if ( bstr == "A3100"){ 	*count = 1;  *classification = "easyLowVoltChannelv1";	}

	// simple sm, high volt, just on/off
	if ( bstr == "A3486"){ 	*count = 2;  *classification = "easyLowVoltChannelv1";	}

	if ( bstr == "A3501"){ 	*count = 12; *classification = "easyHighVoltChannel";	}
	if ( bstr == "A3512"){ 	*count = 6;  *classification = "easyHighVoltChannel";	}
	if ( bstr == "A3535"){ 	*count = 32; *classification = "easyHighVoltChannel";	}
	if ( bstr == "A3540"){ 	*count = 12; *classification = "easyHighVoltChannel";	}

}

void Configuration::_configureChannel( DOMNode *channel_node ){
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( channel_node->getNodeName(), ns, NODE_NAME_MAX );
	// cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl; // Channel

	bool haveIndex = false;
	bool haveName = false;
	bool haveModel = false;

	char cindex[ NODE_NAME_MAX ] = "";
	char cname[ NODE_NAME_MAX ] = "";
	char cmodel[ NODE_NAME_MAX ] = "";
	DOMNamedNodeMap *attributes = channel_node->getAttributes();
	// cout << __FILE__ << " " << __LINE__ << " _configureChannel: #attributes= " << attributes->getLength() << endl;
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char bn [ NODE_NAME_MAX ] = "";
		// <Channel name="Chan003" index="3" model="CAEN Easy Channel A3050D">
		//  <Channel index="0" name="sim_ch0">

		XMLString::transcode( attributes->item( n )->getNodeName(), bn, NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " _configureChannel: attribute= " << bn << endl;
		if ( strcmp(bn, "index") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), cindex, NODE_NAME_MAX );
			haveIndex = true;
			// cout << __FILE__ << " " << __LINE__ << " cindex= " << cindex << endl;
		} else if ( strcmp(bn, "name") == 0 ){
			// XMLString::transcode( attributes->item( n )->getTextContent(), cname, NAME_MAX );
			XMLString::transcode( attributes->item( n )->getTextContent(), cname, CAEN_NAME_MAX );
			haveName = true;
		} else if ( strcmp(bn, "model") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), cmodel, CAEN_NAME_MAX );
			haveModel = true;
		}
	}

	if ( !haveModel ){
		cout << __FILE__ << " " << __LINE__ << " try guessing the channel model from its properties" << endl;
		string channel_type = _classifyChannelByProperties( channel_node );
		sprintf( cmodel, "%s", channel_type.c_str());
		haveModel = true;
	}

	if ( !haveName ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _configureChannel: ERROR no channel name found" << endl;
		throw std::runtime_error( os.str() );
	}
	if ( !haveIndex ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _configureChannel: ERROR no channel index found" << endl;
		throw std::runtime_error( os.str() );
	}
	if ( !haveModel && _debug ){
		cout << __FILE__ << " " << __LINE__ << " _configureChannel: no channel Properties or Model present, using generic"
				<< " index= " << cindex
				<< " cname= " << cname << endl;
	}

	// add another channel to the current crate, board
	sscanf( cindex, "%d", &_currentIndexCrateSlotChannel );
	char key[ NODE_NAME_MAX ] = "";
	sprintf( key, "crate=%d slot=%d channel=%d", _currentIndexCrate, _currentIndexCrateSlot, _currentIndexCrateSlotChannel );
	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	BoardConfig *board = crate->boardConfig_map.at ( _currentIndexCrateSlot );


	if ( _debug ){
		cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " board->name= " << board->name()
		<< " board->description= " << board->description()
		<< " index= "<< _currentIndexCrateSlotChannel
		<< " channel type= " << cmodel << endl;
	}

	// for branch controllers we need EASY channel models as well, otherwise we have to populate "generic"
	if ( !haveModel ) sprintf( cmodel, "generic");
	//if ( haveModel )
	board->channelConfig_map.insert( std::pair<uint32_t, ChannelConfig *>(_currentIndexCrateSlotChannel, new ChannelConfig( _currentIndexCrateSlotChannel, cname, key, cmodel )));
	//else
	//	board->channelConfig_map.insert( std::pair<uint32_t, ChannelConfig *>(_currentIndexCrateSlotChannel, new ChannelConfig( _currentIndexCrateSlotChannel, cname, key, "generic" )));
	// board->channelConfig_map.insert( std::pair<uint32_t, ChannelConfig *>(_currentIndexCrateSlotChannel, new ChannelConfig( _currentIndexCrateSlotChannel, cname, key )));

	ChannelConfig *channelCfg = board->channelConfig_map.at( _currentIndexCrateSlotChannel );
	if ( _debug ){
		cout << __FILE__ << " " << __LINE__ << " _configureChannel added channel "
				<< "{" <<	_currentIndexCrate << ", "
				<<	_currentIndexCrateSlot << ", "
				<<	_currentIndexCrateSlotChannel << "} "
				<< " name= " << channelCfg->name()
				<< " model= " << channelCfg->model()
				<< " key= " << channelCfg->key()
				<< endl;
	}
}

void Configuration::_configureChannelProperty( DOMNode *channelprop_node ){
	APIConfig::PROPERTY_t cr;
	char ns[ NODE_NAME_MAX ] = "";
	XMLString::transcode( channelprop_node->getNodeName(), ns, NODE_NAME_MAX );
	//cout << __FILE__ << " " << __LINE__ << " node= " << ns << endl;

	cr.pAccessType = APIConfig::ReadOnlyProperty; // silly default
	if ( strcmp( ns, "ReadOnlyProperty") == 0 ){
		cr.pAccessType = APIConfig::ReadOnlyProperty;
	} else if ( strcmp( ns, "ReadWriteProperty") == 0 ){
		cr.pAccessType = APIConfig::ReadWriteProperty;
	} else if ( strcmp( ns, "WriteOnlyProperty") == 0 ){
		cr.pAccessType = APIConfig::WriteOnlyProperty;
	}

	DOMNamedNodeMap *attributes = channelprop_node->getAttributes();
	for ( uint32_t n = 0; n < attributes->getLength(); n++ ){
		char ns[ NAME_MAX ] = "";
		char sval[ NODE_NAME_MAX ] = "";
		XMLString::transcode( attributes->item( n )->getNodeName(), ns, NODE_NAME_MAX );
		// cout << __FILE__ << " " << __LINE__ << " attr name= " << ns << endl;

		if ( strcmp(ns, "dataType") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			//cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			cr.pDataType = APIConfig::S;
			if ( strcmp( sval, "S") == 0 ){
				cr.pDataType = APIConfig::S;
			} else if ( strcmp( sval, "I") == 0 ){
				cr.pDataType = APIConfig::I;
			} else if ( strcmp( sval, "F") == 0 ){
				cr.pDataType = APIConfig::F;
			}
		} else if ( strcmp(ns, "name") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			cr.name = string( sval );
		} else if ( strcmp(ns, "propertyName") == 0 ){
			XMLString::transcode( attributes->item( n )->getTextContent(), sval, NODE_NAME_MAX );
			// cout << __FILE__ << " " << __LINE__ << " text content=  " << sval << endl;
			cr.pname = string( sval );
		}
	}

	CrateConfig *crate = _crateConfig_map.at( _currentIndexCrate );
	BoardConfig *board = crate->boardConfig_map.at( _currentIndexCrateSlot );
	ChannelConfig *channel = board->channelConfig_map.at ( _currentIndexCrateSlotChannel );
	// channel->channelAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( ns, cr )); // seems wrong !
	channel->channelAPI_map.insert( std::pair<string,APIConfig::PROPERTY_t>( cr.pname, cr ));

	if ( _debug ) {
		cout << __FILE__ << " " << __LINE__ << " _configureChannelProperty "
				<< "{" <<	_currentIndexCrate << ", "
				<<	_currentIndexCrateSlot << ", "
				<<	_currentIndexCrateSlotChannel << "} "
				" added channel property "
				<< " name= " << cr.name << " access type= " << cr.pAccessType << " data type= " << cr.pDataType << endl;
	}
}



void Configuration::firstGlobalUpdate( void ){
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		if (_debug)	cout << __FILE__ << " " << __LINE__ << " icrate= " << icrate << endl;
		Crate *cr = crate( icrate );
		cr->update();
	}
}


/**
 * populate the s.engine from the shorthand configuration, should lead to the same
 * engine behavior. Just formats are different, for compatibility
 */
void Configuration::populateShortHand( bool debugConfig ){
	int totalPadCount = 0;
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		if (_debug) cout << __FILE__ << " " << __LINE__ << " populateShortHand icrate= " << icrate << endl;
		_createSYCrate( _crateConfig_map.at( icrate ) );

		Crate *cr = crate( icrate );
		cr->setDebug( debugConfig );
		cr->setIntrinsicUpdates( _intrinsicUpdates );
		CrateConfig *crCfg = _crateConfig_map.at( icrate );
		cr->setIPAddress( crCfg->ipAddress() ); // we keep the same ip address as a key for finding the crate handle

		if ( debugConfig ) cr->showProperties();

		for ( uint32_t islot = 0; islot < crCfg->nbPopulatedSlots(); islot++ ){

			// create the boards on the slots which are actually used
			uint32_t populatedSlot = crCfg->populatedSlot( islot );
			cr->addPopulatedSlot( populatedSlot );

			if (_debug) cout << __FILE__ << " " << __LINE__ << " populateShortHand icrate= " << icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << endl;

			// cout << __FILE__ << " " << __LINE__ << " icrate= " << icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << endl;

			BoardConfig *cfgboard = 0;
			{
				std::map<uint32_t, BoardConfig *>::iterator it = crCfg->boardConfig_map.find( populatedSlot );
				if ( it != crCfg->boardConfig_map.end() ){
					cfgboard = crCfg->boardConfig_map.at( populatedSlot );
					if (_debug) cout << __FILE__ << " " << __LINE__ << " populateShortHand board config OK: found icrate= "
							<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << " model= " << cfgboard->model() << endl;

					//cout << " board config OK found icrate= "
					//		<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << " model= " << cfgboard->model() << endl;

				} else {
					std::ostringstream os;
					os << __FILE__ << " " << __LINE__ << " populateShortHand board config not found icrate= "
							<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << endl;
					throw std::runtime_error( os.str() );
				}
			}

			if ( _createBoard( cr, cfgboard ) ) {
				Board *bd = cr->board( populatedSlot );
				bd->setDebug( debugConfig );
				bd->setIntrinsicUpdates( _intrinsicUpdates );
				bd->setMyCrate( icrate );

				if ( debugConfig ) bd->showProperties();

				// create always all channel objects for each board. Their characteristics
				// are defined by the board type. Branch controllers are special: not all
				// available channels are populated, but we count sequentially with marker channels interleaved ;-<
				uint32_t nbChannels = 0;

				/**
				 * branch controllers and generic (simulated) boards have variable amounts of channels, all
				 * other standard boards are fixed by design and have consecutive channel.
				 */
				switch( bd->convert_model2type( bd->model() )){
				case A1676: {
					/**
					 * branch controller.
					 */
					nbChannels = cfgboard->channelConfig_map.size(); // all configured channels up to 1920 in real, but here unlimited
					if (_debug) {
						cout << __FILE__ << " " << __LINE__ << " populateShortHand branch controller model= "
						<< bd->model() << " configuring " << nbChannels << " channels" << endl;
					}

					/**
					 * find out which type of EASY channel we need to create for this branch controller.
					 * Looking at the EASY board type from the explicit shorthand config,
					 * which has already all channels configured in RAM.
					 * Each new EASY board is preceded by a marker channel.
					 */
					for (std::map<uint32_t, ChannelConfig *>::iterator it = cfgboard->channelConfig_map.begin(); it!=cfgboard->channelConfig_map.end(); ++it){
						uint32_t ichannel = it->first;
						ChannelConfig *cfgchannel = it->second;
						cfgchannel->setChannel( ichannel );

						if (_debug) {
							cout << __FILE__ << " " << __LINE__ << " populateShortHand board key= " << bd->key() << " found icrate= "
									<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
									<< " ichannel= " << ichannel << " type= " << cfgchannel->type()
									<< " model= " << cfgchannel->model()
									<< " name= " << cfgchannel->name()
									<< endl;
						}

						switch ( cfgchannel->type() ){
						case ChannelConfig::config_standardHighVoltChannel:{
							_createHighVoltChannel( bd, cfgchannel );
							break;
						}
						case ChannelConfig::config_standardLowVoltChannel:{
							_createLowVoltChannel( bd, cfgchannel );
							break;
						}
						case ChannelConfig::config_easyBoardMarkerChannel:{
							_createEasyBoardMarkerChannel( bd, cfgchannel );
							break;
						}
						case ChannelConfig::config_easyLowVoltChannelv1:{
							float vlimit = 100.0;
							float ilimit = 500.0;
							_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
							break;
						}
						case ChannelConfig::config_easyLowVoltChannelv2:{
							float vlimit = 100.0;
							float ilimit = 500.0;
							_createEASYLowVoltChannelv2( bd, cfgchannel, vlimit, ilimit );
							break;
						}
						case ChannelConfig::config_easyHighVoltChannel:{
							float vlimit = 100000.0;
							float ilimit = 5.0;
							_createEASYHighVoltChannel( bd, cfgchannel, vlimit, ilimit  );
							break;
						}
						default: {
							cout << __FILE__ << " " << __LINE__ << " no easy channel of behavior type "
									<< cfgchannel->type() << " known, can't create it" << endl;
						}
						} // switch .. cfgchannel->type


						if ( debugConfig ) cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " try populating EASY channel on bd->slot= " << bd->slot()
															<< " bd->model()= " << bd->model()
															<< " ichannel= " << ichannel << endl;
						if ( bd->channelExists( ichannel) ){

							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							bd->addPopulatedChannel( ichannel );
							if ( debugConfig ) {
								ch->showProperties();
								cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " board model= "
										<< bd->model() << " (easy BC) key= " << bd->key()
										<< " channel= " << ichannel << " seems OK" << endl;
							}
						} else {
							cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " "
									<< bd->model() << " key= " << bd->key()
									<< " channel= " << ichannel << " does not exist" << endl;
							exit(0);
						}
					}
					break;
				}

				case GENERIC: {
					// these can also be branch controllers which are not declared (type missing). The generic
					// boards can have an infinite number of channels, but they are just classical channels
					// and channel00 is not special, as for branch controllers.
					nbChannels = cfgboard->channelConfig_map.size(); // all configured channels
					bd->configureNbChannels( nbChannels );
					if (_debug)
						cout << __FILE__ << " " << __LINE__ << " populateShortHand generic board model= "
						<< bd->model() << " configuring " << nbChannels << " channels" << endl;

					// channels are NOT populated strictly sequentially: there can be HOLES !!
					for (std::map<uint32_t, ChannelConfig *>::iterator it = cfgboard->channelConfig_map.begin(); it!=cfgboard->channelConfig_map.end(); ++it){
						uint32_t ichannel = it->first;
						ChannelConfig *cfgchannel = it->second;

						if (_debug)
							cout << __FILE__ << " " << __LINE__ << " populateShortHand channel config found icrate= "
							<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
							<< " ichannel= " << ichannel << " name= " << cfgchannel->name() << endl;
						bd->addPopulatedChannel( ichannel );

						_createHighVoltChannel( bd, cfgchannel );
						Channel *ch = bd->channel( ichannel );
						ch->setDebug( debugConfig );
						if ( debugConfig ) ch->showProperties();
					}
					break;
				}

				/**
				 * all standard boards (NOT EASY) have fixed nb of channels by design and
				 * are populated sequentially without holes. We distinguish between complex (high volt)
				 * and simple (low volt) state machine. Default is complex. But we don't have any
				 * channels configured yet, we just add them on the fly, using the board's constructor
				 * which knows the standard board types.
				 */
				default: {
					nbChannels = bd->nbChannelsFromModel();

					if (_debug)
						cout << __FILE__ << " " << __LINE__ << " populateShortHand found standard board model= "
						<< cfgboard->model() << " with " << nbChannels << " channels for this model" << endl;

					Channel::CHANNEL_TYPES_t chType = Channel::primaryPSchannelWithVSELandISEL;
					switch ( bd->smType() ){
					case Board::complexSM: chType = Channel::primaryPSchannelWithVSELandISEL; break;
					case Board::simpleSM:  chType = Channel::primaryPSchannelNoVSEL;break;
					default: {
						cout << __FILE__ << " " << __LINE__ << " populateShortHand ERROR standard board have either complex or simple state machine, asusme complex" << endl;
						chType = Channel::primaryPSchannelWithVSELandISEL;
						break;
					}
					}
					for (uint32_t ichannel = 0; ichannel < nbChannels; ichannel++ ){
						bd->addPopulatedChannel( ichannel );
					    std::ostringstream stm ;
						stm << setfill('0') << setw(3) << ichannel ;
						string channelName = string("Chan") + stm.str();
						ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), chType, ichannel, bd->HVMax(), bd->ilimit(), channelName );
						bd->insertChannel( ichannel, ch );
					}
					break;
				}
				} // switch
			} // if _createBoard
		} // for ... slots
	} // for ... crate
	cout << __FILE__ << " " << __LINE__ << " populateShortHand channel config: total padded channels=  " << totalPadCount << endl;
}

/**
 * count every address space item update in it's dedicated counter.
 * we have a huge map with all counters and names.
 * we can init them here, but using them actually takes a lot of CPU:
 * default is OFF=don't use them, but via a pilot message the scadaCounterFlag
 * can be set true.
 */
void Configuration::initScadaCountersAllCrates( void ){
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		Crate *cr = crate( icrate );
		cr->initCountersMap();
	}
}

void Configuration::populate( bool debugConfig ){
	int totalPadCount = 0;
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		if (_debug) cout << __FILE__ << " " << __LINE__ << " icrate= " << icrate << endl;
		_createSYCrate( _crateConfig_map.at( icrate ) );

		Crate *cr = crate( icrate );
		cr->setDebug( debugConfig );
		cr->setIntrinsicUpdates( _intrinsicUpdates );
		CrateConfig *crCfg = _crateConfig_map.at( icrate );
		cr->setIPAddress( crCfg->ipAddress() ); // we keep the same ip address as a key for finding the crate handle

		if ( debugConfig ) cr->showProperties();

		for ( uint32_t islot = 0; islot < crCfg->nbPopulatedSlots(); islot++ ){

			// create the boards on the slots which are actually used
			uint32_t populatedSlot = crCfg->populatedSlot( islot );
			cr->addPopulatedSlot( populatedSlot );

			if (_debug) cout << __FILE__ << " " << __LINE__ << " icrate= " << icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << endl;

			// cout << __FILE__ << " " << __LINE__ << " icrate= " << icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << endl;

			BoardConfig *cfgboard = 0;
			{
				std::map<uint32_t, BoardConfig *>::iterator it = crCfg->boardConfig_map.find( populatedSlot );
				if ( it != crCfg->boardConfig_map.end() ){
					cfgboard = crCfg->boardConfig_map.at( populatedSlot );
					if (_debug) cout << " board config OK found icrate= "
							<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << " model= " << cfgboard->model() << endl;

					//cout << " board config OK found icrate= "
					//		<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << " model= " << cfgboard->model() << endl;

				} else {
					std::ostringstream os;
					os << __FILE__ << " " << __LINE__ << " board config not found icrate= "
							<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot << endl;
					throw std::runtime_error( os.str() );
				}
			}

			if ( _createBoard( cr, cfgboard ) ) {
				Board *bd = cr->board( populatedSlot );
				bd->setDebug( debugConfig );
				bd->setIntrinsicUpdates( _intrinsicUpdates );
				bd->setMyCrate( icrate );

				if ( debugConfig ) bd->showProperties();

				// create always all channel objects for each board. Their characteristics
				// are defined by the board type. Branch controllers are special: not all
				// available channels are populated, but we count sequentially with marker channels interleaved ;-<
				uint32_t nbChannels = 0;

				// branch controllers and generic (simulated) boards have variable amounts of channels, all others
				// are fixed by design and have consecutive channel.
				switch( bd->convert_model2type( bd->model() )){
				case A1676: {
					/**
					 * branch controller. We also pay attention to the model/type information of each channel,
					 * which we infer from the combination of properties.
					 *
					 * Only configured channels are populated, but to get strictly sequential numbering
					 * we can pad missing channels into the simulation  (see padding code further down)
					 */
					nbChannels = cfgboard->channelConfig_map.size(); // all configured channels up to 1920 in real, but here unlimited
					if (_debug)
						cout << __FILE__ << " " << __LINE__ << " branch controller model= "
						<< bd->model() << " configuring " << nbChannels << " channels" << endl;

					/**
					 * find out which type of channel we need by looking at the specified properties.
					 * we assume here that many boards have identical types of channels, as dictated by the firmware:
					 *
					 * easy marker: has "Temp", no state machine, no PS
					 * easy low volt: has "GlbOffEn" AND NOT "RUp", switch on/off only
					 * easy high volt: has "GlbOffEn" AND "RUp", state machine with ramps
					 * standard low volt: has "V1Set" AND NOT "RUp", switch on/off only
					 * standard high volt: has "V1Set" AND "RUp", state machine with ramps
					 * others: no state machine, no PS
					 */
					for (std::map<uint32_t, ChannelConfig *>::iterator it = cfgboard->channelConfig_map.begin(); it!=cfgboard->channelConfig_map.end(); ++it){
						uint32_t ichannel = it->first;
						ChannelConfig *cfgchannel = it->second;

						if ( cfgchannel->propertyExists("Temp")){
							// the channel is an easy board marker: which has "board like" properties
							if (_debug)
								cout << __FILE__ << " " << __LINE__ << " easy board MARKER channel found icrate= "
								<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
								<< " ichannel= " << ichannel << endl;

							bd->addPopulatedChannel( ichannel );
							_createEasyBoardMarkerChannel( bd, cfgchannel );
							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							if ( debugConfig ) ch->showProperties();

						} else if (cfgchannel->propertyExists("GlbOffEn") && !cfgchannel->propertyExists("RUp") ){
							// easy channel with reduced functionality for low voltage
							if (_debug)
								cout << __FILE__ << " " << __LINE__ << " easy channel LOW VOLT found icrate= "
								<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
								<< " ichannel= " << ichannel << endl;

							bd->addPopulatedChannel( ichannel );
							if ( cfgchannel->propertyExists("OutReg") ){
								_createEASYLowVoltChannelv2( bd, cfgchannel, 100000.0, 20000.0 );
							} else {
								_createEASYLowVoltChannelv1( bd, cfgchannel, 100000.0, 20000.0 );
							}
							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							if ( debugConfig ) {
								ch->showProperties();
							}
						} else if (cfgchannel->propertyExists("GlbOffEn") && cfgchannel->propertyExists("RUp") ){
							// easy channel with ramping
							if (_debug)
								cout << __FILE__ << " " << __LINE__ << " easy channel HIGH VOLT found icrate= "
								<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
								<< " ichannel= " << ichannel << endl;

							bd->addPopulatedChannel( ichannel );
							_createEASYHighVoltChannel( bd, cfgchannel, 10000.0, 200.0 );
							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							if ( debugConfig ) ch->showProperties();

						} else if ( cfgchannel->propertyExists("V1Set") && cfgchannel->propertyExists("RUp") ){
							// the channel is a normal high volt power supply: generic
							if (_debug)
								cout << __FILE__ << " " << __LINE__ << " channel HIGH VOLT found icrate= "
								<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
								<< " ichannel= " << ichannel << " name= " << cfgchannel->name() << endl;
							bd->addPopulatedChannel( ichannel );
							_createHighVoltChannel( bd, cfgchannel );
							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							if ( debugConfig ) ch->showProperties();

						} else if ( cfgchannel->propertyExists("V1Set") && !cfgchannel->propertyExists("RUp") ){
							// the channel is a normal low volt power supply: generic
							if (_debug)
								cout << __FILE__ << " " << __LINE__ << " channel LOW VOLT found icrate= "
								<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
								<< " ichannel= " << ichannel << " name= " << cfgchannel->name() << endl;
							bd->addPopulatedChannel( ichannel );
							_createLowVoltChannel( bd, cfgchannel );
							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );

						} else {
							if (_debug) {
								cout << __FILE__ << " " << __LINE__ << " SHORTHAND CONFIG channel found icrate= "
										<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
										<< " ichannel= " << ichannel << " type= " << cfgchannel->type() << endl;
							}

							// we can use a short form of EASY config where channels are not
							// specified and we do NOT have properties, but just types, for setting
							// up the simulation engine.
							float vlimit = 0;
							float ilimit = 0;
							Channel::ChannelStateMachineConfig_t smtype = Channel::complexSM;
							bd->addPopulatedChannel( ichannel );

							switch ( cfgchannel->type() ){
							// ==========
							// no sm
							case ChannelConfig::A1676A00: {
								_createEasyBoardMarkerChannel( bd, cfgchannel );
								smtype = Channel::easyBoardMarkerChannelSM;
								break;
							}
							// ==========
							// simple sm
							case ChannelConfig::A3484: {
								vlimit = 48;
								ilimit = 72;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								smtype = Channel::simpleSM;
								break;
							}
							case ChannelConfig::A3485:{
								vlimit = 48;
								ilimit = 104;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								smtype = Channel::simpleSM;
								break;
							}
							case ChannelConfig::A3486:{
								vlimit = 48;
								ilimit = 42;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								smtype = Channel::simpleSM;
								break;
							}
							case ChannelConfig::A3486A:{
								vlimit = 48;
								ilimit = 42;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								smtype = Channel::simpleSM;
								break;
							}
							// ==========
							// low volt: simple sm
							case ChannelConfig::A3006: {
								vlimit = 16;
								ilimit = 6;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3009:{
								vlimit = 8;
								ilimit = 9;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3016:{
								vlimit = 9;
								ilimit = 16;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3050:{
								vlimit = 8;
								ilimit = 50;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3100:{
								vlimit = 8;
								ilimit = 100;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							// high volt complex sm
							case ChannelConfig::A3501:{
								vlimit = 100;
								ilimit = 1e-3;
								_createEASYHighVoltChannel( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3512:{
								vlimit = 12000;
								ilimit = 1e-3;
								_createEASYHighVoltChannel( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3535:{
								vlimit = 3200;
								ilimit = 500e-6;
								_createEASYHighVoltChannel( bd, cfgchannel, vlimit, ilimit );
								break;
							}
							case ChannelConfig::A3540:{
								vlimit = 4000;
								ilimit = 1e-3;
								_createEASYHighVoltChannel( bd, cfgchannel, vlimit, ilimit );
								smtype = Channel::complexSM;
								break;
							}

							// other boards: ADC, DAC and Temp sensor
							case ChannelConfig::A3801:
							case ChannelConfig::A3802:
							case ChannelConfig::A3801A:{
								cout << __FILE__ << " " << __LINE__ << " EASY ADC and Temp not implemented, adding fake low volt channel" << endl;
								vlimit = 0;
								ilimit = 0;
								_createEASYLowVoltChannelv1( bd, cfgchannel, vlimit, ilimit );
								smtype = Channel::noSM;
								break;
							}

							default:
							case ChannelConfig::generic: {
								// we have no information on the board type. Assume a complex SM which
								// can ramp up properly. Only HV boards have this in general
								vlimit = 1e6;
								ilimit = 1e6;
								_createHighVoltChannel( bd, cfgchannel );
								smtype = Channel::complexSM;
								break;
							}
							}

							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							ch->configureSM( smtype );
							if ( debugConfig ) ch->showProperties();
							cout << __FILE__ << " " << __LINE__ << " easy board model= "
									<< bd->model() << " key= " << bd->key()
									<< " channel= " << ichannel << " is probably a power converter channel. not yet implemented... leave empty " << endl;
						}
					}

					// if we get a config which is derived from the winccoa world, and not directly from the electronics
					// we might have not used/declared all channels of that board which physically exist.
					// These "missing channels" are a problem for the OPC server, so lets detect them and fill them in.
					// Easy boards pop up in the channel list as Channels with "Temp" property: they are easy board headers. Let's fill in ALL
					// normal easy channels BETWEEN the easy boards.
					if ( _padMissingChannels ) {
						vector<int32_t> easyChannels;

						for ( uint32_t ich = 0; ich < bd->nbChannelObjects(); ich++ ){
							if ( bd->channelExists( ich ) ){
								if ( bd->channel( ich )->isEasyBoardMarkerChannel() ) {
									easyChannels.push_back( -ich ); // headers are negative
								} else {
									easyChannels.push_back( ich );
								}
							} else {
								// pad in a p.s. easy channel which is a copy of the first ps easy channel which was found
								easyChannels.push_back( ich + 100000 ); // mark the padding
								bd->addPopulatedChannel( ich );
								_createPaddedEasyChannel( bd, ich );
								Channel *ch = bd->channel( ich );
								ch->setDebug( debugConfig );
							}
							if (_debug)
								for ( uint32_t k = 0; k < easyChannels.size(); k++ ){
									cout << __FILE__ << " " << __LINE__ << " " << bd->key() << " easyChannel " << k	<< " " << easyChannels[ k ] << endl;
								}

							/// we have not padded the beginning and the end.

						}
					}
					break;
				}

				case GENERIC: {
					// these can also be branch controllers which are not declared (type missing). The generic
					// boards can have an infinite number of channels, but they are just classical channels
					// and channel00 is not special, as for branch controllers.
					nbChannels = cfgboard->channelConfig_map.size(); // all configured channels
					bd->configureNbChannels( nbChannels );
					if (_debug)
						cout << __FILE__ << " " << __LINE__ << " generic board model= "
						<< bd->model() << " configuring " << nbChannels << " channels" << endl;

					// channels are NOT populated strictly sequentially: there can be HOLES !!
					for (std::map<uint32_t, ChannelConfig *>::iterator it = cfgboard->channelConfig_map.begin(); it!=cfgboard->channelConfig_map.end(); ++it){
						uint32_t ichannel = it->first;
						ChannelConfig *cfgchannel = it->second;

						if (_debug)
							cout << __FILE__ << " " << __LINE__ << " channel config found icrate= "
							<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
							<< " ichannel= " << ichannel << " name= " << cfgchannel->name() << endl;
						bd->addPopulatedChannel( ichannel );

						_createHighVoltChannel( bd, cfgchannel );
						Channel *ch = bd->channel( ichannel );
						ch->setDebug( debugConfig );
						if ( debugConfig ) ch->showProperties();
					}
					break;
				}

				case A1519:
				case A1535:
				case A1540:
				case A1590:
				case A1734:
				case A1735:
				case A1821:
				case A1821HN:
				case A1832:
				case A1833:
				case A2518:
				default: {
					// all normal boards have fixed nb of channels by design and
					// are populated sequentially without holes, and have the high voltage state machine
					nbChannels = bd->nbChannelsFromModel();

					if (_debug)
						cout << __FILE__ << " " << __LINE__ << " found "
						<< cfgboard->channelConfig_map.size() << " configured channels" << endl;

					for (uint32_t ichannel = 0; ichannel < nbChannels; ichannel++ ){
						ChannelConfig *cfgchannel = 0;
						std::map<uint32_t, ChannelConfig *>::iterator it = cfgboard->channelConfig_map.find( ichannel );

						if ( it != cfgboard->channelConfig_map.end() ){
							cfgchannel = cfgboard->channelConfig_map.at( ichannel );
							if (_debug)
								cout << __FILE__ << " " << __LINE__ << " channel config found icrate= "
								<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
								<< " ichannel= " << ichannel << " name= " << cfgchannel->name() << endl;
							bd->addPopulatedChannel( ichannel );

							_createHighVoltChannel( bd, cfgchannel );
							Channel *ch = bd->channel( ichannel );
							ch->setDebug( debugConfig );
							if ( debugConfig ) ch->showProperties();
						} else {
							std::ostringstream os;
							os << __FILE__ << " " << __LINE__ << " channel config not found icrate= "
									<< icrate << " islot= " << islot << " populatedSlot= " << populatedSlot
									<< " ichannel= " << ichannel << endl;
							throw std::runtime_error( os.str() );
						}
					}
					break;
				}
				} // switch
			} // if _createBoard
		} // for ... slots
	} // for ... crate
	cout << __FILE__ << " " << __LINE__ << " channel config: total padded channels=  " << totalPadCount << endl;
}

// low voltage, no ramps
bool Configuration::_createEASYLowVoltChannelv1( Board *bd, ChannelConfig *cfgchannel, float vlimit, float ilimit ){
	uint32_t ichannel = cfgchannel->channel();
	string name = _padTo3DigitName( cfgchannel->name(), ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), Channel::easyPSchannelNoVSELv1, ichannel, vlimit, ilimit, name );
	bd->insertChannel( ichannel, ch );
	return( 1 );
}
bool Configuration::_createEASYLowVoltChannelv2( Board *bd, ChannelConfig *cfgchannel, float vlimit, float ilimit ){
	uint32_t ichannel = cfgchannel->channel();
	string name = _padTo3DigitName( cfgchannel->name(), ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), Channel::easyPSchannelNoVSELv2, ichannel, vlimit, ilimit, name );
	bd->insertChannel( ichannel, ch );
	return( 1 );
}

// including ramping
bool Configuration::_createEASYHighVoltChannel( Board *bd, ChannelConfig *cfgchannel, float vlimit, float ilimit ){
	uint32_t ichannel = cfgchannel->channel();
	string name = _padTo3DigitName( cfgchannel->name(), ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), Channel::easyPSchannelWithVSELnoISEL, ichannel, vlimit, ilimit, name );
	bd->insertChannel( ichannel, ch );
	return( 1 );
}

bool Configuration::_createHighVoltChannel( Board *bd, ChannelConfig *cfgchannel ){
	uint32_t ichannel = cfgchannel->channel();
	string name = _padTo3DigitName( cfgchannel->name(), ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), Channel::primaryPSchannelWithVSELandISEL, ichannel, bd->HVMax(), bd->ilimit(), name );

	//if ( strcmp( name.c_str(), "A1532") == 0 ){
	//	ch->configureSM( Channel::simpleSM );
	//}
	bd->insertChannel( ichannel, ch );
	return( 1 );
}

bool Configuration::_createLowVoltChannel( Board *bd, ChannelConfig *cfgchannel ){
	uint32_t ichannel = cfgchannel->channel();
	string name = _padTo3DigitName( cfgchannel->name(), ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), Channel::primaryPSchannelNoVSEL, ichannel, bd->HVMax(), bd->ilimit(), name );

	//if ( strcmp( name.c_str(), "A1532") == 0 ){
	//	ch->configureSM( Channel::simpleSM );
	//}
	bd->insertChannel( ichannel, ch );
	return( 1 );
}

bool Configuration::_createEasyBoardMarkerChannel( Board *bd, ChannelConfig *cfgchannel ){
	uint32_t ichannel = cfgchannel->channel();
	string name = _padTo3DigitName( cfgchannel->name(), ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), ns_HAL_Channel::Channel::easyBoardMarkerChannel, ichannel, 10000, 200, name );
	ch->setParameterOnstate("RemBdName", cfgchannel->easyBoardModel() );
	bd->insertChannel( ichannel, ch );
	return( 1 );
}

bool Configuration::_createPaddedEasyChannel( Board *bd, uint32_t ichannel ){
	string name = _padTo3DigitName( "Chan", ichannel );
	ns_HAL_Channel::Channel *ch = new ns_HAL_Channel::Channel( bd->myCrate(), bd->slot(), Channel::easyPSchannelNoVSELv1, ichannel, bd->HVMax(), bd->ilimit(), name );

	//if ( strcmp( name.c_str(), "A1532") == 0 ){
	//	ch->configureSM( Channel::simpleSM );
	//}
	bd->insertChannel( ichannel, ch );
	return( 1 );
}


string Configuration::_padTo3DigitName( string oldname, int channelIndex )
{
	// correct the channel name to have 3digits with leading zeroes for channel index
	// number at the end of a string
	string newname = oldname;
	std::size_t first = newname.find_first_of("0123456789");
	std::size_t last = newname.find_last_of("0123456789");
	if (( last - first == 1 ) && ( last == newname.size() - 1 )){
		newname.erase( first, 2 );

	    std::ostringstream stm ;
		stm << setfill('0') << setw(3) << channelIndex ;
		newname = string("Chan") + stm.str();

#if 0
		char nb[ 3 ] = "";
		sprintf( nb, "%03d", channelIndex );
		newname += string( nb );
#endif

		cout << __FILE__ << " " << __LINE__ << " oldname= " << oldname << " newname= " << newname << " corrected to 3-digit format" << endl;
		return( newname );
	} else {
		//		cout << __FILE__ << " " << __LINE__ << " name= " << oldname << " can't be corrected to 3-digit format" << endl;
		return( oldname );
	}
}


bool Configuration::_createBoard( Crate *cr, BoardConfig *cfgboard ){
	BOARDTYPE_t type =	Board::convert_model2type( cfgboard->model());
	string firmware = cfgboard->firmware();
	if ( type == none ) {
		return( false ); // slot is empty
	}
	if ( type == unknown )  {
		cout << __FILE__ << " " << __LINE__ << " _createBoard "
				<< " board type unknown will be replaced by generic" << endl;
		type = GENERIC;
	}

	uint32_t populatedSlot = cfgboard->slot();

	// ns_HAL_Board::Board *board = new ns_HAL_Board::Board( populatedSlot, type );
	ns_HAL_Board::Board *board = new ns_HAL_Board::Board( populatedSlot, type, firmware );
	board->setDescription( cfgboard->description() );
	cr->insertBoard( populatedSlot, board );
	return( true );
}


// returns the key=index=crate handle of the crate, or -1 if not found
int Configuration::_createSYCrate( CrateConfig *cfg ){
	CAENHV_SYSTEM_TYPE_t type = SY4527;
	for ( uint32_t k = 0; k < _genericCrateModels.size(); k++ ){
		// cout << __FILE__ << " " << __LINE__ << " _createSYCrate k= " << k << endl;
		if ( strcmp( ( const char * ) _genericCrateModels[ k ].smodel.c_str(), cfg->model().c_str() ) == 0 ){
			//cout << __FILE__ << " " << __LINE__ << " _createSYCrate found generic crate= " << cfg->model().c_str() << endl;
			type = _genericCrateModels[ k ].imodel;
			string modelName = cfg->model();

			uint32_t crateHandle = _crate_map.size();
			//cout << __FILE__ << " " << __LINE__ << " new crateHandle= " << crateHandle << endl;
			ns_HAL_Crate::Crate *cr = new ns_HAL_Crate::Crate( type, crateHandle, modelName );

			// generic crate is default: no other things need to be configured here
			//cout << __FILE__ << " " << __LINE__ << endl;
			_crate_map.insert( std::pair<uint32_t,ns_HAL_Crate::Crate *>( crateHandle, cr ));
			//cout << __FILE__ << " " << __LINE__ << " _createSYCrate "
			//		<< " created crate type " << cfg->model() << " as generic object" << endl;
			return( crateHandle );
		}
	}
	cout << __FILE__ << " " << __LINE__
			<< " crate type " << cfg->model() << " is not generic, must add a generic object and configure it "	<< endl;

	if ( strcmp( "SY1527", cfg->model().c_str() ) == 0 ){
		type = SY1527;
		uint32_t crateHandle = _crate_map.size();
		ns_HAL_Crate::Crate *cr = new ns_HAL_Crate::Crate( type, crateHandle, cfg->model() );
		_crate_map.insert( std::pair<uint32_t,ns_HAL_Crate::Crate *>( crateHandle, cr ));
		cr->configureSY1527();
		return( crateHandle );
	}
	if ( strcmp( "SY1527LC", cfg->model().c_str() ) == 0 ){
		type = SY1527; // same but without display
		uint32_t crateHandle = _crate_map.size();
		ns_HAL_Crate::Crate *cr = new ns_HAL_Crate::Crate( type, crateHandle, cfg->model() );
		_crate_map.insert( std::pair<uint32_t,ns_HAL_Crate::Crate *>( crateHandle, cr ));
		cr->configureSY1527();
		return( crateHandle );
	}
	if ( strcmp( "SY5527", cfg->model().c_str() ) == 0 ){
		type = SY5527;
		uint32_t crateHandle = _crate_map.size();
		ns_HAL_Crate::Crate *cr = new ns_HAL_Crate::Crate( type, crateHandle, cfg->model() );
		_crate_map.insert( std::pair<uint32_t,ns_HAL_Crate::Crate *>( crateHandle, cr ));
		cr->configureSY5527();
		return( crateHandle );
	}
	if ( strcmp( "SY5527LC", cfg->model().c_str() ) == 0 ){
		type = SY5527;
		uint32_t crateHandle = _crate_map.size();
		ns_HAL_Crate::Crate *cr = new ns_HAL_Crate::Crate( type, crateHandle, cfg->model() );
		_crate_map.insert( std::pair<uint32_t,ns_HAL_Crate::Crate *>( crateHandle, cr ));
		cr->configureSY5527LC();
		return( crateHandle );
	}
	cout << __FILE__ << " " << __LINE__ << " _createSYCrate "
			<< " crate type " << cfg->model() << " not found. "	<< endl;
	return( -1 );
}

uint32_t Configuration::totalNbCrates( void ){
	return( _crate_map.size() );
}
uint32_t Configuration::totalNbBoards( void ){
	uint32_t count = 0;
	for ( uint32_t ic = 0; ic < this->totalNbCrates(); ic++ ){
		count += _crate_map.at( ic )->totalNbBoards();
	}
	return( count );
}

uint32_t Configuration::totalNbChannels( void ){
	uint32_t count = 0;
	for ( uint32_t ic = 0; ic < this->totalNbCrates(); ic++ ){
		uint32_t nbslots = _crate_map.at( ic )->MaxNbSlots();
		for ( uint32_t is = 0; is < nbslots; is++ ){
			if ( _crate_map.at( ic )->boardExists( is ) ){
				count += _crate_map.at( ic )->board( is )->nbChannelObjects();
			}
		}
	}
	return( count );
}

/// real power supply ramping channels in all crates
uint32_t Configuration::totalNbPsChannels2( void ){
	uint32_t count = 0;
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		Crate *cr = crate( icrate );
		count += cr->nbChannels() - cr->nbEasyBoards();
	}
	return( count );
}

uint32_t Configuration::totalNbEasyBoards2( void ){
	uint32_t count = 0;
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		Crate *cr = crate( icrate );
		count += cr->nbEasyBoards();
	}
	return( count );
}

uint32_t Configuration::totalNbBoards2( void ){
	uint32_t count = 0;
	for ( uint32_t icrate = 0; icrate < _crateConfig_map.size(); icrate++ ){
		Crate *cr = crate( icrate );
		count += cr->nbBoards();
	}
	return( count );
}


void Configuration::channelStates( int32_t *count_rup, int32_t *count_rdown,
		int32_t *count_on, int32_t *count_cc, int32_t *count_ptrip ){

	for ( uint32_t icr = 0; icr < this->totalNbCrates(); icr++ ){
		uint32_t nbslots = _crate_map.at( icr )->MaxNbSlots();
		for ( uint32_t is = 0; is < nbslots; is++ ){
			if ( _crate_map.at( icr )->boardExists( is ) ){
				for ( uint32_t ich = 0; ich < _crate_map.at( icr )->board( is )->nbChannelObjects(); ich++ ){
					Channel *ch = _crate_map.at( icr )->board( is )->channel( ich );
					Channel::SM_states_t smState = ch->smState();
					switch ( smState ){
					case Channel::state_rampingUp:       (*count_rup)++; break;
					case Channel::state_rampingDown:     (*count_rdown)++; break;
					case Channel::state_ON:              (*count_on)++; break;
					case Channel::state_ConstantCurrent: (*count_cc)++; break;
					case Channel::state_PendingTrip:     (*count_ptrip)++; break;
					default: break;
					}
				}
			}
		}
	}
}


bool ChannelConfig::propertyExists( string pname ){
	for (std::map<string,PROPERTY_t>::iterator it=channelAPI_map.begin(); it!=channelAPI_map.end(); it++){
		// cout << __FILE__ << " " << __LINE__ << " " << it->first << endl;
		if ( it->first == pname ) return( true );
	}
	return( false );
}


} /* namespace ns_HAL_Configuration */

/*
 * Crate.cpp
 *
 *  Created on: Oct 17, 2016
 *      Author: mludwig
 */



#include "../include/Crate.h"
#include "../include/Configuration.h"

using namespace std;

namespace ns_HAL_Crate {

/* static */ uint32_t Crate::nbCrates = 0;
/* static */ string Crate::_gluehn = "(gluehostname)";
/* static */ string Crate::_glueversion = "(glueversion)";

/**
 * enables debugging on channel ramping statistics and status
 */
//#define DEBUG_STATUS 1

Crate::Crate( CAENHV_SYSTEM_TYPE_t s, int h, string mn ) {
	_scadaGlobalCounter = 0;
	_scadaGlobalMute = false;
	_scadaGlobalReset = false;
	_scadaCounterFlag = false;
	_system = s;
	_handle = h;
	_debug_update = false;
	{
		char buf[ 64 ];
		sprintf( buf, "crate%d", _handle );
		_key = string( buf );
	}
	_updateTime = 0;
	_updateDelay_ms = 600; //ms, a bit slower then 2Hz
	_communication = true;
	_modelName = mn;
	_symbolicName = string("BE(n)ICE;-)") + mn;
	_resFlagCfg = 50;
	_frontPanIn = 32;
	_frontPanOut = 0;
	_FP_VSEL = false; // V0Set
	_FP_ISEL = false; // I0Set
	_status = CAENHV_OK;
	gettimeofday( &_now, &_tz );
	gettimeofday( &_lastUpdate, &_tz );

	// this is a generic crate with generic functionality =  a set of
	// properties and behaviors
	_flipv = false;
	_flipc = false;
	_PWVoltage = "5.13V:-13.16V:13.11V:24.17V:48.92V:12.98V";
	_PWCurrent = "2.20A:1.67A:0.48A:0.67A:0.00A:0.00A:0.00A";
	_HVFanSpeed = 32; _flip0 = false;
	_PWFanStat = "1:1788:1:1823:1:1752";_flip1 = false;
	_HVFanStat = "1:1752:1:1644:1:1788:1:1680:1:1644:1:1680";_flip2 = false;
	_MemoryStatus = "506604:481420:   256:  4692";_flip3 = false;
	_CPULoad = "00.01:00.02:00.01";_flip4 = false;
	_ClkFreq = 133;_flip5 = false;
	_ipAddress = "undefined";

	/// \todo nb of slots availabe for crate types
	/// \todo add a generic type of crate which has unlimited slots

	switch ( s ){
	case SY1527:
	case SY2527:
	case SY4527:
	case SY5527:
	case N568:
	case V65XX:
	case N1470:
	case V8100:
	case N568E:
	case DT55XX:
	case FTK:
	case DT55XXE:
	case N1068: {
		_maxNbBoards = MAX_SLOTS;
		// _maxNbBoards = SIM_MAX_NB_BOARDS;
		break;
	}
	}

	// register the generic CAEN properties for all crates
	API::CAEN_PROPERTY_t p;
	_propinsert("NbSlots",  API::ReadOnlyProperty, API::I ); // specially added for simulation

	_propinsert("Sessions",  API::ReadOnlyProperty, API::S );
	_propinsert("ModelName", API::ReadOnlyProperty, API::S );
	_propinsert("SwRelease", API::ReadOnlyProperty, API::S );
	_propinsert("HvPwSM", API::ReadOnlyProperty, API::S );
	_propinsert("HVFanStat", API::ReadOnlyProperty, API::S );
	_propinsert("CPULoad", API::ReadOnlyProperty, API::S );
	_propinsert("MemoryStatus", API::ReadOnlyProperty, API::S );
	_propinsert("PWFanStat", API::ReadOnlyProperty, API::S );
	_propinsert("PWVoltage", API::ReadOnlyProperty, API::S );

	_propinsert("FrontPanIn", API::ReadOnlyProperty, API::I );
	_propinsert("FrontPanOut", API::ReadOnlyProperty, API::I );
	_propinsert("ResFlag", API::ReadOnlyProperty, API::I );
	_propinsert("ClkFreq", API::ReadOnlyProperty, API::I );
	_propinsert("CmdQueueStatus", API::ReadOnlyProperty, API::I );

	_propinsert("HVClkConf", API::ReadWriteProperty, API::S );
	_propinsert("IPAddr", API::ReadWriteProperty, API::S );
	_propinsert("IPNetMsk", API::ReadWriteProperty, API::S );
	_propinsert("IPGw", API::ReadWriteProperty, API::S );
	_propinsert("PWCurrent", API::ReadWriteProperty, API::S );
	_propinsert("SymbolicName", API::ReadWriteProperty, API::S );

	_propinsert("GenSignCfg", API::ReadWriteProperty, API::I );
	_propinsert("ResFlagCfg", API::ReadWriteProperty, API::I );
	_propinsert("OutputLevel", API::ReadWriteProperty, API::I );
	_propinsert("HVFanSpeed", API::ReadWriteProperty, API::I );
	_propinsert("DummyReg", API::ReadWriteProperty, API::I );
	_propinsert("CMDExecMode", API::ReadWriteProperty, API::I );


	// add two synthetic properties for scada testing. They start with lower case
	/// todo eliminate these added props from the crate again: they should be handles by the pilot UI with messages
	_propinsert("scadaCounter", API::ReadWriteProperty, API::I );
	_propinsert("scadaMute", API::ReadWriteProperty, API::I );
	_propinsert("scadaReset", API::ReadWriteProperty, API::I );


	// create the (still without values) HAL properties
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		_propinsert( API::convertPropCAEN2HAL( _caenProp_map.at( it->first )));
	}

	_execCommList.push_back("dummycommand0");
	_execCommList.push_back("dummycommand1");

	// those static RO properties, we need to set them once only
	setPropertyValue("ModelName", ModelName() );
	setPropertyValue("SwRelease", SwRelease() );
	setPropertyValue("IPAddr", IPAddress() );
	setPropertyValue("IPGw", IPGw() );
	setPropertyValue("IPNetMsk", IPNetMsk() );
	setPropertyValue("NbSlots", MaxNbSlots() );

	// RW set
	setPropertyValue("SymbolicName", this->ModelName() );
	setPropertyValue("scadaCounter", 0 );
	setPropertyValue("scadaMute", 0 );

	Crate::nbCrates++;
}



Crate::~Crate() {
}

/* static */ bool Crate::have( uint32_t handle ){
	if ( handle >= Crate::nbCrates ) return ( false );
	return( true );
}

void Crate::_propinsert( string n, API::PACCESS_TYPE_t a, API::PDATA_TYPE_t t ){
	_testPropNotExist( n );
	API::CAEN_PROPERTY_t p;
	p.name = n;	p.pAccessType = a;	p.pDataType = t;
	_caenProp_map.insert( std::pair<string,API::CAEN_PROPERTY_t>( p.name, p ));
}

vector<string> Crate::propertyList( void ){
	vector<string> v;
	v.clear();
	unsigned int cc = 0;
	for (std::map<string,API::CAEN_PROPERTY_t>::iterator it=_caenProp_map.begin(); it!=_caenProp_map.end(); ++it){
		v.push_back( _caenProp_map.at( it->first ).name );
		if ( _debug ) cout << __FILE__ << " " << __LINE__ << " Crate::propertyList " << v[ cc ] << endl;
		cc++;
	}
	return( v );
}

void Crate::getPropertyHALtype( string p, API::HAL_DATA_TYPE_t *type ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		*type = prop.dataType;
	}
}

/// switches all boards and channels to either f=false=default=V0Set or f=true=V1Set
void Crate::pilot_setVSEL( bool f ){
	_FP_VSEL = f;
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		Board *b = this->board( _populatedSlotsv[ iboard ] );
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );
			Channel *ch = b->channel( pchannel );
			ch->pilot_setVtargetFromVSEL( f );
		}
	}
}

void Crate::pilot_setChannelLoad( uint32_t slot, uint32_t channel, float res, float capa ){

	if ( this->boardExists( slot )){
		Board *b = this->board( slot );
		if ( b->channelExists( channel )){
			Channel *ch = b->channel( channel );
			ch->pilot_setLoad( res, capa );
		}
	}
}

void Crate::pilot_setAllChannelLoads( float res, float capa ){

	// loop over all boards::channels
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		Board *b = this->board( _populatedSlotsv[ iboard ] );
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );
			if ( b->channelExists( pchannel )){
				Channel *ch = b->channel( pchannel );
				ch->pilot_setLoad( res, capa );
				cout << "pilot_setAllChannelLoads channel key= " << ch->key()
									<< " res= " << res << " capa= " << capa << endl;
			}
		}
	}
}

/// mixed & nested functionality: set also parameters via the property interface.
/// We abuse the pilot interface for testing
void Crate::pilot_rampAllChannels( float v0set, float i0set, float rup, float rdown) {

	cout << __FILE__ << " " << __LINE__ << " pilot_rampAllChannels " << endl;
	// loop over all boards::channels
	int chCount = 0;
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		Board *b = this->board( _populatedSlotsv[ iboard ] );
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );
			Channel *ch = b->channel( pchannel );

			if ( ch->isEasyBoardMarkerChannel() ) continue;
			if ( ch->isEasyPowerConverterChannel() ) continue;

			switch ( ch->channelType() ){
			case ns_HAL_Channel::Channel::easyPSchannelWithVSELnoISEL :{
			case ns_HAL_Channel::Channel::primaryPSchannelWithVSELandISEL :{
				ch->setParameterValue("V0Set", v0set );
				ch->setParameterValue("I0Set", i0set );
				ch->setParameterValue("RUp", rup );
				ch->setParameterValue("RDWn", rdown );
				int32_t pw = 1;
				ch->setParameterValue("Pw", pw );
				chCount++;
				//cout << "pilot_rampAllChannels channel key= " << ch->key()
				//		<< " ramping up ..." << endl;
				break;
			}
			case ns_HAL_Channel::Channel::easyPSchannelNoVSELv1 :
			case ns_HAL_Channel::Channel::easyPSchannelNoVSELv2 :
			case ns_HAL_Channel::Channel::primaryPSchannelNoVSEL :{
				ch->setParameterValue("V0Set", v0set );
				ch->setParameterValue("I0Set", i0set );
				int32_t pw = 1;
				ch->setParameterValue("Pw", pw );
				chCount++;
				//cout << "pilot_rampAllChannels channel key= " << ch->key()
				//		<< " switching on ..." << endl;
				break;
			}
			}

			} // for
		}
		cout << "pilot_rampAllChannels board= " << iboard << " #channels= " << chCount << " ramping up ..." << endl;
	} // boards
}

void Crate::pilot_rampAllChannelsOnBoard( uint32_t slot, float v0set, float i0set, float rup, float rdown) {

	int chCount = 0;
	cout << __FILE__ << " " << __LINE__ << " pilot_rampAllChannelsOnBoard iboard= " << slot << endl;
	if ( boardExists( slot ) ) {
		Board *b = this->board( slot );
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );

			if ( b->channelExists(pchannel)){
				Channel *ch = b->channel( pchannel );

				if ( ch->isEasyBoardMarkerChannel() ) continue;
				if ( ch->isEasyPowerConverterChannel() ) continue;

				ch->setParameterValue("V0Set", v0set );
				ch->setParameterValue("I0Set", i0set );
				ch->setParameterValue("RUp", rup );
				ch->setParameterValue("RDWn", rdown );
				int32_t pw = 1;
				ch->setParameterValue("Pw", pw );
				chCount++;
				cout << __FILE__ << " " << __LINE__ << " pilot_rampAllChannels channel key= " << ch->key()	<< " ramping up ..." << endl;
			} else {
				cout << __FILE__ << " " << __LINE__ << " pilot_rampAllChannelsOnBoard iboard= " << slot << " slot= "
						<< slot << " channel= " << pchannel << " does not exist, skipping." << endl;
			}
		}
	} else {
		cout << __FILE__ << " " << __LINE__ << " pilot_rampAllChannelsOnBoard iboard= " << slot << " slot= "
				<< slot << " does not exist, skipping." << endl;
	}
	cout << __FILE__ << " " << __LINE__ << " pilot_rampAllChannelsOnBoard chCount= " << chCount << " ramping up ..." << endl;
}

/**
 * switch off (sw==0) or on (sw==1) Pw of all channels
 */
void Crate::pilot_switchPwAllChannels( int sw ) {

	// loop over all boards::channels
	int chCount = 0;
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		if ( this->boardExists( _populatedSlotsv[ iboard ])){
			Board *b = this->board( _populatedSlotsv[ iboard ] );
			for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
				uint32_t pchannel = b->populatedChannel( ich );
				if ( b->channelExists( pchannel )) {
					Channel *ch = b->channel( pchannel );

					if ( ch->isEasyBoardMarkerChannel() ) continue;
					if ( ch->isEasyPowerConverterChannel() ) continue;

					ch->setParameterValue("Pw", sw );
					chCount++;
					//cout << "pilot_switchOffAllChannels channel key= " << ch->key()
					//		<< " ramping down/off ..." << endl;
				} else {
					cout << __FILE__ << " " << __LINE__ << " pilot_switchPwAllChannels iboard= " << iboard
							<< " channel= " << pchannel << " not found, skipping" << endl;
				}
			}
		} else {
			cout << __FILE__ << " " << __LINE__ << " pilot_switchPwAllChannels board at iboard= " << _populatedSlotsv[ iboard ] << " not found, skipping" << endl;
		}
	}
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << "  #channels= " << chCount << " Pw= " << sw << endl;
}

void Crate::pilot_getChannelLoad( uint32_t slot, uint32_t channel, float *res, float *capa ){
	cout << __FILE__ << " " << __LINE__ << " pilot_getChannelLoad slot= " << slot << " channel= " << channel << endl;
	if ( this->boardExists( slot )){
		Board *b = this->board( slot );
		if ( b->channelExists( channel )) {
			Channel *ch = b->channel( channel );
			ch->pilot_getLoad( res, capa );
			cout << __FILE__ << " " << __LINE__ <<  endl;
		} else {
			cout << __FILE__ << " " << __LINE__ << " pilot_getChannelLoad slot= " << slot
					<< " channel= " << channel << " not found, skipping" << endl;
		}
	} else {
		cout << __FILE__ << " " << __LINE__ << " pilot_getChannelLoad board at slot= " << slot << " not found, skipping" << endl;
	}
}

/// this should trip by raising the voltage until the current exceeds the current-limit, with constant load
void Crate::pilot_tripChannelByVoltage( uint32_t slot, uint32_t channel ){
	if ( this->boardExists( slot )){
		Board *b = this->board( slot );
		if ( b->channelExists( channel )) {
			Channel *ch = b->channel( channel );
			float res;
			float capa;
			float imax;
			ch->pilot_getLoad( &res, &capa );
			if ( _FP_ISEL )
				ch->getParameterValue("I1Set", &imax );
			else
				ch->getParameterValue("I0Set", &imax );

			float vlimit = 1.1 * res * imax;

			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByVoltage vlimit= " << vlimit << " V, setting it ! " << endl;

			if ( _FP_VSEL )
				ch->setParameterValue("V1Set", vlimit );
			else
				ch->setParameterValue("V0Set", vlimit );
		} else {
			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByVoltage slot= " << slot
					<< " channel= " << channel << " not found, skipping" << endl;
		}
	} else {
		cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByVoltage board at slot= " << slot << " not found, skipping" << endl;
	}
}

/// this should trip by lowering the current-limit until the current exceeds the current-limit, with constant load and voltage
void Crate::pilot_tripChannelByCurrent( uint32_t slot, uint32_t channel ){
	if ( this->boardExists( slot )){
		Board *b = this->board( slot );
		if ( b->channelExists( channel )) {
			Channel *ch = b->channel( channel );
			float res;
			float capa;
			float vmon;
			float ilimit = 0;
			ch->pilot_getLoad( &res, &capa );
			ch->getParameterValue("VMon", &vmon );

			if ( res > 0 )
				ilimit = 0.9 * vmon / res; // uA

			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByCurrent ilimit= " << ilimit << " A, setting it ! " << endl;

			if ( _FP_ISEL )
				ch->setParameterValue("I1Set", ilimit );
			else
				ch->setParameterValue("I0Set", ilimit );
		} else {
			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByCurrent slot= " << slot
					<< " channel= " << channel << " not found, skipping" << endl;
		}
	} else {
		cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByCurrent board at slot= " << slot << " not found, skipping" << endl;
	}
}

/**
 * this should trip by lowering the load until the current exceeds the current-limit, with constant voltage.
 * we don't touch the capacitance
*/
void Crate::pilot_tripChannelByLoad( uint32_t slot, uint32_t channel ){
	if ( this->boardExists( slot )){
		Board *b = this->board( slot );
		if ( b->channelExists( channel )) {
			Channel *ch = b->channel( channel );
			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByLoad channel key= " << ch->key() << endl;
			float res = 0;
			float capa = 0;
			float vmon;
			float ilimit = 0;
			ch->getParameterValue("VMon", &vmon );
			if ( _FP_ISEL )
				ch->getParameterValue("I1Set", &ilimit );
			else
				ch->getParameterValue("I0Set", &ilimit );

			res = 0.9 * vmon / ilimit;
			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByLoad res= " << res << " Ohm, setting it ! " << endl;
			ch->pilot_setLoad( res, capa );
		} else {
			cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByLoad slot= " << slot
					<< " channel= " << channel << " not found, skipping" << endl;
		}
	} else {
		cout << __FILE__ << " " << __LINE__ << " pilot_tripChannelByLoad board at slot= " << slot << " not found, skipping" << endl;
	}
}

/// switches all boards and channels to either f=false=default=I0Set or f=true=I1Set
void Crate::pilot_setISEL( bool f ){
	_FP_ISEL = f;
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		Board *b = this->board( _populatedSlotsv[ iboard ] );
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );
			Channel *ch = b->channel( pchannel );
			ch->pilot_setImaxFromISEL( f );
		}
	}
}

bool Crate::_testPropExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it == _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= "<< _key;
		cout << __FILE__ << " " << __LINE__ << " _testPropExist property " << p << " not found, key= "<< _key << endl;
		// throw std::runtime_error( os.str() );
		return( false );
	}
	return( true );
}
bool Crate::_testPropNotExist( string p ) {
	std::map<string,API::HAL_PROPERTY_t>::iterator it = _prop_map.find( p );
	if ( it != _prop_map.end()){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= "<< _key;
		cout << __FILE__ << " " << __LINE__ << " _testPropNotExist property " << p << " exists already, key= "<< _key << endl;
		// throw std::runtime_error( os.str() );
		return( false );
	}
	return( true );
}

void Crate::getPropertyValue( string p, string *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}

void Crate::getPropertyValue( string p, int8_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Crate::getPropertyValue( string p, int16_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Crate::getPropertyValue( string p, int32_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Crate::getPropertyValue( string p, uint8_t *retval ){
	_testPropExist( p );
	API::HAL_PROPERTY_t prop = _prop_map.at( p );
	API::getPropValue( prop, retval );
}
void Crate::getPropertyValue( string p, uint16_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Crate::getPropertyValue( string p, uint32_t *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}
void Crate::getPropertyValue( string p, float *retval ){
	if (_testPropExist( p ) ){
		API::HAL_PROPERTY_t prop = _prop_map.at( p );
		API::getPropValue( prop, retval );
	}
}

void Crate::setPropertyValue( string p, string val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_string = _prop_map.at( p ).value_string;
		_prop_map.at( p ).value_string = val;
	}
}
void Crate::setPropertyValue( string p, int8_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_int8 = _prop_map.at( p ).value_int8;
		_prop_map.at( p ).value_int8 = val;
	}
}
void Crate::setPropertyValue( string p, int16_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_int16 = _prop_map.at( p ).value_int16;
		_prop_map.at( p ).value_int16 = val;
	}
}
void Crate::setPropertyValue( string p, int32_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_int32 = _prop_map.at( p ).value_int32;
		_prop_map.at( p ).value_int32 = val;
	}
}
void Crate::setPropertyValue( string p, uint8_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_uint8 = _prop_map.at( p ).value_uint8;
		_prop_map.at( p ).value_uint8 = val;
	}
}
void Crate::setPropertyValue( string p, uint16_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_uint16 = _prop_map.at( p ).value_uint16;
		_prop_map.at( p ).value_uint16 = val;
	}
}
void Crate::setPropertyValue( string p, uint32_t val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_uint32 = _prop_map.at( p ).value_uint32;
		_prop_map.at( p ).value_uint32 = val;
	}
}
void Crate::setPropertyValue( string p, float val ){
	if (_testPropExist( p ) ){
		_prop_map.at( p ).previous_string = _prop_map.at( p ).value_string;
		_prop_map.at( p ).value_string = val;
	}
}

void Crate::insertBoard( uint32_t populatedSlot, Board *b ){
	_board_map.insert(std::pair<uint32_t,ns_HAL_Board::Board *>( populatedSlot, b ));
}

bool Crate::boardExists( uint32_t slot ){
	std::map<uint32_t, Board *>::iterator it;
	it = _board_map.find( slot );
	if ( it == _board_map.end() ) return( false );
	else return( true );
};

// one could still re-discover this one and re-init it, the object still exists.
void Crate::deinit( void ) {
	_communication = false;
}

// fake, but a bit alive
string Crate::PWVoltage( void ) {
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flipv )
			_PWVoltage = "5.13V:-13.16V:13.11V:24.17V:48.92V:12.98V";
		else
			_PWVoltage = "5.12V:-13.14V:13.09V:24.22V:48.98V:13.01V";
		_flipv = !_flipv;
		count = 0;
	}
	return (_PWVoltage );
}
string Crate::PWCurrent( void ) {
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flipc )
			_PWCurrent = "2.20A:1.67A:0.48A:0.67A:0.00A:0.00A:0.00A";
		else
			_PWCurrent = "2.21A:1.68A:0.46A:0.65A:0.01A:0.02A:0.01A";

		_flipc = !_flipc;
		count = 0;
	}
	return (_PWCurrent );
}
int32_t Crate::HVFanSpeed( void ) {
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip0 )
			_HVFanSpeed = 1612;
		else
			_HVFanSpeed = 1721;
		_flip0 = !_flip0;
		count = 0;
	}
	return ( _HVFanSpeed );
}
string Crate::PWFanStat( void ) {
	if (!_intrinsicUpdates ) return ("1:1788:1:1823:1:1752");
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip1 )
			_PWFanStat = "1:1788:1:1823:1:1752";
		else
			_PWFanStat = "1:1779:1:1821:1:1768";
		_flip1 = !_flip1;
		count = 0;
	}
	return( _PWFanStat );
}
/// never changes: don't know actually what this means
string Crate::HvPwSM( void ) {
	if (!_intrinsicUpdates ) return ("1:1:0:0:0");
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip2 )
			_HvPwSM = "1:1:0:0:0";
		else
			_HvPwSM = "1:1:0:0:0";
		_flip6 = !_flip6;
		count = 0;
	}
	return( _HvPwSM );
}
string Crate::HVFanStat( void ) {
	if (!_intrinsicUpdates ) return ("1:1752:1:1644:1:1788:1:1680:1:1644:1:1680");
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip2 )
			_HVFanStat = "1:1752:1:1644:1:1788:1:1680:1:1644:1:1680";
		else
			_HVFanStat = "1:1750:1:1648:1:1793:1:1699:1:1634:1:1690";
		_flip2 = !_flip2;
		count = 0;
	}
	return( _HVFanStat );
}
string Crate::MemoryStatus( void ) {
	if (!_intrinsicUpdates ) return ("506604:481420:   256:  4692");
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip3 )
			_MemoryStatus = "506604:481420:   256:  4692";
		else
			_MemoryStatus = "506603:481421:   256:  4690";
		_flip3 = !_flip3;
		count = 0;
	}
	return( _MemoryStatus );
}
string Crate::CPULoad( void ) {
	if (!_intrinsicUpdates ) return ("00.01:00.02:00.01");
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip4 )
			_CPULoad = "00.01:00.02:00.01";
		else
			_CPULoad = "00.01:00.02:00.11";
		_flip4 = !_flip4;
		count = 0;
	}
	return( _CPULoad );
}
int32_t Crate::ClkFreq( void ) {
	if (!_intrinsicUpdates ) return (133);
	static int count = 20;
	if ( ++count >= 10 ){
		if ( _flip5 )
			_ClkFreq = 133;
		else
			_ClkFreq = 132;
		_flip5 = !_flip5;
		count = 0;
	}
	return ( _ClkFreq );
}
string Crate::SwRelease( void ) {
	return( string( __DATE__ ) + string("crate v0.1"));
}

void Crate::configureSY1527( void ){
	_maxNbBoards = 16;
}
void Crate::configureSY5527( void ){
	_maxNbBoards = 6;
}
void Crate::configureSY5527LC( void ){
	_maxNbBoards = 4;
}

// adjust to ~2Hz call frequency, waste any time remaining
void Crate::intelligentDelay( void ){
	gettimeofday( &_now, &_tz );
	_delta_ms = (double) ( _now.tv_sec - _lastUpdate.tv_sec) * 1000 + (double) ( _now.tv_usec - _lastUpdate.tv_usec) / 1000.0;
	if ( _delta_ms > 0 ) {
		if ( _delta_ms < _updateDelay_ms ){
			_tim1.tv_sec = (uint32_t) ( (_updateDelay_ms - _delta_ms) / 1000 );
			_tim1.tv_nsec = ( _updateDelay_ms - _delta_ms - _tim1.tv_sec * 1000 ) * 1e6;

			_tim1.tv_nsec = ((int64_t)(_tim1.tv_nsec / 1e7 )) * 1e7; // adjust to 10ms steps
			_tim1.tv_nsec += 1e7;
		} else {
			_tim1.tv_sec = 0;
			_tim1.tv_nsec = 100 * 1e6;
			cout << "_intelligentDelay warning: detecting slow-down on update, updateDelay_ms= "
					<< _updateDelay_ms << " delta_ms= " << _delta_ms << endl;
		}
	}

	// just normal paranoia
	if (( _tim1.tv_sec < 0 ) || ( _tim1.tv_nsec <= 20000 )){
		cout << __FILE__ << " " << __LINE__ << " _intelligentDelay: delta_ms= " << _delta_ms << " nanosleep sec= "
				<< _tim1.tv_sec << " nsec= " << _tim1.tv_nsec << " something went wrong in delay calculation, adjusting..." << endl;
		_tim1.tv_sec = 0;
		_tim1.tv_nsec = 100 * 1e6;
	}
	//cout << __FILE__ << " " << __LINE__ << " _intelligentDelay: delta_ms= " << _delta_ms << " nanosleep sec= "
	//				<< _tim1.tv_sec << " nsec= " << _tim1.tv_nsec << " _updateDelay_ms= " << _updateDelay_ms << endl;

	if( nanosleep(&_tim1 , &_tim2) != 0 ) {
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " _intelligentDelay: delta_ms= " << _delta_ms << " nanosleep sec= "
				<< _tim1.tv_sec << " nsec= " << _tim1.tv_nsec << " system call failed or prematurely interrupted" << endl;
		// throw std::runtime_error( os.str() );
	}
}

Crate::EVENT_ITEM_t Crate::fabricateCrateEventItem( std::map<string,API::HAL_PROPERTY_t>::iterator it ){
	EVENT_ITEM_t item;
	item.statusBoard = -1; // this is a crate
	item.caen.SystemHandle = this->handle();
	item.caen.BoardIndex   = -1; // this is a crate
	item.caen.ChannelIndex = -1; // this is a crate
	item.caen.Type = PARAMETER;

	sprintf( item.caen.ItemID, it->first.c_str() ); // the property name
	item.haltype = it->second.dataType;
	switch( it->second.dataType ){
	case API::tstr: sprintf( item.caen.Value.StringValue, it->second.value_string.c_str()); break;
	case API::tint32: item.caen.Value.IntValue = it->second.value_int32; break;
	case API::tint16: item.caen.Value.IntValue = it->second.value_int16; break;
	case API::tint8: item.caen.Value.IntValue = it->second.value_int8; break;
	case API::tuint32: item.caen.Value.IntValue = it->second.value_uint32; break;
	case API::tuint16: item.caen.Value.IntValue = it->second.value_uint16; break;
	case API::tuint8: item.caen.Value.IntValue = it->second.value_uint8; break;
	case API::treal: item.caen.Value.FloatValue = it->second.value_float; break;
	default:
		{
			cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property type not found" << endl;
			break;
		}
	}
	return( item );
}

Crate::EVENT_ITEM_t Crate::fabricateBoardEventItem( std::map<string,API::HAL_PROPERTY_t>::iterator it, Board *b ){
	EVENT_ITEM_t item;
	item.statusBoard = 99; // b->status; // don't know where this is supposed to come from nor what it means
	item.caen.SystemHandle = this->handle();
	item.caen.BoardIndex   = b->slot();
	item.caen.ChannelIndex = -1; // this is a board
	item.caen.Type = PARAMETER;
	sprintf( item.caen.ItemID, it->first.c_str() ); // the property name
	// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
	item.haltype = it->second.dataType;
	switch( it->second.dataType ){
	case API::tstr: sprintf( item.caen.Value.StringValue, it->second.value_string.c_str()); break;
	case API::tint32: item.caen.Value.IntValue = it->second.value_int32; break;
	case API::tint16: item.caen.Value.IntValue = it->second.value_int16; break;
	case API::tint8: item.caen.Value.IntValue = it->second.value_int8; break;
	case API::tuint32: item.caen.Value.IntValue = it->second.value_uint32; break;
	case API::tuint16: item.caen.Value.IntValue = it->second.value_uint16; break;
	case API::tuint8: item.caen.Value.IntValue = it->second.value_uint8; break;
	case API::treal: item.caen.Value.FloatValue = it->second.value_float; break;
	default:
		{
			cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property type not found" << endl;
			break;
		}
	}
	return( item );
}

Crate::EVENT_ITEM_t Crate::fabricateChannelEventItem( std::map<string,API::HAL_PROPERTY_t>::iterator it, Channel *c ){
	EVENT_ITEM_t item;
	/// \todo code BdStatus correctly in the EventData
	item.statusBoard = 998899; // b->status; // don't know where this is supposed to come from nor what it means
	item.caen.SystemHandle = this->handle();
	item.caen.BoardIndex   = c->mySlot();
	item.caen.ChannelIndex = c->channel();
	item.caen.Type = PARAMETER; // 	PARAMETER		= 0,	ALARM			= 1,	KEEPALIVE		= 2

	if (_debug){
		cout << __FILE__ << " " << __LINE__ << " Crate::fabricateChannelEventItem: slot= " << c->mySlot()
					<< " channel= " << c->channel()
					<< " property= " << it->first.c_str() << endl;
	}

	sprintf( item.caen.ItemID, it->first.c_str() ); // the property name

	// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
	// the event items know only string, float and int
	// therefore we have to cast all machine-type integers into signed 32bit ints here.
	// this is a shame but CAEN's API is not detailed like this.
	item.haltype = it->second.dataType;
	switch( it->second.dataType ){
	case API::tstr: sprintf( item.caen.Value.StringValue, it->second.value_string.c_str()); break;
	case API::tint32: item.caen.Value.IntValue = it->second.value_int32; break;
	case API::tint16: item.caen.Value.IntValue = it->second.value_int16; break;
	case API::tint8: item.caen.Value.IntValue = it->second.value_int8; break;
	case API::tuint32: item.caen.Value.IntValue = it->second.value_uint32; break;
	case API::tuint16: item.caen.Value.IntValue = it->second.value_uint16; break;
	case API::tuint8: item.caen.Value.IntValue = it->second.value_uint8; break;
	case API::treal: item.caen.Value.FloatValue = it->second.value_float; break;
	default:
		{
			cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " property type not found" << endl;
			break;
		}
	}

	// debug a specific item
#if 0
	{
		if (( item.caen.ItemID == string("Status")) && ( item.caen.BoardIndex == 0 ) && ( item.caen.ChannelIndex == 0 )){
			cout << __FILE__ << " " << __LINE__ << " fabricateChannelEventItem: status0 = 0x" << hex << item.caen.Value.IntValue << dec
					<< " haltype= " << item.haltype << endl;
		}
	}
#endif
	return( item );
}


/// reset event items, collect the new event items, if two conditions are met:
/// (1) the crate/board/channel item is subscribed to
/// (2) the corresponding crate/board/channel property/parameter has changed it's value enough to justify an item
/// In electronics reality events can come from several crates: EASY crates, but in fact these all appear as
/// many channels of a single board, the branch controller. So... they'll always show the "real mother" controller semantically.
void Crate::collectEventItems( vector<Crate::EVENT_ITEM_t> *evts ){

	evts->clear();
	if ( ! _scadaGlobalMute ){
		for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
			if ( API::changedValue( it ) ){
				evts->push_back( fabricateCrateEventItem( it ) );
				if ( _scadaCounterFlag ) updateCounter( this->key() + string("_") + it->first );
			}
		}
		// boards
		for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
			Board *b = this->board( _populatedSlotsv[ iboard ] );
			map<string,API::HAL_PROPERTY_t> boardItems_map = b->allChangedItems();
			for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=boardItems_map.begin(); it!=boardItems_map.end(); ++it) {
				evts->push_back( fabricateBoardEventItem( it, b ) );
				if ( _scadaCounterFlag ) updateCounter( b->key() + string("_") + it->first );
			}

			// channels
			for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
				uint32_t pchannel = b->populatedChannel( ich );
				Channel *ch = b->channel( pchannel );
				map<string,API::HAL_PROPERTY_t> channelItems_map = ch->allChangedItems();
				for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=channelItems_map.begin(); it!=channelItems_map.end(); ++it) {
					evts->push_back( fabricateChannelEventItem( it, ch ) );
					if ( _scadaCounterFlag ) updateCounter( ch->key() + string("_") + it->first );
				}
			}
		}
		// cout << __FILE__ << " " << __LINE__ << " updated " << _countersMap.size() << " scada counters" << endl;
		// cout << __FILE__ << " " << __LINE__ << " collectEventItems total #items= " <<  evts->size() << endl;

		// we want to avoid very large vectors in one go, and also want to avoid strictly ordered appearance per
		// channel, board, property. Therefore randomize the vector, limit to 10000 elements and drop the others.
		// They will be picked up the next call, since we buffer them until then.
		const uint32_t evtSizeLimit = SIM_EVENT_ITEMS_LIMIT;
		std::random_shuffle ( evts->begin(), evts->end() );
		if ( evts->size() > evtSizeLimit ){
			_itOverflow = evts->begin() + evtSizeLimit;
			_evtsOverflow.assign( _itOverflow, evts->end() );
			evts->erase (evts->begin() + evtSizeLimit, evts->end());
			cout << __FILE__ << " " << __LINE__ << " collectEventItems overflow size= " << _evtsOverflow.size() << endl;
		} else {
			// we append what was left over from the overflow until overflow is empty or
			// until evtSizeLimit is hit again. Start at the beginning of the overflow (==oldest)
			int count = 0;
			while ( _evtsOverflow.size() && (evts->size() < evtSizeLimit) ){
				evts->push_back(_evtsOverflow[ 0 ]);
				_evtsOverflow.erase( _evtsOverflow.begin() );
				count++;
			}
			if ( count )
				cout << __FILE__ << " " << __LINE__ << " collectEventItems appending " << count << " from overflow, size= " << _evtsOverflow.size() << endl;
			if ( _evtsOverflow.size() > SIM_EVENT_ITEMS_OVER_LIMIT ){
				cout << __FILE__ << " " << __LINE__ << " collectEventItems you are pushing it very hard here, have lots of event items "
						<< _evtsOverflow.size() << " from overflow" << endl;
			}
		}
		_scadaGlobalCounter += evts->size(); // we count items
	} else {
		cout << __FILE__ << " " << __LINE__ << " collectEventItems MUTED" << endl;
	}

	if ( _scadaGlobalReset ) {
		cout << __FILE__ << " " << __LINE__ << " collectEventItems RESET" << endl;
		_scadaGlobalCounter = 0;
		_resetCounters();
		_scadaGlobalReset = false;
	}

	if ( _scadaCounterFlag ){
		setPropertyValue("scadaCounter", _scadaGlobalCounter );
		setPropertyValue("scadaReset", _scadaGlobalReset );
	}
}

int32_t Crate::nbChannels( void ){
	int32_t count = 0;
	for ( uint32_t iboard = 0; iboard < _maxNbBoards; iboard++ ){
		if ( this->boardExists( iboard )){
			count += this->board( iboard )->nbChannelObjects();
		}
	}
	return( count );
}

// count all easy boards: look at special Chan000
int32_t Crate::nbEasyBoards( void ){
	int32_t count = 0;
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		Board *b = this->board( _populatedSlotsv[ iboard ] );
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );
			if ( b->channelExists( pchannel ) ) {
				if ( b->channel( pchannel )->isEasyBoardMarkerChannel() )
					count++;
			}
		}
	}
	return( count );
}

int32_t Crate::nbBoards( void ){
	int32_t count = 0;
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		if ( this->boardExists(_populatedSlotsv[ iboard ]) ) count++;
	}
	return( count );
}

/**
 *  updates all dynamic crate parameters, plus all dependent boards, plus all of their channels.
 *  basically faked values and state machine values put into API
 */
void Crate::update( void ){

	if ( _debug_update ) cout << __FILE__ << " " << __LINE__ << " update " << _key << endl;

#if DEBUG_STATUS
	map<string, int> supervisionStatusm;
	bool debugstatus0flag = false;
	bool debugstatus1flag = false;
#endif
	// crate RO
	setPropertyValue("PWVoltage", PWVoltage() );
	setPropertyValue("PWCurrent", PWCurrent() );
	setPropertyValue("HVFanSpeed", HVFanSpeed() );
	setPropertyValue("PWFanStat", PWFanStat() );
	setPropertyValue("HVFanStat", HVFanStat() );
	setPropertyValue("HvPwSM", HvPwSM() );
	setPropertyValue("MemoryStatus", MemoryStatus() );
	setPropertyValue("CPULoad", CPULoad() );
	setPropertyValue("ClkFreq", ClkFreq() );

	// crate dumb RW
	setPropertyValue("SymbolicName", SymbolicName() );
	setPropertyValue("ResFlagCfg", ResFlagCfg() );
	setPropertyValue("FrontPanIn", FrontPanIn() );
	setPropertyValue("FrontPanOut", FrontPanOut() );

	// now, all boards
	for ( uint32_t iboard = 0; iboard < _populatedSlotsv.size(); iboard++ ){
		Board *b = this->board( _populatedSlotsv[ iboard ] );
		if ( _debug_update ) cout << __FILE__ << " " << __LINE__ << " update " << b->key() << endl;

		if ( b->isBranchController()){
			// branch controllers
			if ( _debug_update ) cout << __FILE__ << " " << __LINE__ << " update branch controller " << b->key() << endl;
			b->setParameterValue("Vsel", b->Vsel() );
			b->setParameterValue("HW Reset0", b->HWReset0() );
			b->setParameterValue("Recovery0", b->Recovery0() );
			b->setParameterValue("HW Reset1", b->HWReset0() );
			b->setParameterValue("Recovery1", b->Recovery0() );
			b->setParameterValue("HW Reset2", b->HWReset0() );
			b->setParameterValue("Recovery2", b->Recovery0() );
			b->setParameterValue("HW Reset3", b->HWReset0() );
			b->setParameterValue("Recovery3", b->Recovery0() );
			b->setParameterValue("HW Reset4", b->HWReset0() );
			b->setParameterValue("Recovery4", b->Recovery0() );
			b->setParameterValue("HW Reset5", b->HWReset0() );
			b->setParameterValue("Recovery5", b->Recovery0() );
			b->setParameterValue("GlobalOn", b->GlobalOn() );
			b->setParameterValue("GlobalOff", b->GlobalOff() );
			b->setParameterValue("48VA1676", b->x48VA1676() );
			b->setParameterValue("A1676Ilk", b->x1676Ilk() );
		} else {
			// normal boards
			b->setParameterValue("BdStatus", b->BdStatus() );
			b->setParameterValue("HVMax", b->HVMax() );
			b->setParameterValue("Temp", b->Temp() );
		}

		// now, update all populated channels. Attention to branch controllers.
		//
		// Here we can make a distinction between different types of boards with
		// different behaviors: we can just skip or add calls to update some more
		// specific parts of the API, which reflect the behavior in question,
		// but without touching the underlying code.
		if ( _debug_update ) cout << __FILE__ << " " << __LINE__ << " update " << b->key()
						<< " has " << b->nbPopulatedChannels() << " populated channels " << endl;
		for ( uint32_t ich = 0; ich < b->nbPopulatedChannels(); ich ++){
			uint32_t pchannel = b->populatedChannel( ich );
			Channel *ch = b->channel( pchannel );
			if ( _debug_update ) cout << __FILE__ << " " << __LINE__ << " update " << ch->key() << endl;

			// update channel state machine: one step
			ch->sm();

			// any specific board behavior / API calls go here

#if DEBUG_STATUS
			/**
			 * collect status from all channels
			 */
			{
				if (! ch->isEasyBoardMarkerChannel() ){
					int status;
					ch->getParameterValue("Status", &status );

					// debug just one channel: the first one which shows status 0
					if ( status == 0 && !debugstatus0flag){
						cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " first ch status==0 key= " << ch->key() << endl;
						debugstatus0flag = true;
					}
					if ( status == 1 && !debugstatus1flag){ // channels which do not ramp as well
						cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " first ch status==1 key= " << ch->key() << endl;
						debugstatus1flag = true;
					}
					supervisionStatusm.insert ( std::pair<string,int>( ch->key(), status) );
				}
			}
#endif
		}
	} // for .. iboard


#if DEBUG_STATUS
	/**
	 * analyze the status of all channels
	 */
	std::map<string,int>::iterator it = supervisionStatusm.begin();
	int countStatus0 = 0;
	int countStatus1 = 0;
	int countStatus3 = 0;
	int countStatusX = 0;
	for (it=supervisionStatusm.begin(); it!=supervisionStatusm.end(); ++it){
		// std::cout << it->first << " => " << it->second << '\n';
		switch ( it->second ){
		case 0: countStatus0++; break;
		case 1: countStatus1++; break;
		case 3: countStatus3++; break;
		default: countStatusX++; break;
		}
	}
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " nb channels with status0 = " << countStatus0 << endl;
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " nb channels with status1 = " << countStatus1 << endl;
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " nb channels with status3 = " << countStatus3 << endl;
	cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " nb channels with statusX = " << countStatusX << endl << endl;
#endif

	gettimeofday( &_lastUpdate, &_tz );
}

uint32_t Crate::status( void ){
	/// \todo the crate status is not always SYNC...
	_status = SYNC;
	return( _status );
}

void Crate::showProperties( void ){
	cout << endl << "=== " << __FILE__ << " " << __LINE__ << " showProperties crate key= " << _key << " ===" << endl;
	for ( std::map<string,API::HAL_PROPERTY_t>::iterator it=_prop_map.begin(); it!=_prop_map.end(); ++it) {
		cout << __FILE__ << " " << __LINE__ << " " << it->first << endl;
	}
}
void Crate::subscribeParameter( string param, bool flag ){
	_testPropExist( param );
	_prop_map.at( param ).subscribed = flag;
}

void Crate::setConfiguration( string gluehn, string glueversion ){
	Crate::_gluehn = gluehn; // the last glue code which connected... can have several ;-)
	Crate::_glueversion = glueversion;
}

void Crate::getConfiguration( int32_t *nbcrates, string *configFile, string *enginehostname,
		string *gluehn, string *glueversion,
		string *engineversion ){
	*nbcrates = Crate::nbCrates;
	char hname[ 512 ] = "";
	gethostname( hname, 512 );
	*enginehostname = string( hname );
	*gluehn = Crate::_gluehn;
	*glueversion = Crate::_glueversion;
	*configFile = ns_HAL_Configuration::Configuration::getConfigfile();
	*engineversion = string("s.engine version ") + string(__DATE__) + " " + string(__TIME__);
}


void Crate::getHealthEngine( int32_t *nbCrates, int32_t *nbBoards, int32_t *nbChannels,
		int32_t *nbChannelsRampingUp, int32_t *nbChannelsRampingDwn, int32_t *nbChannelsTripped,
		int32_t *nbChannelsOn, int32_t *nbChannelsCC ){

	ns_HAL_Configuration::Configuration *cfg = ns_HAL_Configuration::Configuration::Instance();
	*nbCrates = cfg->totalNbCrates();
	*nbBoards = cfg->totalNbBoards();
	*nbChannels = cfg->totalNbChannels();
	cfg->channelStates( nbChannelsRampingUp, nbChannelsRampingDwn,
			nbChannelsOn, nbChannelsCC, nbChannelsTripped );
}

std::vector<std::string> Crate::extractNamesFromCountersMap( void ){
	std::vector<std::string> namesv;
	for( CounterMap_t::iterator it = _countersMap.begin(); it != _countersMap.end(); ++it ) {
		namesv.push_back( it->first );
	}
	return ( namesv );
}

std::vector<int32_t> Crate::extractValuesFromCountersMap( void ){
	std::vector<int32_t> valuesv;
	for( CounterMap_t::iterator it = _countersMap.begin(); it != _countersMap.end(); ++it ) {
		valuesv.push_back( it->second );
	}
	return ( valuesv );
}


/**
 * add a scada counter name: using crate and board and channel as well as id.
 */
void Crate::_addCounter( string name ){
	string cname = this->key() + string("_") + name;
	_countersMap.insert ( std::pair<std::string,int32_t>(cname, 0) );
}
void Crate::_addCounter( Board *b, string name ){
	string cname = b->key() + string("_") + name;
	_countersMap.insert ( std::pair<std::string,int32_t>(cname, 0) );
}
void Crate::_addCounter( Channel *c, string name ){
	string cname = c->key() + string("_") + name;
	_countersMap.insert ( std::pair<std::string,int32_t>(cname, 0) );
}
/**
 * update the value of a counter = increase
 */
void Crate::updateCounter( string cname ){
	//cout << __FILE__ << " " << __LINE__ << " updateCounter " << cname << endl;
	_countersMap.at( cname )++;
}
void Crate::updateCounter( Board *b, string cname ){
	//cout << __FILE__ << " " << __LINE__ << " updateCounter " << cname << endl;
	_countersMap.at( cname )++;
}
void Crate::updateCounter( Channel *c, string cname ){
	//cout << __FILE__ << " " << __LINE__ << " updateCounter " << cname << endl;
	_countersMap.at( cname )++;
}
/**
 * reset all counters to 0 in one go
 */
void Crate::_resetCounters( void ){
	for( CounterMap_t::iterator it = _countersMap.begin(); it != _countersMap.end(); ++it ) {
		it->second = 0;
	}
}



/**
 * initializes counter map (names) so that all properties have their counter for the scada tests.
 * all values are 0
 */
void Crate::initCountersMap( void ){

	// crate properties
	for ( map<string,API::HAL_PROPERTY_t>::iterator it0= _prop_map.begin(); it0 != _prop_map.end(); ++it0 ){
		if ( _debug) cout << __FILE__ << " " << __LINE__ << " initCountersMap: crate " << it0->first << endl;
		_addCounter( it0->first );
	}

	// all boards: properties
	for ( std::map<uint32_t,Board *>::iterator it1= _board_map.begin(); it1 != _board_map.end(); ++it1 ){
		Board *board = it1->second;
		vector<std::string> propv0 = board->showProperties();
		for ( unsigned int i0 = 0; i0 < propv0.size(); i0++ ){
			if (_debug ) cout << __FILE__ << " " << __LINE__ << " initCountersMap: board " << propv0[ i0 ] << endl;
			_addCounter( board, propv0[ i0 ] );
		}

		// each channel: properties
		unsigned int nbChannels = board->nbChannelObjects();
		for ( unsigned int ich = 0; ich < nbChannels; ich++ ){
			Channel *channel = board->channel( ich );
			vector<std::string> propv1 = channel->showProperties();
			for ( unsigned int i1 = 0; i1 < propv1.size(); i1++ ){
				if ( _debug ) cout << __FILE__ << " " << __LINE__ << " initCountersMap: channel " << propv1[ i1 ] << endl;
				_addCounter( channel, propv1[ i1 ] );
			}
		}
	}
	if (_debug) 	cout << __FILE__ << " " << __LINE__ << " initCountersMap: have " << _countersMap.size() << " scada counters in total" << endl;
}


} /* namespace ns_HAL_Controller */

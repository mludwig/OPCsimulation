simulation engine
=================

michael.ludwig@cern.ch BE-ICS-FD (frameworks & development)

This is the hardware abstraction object eco-system, "simulation engine", for CAEN power supplies

* hardware architecture/layout is configured by consuming an xml config file, which is versy similar to OPC discovery
* stand-alone task, does networking over zmq
* runs objects for each crate, board, channel
* objects have states according to runtime-history
* reproduces the vendor API behavior
* should run on any cc7 linux ws in user space (check needed shared libs, but all is +_standard)
* should scale to many thousands of channels
* CAEN hardware replaced by generic objects

two sockets: simulation and pilot
single threaded, based on zero-MQ, polling
decoding based google protocol buffers

start simulation:
=================
./simulationHAL -cfg ./simConfig_1c_16b_16h.xml   # 1 crate, 16 boards, each 16 channels
 

start the OPC server, easiest is the discovery mode:
====================================================
.../opc/bin/OpcUaCaenServer --hardwareDiscoveryMode

either install the missing shared libs on your system, or just download them
then after downloading add the load path like i.e. 
export LD_LIBRARY_PATH=`pwd`/lib

 
[mludwig@pcbe[mludwig@pcbe12114 protocolBuffers-cpp]$ export LD_LIBRARY_PATH=/usr/local/lib12114 protocolBuffers-cpp]$ export LD_LIBRARY_PATH=/usr/local/lib


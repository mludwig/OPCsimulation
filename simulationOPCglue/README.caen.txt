glue code specifics for caen
============================


https://icejenkins.cern.ch/view/OPC-UA/job/caen-opcua-server/ build939 or later...
17oct2017


the OpcUaCaenServer depends on specific caen libs:

	CAEN/hwAccess/lib/libcaenhvwrapper.so.5.83 (0x00007f51f2388000)

this is pointing to:	
lrwxrwxrwx. 1 mludwig mludwig        7 Oct 17 06:17 CAEN -> ../CAEN
/home/mludwig/jenkins_downloaded/caen.ua.17oct2017/caen-opcua-server.TEST/CAEN/hwAccess/lib
-rwxr-xr-x. 1 mludwig mludwig 1009280 Oct 17 06:17 libcaenhvwrapper.so.5.83
	
	
so only this lib and dependencies need to be replaced with glue code
	
	
	
some tricks to edit config file for simulator, if channels are NON consecutive:
===============================================================================
cat -n conf.xml // line numbers
cat ./conf.xml | grep "whatever type of line need modif" > x1
cat ./conf.xml | grep -v whatever type of line need modif" > x2
// do the modif on a reduced set of lines on x1
// then, merge modified lines back into original conf.xml
cat x1 x2 | sort -n > x3
// then, eliminate line numbering
cut -d " " -f 2-  x3 > result.xml
 
 
once the simulator is running, discover the structure using the opc and dump the discovered config 
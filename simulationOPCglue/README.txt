simulation OPC glue
===================

michael.ludwig@cern.ch

This is the OPC glue code, which replaces the original shared library for caen HW access in the OPC server

* replaces caen HW shared lib by simulation libs
* dynamical lib loading modified, at runtime 
* reproduces caen API with a "request-reply" architecture
* connects to the simulation engine over tcp/zeroMQ
* requests data from the simulation engine for each API call
* simulation engine replies with appropriate data for each API call
* does not consume noticable amount of extra CPU in the OPC server's host
* messages are encoded using google protocol buffers

./protocolBuffers-c
===================
the definition of the protocol messages for CAEN, glue and pilot
the protocol C-API is used, and the proto-c generator
a simple demo binary is included to show and test simple syntax issues


./config
========
an example of the caenGlueConfig.xml, which is needed to tell
the glue code where the simulation engine is running


prepare, i.e.
=============
cp libsimulation_caen.so bin/CAEN/hwAccess/lib
cd <OPC server runtime dir, usually "bin">/CAEN/hwAccess/lib
mv libcaenhvwrapper.so.5.56 libcaenhvwrapper.so.5.56.org
ln -s libsimulation_caen so

run
===
start simulation engine
check caenGlueConfig.xml, it has to point to the simulation engine's host
start OPC caen server, use clients 
	 




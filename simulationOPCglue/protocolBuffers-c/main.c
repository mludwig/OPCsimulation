// protocolTester main.c
// using zeroMQ, plain C version here, using google protocols in C and zeromq in C
// This is a standalone tester, which runs through all of the generated messages once
// as client, and tests syntax, transmission and decoding/encoding for the CAEN simulation engine.
// the requests are hardcoded, and we only try "one request of each type", since we
// need to test the syntax, but not the full crate discovery


#include "main.h"

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

#include <czmq.h>
#include <google/protobuf-c/protobuf-c.h>
#include "simulationGlueCaen.pb-c.h"
#include "simulationGlueIseg.pb-c.h"


int CAENHV_SubscribeEventMode_clientfd = -1;
struct sockaddr_in serv_addr;
uint32_t systemParamsPort = 99;
uint32_t boardParamsPort = 99;
uint32_t channelParamsPort = 99;


/// \todo subscribe event mode session: it is not entirely clear if we need separate sockets for channel, board and system parameter subscribe
/// \todo subscribe event mode session: can this run on the same host as the OPC server? probably yes...
/// \todo subscribe event mode session: do we need an extra socket/port for each subscription for simulating ? right now this is all grouped into one socket...
void stopEventModeSession( short Port ){
	// we close all subcsrciptions like that: we don't to anything. This should probably be one socket per subscription

}
void startEventModeSession( short Port ){
#if 0
	printf("%s %d startEventModeSession Port= %d CAENHV_SubscribeEventMode_clientfd= %d\n", __FILE__, __LINE__, Port, CAENHV_SubscribeEventMode_clientfd );

	// open a socket if not yet done, otherwise do nothing. Assume we are the server side and have to do the bind
	if ( CAENHV_SubscribeEventMode_clientfd < 0 ) {

		CAENHV_SubscribeEventMode_clientfd = socket( AF_INET, SOCK_STREAM, 0 );
		printf("%s %d startEventModeSession Port= %d listener socket OK\n", __FILE__, __LINE__, Port );
		size_t sz = sizeof( struct sockaddr_in );

		memset(&serv_addr, '0', sizeof(serv_addr));

		serv_addr.sin_family = AF_INET;
		// accept any incoming. This is not very nice, we should
		// accept only this OPC server's IP instead. Maybe create a pilot call later on to figure this out.
		// we could potentially have several OPC servers connect to the same HAL, to i.e. compare UA to open6.
		// leave it totally open for now.
		// demo: have the event mode running on the same host, as specified in the config.xml for the OPC server
		serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // loopback
		serv_addr.sin_port = htons( Port );

		if( connect(CAENHV_SubscribeEventMode_clientfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0 ) {
			printf("%s %d startEventModeSession Port= %d connect failed\n", __FILE__, __LINE__, Port );
			exit(-1);
		}
		printf("%s %d startEventModeSession Port= %d CAENHV_SubscribeEventMode_clientfd= %d connect OK\n", __FILE__, __LINE__, Port, CAENHV_SubscribeEventMode_clientfd );
	} else {
		printf("%s %d startEventModeSession Port= %d CAENHV_SubscribeEventMode_clientfd= %d already have a connection\n", __FILE__, __LINE__, Port, CAENHV_SubscribeEventMode_clientfd );
	}
#endif
}

// convert an array of uint8 to a string = char *
// each number takes 2 characters plus a blankspace
// nb is the number of elements in *in
// buf has to be allocated to 3 * nb + 2, therefore call the _sz before
// returns size of char, but not really needed
int ar_uint8_to_char_sz( size_t nb ){
	return( 3 * nb ); // 2 chars plus blank + 2 ending 0
}
void ar_uint8_to_char( uint8_t *in, char *buf, size_t nb ){
	uint32_t len2 = ar_uint8_to_char_sz( nb );
	memset( buf, 0, len2 );
	char cnum[ 3 ];
	int i = 0;
	for ( i = 0; i < nb; i++){
		sprintf( cnum, "%2x ", in[ i ]); // always 2 dgits
		strcat ( buf, cnum );
	}
}


// convert a string = char * to an array of uint8
// each number takes 2 characters plus a blankspace "FF "
// buf has to be allocated, call by ref
// the number of uint : call _sz
int char_to_ar_uint8_sz( size_t nb ){
	return( nb / 3 );
}
void char_to_ar_uint8( char *in, uint8_t *buf ){
	uint32_t len = strlen( in );
	uint32_t nb = char_to_ar_uint8_sz( len );
	memset( buf, 0, nb );
	char delimiter[] = " ";
	int i = 0;
	char *sub = strtok ( in, delimiter);
	while ( sub != NULL)
	{
		int d = 0;
		sscanf( sub, "%x", &d );
		buf[ i++ ] = d;
		sub = strtok ( NULL, delimiter );
	}
}
// int zmq_send (void *socket, void *buf, size_t len, int flags);

void sendCZMQ_caen( SCaen__CaenMessage gRequest, void *socket_req ) {
	uint32_t len = s_caen__caen_message__get_packed_size( &gRequest );
	bool _debug = true;
	if(_debug) printf( "%s %d generic message packed len= %d\n", __FILE__, __LINE__, len );

	uint8_t buf_uint8[ len ];
	memset( buf_uint8, 0, len );
	size_t sz = s_caen__caen_message__pack( &gRequest, buf_uint8 );
	if(_debug) printf( "%s %d generic message packed sz= %d\n", __FILE__, __LINE__, sz );

	// convert uint8 array into char* and send
	int char_sz = ar_uint8_to_char_sz( sz );
	char buf_char[ char_sz ];
	ar_uint8_to_char( buf_uint8, buf_char, sz );
	if(_debug) printf( "%s %d sending %s len= %d\n", __FILE__, __LINE__, buf_char, len );

	int rc = zmq_send ( socket_req, buf_char, sizeof( buf_char ), 0 );
	if(_debug) printf("%s %d rc= %d ...sent OK\n", rc, __FILE__ , __LINE__ );
}

#if 0
void sendCZMQ_caen( SCaen__CaenMessage gRequest, zsock_t *socket_req ) {
	uint32_t len = s_caen__caen_message__get_packed_size( &gRequest );
	bool _debug = true;
	if(_debug) printf( "%s %d generic message packed len= %d\n", __FILE__, __LINE__, len );

	uint8_t buf_uint8[ len ];
	memset( buf_uint8, 0, len );
	size_t sz = s_caen__caen_message__pack( &gRequest, buf_uint8 );
	if(_debug) printf( "%s %d generic message packed sz= %d\n", __FILE__, __LINE__, sz );


	// convert uint8 array into char* and send
	int char_sz = ar_uint8_to_char_sz( sz );
	char buf_char[ char_sz ];
	ar_uint8_to_char( buf_uint8, buf_char, sz );
	if(_debug) printf( "%s %d sending %s len= %d\n", __FILE__, __LINE__, buf_char, len );
	zstr_send ( socket_req, (const char *) buf_char );
	if(_debug) printf("%s %d ...sent OK\n", __FILE__ , __LINE__ );
}
#endif

void parseCharBuffArray(char* const* charBuffArray, int boardCount, char **result /* must be already allocated */) {
	const char* currentChar = *charBuffArray;
	unsigned short j=0;
	for( j=0; j<boardCount;  j++)	{
		char name[ 30 ] = "";
		int count = 0;
		if(*currentChar) {
			while(*currentChar)
			{
				sprintf( name, "%s%c", name, *currentChar );
				currentChar++;
			}
			sprintf( result[ j ], name );
		} else {
			sprintf( result[ j ], "NA" );
		}
		currentChar++;
	}
}

void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
{
	char ch[ fixedWidth ];
	int i = 0;
	int k = 0;
	int kk = 0;

	printf("%s %d size= %d %s\n", __FILE__, __LINE__, sizeof( buffer ), buffer );

	for ( i = 0; i < sizeof( buffer ); i++ ){
		if ( i < fixedWidth ){
			ch[ k ] = buffer[ i ];
			k++;
		} else {
			ch[ k ] = '\0';
			sprintf( result[ kk ], "%s", ch );
			k = 0;
			kk++;
			sprintf( ch, "");
		}
	}
	printf("%s %d found %d strings\n", __FILE__, __LINE__, kk );
}

void usage( char *p ){
	printf("usage: %s [ -help | -host_hal <h1> | -port_hal <p1> ]\n", p );
	printf("usage: %s [ -debug ]\n", p );
	printf("    defaults: h1=localhost p1=8880\n");
	exit(0);
}

void hexprint( const char *data, int size ) {
	// look into hex representation of message data
	int i = 0;
	printf("hexprint size= %d hex= ", size );
	for ( i = 0; i < size; i++ ){
		printf("%02x ", data[ i ]);
	}
	printf("\n");
}
void hexprint_uint8( const uint8_t *data, int size ) {
	// look into hex representation of message data
	int i = 0;
	printf("hexprint_uint8 size= %d hex= ", size );
	for ( i = 0; i < size; i++ ){
		printf("%02x ", data[ i ]);
	}
	printf("\n");
}

int main( int argc, char **argv ){
	printf( "%s version %s %s\n", argv[0], __DATE__, __TIME__ );
	// https://github.com/protobuf-c/protobuf-c/wiki/Examples
	printf("protocol buffers C version %s %d\n", PROTOBUF_C_VERSION, PROTOBUF_C_VERSION_NUMBER );
	// char host_hal[ NAME_SIZE ] = "*";
	char host_hal[ NAME_SIZE ] = "localhost";
	int port_hal = SIMULATION_GLUE_PORT;
	char configfile[ NAME_SIZE ] = "./simulation_config.xml";


#if 0
	{
		// tests
		int nb = 10;
		int i = 0;
		uint8_t arint[ nb ];
		printf("arint=#");
		for ( i = 0; i < nb; i++ ){
			arint[ i ] = i + 246;
			printf("%x#", arint[ i ]);
		}
		printf("\n");
		int dimchar = ar_uint8_to_char_sz( nb );
		printf(" for %d int allocate %d char\n", nb, dimchar );
		char cc[ dimchar ];
		ar_uint8_to_char( arint, cc, nb );
		printf("cc= %s (len=%d)\n", cc, strlen( cc ));

		// add two 0: zmq delimiter
		strcat ( cc, "\0\0" );
		printf("zmq cc= %s (len=%d)\n", cc, strlen( cc ));

		// and return to uint8
		int nb2 = char_to_ar_uint8_sz( strlen( cc ) );
		uint8_t arint2[ nb2 ];
		char_to_ar_uint8( cc, arint2 );
		printf("arint2=#");
		for ( i = 0; i < nb2; i++ ){
			printf("%x#", arint[ i ]);
		}
		printf("\n");
	}
#endif

	bool _debug = true;

	int i = 0;
	for ( i = 1; i < argc; i++ ){
		if ( 0 == strcmp( argv[i], "-debug")){
			_debug = true;
		}
		if ( 0 == strcmp( argv[i], "-help")){
			usage( argv[0] );
		}
		if ( 0 == strcmp( argv[i], "-h")){
			usage( argv[0] );
		}
		if ( 0 == strcmp( argv[i], "-host_hal") && argc > i ){
			sscanf( argv[i+1], "%s", host_hal );

		}
		if ( 0 == strcmp(argv[i], "-port_hal" ) && argc > i){
			sscanf( argv[i+1], "%d", &port_hal );
		}
	}

	char tcpipport[ NAME_SIZE ];
	sprintf( tcpipport, "tcp://%s:%d", host_hal, port_hal );

	printf("%s connecting to simulationHAL server= %s\n", argv[ 0 ], tcpipport );

	char gmsg[ PROTOCOL_MSG_SIZE ];

#ifdef USE_CZMQ_SHARED
	zsock_t *socket_req = zsock_new_req( tcpipport ); // client, send-recv pattern
	void *zmq_socket (void *context, int type);
	assert( socket_req );
#else
	// zmq static
	int type = ZMQ_REQ;
	void *context = zmq_ctx_new();
	void *socket_req =  zmq_socket( context, type );
	int rc = zmq_connect (socket_req, tcpipport );
	assert (rc == 0);
#endif

	printf("%s %d glue is starting\n", __FILE__ , __LINE__ );

#if 0
	// handshake with HAL
	{
		zstr_send ( socket_req, "glue is starting" );
		char *reply = zstr_recv ( socket_req );
		printf("%s %d glue received: %s length= %d\n", __FILE__ , __LINE__, reply, strlen( reply ) );
	}
#endif


	// produce all possible messages one after the other, then exit
	// the request messages are pair, the replies are impair.
	//  S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_request = 0,
	// ...
	//  S_CAEN__MESSAGE_TYPE__CAENHV_Free_request = 48,
	const int iRequestMin = 0;
	const int iRequestMax = 48;
	int iMessageType = iRequestMin;

	// define a sequence of requests messages, in the logic of one complete API check
	// the HAL is configured with 1 crate and 4 different boards
	// like in the test crate
	int nbSequence = 1000;
	int sequence[ nbSequence ];
	int iseq = 0;

	// 1. a turn over all API messages, not  full discovery. Not much intelligence, but a complete syntax check
	int crateHandle = -1;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropList_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropInfo_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_SetSysProp_request;


	//	foreach board
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_TestBdPresence_request;

	// 	    foreach board-property
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamProp_request; // mode or type
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_request;


	// 	           foreach channel of the board
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_SetChName_request;


	//                   foreach channel-property (explicitly per channel)
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_request; // mode
	// sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_request; // type
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_request;


	// crate stuff
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_request;

	// CAENHVLIB_API CAENHVRESULT CAENHV_Free(void *arg);
	// CAENHVLIB_API char *CAENHV_GetError(int handle);
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_GetError_request;


	// subsription stuff
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_request;
	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_request;

	// event data stuff
#if 0
	CAENHVLIB_API CAENHVRESULT CAENHV_GetEventData(int sck, CAENHV_SYSTEMSTATUS_t *SysStatus, CAENHVEVENT_TYPE_t **EventData, unsigned int *DataNumber);
	CAENHVLIB_API CAENHVRESULT CAENHV_FreeEventData(CAENHVEVENT_TYPE_t **ListOfItemsData);
#endif

	sequence[ iseq++ ] = S_CAEN__MESSAGE_TYPE__CAENHV_DeinitSystem_request;

	nbSequence = iseq;
	for ( iseq = 0; iseq < nbSequence; iseq++ ){

		iMessageType = sequence[ iseq ];
		printf("checking request type= %d\n", iMessageType );

		{
			// fill in generic message, and pack
			SCaen__CaenMessage gRequest;
			s_caen__caen_message__init( &gRequest );
			gRequest.type = iMessageType;

			switch( gRequest.type ){
			case S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_request:{
				SCaen__CAENHVInitSystemRequestM sMessage;
				s_caen__caenhv__init_system_request_m__init( &sMessage );
				gRequest.caenhv_initsystemrequest = &sMessage;
				gRequest.caenhv_initsystemrequest->system = 2;   // SY4527 = 2
				gRequest.caenhv_initsystemrequest->linktype = 0; // LINKTYPE_TCPIP
				gRequest.caenhv_initsystemrequest->username = "username";
				gRequest.caenhv_initsystemrequest->passwd = "passwd";
				gRequest.caenhv_initsystemrequest->arg = "137.138.9.19"; // enicelabpslin, simulated !

				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_initsystemrequest->linktype );
					printf("%s %d %s\n", __FILE__ , __LINE__ , gRequest.caenhv_initsystemrequest->arg );
					printf("%s %d %s\n", __FILE__ , __LINE__ , gRequest.caenhv_initsystemrequest->username );
					printf("%s %d %s\n", __FILE__ , __LINE__ , gRequest.caenhv_initsystemrequest->passwd );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_DeinitSystem_request:{
				SCaen__CAENHVDeinitSystemRequestM sMessage;
				s_caen__caenhv__deinit_system_request_m__init( &sMessage );
				gRequest.caenhv_deinitsystemrequest = &sMessage;
				gRequest.caenhv_deinitsystemrequest->handle = crateHandle;
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_request:{
				SCaen__CAENHVGetCrateMapRequestM sMessage;
				s_caen__caenhv__get_crate_map_request_m__init( &sMessage );
				gRequest.caenhv_getcratemaprequest = &sMessage;
				gRequest.caenhv_getcratemaprequest->handle = 0;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getcratemaprequest->handle );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropList_request:{
				SCaen__CAENHVGetSysPropListRequestM sMessage;
				s_caen__caenhv__get_sys_prop_list_request_m__init( &sMessage );
				gRequest.caenhv_getsysproplistrequest = &sMessage;
				gRequest.caenhv_getsysproplistrequest->handle = crateHandle;
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropInfo_request:{
				SCaen__CAENHVGetSysPropInfoRequestM sMessage;
				s_caen__caenhv__get_sys_prop_info_request_m__init( &sMessage );
				gRequest.caenhv_getsyspropinforequest = &sMessage;
				gRequest.caenhv_getsyspropinforequest->handle = crateHandle;
				gRequest.caenhv_getsyspropinforequest->propname = "ModelName";
				/*
				0. SysPropName= CMDExecMode
				1. SysPropName= CPULoad
				2. SysPropName= ClkFreq
				3. SysPropName= CmdQueueStatus
				4. SysPropName= DummyReg
				5. SysPropName= FrontPanIn
				6. SysPropName= FrontPanOut
				7. SysPropName= GenSignCfg
				8. SysPropName= HVClkConf
				9. SysPropName= HVFanSpeed
				10. SysPropName= HVFanStat
				11. SysPropName= HvPwSM
				12. SysPropName= IPAddr
				13. SysPropName= IPGw
				14. SysPropName= IPNetMsk
				15. SysPropName= MemoryStatus
				16. SysPropName= ModelName
				17. SysPropName= OutputLevel
				18. SysPropName= PWCurrent
				19. SysPropName= PWFanStat
				20. SysPropName= PWVoltage
				21. SysPropName= ResFlag
				22. SysPropName= ResFlagCfg
				23. SysPropName= Sessions
				24. SysPropName= SwRelease
				25. SysPropName= SymbolicName
				 */
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getsyspropinforequest->propname );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_request:{
				SCaen__CAENHVGetSysPropRequestM sMessage;
				s_caen__caenhv__get_sys_prop_request_m__init( &sMessage );
				gRequest.caenhv_getsysproprequest = &sMessage;
				gRequest.caenhv_getsysproprequest->handle = crateHandle;
				gRequest.caenhv_getsysproprequest->propname = "CPULoad";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %s\n", __FILE__ , __LINE__ , gRequest.caenhv_getsysproprequest->propname );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_SetSysProp_request:{
				SCaen__CAENHVSetSysPropRequestM sMessage;
				s_caen__caenhv__set_sys_prop_request_m__init( &sMessage );
				gRequest.caenhv_setsysproprequest = &sMessage;
				gRequest.caenhv_setsysproprequest->handle = crateHandle;
				gRequest.caenhv_setsysproprequest->propname = "SymbolicName";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %s\n", __FILE__ , __LINE__ , gRequest.caenhv_setsysproprequest->propname );
				}
				// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
				// gRequest.caenhv_getsysproprequest->propname = "PWCurrent";
				uint32_t format = 0;  // tstr
				gRequest.caenhv_setsysproprequest->valuestring = "symbNameTest0";
				gRequest.caenhv_setsysproprequest->valuefloat = 0;
				gRequest.caenhv_setsysproprequest->valueint = 0;
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_request:{
				SCaen__CAENHVGetBdParamInfoRequestM sMessage;
				s_caen__caenhv__get_bd_param_info_request_m__init( &sMessage );
				gRequest.caenhv_getbdparaminforequest = &sMessage;
				gRequest.caenhv_getbdparaminforequest->handle = crateHandle;
				gRequest.caenhv_getbdparaminforequest->slot = 0;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getbdparaminforequest->slot );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_TestBdPresence_request:{
				SCaen__CAENHVTestBdPresenceRequestM sMessage;
				s_caen__caenhv__test_bd_presence_request_m__init( &sMessage );
				gRequest.caenhv_testbdpresencerequest = &sMessage;
				gRequest.caenhv_testbdpresencerequest->handle = crateHandle;
				gRequest.caenhv_testbdpresencerequest->slot = 0;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_testbdpresencerequest->slot );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}


			case S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamProp_request:{
				SCaen__CAENHVGetBdParamPropRequestM sMessage;
				s_caen__caenhv__get_bd_param_prop_request_m__init( &sMessage );
				gRequest.caenhv_getbdparamproprequest = &sMessage;
				gRequest.caenhv_getbdparamproprequest->handle = crateHandle;
				gRequest.caenhv_getbdparamproprequest->slot = 0;
				gRequest.caenhv_getbdparamproprequest->parname = "BdStatus";
				gRequest.caenhv_getbdparamproprequest->propname = "Mode";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getbdparamproprequest->slot );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			// get a list of board parameter values, for one specified board parameter, for a specified list of channels
			case S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_request:{
				SCaen__CAENHVGetBdParamRequestM sMessage;
				s_caen__caenhv__get_bd_param_request_m__init( &sMessage );
				gRequest.caenhv_getbdparamrequest = &sMessage;
				gRequest.caenhv_getbdparamrequest->handle = crateHandle;
				const int dim = 2;
				gRequest.caenhv_getbdparamrequest->slotnum = dim; // slot 0 and 3, makes 2 slots to look at
				gRequest.caenhv_getbdparamrequest->parname = "Temp";
				// so we try to read the temperature of the boards in slot 0 and 3:
				int slotList[ dim ];
				slotList[ 0 ] = 0;
				slotList[ 1 ] = 3;
				gRequest.caenhv_getbdparamrequest->slotlist = slotList;
				gRequest.caenhv_getbdparamrequest->n_slotlist = dim;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getbdparamrequest->slotnum );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			/// \todo how to test settable board parameters in the present boards, if they don't exist (only RO exist)
			// but maybe other boards exists which have settable params
			case S_CAEN__MESSAGE_TYPE__CAENHV_SetBdParam_request:{
				SCaen__CAENHVSetBdParamRequestM sMessage;
				s_caen__caenhv__set_bd_param_request_m__init( &sMessage );
				gRequest.caenhv_setbdparamrequest = &sMessage;
				gRequest.caenhv_setbdparamrequest->handle = crateHandle;
				const int dim = 2;
				gRequest.caenhv_setbdparamrequest->slotnum = dim; // slot 0 and 3, makes 2 slots to look at
				gRequest.caenhv_setbdparamrequest->parname = "noSetBdParam";
				// so we try to read the temperature of the boards in slot 0 and 3:
				int slotList[ dim ];
				slotList[ 0 ] = 0;
				slotList[ 1 ] = 3;
				gRequest.caenhv_setbdparamrequest->slotlist = slotList;
				gRequest.caenhv_setbdparamrequest->n_slotlist = dim;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_setbdparamrequest->slotnum );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_request:{
				SCaen__CAENHVGetChParamInfoRequestM sMessage;
				s_caen__caenhv__get_ch_param_info_request_m__init( &sMessage );
				gRequest.caenhv_getchparaminforequest = &sMessage;
				gRequest.caenhv_getchparaminforequest->handle = crateHandle;
				// just check slot 0 channel 0
				gRequest.caenhv_getchparaminforequest->slot = 0;
				gRequest.caenhv_getchparaminforequest->channel = 0;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d slot=%d channel= %d\n", __FILE__ , __LINE__ ,
							gRequest.caenhv_getchparaminforequest->slot, gRequest.caenhv_getchparaminforequest->channel );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_request:{
				SCaen__CAENHVGetChNameRequestM sMessage;
				s_caen__caenhv__get_ch_name_request_m__init( &sMessage );
				gRequest.caenhv_getchnamerequest = &sMessage;
				gRequest.caenhv_getchnamerequest->handle = crateHandle;
				// just check slot 0 channel 0 and 3
				const int dim = 2;
				gRequest.caenhv_getchnamerequest->slot = 0;
				gRequest.caenhv_getchnamerequest->chnum = dim;
				int chList[ dim ];
				chList[ 0 ] = 0;
				chList[ 1 ] = 3;
				gRequest.caenhv_getchnamerequest->chlist = chList;
				gRequest.caenhv_getchnamerequest->n_chlist = dim;

				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d slot=%d\n", __FILE__ , __LINE__ , gRequest.caenhv_getchnamerequest->slot );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_SetChName_request:{
				SCaen__CAENHVSetChNameRequestM sMessage;
				s_caen__caenhv__set_ch_name_request_m__init( &sMessage );
				gRequest.caenhv_setchnamerequest = &sMessage;
				gRequest.caenhv_setchnamerequest->handle = crateHandle;
				// just set slot 0 channel 0 name
				//CAENHVLIB_API CAENHVRESULT  CAENHV_SetChName(int handle, ushort slot,
				 //ushort ChNum, const ushort *ChList, const char *ChName);


				const int dim = 1;
				gRequest.caenhv_setchnamerequest->slot = 0;
				gRequest.caenhv_setchnamerequest->chnum = dim;
				int chList[ dim ];
				chList[ 0 ] = 0;
				gRequest.caenhv_setchnamerequest->chlist = chList;
				gRequest.caenhv_setchnamerequest->n_chlist = dim;
				gRequest.caenhv_setchnamerequest->chname = "setch0test";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d slot=%d\n", __FILE__ , __LINE__ , gRequest.caenhv_setchnamerequest->slot );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_request:{
				SCaen__CAENHVGetChParamPropRequestM sMessage;
				s_caen__caenhv__get_ch_param_prop_request_m__init( &sMessage );
				gRequest.caenhv_getchparamproprequest = &sMessage;
				gRequest.caenhv_getchparamproprequest->handle = crateHandle;
				gRequest.caenhv_getchparamproprequest->slot = 0;
				gRequest.caenhv_getchparamproprequest->channel = 0;
				gRequest.caenhv_getchparamproprequest->parname = "V0Set";
				gRequest.caenhv_getchparamproprequest->propname = "Mode";
				// gRequest.caenhv_getchparamproprequest->propname = "Type";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d slot= %d channel= %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getchparamproprequest->slot,
							gRequest.caenhv_getchparamproprequest->channel  );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}
			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_request:{
				SCaen__CAENHVGetChParamRequestM sMessage;
				s_caen__caenhv__get_ch_param_request_m__init( &sMessage );
				gRequest.caenhv_getchparamrequest = &sMessage;
				gRequest.caenhv_getchparamrequest->handle = crateHandle;
				gRequest.caenhv_getchparamrequest->slot = 0;
				gRequest.caenhv_getchparamrequest->channel = 0;

				uint32_t dim = 2; // channel 0 and 3
				gRequest.caenhv_getchparamrequest->n_chlist = dim;
				int chList[ dim ];
				chList[ 0 ] = 0;
				chList[ 1 ] = 3;
				gRequest.caenhv_getchparamrequest->chlist = chList;
				gRequest.caenhv_getchparamrequest->parname = "V0Set";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d slot= %d channel= %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getchparamrequest->slot,
							gRequest.caenhv_getchparamrequest->channel  );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_request:{
				SCaen__CAENHVSetChParamRequestM sMessage;
				s_caen__caenhv__set_ch_param_request_m__init( &sMessage );
				gRequest.caenhv_setchparamrequest = &sMessage;
				gRequest.caenhv_setchparamrequest->handle = crateHandle;
				gRequest.caenhv_setchparamrequest->slot = 0;
				gRequest.caenhv_setchparamrequest->channel = 0;

				uint32_t dim = 2; // channel 0 and 3
				int chList[ dim ];
				chList[ 0 ] = 0;
				chList[ 1 ] = 3;
				printf("%s %d sending message\n", __FILE__ , __LINE__ );
				gRequest.caenhv_setchparamrequest->chlist = chList;

				printf("%s %d sending message\n", __FILE__ , __LINE__ );
				gRequest.caenhv_setchparamrequest->n_chlist = dim;
				//	gRequest.caenhv_setchparamrequest->setformat = 1; // int=0, float=1, string=2
				// using the parameter name, the HAL finds out itself which is the correct
				// format to use, and selects the field to read accordingly

				gRequest.caenhv_setchparamrequest->parname = "I0Set";
				gRequest.caenhv_setchparamrequest->setvaluefloat = 0.001; // 1mA

				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
					printf("%s %d slot= %d nbChannels= %d I0Set= %g\n", __FILE__ , __LINE__ , gRequest.caenhv_setchparamrequest->slot,
							gRequest.caenhv_setchparamrequest->channel, gRequest.caenhv_setchparamrequest->setvaluefloat );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_request:{
				SCaen__CAENHVGetExecCommListRequestM sMessage;
				s_caen__caenhv__get_exec_comm_list_request_m__init( &sMessage );
				gRequest.caenhv_getexeccommlistrequest = &sMessage;
				gRequest.caenhv_getexeccommlistrequest->handle = crateHandle;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_request:{
				SCaen__CAENHVExecCommRequestM sMessage;
				s_caen__caenhv__exec_comm_request_m__init( &sMessage );
				gRequest.caenhv_execcommrequest = &sMessage;
				gRequest.caenhv_execcommrequest->handle = crateHandle;
				gRequest.caenhv_execcommrequest->commname = "dummyExecCommand";
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_request:{
				// CAENHVLIB_API CAENHVRESULT CAENHV_SubscribeSystemParams(int handle, short Port, const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes);
				SCaen__CAENHVSubscribeSystemParamsRequestM sMessage;
				s_caen__caenhv__subscribe_system_params_request_m__init( &sMessage );
				gRequest.caenhv_subscribesystemparamsrequest = &sMessage;
				gRequest.caenhv_subscribesystemparamsrequest->handle = crateHandle;

				// test 2 system parameters
				uint32_t dim = 2;
				gRequest.caenhv_subscribesystemparamsrequest->paramnum = dim;
				char plist[ 512 ];
				sprintf( plist, "CPULoad:ClkFreq");

				// void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
				// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName

				gRequest.caenhv_subscribesystemparamsrequest->paramnamelist = (char *) plist;
				gRequest.caenhv_subscribesystemparamsrequest->port = systemParamsPort;

				startEventModeSession( systemParamsPort );
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_request:{
				//CAENHVLIB_API CAENHVRESULT CAENHV_SubscribeBoardParams(int handle, short Port, const unsigned short slotIndex,
				//	const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes){

				SCaen__CAENHVSubscribeBoardParamsRequestM sMessage;
				s_caen__caenhv__subscribe_board_params_request_m__init( &sMessage );
				gRequest.caenhv_subscribeboardparamsrequest = &sMessage;
				gRequest.caenhv_subscribeboardparamsrequest->handle = crateHandle;

				// two board params of slot 0
				gRequest.caenhv_subscribeboardparamsrequest->slotindex = 0;

				// test 2 board parameters
				uint32_t dim = 2;
				gRequest.caenhv_subscribeboardparamsrequest->paramnum = dim;
				char plist[ 512 ];
				sprintf( plist, "HVMax:Temp");

				// void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
				// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName

				gRequest.caenhv_subscribeboardparamsrequest->paramnamelist = (char *) plist;
				gRequest.caenhv_subscribeboardparamsrequest->port = boardParamsPort;

				startEventModeSession( boardParamsPort );
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_request:{
				// CAENHVLIB_API CAENHVRESULT CAENHV_SubscribeChannelParams(int handle, short Port, const unsigned short slotIndex,
				// const unsigned short chanIndex, const char *paramNameList,
				// unsigned int paramNum ,char *listOfResultCodes){
				SCaen__CAENHVSubscribeChannelParamsRequestM sMessage;
				s_caen__caenhv__subscribe_channel_params_request_m__init( &sMessage );
				gRequest.caenhv_subscribechannelparamsrequest = &sMessage;
				gRequest.caenhv_subscribechannelparamsrequest->handle = crateHandle;

				// two channel params of slot 0
				gRequest.caenhv_subscribechannelparamsrequest->slotindex = 0;
				gRequest.caenhv_subscribechannelparamsrequest->channelindex = 3;

				// test 2 channel parameters
				uint32_t dim = 2;
				gRequest.caenhv_subscribechannelparamsrequest->paramnum = dim;
				char plist[ 512 ];
				sprintf( plist, "VMon:I0Set");

				// void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
				// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName

				gRequest.caenhv_subscribechannelparamsrequest->paramnamelist = (char *) plist;
				gRequest.caenhv_subscribechannelparamsrequest->port = channelParamsPort;

				startEventModeSession( channelParamsPort );
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_request:{
				// CAENHVLIB_API CAENHVRESULT CAENHV_UnSubscribeSystemParams(int handle, short Port, const char *paramNameList,
				//	unsigned int paramNum ,char *listOfResultCodes){
				SCaen__CAENHVUnSubscribeSystemParamsRequestM sMessage;
				s_caen__caenhv__un_subscribe_system_params_request_m__init( &sMessage );
				gRequest.caenhv_unsubscribesystemparamsrequest = &sMessage;
				gRequest.caenhv_unsubscribesystemparamsrequest->handle = crateHandle;

				// test 2 system parameters
				uint32_t port = 99;
				uint32_t dim = 2;
				gRequest.caenhv_unsubscribesystemparamsrequest->paramnum = dim;
				char plist[ dim ][ CAEN_NAME_MAX12 ];
				sprintf( plist[ 0 ], "CPULoad");
				sprintf( plist[ 1 ], "ClkFreq");

				// void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
				// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName

				gRequest.caenhv_unsubscribesystemparamsrequest->paramnamelist = (char *) plist;
				// gRequest.caenhv_unsubscribesystemparamsrequest->n_paramnamelist = dim;
				gRequest.caenhv_unsubscribesystemparamsrequest->port = port;

				stopEventModeSession( port );
				// we probably don't need to send a request, but lets do it until the needed semantics is entirely clear anyway
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_request:{
				SCaen__CAENHVUnSubscribeBoardParamsRequestM sMessage;
				s_caen__caenhv__un_subscribe_board_params_request_m__init( &sMessage );
				gRequest.caenhv_unsubscribeboardparamsrequest = &sMessage;
				gRequest.caenhv_unsubscribeboardparamsrequest->handle = crateHandle;

				// test the same 2 board parameters
				uint32_t port = 99;
				uint32_t dim = 2;
				gRequest.caenhv_unsubscribeboardparamsrequest->paramnum = dim;
				char plist[ dim ][ CAEN_NAME_MAX12 ];
				sprintf( plist[ 0 ], "HVMax");
				sprintf( plist[ 1 ], "Temp");

				// void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
				// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName

				gRequest.caenhv_unsubscribesystemparamsrequest->paramnamelist = (char *) plist;
				// gRequest.caenhv_unsubscribesystemparamsrequest->n_paramnamelist = dim;
				gRequest.caenhv_unsubscribesystemparamsrequest->port = port;

				stopEventModeSession( port );
				// we probably don't need to send a request, but lets do it until the needed semantics is entirely clear anyway
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_request:{
				SCaen__CAENHVUnSubscribeChannelParamsRequestM sMessage;
				s_caen__caenhv__un_subscribe_channel_params_request_m__init( &sMessage );
				gRequest.caenhv_unsubscribechannelparamsrequest = &sMessage;
				gRequest.caenhv_unsubscribechannelparamsrequest->handle = crateHandle;

				// test the same 2 board parameters
				uint32_t port = 99;
				uint32_t dim = 2;
				gRequest.caenhv_unsubscribechannelparamsrequest->paramnum = dim;
				char plist[ dim ][ CAEN_NAME_MAX12 ];
				sprintf( plist[ 0 ], "VMon");
				sprintf( plist[ 1 ], "I0Set");

				// void parseFixedWidthArrayOfCharBuffers(const char* buffer, const size_t fixedWidth, char **result /* must be already allocated */)
				// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName

				gRequest.caenhv_unsubscribeboardparamsrequest->paramnamelist = (char *) plist;
				// gRequest.caenhv_unsubscribeboardparamsrequest->n_paramnamelist = dim;
				gRequest.caenhv_unsubscribeboardparamsrequest->port = port;

				stopEventModeSession( port );
				// we probably don't need to send a request, but lets do it until the needed semantics is entirely clear anyway
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			// performance critical: gets called in a loop without delay by the OPC server, and has
			// a varying enevt data composition, with a number of items up to the total number
			// of channels existing in that crate (=socket). That is some ~20000 items at max,
			// times the parameters per channel which have changed:
			// "everything which has changed:" goes into an event.
			// the Event rate shall be 2Hz at max, so the reply is slowed down
			// artificially. In a mono-thread design this is the peformance bottleneck:
			// the reply delay has to be adjusted **dynamically** so that 2Hz is more or less kept.
			// I have seen rep times between 100ms and 2.6s in the lab, with just a few boards.
			// So in reality the CAEN controllers don't go that fast either: this is an argument
			// for a mono-thread design of the simulation engine.
			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetEventData_request:{
				SCaen__CAENHVGetEventDataRequestM sMessage;
				s_caen__caenhv__get_event_data_request_m__init( &sMessage );
				gRequest.caenhv_geteventdatarequest = &sMessage;
				gRequest.caenhv_geteventdatarequest->socket = 0; // is this the crate handle ? seems so...
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_FreeEventData_request:{
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_Free_request:{
				if(_debug){
					printf("%s %d doing nothing (not even sending a message)\n", __FILE__ , __LINE__ );
				}
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetError_request:{
				SCaen__CAENHVGetErrorRequestM sMessage;
				s_caen__caenhv__get_error_request_m__init( &sMessage );
				gRequest.caenhv_geterrorrequest = &sMessage;
				gRequest.caenhv_geterrorrequest->handle = crateHandle;
				if(_debug){
					printf("%s %d sending message\n", __FILE__ , __LINE__ );
				}
				sendCZMQ_caen( gRequest, socket_req );
				break;
			}

			default:{
				printf("unknown request message type, exiting\n");
				exit(-1);
				break;
			}
			} // switch
		}

		// collect the reply and deserialize message
		{
			if(_debug) printf("%s %d ... waiting for reply ...\n", __FILE__ , __LINE__ );
#ifdef USE_CZMQ_SHARED
			char *reply;
			reply = zstr_recv ( socket_req );
			int len = strlen( reply );
#else
			// zmq static
			// int zmq_recv (void *socket, void *buf, size_t len, int flags);
			const size_t maxlen = 5000000; // MAX_ZMQ_MSG_SIZE;
			char reply[ maxlen ];
			int len = zmq_recv( socket_req, reply, maxlen, 0 );
			assert ( len != -1 );
#endif



			if(_debug) printf("%s %d received reply %s len= %d\n", __FILE__ , __LINE__, reply, len  );


			// convert it to array uint8
			int uint8_sz = char_to_ar_uint8_sz( len );
			uint8_t buf_uint8[ uint8_sz ];
			char_to_ar_uint8( reply, buf_uint8 );
#ifdef USE_CZMQ_SHARED
			zstr_free( &reply );
#endif

			if(_debug) printf("%s %d got a reply, unpacking generic message uint8_sz= %d\n", __FILE__ , __LINE__, uint8_sz );
			SCaen__CaenMessage *gReply = s_caen__caen_message__unpack( NULL, uint8_sz, buf_uint8 );

			if(_debug) printf("%s %d reply generic message type= %d\n", __FILE__ , __LINE__, gReply->type );
			if ( gReply == NULL)  {
				printf("%s %d error unpacking generic message\n", __FILE__ , __LINE__ );
				exit(1);
			}

			// this is handled by the dispatcher, but here this is just one test case
			// extract the specific reply
			switch ( gReply->type ){
			case S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_reply: {
				SCaen__CAENHVInitSystemReplyM *sReply = gReply->caenhv_initsystemreply;
				printf("%s %d %s\n", __FILE__ , __LINE__ , sReply->handle );

				crateHandle = sReply->handle;
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_DeinitSystem_reply: {
				SCaen__CAENHVDeinitSystemReplyM *sReply = gReply->caenhv_deinitsystemreply;
				printf("%s %d %s\n", __FILE__ , __LINE__ , sReply->ret );
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_reply: {
				SCaen__CAENHVGetCrateMapReplyM *sReply = gReply->caenhv_getcratemapreply;
				int dim = sReply->nrofslot;

				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_reply, dim=%d :\n", __FILE__ , __LINE__, dim );

				// same var names as the CAEN API...
				int NrofChList[ dim ];
				int SerNumList[ dim ];
				int FmwRelMinList[ dim ];
				int FmwRelMaxList[ dim ];
				char ModelList[ dim ][ CAEN_NAME_MAX12 ];
				char DescriptionList[ dim ][ CAEN_DESC_MAX ]; // dimension

				// debug
				{
					int i = 0;
					for ( i = 0; i < dim; i++ ){
						NrofChList[ i ] = sReply->nrofchlist[ i ];
						SerNumList[ i ] = sReply->sernumlist[ i ];
						FmwRelMinList[ i ] = sReply->fmwrelminlist[ i ];
						FmwRelMaxList[ i ] = sReply->fmwrelmaxlist[ i ];
						sprintf( ModelList[ i ], sReply->modellist[ i ] );
						sprintf( DescriptionList[ i ], sReply->descriptionlist[ i ] );

						printf("---board item= %d---\n", i );
						// we don't know in which slot each board is !
						printf("NrofChList[ %d ]= %d (board nb channels)\n", i, NrofChList[ i ] );
						printf("SerNumList[ %d ]= %d (board serial nb)\n", i, SerNumList[ i ] );
						printf("FmwRelMinList[ %d ]= %d (fw min)\n", i, FmwRelMinList[ i ] );
						printf("FmwRelMaxList[ %d ]= %d (fw max)\n", i, FmwRelMaxList[ i ] );
						printf("ModelList[ %d ]= %s (board model)\n", i, ModelList[ i ] );
						printf("DescriptionList[ %d ]= %s (board description)\n", i, DescriptionList[ i ] );
					}
				}
				break;
			}

			case S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropList_reply: {
				SCaen__CAENHVGetSysPropListReplyM *sReply = gReply->caenhv_getsysproplistreply;
				int dim = sReply->numprop;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropList_reply dim=%d :\n", __FILE__ , __LINE__, dim );
				int i = 0;
				for ( i = 0; i < dim; i++ ){
					printf("%d. SysPropName= %s\n", i, sReply->propnamelist[ i ]);
				}
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropInfo_reply: {
				SCaen__CAENHVGetSysPropInfoReplyM *sReply = gReply->caenhv_getsyspropinforeply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropInfo_reply mode=%d type=%d\n", __FILE__ , __LINE__,
						sReply->propmode, sReply->proptype );
				break;

			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply: {
				SCaen__CAENHVGetSysPropReplyM *sReply = gReply->caenhv_getsyspropreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply format=%d \n", __FILE__ , __LINE__,
						sReply->format );
				// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
				// gRequest.caenhv_getsysproprequest->propname = "PWCurrent";
				switch( sReply->format ){
				case 0: {
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply value=%s \n", __FILE__ , __LINE__,
							(char *) sReply->valuestring );
					break;
				}
				case 1: {
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply value=%d \n", __FILE__ , __LINE__,
							sReply->valueint );
					break;
				}
				case 2: {
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply value=%g \n", __FILE__ , __LINE__,
							sReply->valuefloat );
					break;
				}
				default: {
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply unknown format=%d \n", __FILE__ , __LINE__,
							sReply->format );
					break;
				}
				}
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_SetSysProp_reply: {
				printf("%s %d\n", __FILE__ , __LINE__ );
				SCaen__CAENHVSetSysPropReplyM *sReply = gReply->caenhv_setsyspropreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SetSysProp_reply ret=%d \n", __FILE__ , __LINE__, sReply->ret );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_reply: {
				SCaen__CAENHVGetBdParamInfoReplyM *sReply = gReply->caenhv_getbdparaminforeply;
				int dim = sReply->n_parnamelist;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_reply dim=%d :\n", __FILE__ , __LINE__, dim );
				int i = 0;
				for ( i = 0; i < dim; i++ ){
					printf("%d. BdParamList= %s\n", i, sReply->parnamelist[ i ]);
				}
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_TestBdPresence_reply: {
				SCaen__CAENHVTestBdPresenceReplyM *sReply = gReply->caenhv_testbdpresencereply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_TestBdPresence_reply\n", __FILE__ , __LINE__ );
				printf("%s %d nrofch= %d model= %s description= %s\n", __FILE__ , __LINE__, sReply->nrofch, sReply->model, sReply->description );
				printf("%s %d sernum= %d fmwrelmin= %d fmwrelmax= %d\n", __FILE__ , __LINE__, sReply->sernum, sReply->fmwrelmin, sReply->fmwrelmax );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamProp_reply: {
				SCaen__CAENHVGetBdParamPropReplyM *sReply = gReply->caenhv_getbdparampropreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamProp_reply type=%d mode= %d\n", __FILE__ , __LINE__,
						sReply->type, sReply->mode );
				break;
			}

			// ~list of channels
			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_reply: {
				SCaen__CAENHVGetBdParamReplyM *sReply = gReply->caenhv_getbdparamreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_reply format=%d dim=%d\n", __FILE__ , __LINE__,
						sReply->format, sReply->n_valuearrayfloat );

				// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
				int i = 0;
				int dim = sReply->n_valuearrayfloat; // all are the same dim
				for ( i = 0; i < dim; i++ ){
					switch( sReply->format ){
					case 0: {
						printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_reply value=%s \n", __FILE__ , __LINE__,
								(char *) sReply->valuearraystring[ i ] );
						break;
					}
					case 1: {
						printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_reply value=%d \n", __FILE__ , __LINE__,
								sReply->valuearrayint[ i ] );
						break;
					}
					case 2: {
						printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_reply value=%g \n", __FILE__ , __LINE__,
								sReply->valuearrayfloat[ i ] );
						break;
					}
					default: {
						printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_reply unknown format=%d \n", __FILE__ , __LINE__,
								sReply->format );
						break;
					}
					}
				}
				break;
			}
			case S_CAEN__MESSAGE_TYPE__CAENHV_SetBdParam_reply:{
				SCaen__CAENHVSetBdParamReplyM *sReply = gReply->caenhv_setbdparamreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SetBdParam_reply format=%d dim=%d\n", __FILE__ , __LINE__,
						sReply->ret );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_reply: {
				SCaen__CAENHVGetChParamInfoReplyM *sReply = gReply->caenhv_getchparaminforeply;
				int dim = sReply->n_parnamelist;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_reply dim=%d :\n", __FILE__ , __LINE__, dim );
				int i = 0;
				for ( i = 0; i < dim; i++ ){
					printf("%d. ChParamList= %s\n", i, sReply->parnamelist[ i ]);
				}
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_reply: {
				SCaen__CAENHVGetChNameReplyM *sReply = gReply->caenhv_getchnamereply;
				int dim = sReply->n_chnamelist;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_reply dim=%d :\n", __FILE__ , __LINE__, dim );
				int i = 0;
				for ( i = 0; i < dim; i++ ){
					printf("%d. ChName= %s\n", i, sReply->chnamelist[ i ]);
				}
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_SetChName_reply: {
				SCaen__CAENHVSetChNameReplyM *sReply = gReply->caenhv_setchnamereply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SetChName_reply ret= %d\n", __FILE__ , __LINE__, sReply->ret );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_reply: {
				SCaen__CAENHVGetChParamPropReplyM *sReply = gReply->caenhv_getchparampropreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_reply type= %d mode= %d\n", __FILE__ , __LINE__,
						sReply->type, sReply->mode );
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_reply\n", __FILE__ , __LINE__ );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_reply: {
				SCaen__CAENHVGetChParamReplyM *sReply = gReply->caenhv_getchparamreply;
				int dim = sReply->n_valuearrayint;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_reply dim=%d :\n", __FILE__ , __LINE__, dim );

				int i = 0;
				// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
				switch ( sReply->format ){
				case 0:{
					for ( i = 0; i < dim; i++ ){
						printf("%d. valuestr= %s\n", i, sReply->valuearraystring[ i ]);
					}
					break;
				}
				case 1:{
					for ( i = 0; i < dim; i++ ){
						printf("%d. valueint= %d\n", i, sReply->valuearrayint[ i ]);
					}
					break;
				}
				case 2:{
					for ( i = 0; i < dim; i++ ){
						printf("%d. valuefloat= %g\n", i, sReply->valuearrayfloat[ i ]);
					}
					break;
				}
				default:{
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_reply unknown format= %d\n", sReply->format );
					exit(0);
					break;
				}
				}
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVSetChParamReplyM *sReply = gReply->caenhv_setchparamreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_reply ret=%d :\n", __FILE__ , __LINE__, sReply->ret );
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVGetExecCommListReplyM *sReply = gReply->caenhv_getexeccommlistreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_reply num=%d \n", __FILE__ , __LINE__, sReply->numcomm );
				uint32_t i = 0;
				for ( i = 0; i < sReply->n_commnamelist; i++ ){
					printf("%s %d %d.command= %s\n",  __FILE__ , __LINE__, i, sReply->commnamelist[ i ] );
				}
				break;

			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_reply : {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVExecCommReplyM *sReply = gReply->caenhv_execcommreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_reply ret=%d \n", __FILE__ , __LINE__, sReply->ret );
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVSubscribeSystemParamsReplyM *sReply = gReply->caenhv_subscribesystemparamsreply;
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_reply result code %s\n",
							__FILE__ , __LINE__, sReply->listofresultcodes);
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVSubscribeBoardParamsReplyM *sReply = gReply->caenhv_subscribeboardparamsreply;
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_reply result code %s\n", __FILE__ , __LINE__, sReply->listofresultcodes);
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVSubscribeChannelParamsReplyM *sReply = gReply->caenhv_subscribechannelparamsreply;
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_reply result code %s\n", __FILE__ , __LINE__, sReply->listofresultcodes);
				break;
			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVUnSubscribeSystemParamsReplyM *sReply = gReply->caenhv_unsubscribesystemparamsreply;
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_reply result codes %s\n", __FILE__ , __LINE__, i, sReply->listofresultcodes);
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVUnSubscribeBoardParamsReplyM *sReply = gReply->caenhv_unsubscribeboardparamsreply;
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_reply result codes %s\n", __FILE__ , __LINE__, i, sReply->listofresultcodes);
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVUnSubscribeChannelParamsReplyM *sReply = gReply->caenhv_unsubscribechannelparamsreply;
					printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_reply result code %d %s\n", __FILE__ , __LINE__, i, sReply->listofresultcodes);
				break;
			}

			case  S_CAEN__MESSAGE_TYPE__CAENHV_GetEventData_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVGetEventDataReplyM *sReply = gReply->caenhv_geteventdatareply;
				uint32_t dim = sReply->nbitems;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetEventData_reply nb event items=%d \n", __FILE__ , __LINE__, dim );

				int sstatus = sReply->statussystem;
				uint32_t iitem = 0;
				for ( iitem = 0; iitem < dim; iitem++ ){
					uint32_t iboard = sReply->boardindex[ iitem ];
					uint32_t ich    = sReply->channelindex[ iitem ];
					uint32_t itype = sReply->haltype[ iitem ];
					char  *itemID = sReply->itemid[ iitem ];
					uint32_t ihandle = sReply->systemhandle[ iitem ];
					uint32_t bstatus = sReply->statusboard[ iitem ];
					uint32_t caenidtype = sReply->caenidtype[ iitem ];

					printf("item%d: board= %d channel= %d type= %d ID= %s handle= %d bstatus= %d ctype= %d\n\n",
							iitem, iboard, ich, itype, itemID, ihandle, bstatus, caenidtype );

					switch( itype ){
					case 0: {
						char  *val = sReply->stringvalue[ iitem ];
						printf("item%d: %s\n", val );
						break;
					}
					case 1: {
						int32_t val = sReply->intvalue[ iitem ];
						printf("item%d: %d\n", val );
						break;
					}
					case 2: {
						float val = sReply->floatvalue[ iitem ];
						printf("item%d: %g\n", val );
						break;
					}
					}
				}
				break;

			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_FreeEventData_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_FreeEventData_reply: doing nothing\n", __FILE__ , __LINE__);
				break;

			}

			case   S_CAEN__MESSAGE_TYPE__CAENHV_Free_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_Free_reply: doing nothing\n", __FILE__ , __LINE__);
				break;

			}
			case   S_CAEN__MESSAGE_TYPE__CAENHV_GetError_reply: {
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetError_reply\n", __FILE__ , __LINE__);
				SCaen__CAENHVGetErrorReplyM *sReply = gReply->caenhv_geterrorreply;
				printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetError_reply msg=%s \n", __FILE__ , __LINE__, sReply->errmsg );
				break;

			}

			default:{
				printf("unknown reply message type, exiting\n");
				exit(-1);
				break;
			}
			} // switch
			printf("\n---new request type---\n");
		}
	} // for sequence


	// good, that was the basic syntax test. Now we could run some more tests on specific messages, to get performance
	// benchmarks and also hard testing of specific features, like i.e.
	// * talk to A LOT of channels very quickly. 40k objects should work easily, maybe we can have 1e6 objects. Mem should not leak ;-)
	// * ramp a lot of channels, and collect the replies in event mode: test max nb items in an event
	// * try to overload the OPC server with aggressive timings/reply
	// * give back intentionally wrong or weird replies, like fake some electronical problems: see how fault tolerant the OPC is

#ifdef USE_CZMQ_SHARED
	zsock_destroy( &socket_req );
#else
	rc = zmq_disconnect (socket_req, tcpipport );
	assert (rc == 0);
#endif

	return( 0 );

}

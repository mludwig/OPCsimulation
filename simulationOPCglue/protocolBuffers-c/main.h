//  protocolTester main.h


#define SIMULATION_GLUE_PORT  8880
#define SIMULATION_PILOT_PORT 8881

#define NAME_SIZE 512
#define PROTOCOL_MSG_SIZE 524288 // above 1M protocol msg become clunky, so we stay x2 below that limit

// caen api specific stuff... unavoidable
// #define CAEN_NAME_SIZE10 10

#define CAEN_NAME_MAX12 12
#define CAEN_DESC_MAX 28

/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 17 10:26:34 CEST 2016
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors: Fernando Varela, Benjamin Farnham, CERN-BE-ICS-CIC
 *
 * This file is part of the OPC simulation HAL (simHAL) project.
 *
 * The simHAL project is the property of CERN, Geneva, Switzerland, and is not free software, 
 * since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */


package sIseg;

// we organize message types in terms of request-reply pairs
// even ids are always requests, odds are replies. 

//typedef enum IsegResult {
//	ISEG_OK = 0,
//	ISEG_ERROR = 1,
//	ISEG_WRONG_SESSION_NAME
//} IsegResult;
//typedef struct IsegItem {
//	char object[FULLY_QUALIFIED_OBJECT_SIZE];	//line.module.channel.item  0.0.1.VoltageSet
//	char value[VALUE_SIZE];
//	char quality[QUALITY_SIZE];
//	char timeStampLastRefreshed[TIME_SIZE];
//	char timeStampLastChanged[TIME_SIZE];
//} IsegItem;
//#define EmptyIsegItem { "" ,"" , "", "", ""}
//
//typedef struct IsegItemProperty {
//	char object[FULLY_QUALIFIED_OBJECT_SIZE];	//line.module.channel.item  0.0.1.VoltageSet
//	char type[DATA_TYPE_SIZE];	// "R4", "UI4", "STR", "BOOL"
//	char unit[UNIT_SIZE];		// "V", "A", "C", "%", "%/s", "s"
//	char access[ACCESS_SIZE];	// "R", "W", "RW"
//	char quality[QUALITY_SIZE];
//} IsegItemProperty;
//#define EmptyIsegItemProperty { "", "", "", "", "" }


// === glue: specific message definitions===
// generic messages from/to the OPC server glue code
// the ISEG api rev13 version 21 june 2016 is fully defined by 
// 24 different request/reply messages
enum MessageType {
	ISEG_connect_request = 0; 
	ISEG_connect_reply  = 1;

	ISEG_getVersion_request = 2;
	ISEG_getVersion_reply = 3;

	ISEG_disconnect_request = 4;
	ISEG_disconnect_reply = 5;

	ISEG_setItem_request = 6;
	ISEG_setItem_reply = 7;

	ISEG_getItem_request = 8;
	ISEG_getItem_reply = 9;
	
	ISEG_getItemProperty_request = 10;
	ISEG_getItemProperty = 11;

	pilot_setConfiguration_request = 62;
	pilot_setConfiguration_reply = 63;

    // ...	
}



message IsegMessage {
  	// message type used for routing messages to their decoding methods
  	required MessageType type = 1;
  
  	optional ISEG_connect_request_m ISEG_connect_request = 2;
 	optional ISEG_connect_reply_m ISEG_connect_reply = 3;
 	
	optional ISEG_getVersion_request_m ISEG_getVersion_request = 4;
	optional ISEG_getVersion_reply_m ISEG_getVersion_reply = 5;

	optional ISEG_disconnect_request_m ISEG_disconnect_request = 6;
	optional ISEG_disconnect_reply_m ISEG_disconnect_reply = 7;

	optional ISEG_setItem_request_m ISEG_setItem_request = 8;
	optional ISEG_setItem_reply_m ISEG_setItem_reply = 9;

	optional ISEG_getItem_request_m ISEG_getItem_request = 10;
	optional ISEG_getItem_reply_m ISEG_getItem_reply = 11;
	
	optional ISEG_getItemProperty_request_m ISEG_getItemProperty_request = 12;
	optional ISEG_getItemProperty_reply_m ISEG_getItemProperty_reply = 13;

	optional ISEG_getVersionString_request_m ISEG_getVersionString_request = 14;
	optional ISEG_getVersionString_reply_m ISEG_getVersionString_reply = 15;
	
	// unchanged, same for CAEN and ISEG: we want to use the same pilot
	optional Pilot_SetConfiguration_request_m pilotSetConfigurationRequest = 16; 
    optional Pilot_SetConfiguration_reply_m   pilotSetConfigurationReply = 17;
}


message ISEG_connect_request_m {
// IsegResult iseg_connect(const char *name, const char *interface, void *reserved);
	required string name = 100;
	required string interface = 101;
	required int32 reserved = 102; // is 0 normally for now (july 2017)
}
message ISEG_connect_reply_m {
	required int32 ret = 103;
}

message ISEG_getVersion_request_m {
// unsigned int iseg_getVersion(void);
	required int32 dummy = 104;
}
message ISEG_getVersion_reply_m {
	required int32 versionNb = 105;
}

message ISEG_getVersionString_request_m {
// char * iseg_getVersionString(void);
	required int32 dummy = 104;
}
message ISEG_getVersionString_reply_m {
	required string versionString = 106;
}



message  ISEG_disconnect_request_m {
// IsegResult iseg_disconnect(const char *name);
	required string name = 107;
}

message ISEG_disconnect_reply_m {
	required int32 ret = 103;
}

message ISEG_setItem_request_m {
// IsegResult iseg_setItem(const char *name, const char *object, const char *value);
	required string name = 107;
	required string object = 108;
	required string value = 109;
}

message ISEG_setItem_reply_m {
	required int32 ret = 103;
}

message ISEG_getItem_request_m {
// IsegItem iseg_getItem(const char *name, const char *object);
	required string name = 107;
	required string object = 108;
}


message ISEG_getItem_reply_m {
	// IsegItem
	required string object = 108;	//line.module.channel.item  0.0.1.VoltageSet
	required string value = 109;
	required string quality = 110;
	required string timeStampLastRefreshed = 111;
	required string timeStampLastChanged = 112;	
}
	
message ISEG_getItemProperty_request_m {
// IsegItemProperty iseg_getItemProperty(const char *name, const char *object);
	required string name = 107;
	required string object = 108;
}
	
message ISEG_getItemProperty_reply_m {
	// IsegProperty
	required string object = 108;	//line.module.channel.item  0.0.1.VoltageSet
	required string type = 113;	// "R4", "UI4", "STR", "BOOL"
	required string unit = 114;		// "V", "A", "C", "%", "%/s", "s"
	required string access = 115; 	// "R", "W", "RW"
	required string quality = 110;		
}


// they stay unchanged for iseg and caen
message Pilot_SetConfiguration_request_m {
	required int32 crateHandle = 200;
	required string gluehn = 205;
	required string glueversion = 206;
	required int32 vendor = 207;
}
message Pilot_SetConfiguration_reply_m {
	optional int32 dummy = 178;
}

// =========================================
// ===pilot: specific message definitions===
enum PilotMessageType {
	pilot_setNoise_request = 0;
	pilot_setNoise_reply = 1;

	pilot_setLoad_request = 2;
	pilot_setLoad_reply = 3;

	pilot_getHealthEngine_request = 4;
	pilot_getHealthEngine_reply = 5;

	pilot_setCrateVSEL_request = 6;
	pilot_setCrateVSEL_reply = 7;

	pilot_getCrateHandles_request = 8;
	pilot_getCrateHandles_reply = 9;

	pilot_getLoad_request = 10;
	pilot_getLoad_reply = 11;

	pilot_dummy_request = 12;
	pilot_dummy_reply = 13;

	pilot_setAllLoads_request = 14;
	pilot_setAllLoads_reply = 15;

	pilot_rampAll_request = 16;
	pilot_rampAll_reply = 17;

	pilot_switchOffAll_request = 18;
	pilot_switchOffAll_reply = 19;
	
	pilot_getUpdateDelay_request = 20;
	pilot_getUpdateDelay_reply = 21;
	
	pilot_setUpdateDelay_request = 22;
	pilot_setUpdateDelay_reply = 23;
	
	pilot_setCrateISEL_request = 24;
	pilot_setCrateISEL_reply = 25;
	
	pilot_tripChannelByVoltage_request = 26;
	pilot_tripChannelByVoltage_reply = 27;
	
	pilot_tripChannelByCurrent_request = 28;
	pilot_tripChannelByCurrent_reply = 29;
	
	pilot_tripChannelByLoad_request = 30;
	pilot_tripChannelByLoad_reply = 31;
	
	pilot_getConfiguration_request = 32;
	pilot_getConfiguration_reply = 33;
	
	pilot_rampAllOnBoard_request = 34;
	pilot_rampAllOnBoard_reply = 35;
	
	// ... 
}

message PilotMessage {
  // message type used for routing messages to their decoding methods
  required PilotMessageType type = 500;
  
  optional Pilot_SetNoise_request_m pilotSetNoiseRequest = 501;
  optional Pilot_SetNoise_reply_m pilotSetNoiseReply = 502;
  
  optional Pilot_SetLoad_request_m pilotSetLoadRequest = 503;
  optional Pilot_SetLoad_reply_m   pilotSetLoadReply = 504;
  
  optional Pilot_GetHealthEngine_request_m pilotGetHealthEngineRequest = 505;
  optional Pilot_GetHealthEngine_reply_m   pilotGetHealthEngineReply = 506;
  
  optional Pilot_SetCrateVSEL_request_m pilotSetCrateVSELRequest = 507;
  optional Pilot_SetCrateVSEL_reply_m   pilotSetCrateVSELReply = 509;
 
  optional Pilot_GetCrateHandles_request_m pilotGetCrateHandlesRequest = 510;
  optional Pilot_GetCrateHandles_reply_m   pilotGetCrateHandlesReply = 511;
 
  optional Pilot_GetLoad_request_m pilotGetLoadRequest = 512;
  optional Pilot_GetLoad_reply_m   pilotGetLoadReply = 513;
 
  optional Pilot_Dummy_request_m pilotDummyRequest = 514;
  optional Pilot_Dummy_reply_m   pilotDummyReply = 515;
  
  optional Pilot_SetAllLoads_request_m pilotSetAllLoadsRequest = 516;
  optional Pilot_SetAllLoads_reply_m   pilotSetAllLoadsReply = 517;
  
  optional Pilot_RampAll_request_m pilotRampAllRequest = 518;
  optional Pilot_RampAll_reply_m   pilotRampAllReply = 519;
  
  optional Pilot_SwitchOffAll_request_m pilotSwitchOffAllRequest = 520;
  optional Pilot_SwitchOffAll_reply_m   pilotSwitchOffAllReply = 521;

  optional Pilot_GetUpdateDelay_request_m pilotGetUpdateDelayRequest = 522;
  optional Pilot_GetUpdateDelay_reply_m   pilotGetUpdateDelayReply = 523;

  optional Pilot_SetUpdateDelay_request_m pilotSetUpdateDelayRequest = 524;
  optional Pilot_SetUpdateDelay_reply_m   pilotSetUpdateDelayReply = 525;

  optional Pilot_SetCrateISEL_request_m pilotSetCrateISELRequest = 526;
  optional Pilot_SetCrateISEL_reply_m   pilotSetCrateISELReply = 527;
  
  optional Pilot_TripChannelByVoltage_request_m pilotTripChannelByVoltageRequest = 528;
  optional Pilot_TripChannelByVoltage_reply_m   pilotTripChannelByVoltageReply = 529;
  
  optional Pilot_TripChannelByCurrent_request_m pilotTripChannelByCurrentRequest = 530;
  optional Pilot_TripChannelByCurrent_reply_m   pilotTripChannelByCurrentReply = 531;
  
  optional Pilot_TripChannelByLoad_request_m pilotTripChannelByLoadRequest = 532;
  optional Pilot_TripChannelByLoad_reply_m   pilotTripChannelByLoadReply = 533;
  
  optional Pilot_GetConfiguration_request_m pilotGetConfigurationRequest = 534;
  optional Pilot_GetConfiguration_reply_m   pilotGetConfigurationReply = 535;
 
  optional Pilot_RampAllOnBoard_request_m pilotRampAllOnBoardRequest = 536;
  optional Pilot_RampAllOnBoard_reply_m   pilotRampAllOnBoardReply = 537;
 
  // ...
}


message Pilot_SetNoise_request_m {
	required int32 crateHandle = 600;
	required int32 slot = 601;
	required int32 channel = 602;
  	required float noise = 603;
}
message Pilot_SetNoise_reply_m {
  required int32 pret = 604;
}

message Pilot_SetLoad_request_m {
	required int32 crateHandle = 600;
	required int32 slot = 601;
	required int32 channel = 602;
  	required float resistance = 605;
  	required float capacitance = 606;
}
message Pilot_SetLoad_reply_m {
  required int32 pret = 604;
}
message Pilot_SetAllLoads_request_m {
	required int32 crateHandle = 600;
  	required float resistance = 605;
  	required float capacitance = 606;
}
message Pilot_SetAllLoads_reply_m {
  required int32 pret = 604;
}
message Pilot_RampAll_request_m {
	required int32 crateHandle = 600;
  	required float v0set = 616;
  	required float i0set = 617;
  	required float rup = 618;
  	required float rdown = 619;
}
message Pilot_RampAll_reply_m {
  required int32 pret = 604;
}
message Pilot_RampAllOnBoard_request_m {
	required int32 crateHandle = 600;
	required int32 iboard = 628;
  	required float v0set = 616;
  	required float i0set = 617;
  	required float rup = 618;
  	required float rdown = 619;
}
message Pilot_RampAllOnBoard_reply_m {
  required int32 pret = 604;
}

message Pilot_SwitchOffAll_request_m {
	required int32 crateHandle = 600;
}
message Pilot_SwitchOffAll_reply_m {
  required int32 pret = 604;
}
message Pilot_Dummy_request_m {
	optional int32 crateHandle = 600;
}
message Pilot_Dummy_reply_m {
  optional int32 pret = 604;
}

message Pilot_GetLoad_request_m {
	required int32 crateHandle = 600;
	required int32 slot = 601;
	required int32 channel = 602;
}
message Pilot_GetLoad_reply_m {
   	required float resistance = 605;
  	required float capacitance = 606; 
}
message Pilot_TripChannelByVoltage_request_m {
	required int32 crateHandle = 600;
	required int32 slot = 601;
	required int32 channel = 602;
}
message Pilot_TripChannelByVoltage_reply_m {
   	optional int32 pret = 604; 
}
message Pilot_TripChannelByCurrent_request_m {
	required int32 crateHandle = 600;
	required int32 slot = 601;
	required int32 channel = 602;
}
message Pilot_TripChannelByCurrent_reply_m {
   	optional int32 pret = 604; 
}
message Pilot_TripChannelByLoad_request_m {
	required int32 crateHandle = 600;
	required int32 slot = 601;
	required int32 channel = 602;
}
message Pilot_TripChannelByLoad_reply_m {
   	optional int32 pret = 604; 
}

message Pilot_GetHealthEngine_request_m {
	required int32 crateHandle = 600;
	// required string engineip = 607;
}

message Pilot_GetHealthEngine_reply_m {
  required int32 nbCrates = 608;
  required int32 nbBoards = 609;
  required int32 nbChannels = 610;
  required int32 nbChannelsRampingUp = 611;
  required int32 nbChannelsRampingDwn = 612;
  required int32 nbChannelsTripped = 626;
  required int32 nbChannelsOn = 613;
  required int32 nbChannelsCC = 614;
  required float eventDelay = 627;
  required float updateDelay = 628;
}

message Pilot_SetCrateVSEL_request_m {
	required int32 crateHandle = 600;
	required int32 vsel = 615;
}
message Pilot_SetCrateVSEL_reply_m {
	required int32 crateHandle = 600;
}

message Pilot_GetCrateHandles_request_m {
	required string engineip = 607;
}
message Pilot_GetCrateHandles_reply_m {
  	repeated int32 handles = 615 [packed=true];
  	repeated string ips = 616;
}

message Pilot_GetUpdateDelay_request_m {
	required int32 crateHandle = 600;
}
message Pilot_GetUpdateDelay_reply_m {
  	required float updateDelay = 621;
}
message Pilot_SetUpdateDelay_request_m {
	required int32 crateHandle = 600;
  	required float updateDelay = 621;
}
message Pilot_SetUpdateDelay_reply_m {
  	required int32 pret = 604;
}
message Pilot_SetCrateISEL_request_m {
	required int32 crateHandle = 600;
	required int32 isel = 620;
}
message Pilot_SetCrateISEL_reply_m {
	required int32 crateHandle = 600;
}
message Pilot_GetConfiguration_request_m {
	required int32 crateHandle = 600;
}
message Pilot_GetConfiguration_reply_m {
	required string configfile = 622;
	required int32 nbCrates = 623;
	required string enginehn = 624;
	required string gluehn = 625;
	required string glueversion = 626;
	required string engineversion = 627;
}

glue code for iseg, specifics
=============================


referring to iseg quasar server
build 1420 https://icejenkins.cern.ch/view/OPC-UA/job/iseg-opcua-server/ 
17oct2017
/home/mludwig/jenkins_downloaded/iseg.ua.17oct2017/iseg-opcua-server.TEST/bin

the OpcUaIsegServer depends on specific iseg libs:
	libisegHAL.so.1 (0x00007f8ca6116000)
	libisegHalCan.so.1 (0x00007f8c9c777000)
	
	which are both hw access:
	
lrwxrwxrwx. 1 mludwig mludwig       43 Oct 17 05:58 libisegHalCan.so.1 -> ../ISEG/hwAccess/lib/libisegHalCan.so.1.1.2
lrwxrwxrwx. 1 mludwig mludwig       40 Oct 17 05:58 libisegHAL.so.1 -> ../ISEG/hwAccess/lib/libisegHAL.so.1.1.2

so I assume that BOTH libs have to be repaced with simulation glue.

the ./libisegHAL.so.1.1.2
depends on 
	libisegHalCan.so.1 => not found
	libQtCore.so.4 => /usr/lib64/libQtCore.so.4 (0x00007f4ba95de000) (!!!)
	
whereas the libisegHalCan.so.1.1.2
depends only on system.

so this is a two-stage implementation, and it should be enough to replace just the libisegHAL.so

	             U can_connect
                 U can_disconnect
                 U can_getItemProperty
                 U can_getItemValue
                 U can_getVersion
                 U can_getVersionString
                 U can_setItemValue
                 
calls to the iseg API


set up symlink in /home/mludwig/jenkins_downloaded/iseg.ua.17oct2017/iseg-opcua-server.TEST/ISEG/hwAccess/lib:
lrwxrwxrwx. 1 mludwig mludwig     64 Oct 17 11:45 libisegHAL.so.1.1.2 -> /home/mludwig/OPCsimulation/simulationOPCglue/src/libglueiseg.so
which looks for
		dload_lib( "./lib/libsimiseg.so", &dlhandle_sim );
		
/home/mludwig/jenkins_downloaded/iseg.ua.17oct2017/lib has
lrwxrwxrwx. 1 mludwig mludwig 63 Oct 17 11:49 libsimiseg.so -> /home/mludwig/OPCsimulation/simulationOPCglue/src/libsimiseg.so

and
	/home/mludwig/jenkins_downloaded/iseg.ua.17oct2017/iseg-opcua-server.TEST/bin
	lrwxrwxrwx. 1 mludwig mludwig        9 Oct 17 11:50 lib -> ../../lib
		
		
so that the ./lib/libsimiseg.so can be found easily from the bin directory.



	
	
====
	
for windows the iseg server uses PEAK can libs
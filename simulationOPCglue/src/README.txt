simulation OPC glue
===================

michael.ludwig@cern.ch

CAEN
====

This is the OPC glue code, which replaces the original shared library for caen HW access in the OPC server

* replaces caen HW shared lib by simulation libs
* dynamical lib loading modified, at runtime 
* reproduces caen API with a "request-reply" architecture
* connects to the simulation engine over tcp/zeroMQ
* requests data from the simulation engine for each API call
* simulation engine replies with appropriate data for each API call
* does not consume noticable amount of extra CPU in the OPC server's host
* messages are encoded using google protocol buffers

run
1. start simulation engine first
2. check caenGlueConfig.xml, it has to point to the simulation engine's host
3. the caenGlueConfig.xml has to be present, and the HALIP has to point to the simulation engine
4. start OPC caen server with the same config as the simulation engine, i.e
	./OpcUaCaenServer --config_file ./lab0.simulation.xml
	or 
	./OpcUaCaenServer --hardwareDiscoveryMode

use clients i.e. uaexpert to connect in the usual way
	 


ISEG
====
(in progress)




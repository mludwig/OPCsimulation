// the caen api which is linked against the OPC server initially: it replaces only the CAENHV_InitSystem call


#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>

#include "caenAPI.h"
/// \todo the return code of each call should be added as "call by reference"


#include "../protocolBuffers-c/simulationGlueCaen.pb-c.h"

#define DEBUG_NONE 0
#define DEBUG_MAPPED_FUNCTIONS 1

#define MAX_FNAME 64
#define MAX_CH_NAME 12

// int debuglevel = DEBUG_MAPPED_FUNCTIONS;
int debuglevel = DEBUG_NONE;

int caenglueInitialized = 0;

/// ===dynamical lib loading stuff: explicitly mapped library functions for caen===
void (*CAENHV_InitSystem_mapped)(CAENHV_SYSTEM_TYPE_t system,
		int LinkType, void *Arg, const char *UserName, const char *Passwd,  int *handle);

void (*CAENHV_DeinitSystem_mapped)(int handle);

void (*CAENHV_GetCrateMap_mapped)(int handle,
		ushort *NrOfSlot, ushort **NrofChList, char **ModelList, char **DescriptionList,
		ushort **SerNumList, uchar **FmwRelMinList, uchar **FmwRelMaxList);

void (*CAENHV_GetSysPropList_mapped)(int handle,
		ushort *NumProp, char **PropNameList);

void (*CAENHV_GetSysPropInfo_mapped)(int handle,
		const char *PropName, unsigned *PropMode, unsigned *PropType);

void (*CAENHV_GetSysProp_mapped)(int handle,
		const char *PropName, void *Result);

void (*CAENHV_SetSysProp_mapped)(int handle,
		const char	*PropName, void *Set);

void (*CAENHV_GetBdParam_mapped)(int handle,
		ushort slotNum, const ushort *slotList, const char *ParName, void *ParValList);

void (*CAENHV_SetBdParam_mapped)(int handle,
		ushort slotNum, const ushort *slotList, const char *ParName, void *ParValue);

void (*CAENHV_GetBdParamProp_mapped)(int handle,
		ushort slot, const char *ParName, const char *PropName, void *retval);

void (*CAENHV_GetBdParamInfo_mapped)(int handle,
		ushort slot, char **ParNameList);

void (*CAENHV_TestBdPresence_mapped)(int handle,
		ushort slot, ushort *NrofCh, char **Model, char **Description, ushort *SerNum,
		uchar *FmwRelMin, uchar *FmwRelMax);

void (*CAENHV_GetChParamProp_mapped)(int handle,
		ushort slot, ushort Ch, const char *ParName, const char *PropName, void *retval);

void (*CAENHV_GetChParamInfo_mapped)(int handle, ushort slot, ushort Ch,
		char **ParNameList, int *ParNumber);

void (*CAENHV_GetChName_mapped)(int handle, ushort slot,
		ushort ChNum, const ushort *ChList, char (*ChNameList)[MAX_CH_NAME]);

void (*CAENHV_SetChName_mapped)(int handle, ushort slot,
		ushort ChNum, const ushort *ChList, const char *ChName);

void (*CAENHV_GetChParam_mapped)(int handle, ushort slot,
		const char *ParName, ushort ChNum, const ushort *ChList, void *ParValList);

void (*CAENHV_SetChParam_mapped)(int handle, ushort slot,
		const char *ParName, ushort ChNum, const ushort *ChList, void *ParValue);

void (*CAENHV_GetExecCommList_mapped)(int handle,
		ushort *NumComm, char **CommNameList);

void (*CAENHV_ExecComm_mapped)(int handle, const char *CommName);

void (*CAENHV_SubscribeSystemParams_mapped)(int handle, short Port, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes);

void (*CAENHV_SubscribeBoardParams_mapped)(int handle, short Port, const unsigned short slotIndex,
		const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes);

void (*CAENHV_SubscribeChannelParams_mapped)(int handle, short Port, const unsigned short slotIndex,
		const unsigned short chanIndex, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes);

void (*CAENHV_UnSubscribeSystemParams_mapped)(int handle, short Port, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes);

void (*CAENHV_UnSubscribeBoardParams_mapped)(int handle, short Port, const unsigned short slotIndex,
		const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes);

void (*CAENHV_UnSubscribeChannelParams_mapped)(int handle, short Port, const unsigned short slotIndex,
		const unsigned short chanIndex, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes);

/// \todo we need to return an error from CAENHV_GetError_mapped
void (*CAENHV_GetError_mapped)(int handle);

void (*CAENHV_GetEventData_mapped)(int sck, CAENHV_SYSTEMSTATUS_t *SysStatus,
		CAENHVEVENT_TYPE_t **EventData, unsigned int *DataNumber);

void (*CAENHV_FreeEventData_mapped)(CAENHVEVENT_TYPE_t **ListOfItemsData);

void (*CAENHV_Free_mapped)(void *arg);

void (*CAENHVLibSwRel_mapped)(void *arg);

/// ===============================================================================

void dload_lib( char *lname, void **dlhandle ){
	char *error;
	dlerror();
	// *dlhandle = dlopen ( lname, RTLD_GLOBAL | RTLD_NOW /* RTLD_LAZY */ );
	*dlhandle = dlopen ( lname, RTLD_GLOBAL | RTLD_LAZY  );
	if (! *dlhandle) {
		fprintf( stderr, "\nERROR %s %d %s : dlopen (\"%s\", RTLD_GLOBAL | RTLD_NOW ); failed\n", __FILE__, __LINE__ , dlerror(), lname );
		exit(1);
	}
	printf("OK loaded lib %s\n", lname );
}

// map a function pointer fp
void dlsym_lib(void *dlhandle, char *fname, void **fp ){
	dlerror();
	*fp = dlsym( dlhandle, fname );
	char *error;
	if ((error = dlerror()) != NULL)  {
		fprintf( stderr, "%s %d failed mapping function \"%s\": %s\n", __FILE__, __LINE__, fname, error );
		exit(1);
	}
	printf( "OK mapped function \"%s\"\n", fname );
}

/// CAENHV_InitSystem does a lot of background work on the first call: it loads the missing libraries into
/// memory and maps the methods and symbold needed: protocol buffers and czmq and their dependencies.
/// Once that is done (first call only) it uses the mapped function to get a handle of an existing crate from the HAL.
CAENHVLIB_API CAENHVRESULT CAENHV_InitSystem(CAENHV_SYSTEM_TYPE_t system,
		int LinkType, void *Arg, const char *UserName, const char *Passwd,  int *handle){

	if ( ! caenglueInitialized ) {

		// opaque lib handles, keep them just locally on the stack. We need them for the mappings, once.
		void *dlhandle_xml2;
		void *dlhandle_protobuf;
		void *dlhandle_protoc;
		void *dlhandle_czmq;
		void *dlhandle_lzma;
		void *dlhandle_sim;
		dload_lib( "./lib/libxml2.so", &dlhandle_xml2 );
		dload_lib( "./lib/libprotobuf-c.so", &dlhandle_protobuf );
		dload_lib( "./lib/libprotoc.so", &dlhandle_protoc );
		dload_lib( "./lib/libczmq.so", &dlhandle_czmq );
		dload_lib( "./lib/liblzma.so", &dlhandle_lzma );

		// and the simulation interface which uses all of the above
		dload_lib( "./lib/libsimcaen.so", &dlhandle_sim );


		// in order to hand down all calls arriving here to the simulation,
		// we have to map all functions of libsim_caen here using the dlhandle_sim.
		// Fortunately the caen wrapper functions are not calling themselves during execution,
		// they are not self-referencing, but just "straight calls".
		dlerror();
		char *error;

		// ... map all the CAEN API
		dlsym_lib( dlhandle_sim, "_CAENHV_InitSystem", (void **) &CAENHV_InitSystem_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_DeinitSystem", (void **) &CAENHV_DeinitSystem_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetCrateMap", (void **) &CAENHV_GetCrateMap_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetSysPropList", (void **) &CAENHV_GetSysPropList_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetSysPropInfo", (void **) &CAENHV_GetSysPropInfo_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetSysProp", (void **) &CAENHV_GetSysProp_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SetSysProp", (void **) &CAENHV_SetSysProp_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetBdParam", (void **) &CAENHV_GetBdParam_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SetBdParam", (void **) &CAENHV_SetBdParam_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetBdParamProp", (void **) &CAENHV_GetBdParamProp_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetBdParamInfo", (void **) &CAENHV_GetBdParamInfo_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_TestBdPresence", (void **) &CAENHV_TestBdPresence_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetChParamProp", (void **) &CAENHV_GetChParamProp_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetChParamInfo", (void **) &CAENHV_GetChParamInfo_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetChName", (void **) &CAENHV_GetChName_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SetChName", (void **) &CAENHV_SetChName_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetChParam", (void **) &CAENHV_GetChParam_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SetChParam", (void **) &CAENHV_SetChParam_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetExecCommList", (void **) &CAENHV_GetExecCommList_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_ExecComm", (void **) &CAENHV_ExecComm_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SubscribeSystemParams", (void **) &CAENHV_SubscribeSystemParams_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SubscribeBoardParams", (void **) &CAENHV_SubscribeBoardParams_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_SubscribeChannelParams", (void **) &CAENHV_SubscribeChannelParams_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_UnSubscribeSystemParams", (void **) &CAENHV_UnSubscribeSystemParams_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_UnSubscribeBoardParams", (void **) &CAENHV_UnSubscribeBoardParams_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_UnSubscribeChannelParams", (void **) &CAENHV_UnSubscribeChannelParams_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetError", (void **) &CAENHV_GetError_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_GetEventData", (void **) &CAENHV_GetEventData_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_FreeEventData", (void **) &CAENHV_FreeEventData_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHV_Free", (void **) &CAENHV_Free_mapped );
		dlsym_lib( dlhandle_sim, "_CAENHVLibSwRel", (void **) &CAENHVLibSwRel_mapped );

		caenglueInitialized = 1;
	}

	// and now, since we wanted to initialize the system CAENHV_InitSystem in fact, we use
	// the mapped function from dlhandle_sim to actually produce a protocol-message-request,
	// send it down to the HAL, receive a reply in return and get that one back here and fill in the
	// proper parameters (here: just the *handle)
	CAENHV_InitSystem_mapped( system, LinkType, Arg, UserName, Passwd, handle);
	printf( "OK called CAENHV_InitSystem_mapped, returning handle= %d\n", *handle );
	return( 0 );
}

CAENHVLIB_API char* CAENHVLibSwRel(void) {
	// we can ask the HAL for a SW version as well
	char *version = malloc( sizeof( char )* 256 );
	sprintf( version, "CAENHVLibSwRel simulation glue code %s %s", __DATE__, __TIME__ );
	return( version );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_DeinitSystem(int handle){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_DeinitSystem_mapped\n", __FILE__, __LINE__ );
	CAENHV_DeinitSystem_mapped(handle);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_DeinitSystem_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_GetCrateMap(int handle,
		ushort *NrOfSlot, ushort **NrofChList, char **ModelList, char **DescriptionList,
		ushort **SerNumList, uchar **FmwRelMinList, uchar **FmwRelMaxList){

	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetCrateMap_mapped\n", __FILE__, __LINE__ );

	CAENHV_GetCrateMap_mapped( handle, NrOfSlot, NrofChList, ModelList, DescriptionList,
			SerNumList, FmwRelMinList, FmwRelMaxList);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetCrateMap_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetSysPropList(int handle, ushort *NumProp, char **PropNameList){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetSysPropList_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetSysPropList_mapped( handle,NumProp, PropNameList);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetSysPropList_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetSysPropInfo(int handle,
		const char *PropName, unsigned *PropMode, unsigned *PropType){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetSysPropInfo_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetSysPropInfo_mapped( handle, PropName, PropMode, PropType);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetSysPropInfo_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetSysProp(int handle,
		const char *PropName, void *Result){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetSysProp_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetSysProp_mapped( handle, PropName, Result );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetSysProp_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_SetSysProp(int handle,
		const char	*PropName, void *Set){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SetSysProp_mapped\n", __FILE__, __LINE__ );
	CAENHV_SetSysProp_mapped( handle, PropName, Set);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SetSysProp_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetBdParam(int handle,
		ushort slotNum, const ushort *slotList, const char *ParName, void *ParValList){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetBdParam_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetBdParam_mapped( handle, slotNum, slotList, ParName, ParValList);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetBdParam_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_SetBdParam(int handle,
		ushort slotNum, const ushort *slotList, const char *ParName, void *ParValue){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SetBdParam_mapped\n", __FILE__, __LINE__ );
	CAENHV_SetBdParam_mapped( handle, slotNum, slotList, ParName, ParValue);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SetBdParam_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetBdParamProp(int handle,
		ushort slot, const char *ParName, const char *PropName, void *retval){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetBdParamProp_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetBdParamProp_mapped( handle, slot, ParName, PropName, retval);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetBdParamProp_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetBdParamInfo(int handle,
		ushort slot, char **ParNameList){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetBdParamInfo_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetBdParamInfo_mapped( handle, slot, ParNameList);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetBdParamInfo_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_TestBdPresence(int handle,
		ushort slot, ushort *NrofCh, char **Model, char **Description, ushort *SerNum,
		uchar *FmwRelMin, uchar *FmwRelMax){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetBdParamInfo_mapped\n", __FILE__, __LINE__ );
	CAENHV_TestBdPresence_mapped( handle, slot, NrofCh, Model, Description, SerNum,
			FmwRelMin, FmwRelMax);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetBdParamInfo_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetChParamProp(int handle,
		ushort slot, ushort Ch, const char *ParName, const char *PropName, void *retval){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetChParamProp_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetChParamProp_mapped( handle, slot, Ch, ParName, PropName, retval);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetChParamProp_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_GetChParamInfo(int handle, ushort slot, ushort Ch,
		char **ParNameList, int *ParNumber){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetChParamInfo_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetChParamInfo_mapped( handle, slot, Ch, ParNameList, ParNumber);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetChParamInfo_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetChName(int handle, ushort slot,
		ushort ChNum, const ushort *ChList, char (*ChNameList)[MAX_CH_NAME]){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetChName_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetChName_mapped(handle, slot, ChNum, ChList, ChNameList );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetChName_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_SetChName(int handle, ushort slot,
		ushort ChNum, const ushort *ChList, const char *ChName){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SetChName_mapped\n", __FILE__, __LINE__ );
	CAENHV_SetChName_mapped( handle, slot, ChNum, ChList, ChName);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SetChName_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetChParam(int handle, ushort slot,
		const char *ParName, ushort ChNum, const ushort *ChList, void *ParValList){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetChParam_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetChParam_mapped( handle, slot, ParName, ChNum, ChList, ParValList );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetChParam_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_SetChParam(int handle, ushort slot,
		const char *ParName, ushort ChNum, const ushort *ChList, void *ParValue){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SetChParam_mapped\n", __FILE__, __LINE__ );
	CAENHV_SetChParam_mapped( handle, slot, ParName, ChNum, ChList, ParValue);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SetChParam_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_GetExecCommList(int handle,
		ushort *NumComm, char **CommNameList){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetExecCommList_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetExecCommList_mapped( handle, NumComm, CommNameList);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetExecCommList_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT  CAENHV_ExecComm(int handle, const char *CommName){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_ExecComm_mapped\n", __FILE__, __LINE__ );
	CAENHV_ExecComm_mapped( handle, CommName);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_ExecComm_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_SubscribeSystemParams(int handle, short Port, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SubscribeSystemParams_mapped\n", __FILE__, __LINE__ );
	CAENHV_SubscribeSystemParams_mapped( handle, Port, paramNameList, paramNum, listOfResultCodes);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SubscribeSystemParams_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_SubscribeBoardParams(int handle, short Port, const unsigned short slotIndex,
		const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SubscribeBoardParams_mapped\n", __FILE__, __LINE__ );
	CAENHV_SubscribeBoardParams_mapped( handle, Port, slotIndex,
			paramNameList, paramNum, listOfResultCodes);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SubscribeBoardParams_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_SubscribeChannelParams(int handle, short Port, const unsigned short slotIndex,
		const unsigned short chanIndex, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_SubscribeChannelParams_mapped\n", __FILE__, __LINE__ );
	CAENHV_SubscribeChannelParams_mapped( handle, Port, slotIndex,
			chanIndex, paramNameList,
			paramNum, listOfResultCodes );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_SubscribeChannelParams_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_UnSubscribeSystemParams(int handle, short Port, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_UnSubscribeSystemParams_mapped\n", __FILE__, __LINE__ );
	CAENHV_UnSubscribeSystemParams_mapped( handle, Port, paramNameList,
			paramNum, listOfResultCodes);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_UnSubscribeSystemParams_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_UnSubscribeBoardParams(int handle, short Port, const unsigned short slotIndex,
		const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_UnSubscribeBoardParams_mapped\n", __FILE__, __LINE__ );
	CAENHV_UnSubscribeBoardParams_mapped( handle, Port, slotIndex, paramNameList, paramNum, listOfResultCodes);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_UnSubscribeBoardParams_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_UnSubscribeChannelParams(int handle, short Port, const unsigned short slotIndex,
		const unsigned short chanIndex, const char *paramNameList,
		unsigned int paramNum ,char *listOfResultCodes){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_UnSubscribeChannelParams_mapped\n", __FILE__, __LINE__ );
	CAENHV_UnSubscribeChannelParams_mapped( handle, Port, slotIndex, chanIndex, paramNameList, paramNum, listOfResultCodes);
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_UnSubscribeChannelParams_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}


CAENHVLIB_API char * CAENHV_GetError(int handle){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetError_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetError_mapped( handle ); /// \todo we need to return an error from CAENHV_GetError_mapped
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetError_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_GetEventData(int sck, CAENHV_SYSTEMSTATUS_t *SysStatus,
		CAENHVEVENT_TYPE_t **EventData, unsigned int *DataNumber){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_GetEventData_mapped\n", __FILE__, __LINE__ );
	CAENHV_GetEventData_mapped( sck, SysStatus, EventData, DataNumber );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_GetEventData_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_FreeEventData(CAENHVEVENT_TYPE_t **ListOfItemsData){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_FreeEventData_mapped\n", __FILE__, __LINE__ );
	CAENHV_FreeEventData_mapped( ListOfItemsData );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_FreeEventData_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}

CAENHVLIB_API CAENHVRESULT CAENHV_Free(void *arg){
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS )  printf("%s %d calling CAENHV_Free_mapped\n", __FILE__, __LINE__ );
	CAENHV_Free_mapped( arg );
	if ( debuglevel == DEBUG_MAPPED_FUNCTIONS ) printf("%s %d returning CAENHV_Free_mapped\n", __FILE__, __LINE__ );
	return( 0 );
}


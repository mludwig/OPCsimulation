// the caen api which is linked against the OPC server initially: it replaces only the CAENHV_InitSystem call
 


#define CAENHVLIB_API

// b.s.
#ifndef uchar
#define uchar unsigned char
#endif
#ifndef ushort
#define ushort unsigned short
#endif
#ifndef ulong
#define ulong unsigned int
#endif


typedef int CAENHVRESULT;
typedef enum {
	SY1527		= 0,
	SY2527		= 1,
	SY4527		= 2,
	SY5527		= 3,
	N568		= 4,
	V65XX		= 5,
	N1470		= 6,
	V8100		= 7,
	N568E		= 8,
	DT55XX		= 9,
	FTK			= 10,
	DT55XXE		= 11,
	N1068		= 12
} CAENHV_SYSTEM_TYPE_t;

typedef enum {
	SYNC		= 0,
	ASYNC		= 1,
	UNSYNC		= 2,
	NOTAVAIL	= 3
} CAENHV_EVT_STATUS_t;

typedef struct {
	CAENHV_EVT_STATUS_t	System;
	CAENHV_EVT_STATUS_t	Board[16];
} CAENHV_SYSTEMSTATUS_t;



typedef union {
	char			StringValue[1024];
	float			FloatValue;
	int				IntValue;
} IDValue_t;

typedef enum {
	PARAMETER		= 0,
	ALARM			= 1,
	KEEPALIVE		= 2
}CAENHV_ID_TYPE_t;

typedef struct {
	CAENHV_ID_TYPE_t	Type;
	int					SystemHandle;
	int					BoardIndex;
	int					ChannelIndex;
	char				ItemID[20];
	IDValue_t			Value;
} CAENHVEVENT_TYPE_t;



CAENHVLIB_API CAENHVRESULT CAENHV_InitSystem(CAENHV_SYSTEM_TYPE_t system, 
		int LinkType, void *Arg, const char *UserName, const char *Passwd,  int *handle);
		
CAENHVLIB_API CAENHVRESULT _CAENHV_GetCrateMap(int handle,
 ushort *NrOfSlot, ushort **NrofChList, char **ModelList, char **DescriptionList,
 ushort **SerNumList, uchar **FmwRelMinList, uchar **FmwRelMaxList);

/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 17 10:26:34 CEST 2016
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors: Fernando Varela, Benjamin Farnham, CERN-BE-ICS-CIC
 *
 * This file is part of the OPC simulation HAL (simHAL) project.
 *
 * The simHAL project is the property of CERN, Geneva, Switzerland, and is not free software,
 * since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */
/*
 * main.c
 *
 * this tester provides access from the OPC API to the simulationHAL. It serves two purposes:
 * 1. perform simple non-GUI tests during development and debugging, without having to run an OPC server
 * 2. document simulation API for the OPC server, reference
 * It is written in C and replaces the vendor specific API calls to real hardware by simulation API calls
 * which have an identical signature, but which connect to the simulation HAL instead of the real electronics.
 *
 *
 *  Created on: Aug 15, 2016
 *      Author: mludwig
 */



#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "main.h"

#include "../protocolBuffers-c/simulationGlueCaen.pb-c.h"
#include "simcaenAPI.h"

#define BUFFER_LENGTH 256


void usage( char *p ){
	printf("usage: %s [ -help | -host_hal <h1> | -port_hal <p1> ]\n", p);
	printf("    defaults: h1=localhost p1=8880\n" );
	exit(0);
}

void glueVersion( char vv[ BUFFER_LENGTH ] ){
	char hostname[ 127 ];
	gethostname( hostname, 126 );
	// sprintf( vv, "%s_%s_by_%s_on_%s", __DATE__, __TIME__, getenv("USER"), getenv("HOST") );
	sprintf( vv, "%s_%s_by_%s_on_%s", __DATE__, __TIME__, getenv("USER"), hostname );
}

#if 0
vector<string> parseCharBuffArray(char* const* charBuffArray, int boardCount) {
	vector<string> result;
	const char* currentChar = *charBuffArray;
	for(unsigned short j=0; j<boardCount;  j++)	{
		if(*currentChar) {
			ostringstream name;
			while(*currentChar)
			{
				name << *currentChar;
				currentChar++;
			}
			result.push_back(name.str());
		} else {
			result.push_back("NA"); // not available
		}
		currentChar++;
	}
	return result;
}
#endif

extern char** calloc_charchar( uint32_t dim1, uint32_t dim2 );
#if 0
char** calloc_charchar( uint32_t dim1, uint32_t dim2 ){
	uint32_t i = 0;
	char **rr = (char **) calloc( dim1, (unsigned long int) sizeof( char * ) );
	for ( i = 0; i < dim1; i++ ){
		rr[ i ] = (char *) calloc( dim2, (unsigned long int) sizeof( char ) );
	}
	return( rr );
}
#endif


// return allocated
char ** parseCharBuffArray( char* const* charBuffArray, int boardCount) {
	const int sz = 12;
	char **result = calloc_charchar( boardCount, sz );
	const char* currentChar = *charBuffArray;
	unsigned short j = 0;
	for( j = 0; j < boardCount;  j++ ) {
		uint32_t ir = 0;
		if(*currentChar) {
			while(*currentChar)	{
				result[ j ][ ir++ ] = *currentChar;
				currentChar++;
			}
		} else {
			sprintf( result[ j ], "NA"); // not available
		}
		currentChar++;
	}
	return result;
}

int main( int argc, char **argv ){
	char version[ BUFFER_LENGTH ];
	glueVersion( version );
	printf("Glue Caen tester VERSION= %s\n", version );


	char *caenversion = _CAENHVLibSwRel();
	printf("caenversion= %s\n", caenversion );

	int handle;
	CAENHVRESULT result = CAENHV_OK;
	CAENHV_SYSTEM_TYPE_t system = ( CAENHV_SYSTEM_TYPE_t ) 2; // CAENHV_SYSTEM_TYPE_t::SY4527;
	int LinkType = LINKTYPE_TCPIP;
	char Arg[ 127 ] = "137.138.9.19";

	// call all lib functions step-by-step: the simulator should reply well


	result = _CAENHV_InitSystem( system, LinkType, Arg, "admin", "admin", &handle);

	{
		//		CAENHVLIB_API CAENHVRESULT CAENHV_GetCrateMap(int handle,
		//	 ushort *NrOfSlot, ushort **NrofChList, char **ModelList, char **DescriptionList,
		//	 ushort **SerNumList, uchar **FmwRelMinList, uchar **FmwRelMaxList)
		ushort NrOfSlot;
		ushort *NrofChList;
		char *ModelList = 0;
		char *DescriptionList = 0;
		uint16_t *SerNumList;
		uint8_t *FmwRelMinList;
		uint8_t *FmwRelMaxList;
		result = _CAENHV_GetCrateMap( handle, &NrOfSlot, &NrofChList, &ModelList, &DescriptionList,
				&SerNumList, &FmwRelMinList, &FmwRelMaxList);
		printf("%s %d result= %d\n", __FILE__ , __LINE__, (int) result );
		int i = 0;
		printf("%s %d ---system NrOfSlot= %d---\n", __FILE__ , __LINE__, NrOfSlot );
		for ( i = 0; i < NrOfSlot; i++ ){
			printf("%s %d ---system slot= %d---\n", __FILE__ , __LINE__, i );
			// we don't know in which slot each board is !
			printf("%s %d NrofChList[ %d ]= %d (board nb channels)\n", __FILE__ , __LINE__, i, NrofChList[ i ] );
			printf("%s %d SerNumList[ %d ]= %d (board serial nb)\n", __FILE__ , __LINE__, i, SerNumList[ i ] );
			printf("%s %d FmwRelMinList[ %d ]= %d (fw min)\n", __FILE__ , __LINE__, i, FmwRelMinList[ i ] );
			printf("%s %d FmwRelMaxList[ %d ]= %d (fw max)\n", __FILE__ , __LINE__, i, FmwRelMaxList[ i ] );

			/// \todo char** desicriontion and model list :remaining unclear for the moment
			//printf("%s %d ModelList[ %d ]= %s (board model)\n", __FILE__ , __LINE__, i, ModelList[ i ] );
			//printf("%s %d DescriptionList[ %d ]= %s (board description)\n", __FILE__ , __LINE__, i, DescriptionList[ i ] );

		}
		_CAENHV_Free( ModelList );
		_CAENHV_Free( DescriptionList );


		printf("%s %d exiting... \n", __FILE__ , __LINE__ );
		exit(0);


		{
			uint16_t NumProp;
			char *PropNameList = 0;
			result = _CAENHV_GetSysPropList( handle,	&NumProp, &PropNameList );
			printf("%s %d NumProp= %d\n", __FILE__ , __LINE__, NumProp );

			// it is zero delimited strings, decode first
			char **pnames = parseCharBuffArray( &PropNameList, NumProp );
			for ( i = 0; i < NumProp; i++ ){
				printf("%s %d PropNameList[ %d ]= %s (system prop name)\n", __FILE__ , __LINE__, i, pnames[ i ] );
			}

			uint32_t PropMode;
			uint32_t PropType;
			result = _CAENHV_GetSysPropInfo( handle, "ModelName", &PropMode, &PropType);
			printf("%s %d mode=%d type=%d\n", __FILE__ , __LINE__, PropMode, PropType);
		}

		// etc etc, just a C version of the caen discovery tool.

		/// \todo immplement pilot interface for loads, noise, collective ramping


	}

	return( 0 );
}



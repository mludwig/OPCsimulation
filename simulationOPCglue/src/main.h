/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 17 10:26:34 CEST 2016
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors: Fernando Varela, Benjamin Farnham, CERN-BE-ICS-CIC
 *
 * This file is part of the OPC simulation HAL (simHAL) project.
 *
 * The simHAL project is the property of CERN, Geneva, Switzerland, and is not free software,
 * since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */
/*
 * main.h
 *
 *  Created on: Aug 15, 2016
 *      Author: mludwig
 */

#ifndef SRC_MAIN_H_
#define SRC_MAIN_H_



#endif /* SRC_MAIN_H_ */

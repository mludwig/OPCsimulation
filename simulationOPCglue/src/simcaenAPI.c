/* © Copyright CERN, Geneva, Switzerland, 2016.  All rights not expressly granted are reserved.
 *
 *  Created on: Wed Aug 17 10:26:34 CEST 2016
 * 		Author: Michael Ludwig <michael.ludwig@cern.ch>
 *      Contributors: Fernando Varela, Benjamin Farnham, CERN-BE-ICS-CIC
 *
 * This file is part of the OPC simulation HAL (simHAL) project. This code is the
 * replacement of the CAEN API with compatible glue code to interface to the simulation engine.
 * No hardware access is done. Communication with the simulatyion engine is through
 * request-reply message pairs, in the form of google protocol buffers.
 *
 * The simHAL project is the property of CERN, Geneva, Switzerland, and is not free software,
 * since it builds on top of vendor
 * specific communication interfaces and architectures, which are generally non-free and
 * are subject to licensing and/or registration. Please refer to the relevant
 * collaboration agreements between CERN ICS and the vendors for further details.
 *
 * The non-vendor specific parts of the software can be made available on request
 * under the GNU Lesser General Public Licence,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public Licence for more details <http://www.gnu.org/licenses/>.
 */

/*! \mainpage VEndor UNified Simulation (VENUS) glue lib and lib loader, in C.
 * \section intro_sec Overview
 *
 * The OPC server (for CAEN power supplies) will access simulated hardware instead of real hardware. This
 * is achieved by replacing the shared lib for hardware access with a lib containing simulation glue code.
 * The simulation glue code loads further needed libs at initialisation (first call to init) and then,
 * for every function call (these are in C) sends a message to the simulation engine and gets a specific
 * reply. Obviously the simulation engine has to run somewhere (can be any host, as long as the ports are open).
 * In order to control aspects of the simulation which are not handled by the classical hardware lib, like i.e.
 * channel loads (in Ohm/Farad) and provoking trips or certain collective actions (i.e. ramp all channels)
 * a extra entry point to the simulation engine, the pilot interface, is needed. Obviously a pilot client
 * connecting to the simulation engine via the pilot API is needed then as well.
 * The Components are therefore:
 * 1. glue code lib (C)
 * 2. caen API translator lib (C)
 * 3. s.engine (C++)
 * 4. pilot UI binary (C++)
 *
 * How to run it:
 * a. The s.engine is configured to run a certain hardware simulation, at startup. This xml config file is very similar,
 * but not identical, to the OPC server configuration.
 * b. the glue (this section) is configured using file caenGlueConfig.xml, which basically tells the glue code where the s.engine
 *    runs on the network.
 * c. the OPC server binary is started, but the hardware lib (libcaenhvwrapper.so.5.82 or similar)
 *   from caen are replaced using the simulation loader lib ( libsimulation_caen.so ). Use symlinks to do this.
 *   The OPC server is started best with --hardwareDiscoveryMode so that is actually interrogates the s.engine
 *   for a full discovery of the hardware. This is practical, since like this only the s.engine "has to know" the HW.
 * d. you can now connect your clients (uaexpert, WinCCOA project...) to the OPC server's usual opc
 *    connection: opc.tcp://<host>:4527. You can also use the pilot to manipulate the s.engine behaviour.
 * see
 * 		https://readthedocs.web.cern.ch/display/ICKB/Hardware+Simulation+-+VENUS
 * for complete doc.
 */

#include "simcaenAPI.h"

#include <stdlib.h>
#include <dlfcn.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#include <czmq.h>
#include <google/protobuf-c/protobuf-c.h>
#include <libxml2/libxml/xmlreader.h>

#include "../protocolBuffers-c/simulationGlueCaen.pb-c.h"

// CAENHVLIB_API is NULL for unix

/// \todo: return the correct error codes. This needs trying out with erroneous syntax



#define NAME_SIZE 128
#define CAEN_NAME_MAX12 MAX_CH_NAME // 12
#define CAEN_PARAM_MAX10 MAX_PARAM_NAME // 10
#define CAEN_EXECCOMM_MAX128 128
#define CAEN_DESC_MAX 28
#define MAX_NB_NAMES 200
#define LIST_SIZE 4096     // list of names
#define STRING_SIZE 64
#define GLUE_OK 0


// crates/controllers are handled in parallel by the OPC server, each in it's own thread
// we want that all communication for one crate is serialized into the thread of the simulation
// engine which deals with it. We need to keep a request-reply protocol message pattern strictly.
// Since the simulation engine is monothreaded we'll use one global **shared** mutex
// this needs to be initialized with pthread_attributes, since the default is private.
pthread_mutex_t gmutex = PTHREAD_MUTEX_INITIALIZER;
// pthread_mutexattr_t gattr;


char host_hal[ NAME_SIZE ] = "localhost";
char port_hal[ NAME_SIZE]  = "8880";
char tcpipport[ NAME_SIZE ] = "4841";
bool caen_glue_configured = 0;

zsock_t *socket_req;

/**
 * todo improve request-reply debugging in the glue code
 * maybe even switch it on with a pilot message
 */
bool _debug_request = false;
bool _debug_reply = false;

// event mode session
struct sockaddr_in serv_addr;
int CAENHV_SubscribeEventMode_socketfd = -1;
struct timeval keepalive_tv0, keepalive_tv1;
struct timezone keepalive_tz;

SUBSCRIPTION_QUADS_TABLE2_t sTable2[ MAX_NB_SIMULATED_CRATES ];
int nbSimSubscriptions2 = 0;

char *sysErr( int e ){
	switch ( e ){
	case E2BIG: return("sysErr:         Argument list too long (POSIX.1).");
	case EACCES: return(" sysErr:         Permission denied (POSIX.1).");
	case EADDRINUSE : return(" sysErr:      Address already in use (POSIX.1).");
	case EADDRNOTAVAIL : return("sysErr:   Address not available (POSIX.1).");
	case EAFNOSUPPORT: return("sysErr:    Address family not supported (POSIX.1).");
	case EAGAIN : return(" sysErr:        Resource temporarily unavailable");
	case EBUSY : return("sysErr:   The mutex could not be acquired because it was already locked.");
	case EINVAL : return("sysErr:  The value specified by mutex does not refer to an initialized mutex object.");
	case EDEADLK : return("sysErr:    The current thread already owns the mutex.");
	case EALREADY : return(" sysErr:        Connection already in progress (POSIX.1).");
	case EPERM : return(" sysErr:  The current thread does not own the mutex.");
	}
	return("sysErr: OK");
}



/// \todo subscribe event mode session: one connection per crate. From the port we find out which one
/// and we should close it.
void stopEventModeSession( short Port ){

}


// the subscription registration -for system parameters- is starting with a call to CAENHV_SubscribeSystemParameters,
// with a crate-handle and port as parameters and the and crate-ip as answer: all are handed down to here.
// Now the OPC server expects
// a connection-bind between its' event reception thread and an event server feeding back the events,
// for each crate/controller it has configured. The OPC-server talks to it's connected OPC-event-reception-socket
// to receive events, in fact the OPC-event-reception-socket is the parameter for the GetEventData call.
//
// A crate/controller is equivalent to a unique IP address from the point of
// view of the OPC server, and it does a real networking connect. That is the crate-ip.
// The event receptions (for each crate-ip) on the OPC side use the **specific socket** which was connected
// to that specific event server (via port, crate-ip) to request new events.
// Now, the simulation engine - presently in a monotask/thread design - lives on one dedicated IP, which is
// obviously the host-port of the host it is running on. That simulation-engine-ip is configured "under the hood"
// when the glue code is initialized by the first init call, by reading in "caenGlueConfig.xml" (name is hardcoded).
// Obviously the simulation-engine-ip and the crate-ip are NOT the same, and they NEVER can be
// because the real IP is taken already by real electronics (which we want to leave alone) ! The glue code runs on the same
// ip as the OPC server, obviously. So anyway, the crate-ip will have to be something different that originally.
// * The cleanest solution is to implement an event server which starts up on a given dedicated port in the crate-ip, and
// make the crate-ip the same as the simulation-engine-ip. Like this these servers can be threads of the simulation engine.
// Unfortunately we will need a lot of free IP addresses for that plus the networking and CPUs for running the simulations.
// * Until I find time to cleanly implement and test exactly that, lets take a short cut, for the mono-thread design,
// and see how far we get with that already. In the simulation, the crate handles are assigned 0,1,2... for each
// successive init call. Also we assume that for each crate there is full event mode subscription (that is how the
// caen-OPC server is currently programmed at CERN), and that this subscription is started BEFORE the next crate is initialized.
// The OPC server waits 1000ms for each subscription to be established, and once it is established, it starts listening
// to events immediately as fast as possible.

// I'm quite sure this does not entirely hold in the multi-threaded design of the OPC server, especially
// if we subscribe first to one slow crate and then to one fast crate (the first fast event overtakes the first slow event)
// but we'll see. Otherwise Quasar.cpp:: initialize() would
// have to be slowed down a bit to ensure serialisation of first calls, but that looks like a
// state machine for each crate which all go through connection for each crate in a serialized way.
//
// We need a table of quadruplets { crate-ip, port, socket, handle } for all crates, where each line is
// one crate subscription, and where the
// socket=OPC-event-reception-socket corresponds to the correct handle=simulation-crate-handle.
// this is how the shortcut will work. Each call to GetEventData has sock as parameter. Each value of sock which is not
// yet entered into the table of quadruplets gets the next crate handle assigned, starting from 0.
// for a single crate this is easy ;-)
// for several crates this might lead to event reception on the "wrong crate" which is kind of funny, but of
// course not acceptable. At least the same wrong crate will always reply wrongly. ;-(

// -1: max nb of crates exceeded for simulation
// socket still undetermined
void initSimSubscriptions2( void ){
	nbSimSubscriptions2 = 0;
	int i = 0;
	for ( i = 0; i < MAX_NB_SIMULATED_CRATES; i++ ){
		sTable2[ i ].handle = -1;
		sTable2[ i ].opcsocket = -1;
		sTable2[ i ].localsocket = -1;
		sTable2[ i ].port = -1;
		sprintf( sTable2[ i ].ip, "(noip)" );
	}
}

void showSimSubscriptions2( void ){
	int k = 0;
	printf("%s %d showSimSubscriptions2 sim3 sim2 =====start==( %d )=======\n", __FILE__, __LINE__, nbSimSubscriptions2);
	for ( k = 0; k < nbSimSubscriptions2; k++ ){
		printf("%s %d showSimSubscriptions2 sim3 sim2 have: [%d] handle= %d port= %d localsocket= %d opcsocket= %d ip= %s\n", __FILE__, __LINE__, k,
				sTable2[ k ].handle, sTable2[ k ].port, sTable2[ k ].localsocket, sTable2[ k ].opcsocket, sTable2[ k ].ip );
	}
	printf("%s %d showSimSubscriptions2 sim3 sim2 =====end=========\n", __FILE__, __LINE__);
}


// returns the index found, or -1
int haveSimSubscriptionHandle2( int handle ){
	int i = 0;
	for ( i = 0; i < nbSimSubscriptions2; i++ ){
		if ( sTable2[ i ].handle == handle ) {
			// printf("%s %d haveSimSubscriptionHandle2: found handle= %d\n", __FILE__, __LINE__, handle );
			return( i );
		}
	}
	// printf("%s %d haveSimSubscriptionHandle2: did not find handle= %d\n", __FILE__, __LINE__, handle );
	return( -1 ); // not found
}


/// add a new subscription, but with the opc socket (server side) still unassigned. Only allow one
/// unassigned connection at a time: wait until a getEventData request for this unassigned socket comes in, then
/// the socket can be assigned without ambiguity and we can add another crate.
void addSimSubscription2( int handle, int port, char *ip, int localsocket ){
	if ( nbSimSubscriptions2 >= MAX_NB_SIMULATED_CRATES ) {
		printf("%s %d addSimSubscription2 sim3 nbSimSubscriptions > %d, refusing to add another crate subscription...\n",
				__FILE__, __LINE__, MAX_NB_SIMULATED_CRATES );
		return;
	}
	if ( haveSimSubscriptionHandle2( handle ) >= 0 ){
		// printf("%s %d addSimSubscription2 sim2 : have already %s:%d handle= %d localsocket= %d OK\n", __FILE__, __LINE__, ip, port, handle, localsocket );
		return;
	}
	sTable2[ nbSimSubscriptions2 ].handle = handle;
	sTable2[ nbSimSubscriptions2 ].port = port;
	sTable2[ nbSimSubscriptions2 ].localsocket = localsocket;
	sTable2[ nbSimSubscriptions2 ].opcsocket = -1;  // not yet associated to a crate
	sprintf( sTable2[ nbSimSubscriptions2 ].ip, ip );

	// printf("%s %d addSimSubscription2 sim3 : new %s:%d handle= %d localsocket= %d OK\n", __FILE__, __LINE__, ip, port, handle, localsocket );
	nbSimSubscriptions2++;
	//showSimSubscriptions2();
}

/// we assign the opc socket to the next free subscription, and since we have a mutex
/// there is only one free without ambiguity. When done, unlock the mutex so that we can
/// accept more connections. The OPC server will "see a slow client"
/// which accepts only one by one connections, not in parallel (like a true server).
void findAndAssignNextFreeHandle( int opcsocket ){
	printf("%s %d findAndAssignNextFreeHandle sim2 opcsocket= %d nbSimSubscriptions2= %d\n", __FILE__, __LINE__,
			opcsocket, nbSimSubscriptions2 );
	//showSimSubscriptions2();
	int k = 0;
	for ( k = 0; k < nbSimSubscriptions2; k++ ){
		if (( sTable2[ k ].opcsocket < 0 ) && ( sTable2[ k ].handle >= 0 )){
			sTable2[ k ].opcsocket = opcsocket;
			return;
		}
	}
}


/// exploits the subscription table
int convertSocketToHandle2( int opcsocket ){
	// printf("%s %d convertSocketToHandle2 sim2: looking for opcsocket= %d\n", __FILE__, __LINE__, opcsocket );
	int i = 0;
	for ( i = 0; i < nbSimSubscriptions2; i++ ) {
		if ( sTable2[ i ].opcsocket == opcsocket ) {
			//	printf("%s %d convertSocketToHandle2: found opcsocket= %d returning handle= %d ip= %s localsocket= %d port= %d\n", __FILE__, __LINE__,
			//			opcsocket, sTable2[ i ].handle, sTable2[ i ].ip, sTable2[ i ].localsocket, sTable2[ i ].port );
			return( sTable2[ i ].handle );
		}
	}
	printf("%s %d convertSocketToHandle2 sim2 : did not find socket= %d returning -1\n", __FILE__, __LINE__, opcsocket );
	return(-1);
}

/// exploits the association table
char* convertSocketToIP2( int opcsocket ){
	int i = 0;
	for ( i = 0; i < nbSimSubscriptions2; i++ ) {
		if ( sTable2[ i ].opcsocket == opcsocket ) {
			printf("%s %d convertSocketToIP2: found opcsocket= %d returning ip= %s\n", __FILE__, __LINE__, opcsocket, sTable2[ i ].ip );
			return( sTable2[ i ].ip );
		}
	}
	printf("%s %d convertSocketToIP2: did not find opcsocket= %d returning noipfound\n", __FILE__, __LINE__, opcsocket );
	return("noipfound");
}

/**
 * we are behaving like a CAEN controller, which is a client pushing data to the server which
 * listens on <Port> = 4527. So we have to create&bind a socket and make a connect from the server.
 */
void startEventModeSession2( int handle, short Port, char *crateIP, int *sock ){
	// if the crate handle return the localsocket as found
	int ret = haveSimSubscriptionHandle2( handle );
	if ( ret > -1 ) {
		*sock = sTable2[ ret ].localsocket;
		return;
	}
	struct sockaddr_in opc_server;
	*sock = socket(AF_INET , SOCK_STREAM , 0);
	if ( *sock == -1)
	{
		printf("%s %d startEventModeSession: Could not create socket\n", __FILE__, __LINE__);
	}
	printf("%s %d startEventModeSession: Socket created %d\n", __FILE__, __LINE__, *sock );

	/// \todo check for 192.168.x.x for safety
#if 0
	// IPv4 reserves all addresses in the range 127.0.0.0 up to 127.255.255.255 for use as loopback
	char *pch = strstr( crateIP, "127.");
	if ( pch == NULL ){
		printf("%s %d startEventModeSession Port= %d requested ip= %s\n", __FILE__, __LINE__, Port, crateIP );
		printf("%s %d startEventModeSession this is not a loopback ! Please correct the OPC server config for simulation\n");
		exit(0);
	}
#endif

	// connect to OPC-UA server, which is the same task, with the ip and Port as specified
	memset(&serv_addr, '0', sizeof(serv_addr));
	opc_server.sin_addr.s_addr = inet_addr( crateIP );
	opc_server.sin_family = AF_INET;
	opc_server.sin_port = htons( Port ); // just the network byte order
	printf("%s %d startEventModeSession Port= %d crateIP= %s sock= %d connecting...\n", __FILE__, __LINE__, Port, crateIP, *sock );
	if (connect( *sock , (struct sockaddr *)&opc_server , sizeof(opc_server)) < 0)
	{
		printf("%s %d startEventModeSession %s:%d socket= %d connect() failed\n", __FILE__, __LINE__, crateIP, Port, *sock );
		exit(-1);
	}
	printf("%s %d startEventModeSession Port= %d crateIP= %s sock= %d ...connect OK\n", __FILE__, __LINE__, Port, crateIP, *sock );

	// register the connection, but with the opc socket still unknown.
	addSimSubscription2( handle, Port, crateIP, *sock );
	printf("%s %d startEventModeSession finished OK Port= %d crateIP= %s sock= %d\n", __FILE__, __LINE__, Port, crateIP, *sock );
}


// convert an array of uint8 to a string = char *
// each number takes 2 characters plus a blankspace
// nb is the number of elements in *in
// buf has to be allocated to 3 * nb + 2, therefore call the _sz before
// returns size of char, but not really needed
int ar_uint8_to_char_sz( size_t nb ){
	return( 3 * nb + 2 ); // 2 chars plus blank + 2 ending 0
}
void ar_uint8_to_char( uint8_t *in, char *buf, size_t nb ){
	uint32_t len2 = ar_uint8_to_char_sz( nb );
	memset( buf, 0, len2 );
	char cnum[ 3 ];
	uint32_t i = 0;
	for ( i = 0; i < nb; i++){
		sprintf( cnum, "%2x ", in[ i ]); // always 2 dgits
		strcat ( buf, cnum );
	}
}

// convert a string = char * to an array of uint8
// each number takes 2 characters plus a blankspace "FF "
// buf has to be allocated, call by ref
// the number of uint : call _sz
int char_to_ar_uint8_sz( size_t nb ){
	return( nb / 3 );
}
void char_to_ar_uint8( char *in, uint8_t *buf ){
	uint32_t len = strlen( in );
	uint32_t nb = char_to_ar_uint8_sz( len );
	memset( buf, 0, nb );
	char delimiter[] = " ";
	int i = 0;
	char *sub = strtok ( in, delimiter);
	while ( sub != NULL)
	{
		int d = 0;
		sscanf( sub, "%x", &d );
		buf[ i++ ] = d;
		sub = strtok ( NULL, delimiter );
	}
}

char** calloc_charchar( uint32_t dim1, uint32_t dim2 ){
	uint32_t i = 0;
	char **rr = (char **) calloc( dim1, (unsigned long int) sizeof( char * ) );
	for ( i = 0; i < dim1; i++ ){
		rr[ i ] = (char *) calloc( dim2, (unsigned long int) sizeof( char ) );
	}
	return( rr );
}

uint16_t* calloc_uint16( uint32_t dim1 ){
	uint16_t *rr = (uint16_t *) calloc( dim1, (unsigned long int) sizeof( uint16_t  ) );
	return( rr );
}

uchar** calloc_ucharuchar( uint32_t dim1 ){
	uint32_t i = 0;
	uchar **rr = (uchar **) calloc( dim1, (unsigned long int) sizeof( uchar * ) );
	for ( i = 0; i < dim1; i++ ){
		rr[ i ] = (uchar *) calloc( 1, (unsigned long int) sizeof( uchar ) );
	}
	return( rr );
}

uchar* calloc_uchar( uint32_t dim1 ){
	uchar *rr = (uchar *) calloc( dim1, (unsigned long int) sizeof( uchar * ) );
	return( rr );
}

void free_charchar( char **v, uint32_t dim1 ){
	// need 2dim dealloc as well
	uint32_t i = 0;
	for ( i = 0; i < dim1; i++ ){
		free( v[ i ] );
	}
	free( v );
}

// delimited by \0, variable size
char *buildNullDelimitedListAsString( char **nameList, int nb ){
	int pos = 0;
	uint32_t k = 0;
	int i = 0;
	char *resList = (char *) calloc( LIST_SIZE, sizeof( char ));
	for ( i = 0; i < nb; i++ ){
		for ( k = 0; k < strlen( nameList[ i ]); k++ ){
			resList[ pos++ ] = nameList[ i ][ k ];
		}
		resList[ pos++ ] = '\0';
	}
	resList[ pos++ ] = '\0';
	return( resList );
}

/**
 * with this rather early version of czmq the sending of pure data does not work properly.
 * Therefore I am coding everything into strings (remember: zmq is NOT data neutral,
 * it manipulates certain string elements !!) and decode it back my own way. Slower but foolproof.
 */
void sendCZMQ_caen( SCaen__CaenMessage gRequest, zsock_t *socket_req ) {
	uint32_t len = s_caen__caen_message__get_packed_size( &gRequest );
	bool _debug = false;
	if(_debug) printf( "%s %d generic message packed len= %d\n", __FILE__, __LINE__, len );

	uint8_t buf_uint8[ len ];
	memset( buf_uint8, 0, len );
	size_t sz = s_caen__caen_message__pack( &gRequest, buf_uint8 );
	if(_debug) printf( "%s %d generic message packed sz= %d\n", __FILE__, __LINE__, sz );

	// convert uint8 array into char* and send
	int char_sz = ar_uint8_to_char_sz( sz );
	char buf_char[ char_sz ];

	ar_uint8_to_char( buf_uint8, buf_char, sz );
	if(_debug) printf( "%s %d sending %s len= %d\n", __FILE__, __LINE__, buf_char, len );
	zstr_send ( socket_req, (const char *) buf_char );
	if(_debug) printf("%s %d ...sent OK\n", __FILE__ , __LINE__ );
}


void receiveCZMQ_caen( zsock_t *sock, SCaen__CaenMessage **msg ) {
	bool _debug = false;
	char *reply;
	reply = zstr_recv ( sock );
	size_t len = strlen( reply );
	if(_debug) printf("%s %d receiveCZMQ2: reply %s len= %d\n", __FILE__ , __LINE__, reply, len  );

	// convert it to array uint8
	int uint8_sz = char_to_ar_uint8_sz( len );
	uint8_t buf_uint8[ uint8_sz ];
	char_to_ar_uint8( reply, buf_uint8 );
	zstr_free( &reply );

	if(_debug) printf("%s %d receiveCZMQ2: got a reply, unpacking generic message uint8_sz= %d\n", __FILE__ , __LINE__, uint8_sz );
	*msg = s_caen__caen_message__unpack( NULL, uint8_sz, buf_uint8 );
}


/**
 * processNode:
 * reader: the xmlReader
 *
 * Dump information about the current node
 */
static void processNode( xmlTextReaderPtr reader) {
	const xmlChar *name, *value, *ip, *port, *dlevel;
	int nbAttr = -1;
	name = xmlTextReaderConstName(reader);
	if (name == NULL) name = BAD_CAST "--";
	int depth = xmlTextReaderDepth(reader);
	int type = xmlTextReaderNodeType(reader);
	bool hasAttr = xmlTextReaderHasAttributes(reader);

	// <caenGlue HALIP="137.138.9.19"></caenGlue>
	if (( depth == 1 ) && ( type == 1 ) && ( strcmp( (const char *) name, "caenGlue") == 0 ) && hasAttr ){
		ip = xmlTextReaderGetAttribute( reader, "HALIP" );
		printf("found HAL ip= %s\n", ip );
		sprintf( host_hal, ip );

		port = xmlTextReaderGetAttribute( reader, "HALPORT" );
		printf("found HAL port= %s\n", port );
		sprintf( port_hal, port );

		dlevel = xmlTextReaderGetAttribute( reader, "DEBUGLEVEL" );
		printf("found DEBUGLEVEL= %s\n", dlevel );
		if ( strcmp( (const char *) dlevel, "0") == 0 ) {
			_debug_reply = false;
		}
		if ( strcmp( (const char *) dlevel, "1") == 0 ) {
			_debug_reply = true;
		}
		printf("_debug_reply= %d\n", _debug_reply );
	}
}

/**
 * streamFile:
 * filename: the file name to parse
 *
 * Parse and print information about an XML file.
 */
static void streamFile(const char *filename) {
    xmlTextReaderPtr reader;
    int ret;
    reader = xmlReaderForFile(filename, NULL, 0);
    if (reader != NULL) {
        ret = xmlTextReaderRead(reader);
        while (ret == 1) {
            processNode(reader);
            ret = xmlTextReaderRead(reader);
        }
        xmlFreeTextReader(reader);
        if (ret != 0) {
            fprintf(stderr, "%s %d GLUE  %s : failed to parse\n",  __FILE__, __LINE__, filename);
        }
    } else {
        fprintf(stderr, "%s %d GLUE caen Unable to open %s, assuming defaults\n", __FILE__, __LINE__, filename);
        _debug_request = true;
        _debug_reply = true;
        fprintf(stderr, "%s %d GLUE caen defaults: %s %s debug request= %d reply= %d\n", __FILE__, __LINE__, host_hal, port_hal, _debug_request, _debug_reply );
    }
}



/// read in glue configuration from xml
/// http://www.xmlsoft.org/examples/reader1.c
int caenGlueConfigure( void ){
	_debug_reply = true;
	char cfg[ 127 ] = "caenGlueConfig.xml";
	printf("%s %d GLUE trying to read configuration from file %s\n", __FILE__ , __LINE__, cfg );
	streamFile( cfg );
	xmlCleanupParser();
	// i.e. <caenGlue HALIP="128.141.159.228" HALPORT="8880" DEBUGLEVEL="1"></caenGlue>

	int ret = pthread_mutex_init( &gmutex, NULL );
	if ( ret ) printf("pthread_mutex_init failed\n");

	initSimSubscriptions2();
	return( GLUE_OK );
}


/**
 * ############# MAPPED CAEN API STARTS HERE #################
 *
 * the principle is rather simple: once inside each specific api call,
 * send a request message of the specific format which belogs functionality-wise
 * to that api cal down to the s.engine. The s.engine figures out the correct reply
 * for that specific case and sends it back as a reply, which we have to wait for.
 * It is a primordial design feature to keep this strict request-reply pattern
 * for each specific api-call, hence we need the global mutex to protect these
 * critical sections. The OPC-UA server is multithreaded by design, and we
 * have on thread for "linear user requests" and one for the event mode.
 */
char version[ 127 ] = "";
CAENHVLIB_API char* _CAENHVLibSwRel(void) {
	sprintf( version, "GLUE CAENHVLibSwRel simulation %s %s", __DATE__, __TIME__ );
	return( version );
}

/** first call to caen api for initialization.
  */
/// the OPC server asks access to a IP and expects a handle as reply.
/// What happens here in the simulation is different:
/// (0) the dynamic libs are loaded and mapped so that we can do sth
/// (1) the HAL is configured, running and listens at a  specific hal->ip:port
///     That hal->ip::port must be known in the glue code, but stay hidden from the OPC server.
///     the simplest way is to read hal->ip::port from a file.
/// (2) a request message is sent to the HAL (over hal->ip::port, indicating the wanted IP opc->ip for the
///     controller in question
/// (3) the HAL searches through it's configured crates and tries to find one which has, as a primary and unique key
///     the opc->ip . When found, the HAL returns the handle of that crate (=controller) in a reply message
CAENHVLIB_API CAENHVRESULT _CAENHV_InitSystem(CAENHV_SYSTEM_TYPE_t system, int LinkType, void *Arg,
		const char *UserName, const char *Passwd,  int *handle) {

	if ( !caen_glue_configured ){
		printf("%s %d GLUE configuring glue since it was not yet done\n", __FILE__, __LINE__ );
		{
			printf("%s %d GLUE configuring mutex\n", __FILE__, __LINE__ );
			pthread_mutexattr_t attr;
			int type;
			int ret = pthread_mutexattr_init(&attr);
			ret = pthread_mutexattr_gettype(&attr, &type);
			type = PTHREAD_MUTEX_ERRORCHECK_NP;
			ret = pthread_mutexattr_settype( &attr, type);
			ret = pthread_mutex_init( &gmutex, &attr );
			printf("%s %d GLUE configuring mutex OK, unlocking...\n", __FILE__, __LINE__ );

			pthread_mutex_unlock(&gmutex);
			printf( "%s\n", sysErr( ret ));
			printf("%s %d GLUE configuring mutex unlocked OK\n", __FILE__, __LINE__ );
		}

		int ret = caenGlueConfigure();
		if ( ret == GLUE_OK ) {
			printf("%s %d GLUE configuring glue is OK\n", __FILE__, __LINE__ );
			caen_glue_configured = true;
			sprintf( tcpipport, "tcp://%s:%s", host_hal, port_hal );
			char hn[ 127 ];
			int ret = gethostname( hn, 127 );
			socket_req = zsock_new_req( tcpipport ); // client, send-recv pattern
			// zsock_set_unbounded( socket_req );
			assert( socket_req );
			printf("%s %d GLUE configured HAL at %s, running on host= %s ( ret = %d )\n", __FILE__, __LINE__, tcpipport, hn, ret );
		} else {
			fprintf(stderr, "GLUE configure problem for caen glue code, ret= %d", ret );
			return( CAENHV_SYSERR );
		}
	}

	/** send some generic information down to the s.engine, use CAEN message for this since we are
	 * in the caen glue code
	 **/
	{
		printf("%s %d GLUE send some information down to the s.engine\n", __FILE__, __LINE__ );
		SCaen__CaenMessage pRequest;
		s_caen__caen_message__init( &pRequest );

		pRequest.type = S_CAEN__MESSAGE_TYPE__pilot_setConfiguration_request;
		SCaen__PilotSetConfigurationRequestM sMessage;
		s_caen__pilot__set_configuration_request_m__init( &sMessage );
		pRequest.pilotsetconfigurationrequest = &sMessage;
		pRequest.pilotsetconfigurationrequest->cratehandle = 0;
		char hname[ 512 ] = "";
		int ret = gethostname( hname, 512 );
		pRequest.pilotsetconfigurationrequest->gluehn = hname;
		pRequest.pilotsetconfigurationrequest->glueversion = _CAENHVLibSwRel();
		pRequest.pilotsetconfigurationrequest->vendor = VENDOR_CAEN;

		while( pthread_mutex_trylock( &gmutex ) ){}
		sendCZMQ_caen( pRequest, socket_req );
		SCaen__CaenMessage *pReply;
		receiveCZMQ_caen( socket_req, &pReply );
		pthread_mutex_unlock(&gmutex);

		SCaen__PilotSetConfigurationReplyM *sReply = pReply->pilotsetconfigurationreply;
		s_caen__caen_message__free_unpacked( pReply, NULL );
		printf("%s %d GLUE sent information down to the s.engine OK\n", __FILE__, __LINE__ );
	}


	// fill in generic message, set specific message and send to HAL
	{
		SCaen__CaenMessage gRequest;
		s_caen__caen_message__init( &gRequest );

		gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_InitSystem_request;
		SCaen__CAENHVInitSystemRequestM sMessage;
		s_caen__caenhv__init_system_request_m__init( &sMessage );
		gRequest.caenhv_initsystemrequest = &sMessage;
		gRequest.caenhv_initsystemrequest->system = (int) system;   // i.e. SY4527 = 2
		gRequest.caenhv_initsystemrequest->linktype = LinkType;     // i.e. LINKTYPE_TCPIP
		gRequest.caenhv_initsystemrequest->username = (char *) UserName;
		gRequest.caenhv_initsystemrequest->passwd = (char *) Passwd;
		gRequest.caenhv_initsystemrequest->arg = (char *) Arg; // crate primary key: unique ;-)

		while( pthread_mutex_trylock( &gmutex ) ){}
		sendCZMQ_caen( gRequest, socket_req );
		SCaen__CaenMessage *gReply;
		receiveCZMQ_caen( socket_req, &gReply );
		pthread_mutex_unlock(&gmutex);

		SCaen__CAENHVInitSystemReplyM *sReply = gReply->caenhv_initsystemreply;
		*handle = sReply->handle;
		if ( _debug_reply ) printf("%s %d handle= %d\n", __FILE__ , __LINE__ , sReply->handle );
		s_caen__caen_message__free_unpacked( gReply, NULL );
	}
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_DeinitSystem(int handle){
	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_DeinitSystem_request;

	SCaen__CAENHVDeinitSystemRequestM sMessage;
	s_caen__caenhv__deinit_system_request_m__init( &sMessage );
	gRequest.caenhv_deinitsystemrequest = &sMessage;
	gRequest.caenhv_deinitsystemrequest->handle = handle;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVDeinitSystemReplyM *sReply = gReply->caenhv_deinitsystemreply;
	int ret = sReply->ret;

	// deallocate
	s_caen__caen_message__free_unpacked( gReply, NULL );

	if ( _debug_reply ) printf("%s %d ret= %d\n", __FILE__ , __LINE__ , ret );
	if ( ret == 0 ) return( CAENHV_OK );
	else return( CAENHV_SYSERR );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_GetCrateMap(int handle,
 ushort *NrOfSlot, ushort **NrofChList, char **ModelList, char **DescriptionList,
 ushort **SerNumList, uchar **FmwRelMinList, uchar **FmwRelMaxList){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetCrateMap_request;

	SCaen__CAENHVGetCrateMapRequestM sMessage;
	s_caen__caenhv__get_crate_map_request_m__init( &sMessage );
	gRequest.caenhv_getcratemaprequest = &sMessage;
	gRequest.caenhv_getcratemaprequest->handle = handle;

	if (_debug_request ) printf("%s %d _CAENHV_GetCrateMap handle= %d\n",
			__FILE__, __LINE__, gRequest.caenhv_getcratemaprequest->handle );


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);


	SCaen__CAENHVGetCrateMapReplyM *sReply = gReply->caenhv_getcratemapreply;
	int dim = sReply->nrofslot;

	// same var names as the CAEN API, and we allocate them. Upper layer
	// SW must deallocate them after the call. We copy them because the protocol
	// buffers and also czmq have their own alloc/dealloc dynamical strategies.
	*NrofChList = calloc_uint16( dim );
	*SerNumList = calloc_uint16( dim );
	*FmwRelMinList = calloc_uchar( dim );
	*FmwRelMaxList = calloc_uchar( dim );
	// calloc long strings, I don't know how long the descriptions may be.
	uint32_t dimDescription = 128;
	*DescriptionList = calloc( dim * dimDescription, (unsigned long int) sizeof( char ) );
	*ModelList = calloc( dim * CAEN_NAME_MAX12, (unsigned long int) sizeof( char ) );

	// copy & debug
	*NrOfSlot = dim;
	if ( _debug_reply ) printf("%s %d _CAENHV_GetCrateMap NrOfSlot= %d\n", __FILE__ , __LINE__, *NrOfSlot );
	int i = 0;
	int length_m = 0;
	int length_d = 0;
	const char nl = '\0';
	for ( i = 0; i < dim; i++ ){
		(*NrofChList)[ i ] = sReply->nrofchlist[ i ];
		(*SerNumList)[ i ] = sReply->sernumlist[ i ];

		(*FmwRelMinList)[ i ] = sReply->fmwrelminlist[ i ];
		(*FmwRelMaxList)[ i ] = sReply->fmwrelmaxlist[ i ];

		if ( i == 0 ){
			length_m += sprintf( *ModelList, "%s", sReply->modellist[ i ] );
			length_d += sprintf( *DescriptionList, "%s", sReply->descriptionlist[ i ] );
		} else {
			int p_m = length_m;
			length_m += sprintf( *ModelList + length_m, "%s%c%s", *ModelList + p_m, nl, sReply->modellist[ i ] ); // append 0-delimited
			int p_d = length_m;
			length_d += sprintf( *DescriptionList + length_d, "%s%c%s", *DescriptionList + p_d, nl, sReply->descriptionlist[ i ] ); // append 0-delimited
		}

		if ( _debug_reply ){
			printf("%s %d _CAENHV_GetCrateMap ---board count= %d---\n", __FILE__ , __LINE__, i );
			printf("%s %d _CAENHV_GetCrateMap NrofChList[ %d ]= %d (board nb channels)\n", __FILE__ , __LINE__, i, (*NrofChList)[ i ] );
			printf("%s %d _CAENHV_GetCrateMap SerNumList[ %d ]= %d (board serial nb)\n", __FILE__ , __LINE__, i, (*SerNumList)[ i ] );
			printf("%s %d _CAENHV_GetCrateMap FmwRelMinList[ %d ]= %d (fw min)\n", __FILE__ , __LINE__, i, (*FmwRelMinList)[ i ] );
			printf("%s %d _CAENHV_GetCrateMap FmwRelMaxList[ %d ]= %d (fw max)\n", __FILE__ , __LINE__, i, (*FmwRelMaxList)[ i ] );

			// can't properly print out the 0-delimited string, take it from the message therefore
			printf("%s %d _CAENHV_GetCrateMap ModelList[ %d ]= %s (board model)\n", __FILE__ , __LINE__, i, sReply->modellist[ i ] );
			printf("%s %d _CAENHV_GetCrateMap DescriptionList[ %d ]= %s (board description)\n", __FILE__ , __LINE__, i, sReply->descriptionlist[ i ] );
		}
	}
	//printf("%s %d exit\n", __FILE__ , __LINE__);
	//exit(0);

	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetSysPropList(int handle, ushort *NumProp, char **PropNameList ){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropList_request;

	SCaen__CAENHVGetSysPropListRequestM sMessage;
	s_caen__caenhv__get_sys_prop_list_request_m__init( &sMessage );
	gRequest.caenhv_getsysproplistrequest = &sMessage;
	gRequest.caenhv_getsysproplistrequest->handle = handle;


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);


	SCaen__CAENHVGetSysPropListReplyM *sReply = gReply->caenhv_getsysproplistreply;

	int dim = sReply->numprop;
	if ( _debug_reply ){
		printf("%s %d dim=%d :\n", __FILE__ , __LINE__, dim );
		int i = 0;
		for ( i = 0; i < dim; i++ ){
			printf("%s %d %d. SysPropName= %s\n", __FILE__ , __LINE__, i, sReply->propnamelist[ i ]);
		}
	}
	char *list = buildNullDelimitedListAsString( sReply->propnamelist, dim );
	*PropNameList = list; // allocated
	*NumProp = dim;
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetSysPropInfo(int handle,
 const char *PropName, unsigned *PropMode, unsigned *PropType){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetSysPropInfo_request;
	SCaen__CAENHVGetSysPropInfoRequestM sMessage;
	s_caen__caenhv__get_sys_prop_info_request_m__init( &sMessage );
	gRequest.caenhv_getsyspropinforequest = &sMessage;
	gRequest.caenhv_getsyspropinforequest->handle = handle;
	gRequest.caenhv_getsyspropinforequest->propname = ( char *) PropName;


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetSysPropInfoReplyM *sReply = gReply->caenhv_getsyspropinforeply;
	if ( _debug_reply ){
		printf("%s %d _CAENHV_GetSysPropInfo mode=%d type=%d\n", __FILE__ , __LINE__, sReply->propmode, sReply->proptype );
	}
	*PropMode = sReply->propmode;
	*PropType = sReply->proptype;
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetSysProp(int handle, const char *PropName, void *Result ){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_request;
	SCaen__CAENHVGetSysPropRequestM sMessage;
	s_caen__caenhv__get_sys_prop_request_m__init( &sMessage );
	gRequest.caenhv_getsysproprequest = &sMessage;
	gRequest.caenhv_getsysproprequest->handle = handle;
	gRequest.caenhv_getsysproprequest->propname = (char *) PropName;

	if( _debug_request ){
		printf("%s %d _CAENHV_GetSysProp handle= %d prop= %s\n",
				__FILE__ , __LINE__ , gRequest.caenhv_getsysproprequest->handle,
		gRequest.caenhv_getsysproprequest->propname );
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetSysPropReplyM *sReply = gReply->caenhv_getsyspropreply;
	if ( _debug_reply ) printf("%s %d _CAENHV_GetSysProp: PropName= %s format=%d \n", __FILE__ , __LINE__, PropName, sReply->format );
	// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;

	switch( sReply->format ) {
	case tstr: {
		if ( _debug_reply ) printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply value=%s \n", __FILE__ , __LINE__,
				sReply->valuestring );
		sprintf( (char *) Result, sReply->valuestring );
		break;
	}
	case tint32: {
		if ( _debug_reply ) printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply value=%d \n", __FILE__ , __LINE__,
				sReply->valueint );
		*((int *) Result) = sReply->valueint;
		break;
	}
	case treal: {
		if ( _debug_reply ) printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply value=%g \n", __FILE__ , __LINE__,
				sReply->valuefloat );
		*((float *) Result) = sReply->valuefloat;
		break;
	}
	default: {
		printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetSysProp_reply unknown format=%d \n", __FILE__ , __LINE__,
				sReply->format );
		return( CAENHV_READERR );
		break;
	}
	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_SetSysProp(int handle,
		const char	*PropName, void *Set){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SetSysProp_request;
	SCaen__CAENHVSetSysPropRequestM sMessage;
	s_caen__caenhv__set_sys_prop_request_m__init( &sMessage );
	gRequest.caenhv_setsysproprequest = &sMessage;
	gRequest.caenhv_setsysproprequest->handle = handle;
	gRequest.caenhv_setsysproprequest->propname = (char *) PropName;

	// here we don't know the format of the value, so we need to transfer all possible formats
	// thank you CAEN for such a good API ;-(
	gRequest.caenhv_setsysproprequest->valuestring = (char *) Set;
	gRequest.caenhv_setsysproprequest->valueint = *((int32_t * ) Set);
	gRequest.caenhv_setsysproprequest->valuefloat = *((float * ) Set);

	//gRequest.caenhv_setsysproprequest->has_valueint = true;
	//gRequest.caenhv_setsysproprequest->has_valuefloat = true;
	printf("%s %d _CAENHV_SetSysProp %d %d\n", __FILE__, __LINE__, *((int32_t *) Set),  gRequest.caenhv_setsysproprequest->valueint ) ;


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVSetSysPropReplyM *sReply = gReply->caenhv_setsyspropreply;
	if ( _debug_reply ) printf("%s %d ret=%d \n", __FILE__ , __LINE__, sReply->ret );
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

/// for a several boards which are indicated by slotList and it's dimension, slotNum
/// the values for one parameter are returned.
CAENHVLIB_API CAENHVRESULT  _CAENHV_GetBdParam(int handle,
 ushort slotNum, const ushort *slotList, const char *ParName, void *ParValList){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParam_request;
	SCaen__CAENHVGetBdParamRequestM sMessage;
	s_caen__caenhv__get_bd_param_request_m__init( &sMessage );
	gRequest.caenhv_getbdparamrequest = &sMessage;
	gRequest.caenhv_getbdparamrequest->handle = handle;
	gRequest.caenhv_getbdparamrequest->slotnum = slotNum;
	gRequest.caenhv_getbdparamrequest->parname = ( char *) ParName;
	gRequest.caenhv_getbdparamrequest->n_slotlist = slotNum;

	gRequest.caenhv_getbdparamrequest->slotlist = calloc( slotNum, sizeof( int ));
	int i = 0;
	for ( i = 0; i < slotNum; i++ ){
		gRequest.caenhv_getbdparamrequest->slotlist[ i ] = slotList[ i ];
	}

	if(_debug_reply ){
		printf("%s %d _CAENHV_GetBdParam (nb-of-slots=) slotnum= %d\n", __FILE__ , __LINE__ , gRequest.caenhv_getbdparamrequest->slotnum );
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	free( gRequest.caenhv_getbdparamrequest->slotlist );

	SCaen__CAENHVGetBdParamReplyM *sReply = gReply->caenhv_getbdparamreply;
	if ( _debug_reply ) printf("%s %d _CAENHV_GetBdParam ParName= %s format=%d dim=%d\n", __FILE__ , __LINE__,
			ParName, sReply->format, (int) sReply->n_valuearrayfloat );

	// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
	int dim = sReply->n_valuearrayfloat; // all are the same dim and format
	switch( sReply->format ){
	case PARAM_TYPE_NUMERIC: {
		for ( i = 0; i < dim; i++ ){
			((float *)ParValList)[ i ] = sReply->valuearrayfloat[ i ];
		}
		break;
	}
	// case PARAM_TYPE_CHSTATUS:
	case PARAM_TYPE_ONOFF:
	case PARAM_TYPE_BINARY:
	case PARAM_TYPE_BDSTATUS: {
		for ( i = 0; i < dim; i++ ){
			((uint32_t *)ParValList)[ i ] = sReply->valuearrayint[ i ];
		}
		break;
	}
	case PARAM_TYPE_STRING: {
		for ( i = 0; i < dim; i++ ){
			sprintf( ((char **)ParValList)[ i ], sReply->valuearraystring[ i ] );
		}
		break;
	}
	default: {
		// probably an enum
		printf("%s %d _CAENHV_GetBdParamProp unknown type=%d\n", __FILE__ , __LINE__,
				sReply->format );
		return( CAENHV_READERR );
		break;
	}
	}

	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}


/// set a nb of boards in slots (slotlist) for ParName to ParValue (scalar)
CAENHVLIB_API CAENHVRESULT  _CAENHV_SetBdParam(int handle,
 ushort slotNum, const ushort *slotList, const char *ParName, void *ParValue){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SetBdParam_request;
	SCaen__CAENHVSetBdParamRequestM sMessage;
	s_caen__caenhv__set_bd_param_request_m__init( &sMessage );
	gRequest.caenhv_setbdparamrequest = &sMessage;
	gRequest.caenhv_setbdparamrequest->handle = handle;
	gRequest.caenhv_setbdparamrequest->slotnum = slotNum;
	gRequest.caenhv_setbdparamrequest->slotlist = calloc( slotNum, sizeof( int ));


	gRequest.caenhv_setbdparamrequest->n_slotlist = slotNum;
	gRequest.caenhv_setbdparamrequest->parname = (char *) ParName;

	// here we don't know the format of the value, so need to transfer all possible formats as arrays
	// thank you CAEN for such a good API ;-((
	gRequest.caenhv_setbdparamrequest->valuestring = (char *) ParValue;
	gRequest.caenhv_setbdparamrequest->valueint = *((int32_t *) ParValue);
	gRequest.caenhv_setbdparamrequest->valuefloat = *((float *) ParValue);
	int i = 0;
	for ( i = 0; i < slotNum; i++ ){
		gRequest.caenhv_setbdparamrequest->slotlist[ i ] = slotList[ i ];
	}
	if(_debug_request) {
		printf("%s %d ParName= %s slotList[ 0 ]= %d ParValue[ 0 ]=%g \n", __FILE__ , __LINE__,
				ParName, slotList[ 0 ], ((float *) ParValue)[ 0 ] );
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVSetBdParamReplyM *sReply = gReply->caenhv_setbdparamreply;
	if ( _debug_reply )
		printf("%s %d ret=%d \n", __FILE__ , __LINE__, sReply->ret );

	free( gRequest.caenhv_setbdparamrequest->slotlist );

	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

/// get one parameter from one board: either type or mode
/// i.e. ParName="Temp" and "propName="Type" means we have to return the
/// property=Temp data type which is float = CAEN_WRAP_PARAM_TYPE_NUMERIC=0
CAENHVLIB_API CAENHVRESULT  _CAENHV_GetBdParamProp(int handle,
 ushort slot, const char *ParName, const char *PropName, void *retval){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamProp_request;
	SCaen__CAENHVGetBdParamPropRequestM sMessage;
	s_caen__caenhv__get_bd_param_prop_request_m__init( &sMessage );
	gRequest.caenhv_getbdparamproprequest = &sMessage;
	gRequest.caenhv_getbdparamproprequest->handle = handle;
	gRequest.caenhv_getbdparamproprequest->slot = slot;
	gRequest.caenhv_getbdparamproprequest->parname = ( char * ) ParName;
	gRequest.caenhv_getbdparamproprequest->propname = ( char * ) PropName;
	if(_debug_reply){
		printf("%s %d _CAENHV_GetBdParamProp handle= %d slot= %d ParName= %s PropName= %s\n", __FILE__ , __LINE__ ,
				gRequest.caenhv_getbdparamproprequest->handle,
				gRequest.caenhv_getbdparamproprequest->slot,
				ParName, PropName );
		printf("%s %d _CAENHV_GetBdParamProp ParName= %s\n", __FILE__ , __LINE__ , ParName );
	}


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetBdParamPropReplyM *sReply = gReply->caenhv_getbdparampropreply;
	if(_debug_reply){
		printf("%s %d _CAENHV_GetBdParamProp ParName= %s PropName= %s reply type= %d mode= %d \n",
			__FILE__ , __LINE__,
			ParName, PropName, sReply->type, sReply->mode );
	}
	if ( sReply->mode == PARAM_MODE_WRONLY ) return( CAENHV_READERR );

	if ( strcmp( PropName, "Mode" ) == 0 ) {
		*(( int32_t * ) retval) = sReply->mode;
		return( CAENHV_OK );
	}
	if ( strcmp( PropName, "Type" ) == 0 ) {
		*(( int32_t * ) retval) = sReply->type;
		return( CAENHV_OK );
	}

	switch( sReply->type ){
	case PARAM_TYPE_NUMERIC: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*(( float * ) retval) = sReply->valuefloat;
		if ( strcmp( PropName, "Minval" ) == 0 )
			*(( float * ) retval) = sReply->minval;
		if ( strcmp( PropName, "Maxval" ) == 0 )
			*(( float * ) retval) = sReply->maxval;
		if ( strcmp( PropName, "Unit" ) == 0 )
			*(( uint16_t * ) retval) = sReply->unit;
		if ( strcmp( PropName, "Exp" ) == 0 )
			*(( int16_t * ) retval) = sReply->exp;
		break;
	}
	case PARAM_TYPE_ONOFF: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*(( uint16_t * ) retval) = sReply->valueonoff;
		if ( strcmp( PropName, "Onstate" ) == 0 )
			sprintf( ( char * ) retval, sReply->onstate );
		if ( strcmp( PropName, "Offstate" ) == 0 )
			sprintf( ( char * ) retval, sReply->offstate );
		break;
	}
#if 0
	case PARAM_TYPE_CHSTATUS: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*((uint32_t * ) retval) = sReply->valuechstatus;
		break;
	}
#endif
	case PARAM_TYPE_BDSTATUS: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*((uint32_t * ) retval) = sReply->valuebdstatus;
		break;
	}
	case PARAM_TYPE_BINARY: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*((uint32_t * ) retval) = sReply->valuebinary;
		break;
	}
	case PARAM_TYPE_STRING: {
		if ( strcmp( PropName, "Value" ) == 0 )
			sprintf( ( char * ) retval, sReply->valuestring );
		break;
	}
	default: {
		// probably an enum
		printf("%s %d _CAENHV_GetBdParamProp unknown type=%d\n", __FILE__ , __LINE__,
				sReply->type );
		return( CAENHV_READERR );
		break;
	}
	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetBdParamInfo(int handle, ushort slot, char **ParNameList){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_request;
	SCaen__CAENHVGetBdParamInfoRequestM sMessage;
	s_caen__caenhv__get_bd_param_info_request_m__init( &sMessage );
	gRequest.caenhv_getbdparaminforequest = &sMessage;
	gRequest.caenhv_getbdparaminforequest->handle = handle;
	gRequest.caenhv_getbdparaminforequest->slot = slot;
	if(_debug_reply){
		printf("%s %d %s %d\n", __FILE__ , __LINE__ , " _CAENHV_GetBdParamInfo slot= ", gRequest.caenhv_getbdparaminforequest->slot );
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);
	SCaen__CAENHVGetBdParamInfoReplyM *sReply = gReply->caenhv_getbdparaminforeply;
	uint32_t dim = sReply->n_parnamelist;
	uint32_t i = 0;
	if(_debug_reply) {
		for ( i = 0; i < dim; i++ ){
			printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetBdParamInfo_reply: %d. BdParamList= %s\n", __FILE__ , __LINE__, i, sReply->parnamelist[ i ]);
		}
	}

	*ParNameList = calloc( dim * CAEN_PARAM_MAX10 + 2, (unsigned long int) sizeof( char ) );
	// each parameter is 10 chars long, including the \0 at the end.
	// if the actual name is shorter, trailing end is filled with crap (caen=leaves it uninitialized)
	// we are NOT crapping: calloc inits with 0 properly
	// OPC caen server codes this as 10
	int length_p = 0;
	for ( i = 0; i < dim; i++ ){
		sprintf( *ParNameList + length_p, "%s", sReply->parnamelist[ i ] );
		length_p += CAEN_PARAM_MAX10;
	}
	sprintf( *ParNameList + length_p, "\0\0" ); // terminate correctly
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_TestBdPresence(int handle,
 ushort slot, ushort *NrofCh, char **Model, char **Description, ushort *SerNum,
 uchar *FmwRelMin, uchar *FmwRelMax){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_TestBdPresence_request;
	SCaen__CAENHVTestBdPresenceRequestM sMessage;
	s_caen__caenhv__test_bd_presence_request_m__init( &sMessage );
	gRequest.caenhv_testbdpresencerequest = &sMessage;
	gRequest.caenhv_testbdpresencerequest->handle = handle;
	gRequest.caenhv_testbdpresencerequest->slot = slot;
	if(_debug_reply){
		printf("%s %d %d\n", __FILE__ , __LINE__ , gRequest.caenhv_testbdpresencerequest->slot );
	}


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVTestBdPresenceReplyM *sReply = gReply->caenhv_testbdpresencereply;

	*NrofCh = sReply->nrofch;
	*Model = sReply->model;
	*Description = sReply->description;
	*SerNum = (uint16_t ) sReply->sernum;
	*FmwRelMin = (uint8_t) sReply->fmwrelmin;
	*FmwRelMax = (uint8_t) sReply->fmwrelmax;
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetChParamProp(int handle,
 ushort slot, ushort Ch, const char *ParName, const char *PropName, void *retval){
	printf("%s %d %s\n", __FILE__, __LINE__, __FUNCTION__ );

	// branch controllers have the first channel00 configured like a board, but with RO properties only.
	// The HAL knows if a board at a given slot is a branch controller, and will reply with a
	// different set of parameters and properties for channel00 in this case.

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamProp_request;
	SCaen__CAENHVGetChParamPropRequestM sMessage;
	s_caen__caenhv__get_ch_param_prop_request_m__init( &sMessage );
	gRequest.caenhv_getchparamproprequest = &sMessage;
	gRequest.caenhv_getchparamproprequest->handle = handle;
	gRequest.caenhv_getchparamproprequest->slot = slot;
	gRequest.caenhv_getchparamproprequest->channel = Ch;

	printf("%s %d %s\n", __FILE__, __LINE__, __FUNCTION__ );

	// allocate mem
	gRequest.caenhv_getchparamproprequest->parname = (char *) calloc( MAX_PARAM_NAME, sizeof( char ) );
	gRequest.caenhv_getchparamproprequest->propname = (char *) calloc( MAX_PARAM_NAME, sizeof( char ) );

	printf("%s %d %s\n", __FILE__, __LINE__, __FUNCTION__ );

	sprintf( gRequest.caenhv_getchparamproprequest->parname, "%s", ParName );
	sprintf( gRequest.caenhv_getchparamproprequest->propname, "%s", PropName );
	//gRequest.caenhv_getchparamproprequest->parname = ( char * ) ParName;
	//gRequest.caenhv_getchparamproprequest->propname = ( char * ) PropName;

	//if(_debug_reply){
		printf("%s %d _CAENHV_GetChParamProp slot= %d ParName=%s PropName=%s\n", __FILE__ , __LINE__ ,
				gRequest.caenhv_getchparamproprequest->slot,
				gRequest.caenhv_getchparamproprequest->parname,
				gRequest.caenhv_getchparamproprequest->propname) ;
//	}


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetChParamPropReplyM *sReply = gReply->caenhv_getchparampropreply;
	if(_debug_reply){
		printf("%s %d _CAENHV_GetChParamProp ParName= %s PropName= %s reply type= %d mode= %d \n",
			__FILE__ , __LINE__,
			ParName, PropName, sReply->type, sReply->mode );
	}
	if ( sReply->mode == PARAM_MODE_WRONLY ) {
		printf("%s %d _CAENHV_GetChParamProp WRONLY parameter", __FILE__ , __LINE__ );
		return( CAENHV_READERR );
	}

	if ( strcmp( PropName, "Mode" ) == 0 ) {
		*(( int32_t * ) retval) = sReply->mode;
		return( CAENHV_OK );
	}
	if ( strcmp( PropName, "Type" ) == 0 ) {
		*(( int32_t * ) retval) = sReply->type;
		return( CAENHV_OK );
	}

	switch( sReply->type ){
	case PARAM_TYPE_NUMERIC: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*(( float * ) retval) = sReply->valuefloat;
		if ( strcmp( PropName, "Minval" ) == 0 )
			*(( float * ) retval) = sReply->minval;
		if ( strcmp( PropName, "Maxval" ) == 0 )
			*(( float * ) retval) = sReply->maxval;
		if ( strcmp( PropName, "Unit" ) == 0 )
			*(( uint16_t * ) retval) = sReply->unit;
		if ( strcmp( PropName, "Exp" ) == 0 )
			*(( int16_t * ) retval) = sReply->exp;
		break;
	}
	case PARAM_TYPE_ONOFF: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*(( uint16_t * ) retval) = sReply->valueonoff;
		if ( strcmp( PropName, "Onstate" ) == 0 )
			sprintf( ( char * ) retval, sReply->onstate );
		if ( strcmp( PropName, "Offstate" ) == 0 )
			sprintf( ( char * ) retval, sReply->offstate );
		break;
	}
	case PARAM_TYPE_CHSTATUS: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*((int32_t * ) retval) = sReply->valuechstatus;
		break;
	}
#if 0
	case PARAM_TYPE_BDSTATUS: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*((int32_t * ) retval) = sReply->valuebdstatus;
		break;
	}
#endif
	case PARAM_TYPE_BINARY: {
		if ( strcmp( PropName, "Value" ) == 0 )
			*((int32_t * ) retval) = sReply->valuebinary;
		break;
	}
	case PARAM_TYPE_STRING: {
		if ( strcmp( PropName, "Value" ) == 0 )
			sprintf( ( char * ) retval, sReply->valuestring );
		break;
	}
	default: {
		// probably an enum
		printf("%s %d _CAENHV_GetChParamProp unknown type=%d\n", __FILE__ , __LINE__,
				sReply->type );
		return( CAENHV_READERR );
		break;
	}
	}
	printf("%s %d debug \n", __FILE__ , __LINE__);

	s_caen__caen_message__free_unpacked( gReply, NULL );

	printf("%s %d debug \n", __FILE__ , __LINE__);
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_GetChParamInfo(int handle, ushort slot, ushort Ch,
		char **ParNameList, int *ParNumber){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_request;
	SCaen__CAENHVGetChParamInfoRequestM sMessage;
	s_caen__caenhv__get_ch_param_info_request_m__init( &sMessage );
	gRequest.caenhv_getchparaminforequest = &sMessage;
	gRequest.caenhv_getchparaminforequest->handle = handle;
	gRequest.caenhv_getchparaminforequest->slot = slot;
	gRequest.caenhv_getchparaminforequest->channel = Ch;
	if( _debug_reply ){
		printf("%s %d slot=%d channel= %d\n", __FILE__ , __LINE__ ,
				gRequest.caenhv_getchparaminforequest->slot, gRequest.caenhv_getchparaminforequest->channel );
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetChParamInfoReplyM *sReply = gReply->caenhv_getchparaminforeply;
	int dim = sReply->n_parnamelist;
	if( _debug_reply ) {
		int i = 0;
		for ( i = 0; i < dim; i++ ){
			printf( "%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_reply sReply %d params= %s\n",
					__FILE__ , __LINE__, i, sReply->parnamelist[ i ] );
		}
	}

	*ParNameList = ( char *) calloc( (dim + 1) * CAEN_PARAM_MAX10, (unsigned long int) sizeof( char ) );
	// expect one string with fixed size delimited names inside, NOT an array of strings
	// each parameter is 10 chars long, including the \0 at the end.
	// if the actual name is shorter, trailing end is filled with crap (caen=leaves it uninitialized)
	// calloc inits with 0 properly, and we add a \0\0 at the end
	*ParNumber = dim;
	int length_p = 0;
	int i = 0;
	for ( i = 0; i < dim; i++ ){
		sprintf( *ParNameList + length_p, "%s", sReply->parnamelist[ i ] );
		length_p += CAEN_PARAM_MAX10;
	}
	sprintf( *ParNameList + length_p, "\0\0" );


#if 0
	if( _debug_reply ) {
		// printing will fail because of the \0 delimiters
		printf( "%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChParamInfo_reply ParNameList= %s\n",  __FILE__ , __LINE__, i, (char *) *ParNameList );
	}
#endif
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetChName(int handle, ushort slot,
		ushort ChNum, const ushort *ChList, char (*ChNameList)[CAEN_NAME_MAX12]){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_request;
	SCaen__CAENHVGetChNameRequestM sMessage;
	s_caen__caenhv__get_ch_name_request_m__init( &sMessage );
	gRequest.caenhv_getchnamerequest = &sMessage;
	gRequest.caenhv_getchnamerequest->handle = handle;
	gRequest.caenhv_getchnamerequest->slot = slot;
	gRequest.caenhv_getchnamerequest->chnum = ChNum;
	uint32_t dim = ChNum;

	// should be allocated, but probably isnt ;-( just leave it alone
	gRequest.caenhv_getchnamerequest->chlist = (int32_t *) ChList;
	// ChList = calloc_uint16( dim );

	gRequest.caenhv_getchnamerequest->n_chlist = dim;
	if( _debug_reply ){
		printf("%s %d sending message\n", __FILE__ , __LINE__ );
		printf("%s %d slot=%d\n", __FILE__ , __LINE__ , gRequest.caenhv_getchnamerequest->slot );
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetChNameReplyM *sReply = gReply->caenhv_getchnamereply;
	dim = sReply->n_chnamelist; // should be the same
	if( _debug_reply ) {
		printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetChName_reply dim=%d :\n", __FILE__ , __LINE__, dim );
	}
	uint32_t i = 0;
	for ( i = 0; i < dim; i++ ){
		printf( "%s %d %d %s\n", __FILE__, __LINE__, i, sReply->chnamelist[ i ] );
		sprintf( ChNameList[ i ], sReply->chnamelist[ i ] );
	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_SetChName(int handle, ushort slot,
 ushort ChNum, const ushort *ChList, const char *ChName){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SetChName_request;

	SCaen__CAENHVSetChNameRequestM sMessage;
	s_caen__caenhv__set_ch_name_request_m__init( &sMessage );
	gRequest.caenhv_setchnamerequest = &sMessage;
	gRequest.caenhv_setchnamerequest->handle = handle;
	gRequest.caenhv_setchnamerequest->slot = slot;
	gRequest.caenhv_setchnamerequest->chnum = ChNum;
	gRequest.caenhv_setchnamerequest->chlist = ( int32_t *) ChList;
	// it does not make much sense to set several channels to the same name,
	// but implement anyway..
	gRequest.caenhv_setchnamerequest->chname = ( char * ) ChName;
	if(_debug_reply){
		printf("%s %d sending message\n", __FILE__ , __LINE__ );
		printf("%s %d slot=%d\n", __FILE__ , __LINE__ , gRequest.caenhv_setchnamerequest->slot );
	}


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVSetChNameReplyM *sReply = gReply->caenhv_setchnamereply;
	if ( sReply->ret != 0 ) {
		printf("%s %d problem receiving reply message\n", __FILE__ , __LINE__ );
	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

/// it seems like this is called per channel from the OPC caen server, much more performant would be
/// to call it for channel lists, like it is intended
CAENHVLIB_API CAENHVRESULT  _CAENHV_GetChParam(int handle, ushort slot,
 const char *ParName, ushort ChNum, const ushort *ChList, void *ParValList){
	if ( ChNum != 1 ){
		printf("%s %d _CAENHV_GetChParam only for one channel\n", __FILE__, __LINE__ );
		exit(0);
	}
	if(_debug_reply){
		printf("%s %d ParName= %s\n", __FILE__ , __LINE__, ParName );
	}

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetChParam_request;
	SCaen__CAENHVGetChParamRequestM sMessage;
	s_caen__caenhv__get_ch_param_request_m__init( &sMessage );
	gRequest.caenhv_getchparamrequest = &sMessage;
	gRequest.caenhv_getchparamrequest->handle = handle;
	gRequest.caenhv_getchparamrequest->slot = slot;
	gRequest.caenhv_getchparamrequest->channel = ChNum; // nb of channels
	gRequest.caenhv_getchparamrequest->parname = (char *) ParName;
	gRequest.caenhv_getchparamrequest->n_chlist = ChNum;
	int32_t xlist = 0;
	gRequest.caenhv_getchparamrequest->chlist = &xlist;
	gRequest.caenhv_getchparamrequest->chlist[ 0 ] = ChList[ 0 ];

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetChParamReplyM *sReply = gReply->caenhv_getchparamreply;
	int dim = sReply->n_valuearrayint;
	if ( dim != 1 ){
		printf("%s %d _CAENHV_GetChParam only for one channel\n", __FILE__, __LINE__ );
		exit(0);
	}
	int i = 0;
	switch( sReply->format ) {
	case tstr: {
		// in the general case we need a 2dim char** allocation... untested...
		for ( i = 0; i < dim; i++ ){
			sprintf( ((char **) ParValList)[ i ], sReply->valuearraystring[ i ]);
		}
		break;
	}
	case tuint8:
	case tuint16:
	case tuint32:
	case tint8:
	case tint16:
	case tint32: {
		for ( i = 0; i < dim; i++ ){
			((int32_t *) ParValList)[ i ] = sReply->valuearrayint[ i ];
		}
		break;
	}
	case treal: {
		for ( i = 0; i < dim; i++ ){
			((float *) ParValList)[ i ] = sReply->valuearrayfloat[ i ];
		}
		break;
	}
	default: {
		printf("%s %d _CAENHV_GetChParam unknown format=%d \n", __FILE__ , __LINE__,
				sReply->format );
		return( CAENHV_READERR );
		break;
	}
	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}


/// todo digest return codes from HAL/protocol and reflect them correctly as CAEN returns from the lib
CAENHVLIB_API CAENHVRESULT  _CAENHV_SetChParam( int handle, ushort slot,
		const char *ParName, ushort ChNum, const ushort *ChList, void *ParValue){

	if(_debug_reply){
		printf("%s %d _CAENHV_SetChParam V0Set ParName= %s handle= %d slot = %d channel = %d\n", __FILE__, __LINE__, ParName, handle, slot, ChList[ 0 ] );
	}
	//if ( strcmp( ParName, "V0Set") == 0 ) {
		printf("%s %d _CAENHV_SetChParam V0Set= %g\n", __FILE__, __LINE__, *((float *) ParValue) );
	//}
	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_request;
	SCaen__CAENHVSetChParamRequestM sMessage;
	s_caen__caenhv__set_ch_param_request_m__init( &sMessage );
	gRequest.caenhv_setchparamrequest = &sMessage;
	gRequest.caenhv_setchparamrequest->handle = handle;
	gRequest.caenhv_setchparamrequest->slot = slot;
	gRequest.caenhv_setchparamrequest->channel = ChNum;
	gRequest.caenhv_setchparamrequest->parname = ( char * ) ParName;
	gRequest.caenhv_setchparamrequest->n_chlist = ChNum;

	gRequest.caenhv_setchparamrequest->chlist = calloc( ChNum, sizeof( int ));
	int i = 0;
	for ( i = 0; i < ChNum; i++ ){
		gRequest.caenhv_setchparamrequest->chlist[ i ] = ChList[ i ];
	}

	// transmit all 3 formats of the scalar..
	gRequest.caenhv_setchparamrequest->setvalueint = *(( int32_t * ) ParValue);
	gRequest.caenhv_setchparamrequest->setvaluefloat = *(( float *) ParValue);
	gRequest.caenhv_setchparamrequest->setvaluestring = ( char * ) ParValue;

	if(_debug_reply){
		printf("%s %d _CAENHV_SetChParam ParName= %s slot= %d nbChannels= %d [%d %g %s]\n", __FILE__ , __LINE__ ,
				ParName,
				gRequest.caenhv_setchparamrequest->slot,
				gRequest.caenhv_setchparamrequest->channel,
				gRequest.caenhv_setchparamrequest->setvalueint,
				gRequest.caenhv_setchparamrequest->setvaluefloat,
				gRequest.caenhv_setchparamrequest->setvaluestring );
	}
	// printf("%s %d _CAENHV_SetChParam try locking mutex tid= %d\n", __FILE__, __LINE__, (int) pthread_self() );

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	printf("%s %d _CAENHV_SetChParam waiting for answer\n", __FILE__, __LINE__ );
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	free( gRequest.caenhv_setchparamrequest->chlist );

	SCaen__CAENHVSetChParamReplyM *sReply = gReply->caenhv_setchparamreply;
	if(_debug_reply) printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SetChParam_reply ret=%d :\n", __FILE__ , __LINE__, sReply->ret );
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_GetExecCommList(int handle,
 ushort *NumComm, char **CommNameList){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_request;
	SCaen__CAENHVGetExecCommListRequestM sMessage;
	s_caen__caenhv__get_exec_comm_list_request_m__init( &sMessage );
	gRequest.caenhv_getexeccommlistrequest = &sMessage;
	gRequest.caenhv_getexeccommlistrequest->handle = handle;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetExecCommListReplyM *sReply = gReply->caenhv_getexeccommlistreply;
	if(_debug_reply)
	printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_GetExecCommList_reply num=%d \n", __FILE__ , __LINE__, sReply->numcomm );
	uint32_t dim = sReply->n_commnamelist;
	CommNameList =  calloc_charchar( dim, CAEN_EXECCOMM_MAX128 );
	uint32_t i = 0;
	for ( i = 0; i < sReply->n_commnamelist; i++ ){
		sprintf( CommNameList[ i ], sReply->commnamelist[ i ]);
	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT  _CAENHV_ExecComm(int handle, const char *CommName){

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_request;
	SCaen__CAENHVExecCommRequestM sMessage;
	s_caen__caenhv__exec_comm_request_m__init( &sMessage );
	gRequest.caenhv_execcommrequest = &sMessage;
	gRequest.caenhv_execcommrequest->handle = handle;
	gRequest.caenhv_execcommrequest->commname = ( char * ) CommName;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVExecCommReplyM *sReply = gReply->caenhv_execcommreply;
	if(_debug_reply) printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_ExecComm_request ret=%d :\n", __FILE__ , __LINE__, sReply->ret );
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

/// subscribing to system parameters means that whenever the value of any of those
/// parameters changes, then the parameter(s) which have changed values become items in
/// an event which is issued
/// \todo reply codes, what is expected? maybe a string with reply codes like "ok:ok:err:ok" etc
CAENHVLIB_API CAENHVRESULT _CAENHV_SubscribeSystemParams(int handle, short Port, const char *paramNameList,
														unsigned int paramNum ,char *listOfResultCodes){

	if(_debug_reply) {
		printf("%s %d CAENHV_SubscribeSystemParams handle= %d Port= %d\n", __FILE__, __LINE__, handle, Port );
		// handle= 0 Port= 4527
		printf("%s %d CAENHV_SubscribeSystemParams paramNum= %d paramNameList= %s\n", __FILE__, __LINE__, paramNum, paramNameList );
		// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName
	}

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeSystemParams_request;
	SCaen__CAENHVSubscribeSystemParamsRequestM sMessage;
	s_caen__caenhv__subscribe_system_params_request_m__init( &sMessage );
	gRequest.caenhv_subscribesystemparamsrequest = &sMessage;

	gRequest.caenhv_subscribesystemparamsrequest->handle = handle;
	gRequest.caenhv_subscribesystemparamsrequest->port = Port;
	gRequest.caenhv_subscribesystemparamsrequest->paramnum = paramNum;
	gRequest.caenhv_subscribesystemparamsrequest->paramnamelist = (char *) paramNameList;


	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);


	SCaen__CAENHVSubscribeSystemParamsReplyM *sReply = gReply->caenhv_subscribesystemparamsreply;
	listOfResultCodes = sReply->listofresultcodes;

	// in order to get the IP address from the crate we have asked the HAL on the handle.
	// Now we have a crate handle, a port and an IP address
	// we make a connection and get our local socket.
	int localsock;
	printf("%s %d _CAENHV_SubscribeSystemParams sim2 handle= %d Port= %d\n", __FILE__, __LINE__, handle, Port );
	startEventModeSession2( handle, Port, sReply->crateip, &localsock );

	// add the quadruplet : handle, Port, socket, ip for each eventMode connection
	addSimSubscription2( handle, Port, sReply->crateip, localsock );

	// this is probably what is expected: a string with reply codes like "ok:ok:err:ok" etc
	if(_debug_reply) printf("%s %d S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeystemParams_reply rcodes= %s :\n", __FILE__ , __LINE__, listOfResultCodes );
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_SubscribeBoardParams(int handle, short Port, const unsigned short slotIndex,
														const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes){
	if(_debug_reply) {
		printf("%s %d _CAENHV_SubscribeBoardParams handle= %d Port= %d\n", __FILE__, __LINE__, handle, Port );
		// handle= 0 Port= 4527
		printf("%s %d _CAENHV_SubscribeBoardParams paramNum= %d paramNameList= %s\n", __FILE__, __LINE__, paramNum, paramNameList );
		// paramNum= 3 paramNameList= BdStatus:HVMax:Temp
	}

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeBoardParams_request;
	SCaen__CAENHVSubscribeBoardParamsRequestM sMessage;
	s_caen__caenhv__subscribe_board_params_request_m__init( &sMessage );
	gRequest.caenhv_subscribeboardparamsrequest = &sMessage;

	gRequest.caenhv_subscribeboardparamsrequest->handle = handle;
	gRequest.caenhv_subscribeboardparamsrequest->port = Port;
	gRequest.caenhv_subscribeboardparamsrequest->slotindex = slotIndex;
	gRequest.caenhv_subscribeboardparamsrequest->paramnum = paramNum;
	gRequest.caenhv_subscribeboardparamsrequest->paramnamelist = (char *) paramNameList;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVSubscribeBoardParamsReplyM *sReply = gReply->caenhv_subscribeboardparamsreply;
	listOfResultCodes = sReply->listofresultcodes;

	// in order to get the IP address from the crate we have asked the HAL on the handle.
	// Now we have a crate handle, a port and an IP address
	// we make a connection and get our local socket.
	int localsock;
	startEventModeSession2( handle, Port, sReply->crateip, &localsock );

	// add the quadruplet : handle, Port, socket, ip for each eventMode connection
	printf("%s %d _CAENHV_SubscribeBoardParams sim2 handle= %d Port= %d\n", __FILE__, __LINE__, handle, Port );
	addSimSubscription2( handle, Port, sReply->crateip, localsock );

	// this is probably what is expected: a string with reply codes like "ok:ok:err:ok" etc
	if(_debug_reply) printf("%s %d _CAENHV_SubscribeBoardParams rcodes= %s :\n", __FILE__ , __LINE__, listOfResultCodes );
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_SubscribeChannelParams(int handle, short Port, const unsigned short slotIndex,
														const unsigned short chanIndex, const char *paramNameList,
														unsigned int paramNum ,char *listOfResultCodes){
	if(_debug_reply) {
		// handle= 0 Port= 4527
		// paramNum= 16 paramNameList= CPULoad:ClkFreq:FrontPanIn:FrontPanOut:GenSignCfg:HVFanStat:HvPwSM:IPAddr:IPGw:IPNetMsk:MemoryStatus:ModelName:ResFlagCfg:Sessions:SwRelease:SymbolicName
		printf("%s %d _CAENHV_SubscribeChannelParams handle= %d port= %d slot= %d channel= %d paramNum= %d paramNameList= %s\n", __FILE__, __LINE__,
				handle, Port, slotIndex, chanIndex, paramNum, paramNameList );
	}

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_SubscribeChannelParams_request;
	SCaen__CAENHVSubscribeChannelParamsRequestM sMessage;
	s_caen__caenhv__subscribe_channel_params_request_m__init( &sMessage );
	gRequest.caenhv_subscribechannelparamsrequest = &sMessage;

	gRequest.caenhv_subscribechannelparamsrequest->handle = handle;
	gRequest.caenhv_subscribechannelparamsrequest->port = Port;
	gRequest.caenhv_subscribechannelparamsrequest->slotindex = slotIndex;
	gRequest.caenhv_subscribechannelparamsrequest->channelindex = chanIndex;
	gRequest.caenhv_subscribechannelparamsrequest->paramnum = paramNum;
	gRequest.caenhv_subscribechannelparamsrequest->paramnamelist = (char *) paramNameList;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVSubscribeChannelParamsReplyM *sReply = gReply->caenhv_subscribechannelparamsreply;
	listOfResultCodes = sReply->listofresultcodes;

	// in order to get the IP address from the crate we have asked the HAL on the handle.
	// Now we have a crate handle, a port and an IP address
	// we make a connection and get our local socket.
	int localsock;
	startEventModeSession2( handle, Port, sReply->crateip, &localsock );

	// add the quadruplet : handle, Port, socket, ip for each eventMode connection
	// printf("%s %d _CAENHV_SubscribeChannelParams sim2 handle= %d port= %d\n", __FILE__, __LINE__, handle, Port );
	addSimSubscription2( handle, Port, sReply->crateip, localsock );

	// this is probably what is expected: a string with reply codes like "ok:ok:err:ok" etc
	if(_debug_reply) printf("%s %d _CAENHV_SubscribeChannelParams rcodes= %s :\n", __FILE__ , __LINE__, listOfResultCodes );
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_UnSubscribeSystemParams(int handle, short Port, const char *paramNameList,
														unsigned int paramNum ,char *listOfResultCodes){
	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeSystemParams_request;
	SCaen__CAENHVUnSubscribeSystemParamsRequestM sMessage;
	s_caen__caenhv__un_subscribe_system_params_request_m__init( &sMessage );
	gRequest.caenhv_unsubscribesystemparamsrequest = &sMessage;
	gRequest.caenhv_unsubscribesystemparamsrequest->handle = handle;
	gRequest.caenhv_unsubscribesystemparamsrequest->port = Port;
	gRequest.caenhv_unsubscribesystemparamsrequest->paramnamelist = (char *) paramNameList;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	// a string with reply codes like "ok:ok:err:ok" etc
	SCaen__CAENHVUnSubscribeSystemParamsReplyM *sReply = gReply->caenhv_unsubscribesystemparamsreply;
	listOfResultCodes = sReply->listofresultcodes;
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_UnSubscribeBoardParams(int handle, short Port, const unsigned short slotIndex,
														const char *paramNameList, unsigned int paramNum ,char *listOfResultCodes){
	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeBoardParams_request;
	SCaen__CAENHVUnSubscribeBoardParamsRequestM sMessage;
	s_caen__caenhv__un_subscribe_board_params_request_m__init( &sMessage );
	gRequest.caenhv_unsubscribeboardparamsrequest = &sMessage;
	gRequest.caenhv_unsubscribeboardparamsrequest->handle = handle;
	gRequest.caenhv_unsubscribeboardparamsrequest->port = Port;
	gRequest.caenhv_unsubscribeboardparamsrequest->paramnamelist = (char *) paramNameList;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	// a string with reply codes like "ok:ok:err:ok" etc
	SCaen__CAENHVUnSubscribeBoardParamsReplyM *sReply = gReply->caenhv_unsubscribeboardparamsreply;
	listOfResultCodes = sReply->listofresultcodes;
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API CAENHVRESULT _CAENHV_UnSubscribeChannelParams(int handle, short Port, const unsigned short slotIndex,
															const unsigned short chanIndex, const char *paramNameList,
															unsigned int paramNum ,char *listOfResultCodes){
	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_UnSubscribeChannelParams_request;
	SCaen__CAENHVUnSubscribeChannelParamsRequestM sMessage;
	s_caen__caenhv__un_subscribe_channel_params_request_m__init( &sMessage );
	gRequest.caenhv_unsubscribechannelparamsrequest = &sMessage;
	gRequest.caenhv_unsubscribechannelparamsrequest->handle = handle;
	gRequest.caenhv_unsubscribechannelparamsrequest->port = Port;
	gRequest.caenhv_unsubscribechannelparamsrequest->paramnamelist = (char *) paramNameList;

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	// a string with reply codes like "ok:ok:err:ok" etc
	SCaen__CAENHVUnSubscribeChannelParamsReplyM *sReply = gReply->caenhv_unsubscribechannelparamsreply;
	listOfResultCodes = sReply->listofresultcodes;
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}

CAENHVLIB_API char *_CAENHV_GetError(int handle){
	return( CAENHV_OK );
}

#ifdef WIN32
CAENHVLIB_API CAENHVRESULT CAENHV_GetEventData(SOCKET sck, CAENHV_SYSTEMSTATUS_t *SysStatus,
												CAENHVEVENT_TYPE_t **EventData, unsigned int *DataNumber){
	RECORD( (char *)__FILE__, __LINE__ );
	return( CAENHV_OK );
}
#else



void keepalivePeriodReset( void ){
	gettimeofday( &keepalive_tv0, &keepalive_tz );
}

bool keepalivePeriodReached( void ){
	gettimeofday( &keepalive_tv1, &keepalive_tz );
	double delta = (double) ( keepalive_tv1.tv_sec - keepalive_tv0.tv_sec );
	if ( delta > 30 ) {
		keepalivePeriodReset();
		return ( true );
	} else {
		return( false );
	}
}

/**
 * we return event items of all data which has changed. If there are more than 10k items do some buffering.
 */
CAENHVLIB_API CAENHVRESULT _CAENHV_GetEventData( int sck, CAENHV_SYSTEMSTATUS_t *SysStatus,
												CAENHVEVENT_TYPE_t **EEventData, unsigned int *DataNumber){

	// how do we know which crate has to reply on that (remote) socket ?
	int crateHandle = convertSocketToHandle2( sck );
	if ( crateHandle < 0 ) {
		printf("%s %d _CAENHV_GetEventData sim3 sck= %d could not find a crate handle, take next available =========\n", __FILE__, __LINE__, sck );
		findAndAssignNextFreeHandle( sck );
		crateHandle = convertSocketToHandle2( sck );
		printf("%s %d _CAENHV_GetEventData sim3 found sck= %d  crate handle= %d =========\n", __FILE__, __LINE__, sck, crateHandle );
		showSimSubscriptions2();
		// pthread_mutex_unlock(&evtModemutex);
	}

	SCaen__CaenMessage gRequest;
	s_caen__caen_message__init( &gRequest );
	gRequest.type = S_CAEN__MESSAGE_TYPE__CAENHV_GetEventData_request;
	SCaen__CAENHVGetEventDataRequestM sMessage;
	s_caen__caenhv__get_event_data_request_m__init( &sMessage );
	gRequest.caenhv_geteventdatarequest = &sMessage;
	gRequest.caenhv_geteventdatarequest->socket = sck;	// thats the OPC client socket for subscription
	gRequest.caenhv_geteventdatarequest->handle = crateHandle;

	// make sure the rest of the comm API calls have a chance to acquire the mutex, since
	// the OPC server is constantly waiting for this API call in synchronization. For this we
	// must spend some time outside this process
	{
		struct timespec ts, tr;
		ts.tv_sec = 0;
		ts.tv_nsec = 100000000; // 100ms
		nanosleep(&ts, &tr);
	}

	while( pthread_mutex_trylock( &gmutex ) ){}
	sendCZMQ_caen( gRequest, socket_req );
	SCaen__CaenMessage *gReply;
	receiveCZMQ_caen( socket_req, &gReply );
	pthread_mutex_unlock(&gmutex);

	SCaen__CAENHVGetEventDataReplyM *sReply = gReply->caenhv_geteventdatareply;
	int i = 0;

	// fill it in
	uint32_t nbOfEventItems = sReply->nbitems;
	SysStatus->System = sReply->statussystem;
	for ( i = 0; i < MAX_NB_BOARDS_PER_CRATE; i++ ) {
		SysStatus->Board[ i ] = sReply->statusboard[ i ]; // 16 board status
	}
	*DataNumber = nbOfEventItems;
	if(_debug_reply) {
		printf("%s %d SCaen__CAENHVGetEventDataReply sim3  sck= %d crateHandle= %d nbOfEventItems=%d\n", __FILE__ , __LINE__, sck, crateHandle, nbOfEventItems );
	}

	/**
	 * throw out keepalive events if there is nothing to say, periodically. Often this is
	 * not needed because the temperature
	 * changes by little amounts randomly all the time. These litte changes can be
	 * switched off as well, using the intrinsic update flag (command line).
	 * Then in fact we should produce keepalive events.
	 */
	if (( nbOfEventItems == 0 ) && keepalivePeriodReached() ){
		nbOfEventItems = 1;
		*DataNumber = nbOfEventItems;
		*EEventData = (CAENHVEVENT_TYPE_t *) calloc ( sizeof( CAENHVEVENT_TYPE_t ), nbOfEventItems );
		// CAENHVEVENT_TYPE_t EEventData[ nbOfEventItems ];
		(*EEventData)[ 0 ].BoardIndex = 0;
		(*EEventData)[ 0 ].ChannelIndex = 0;
		sprintf( (*EEventData)[ i ].ItemID, "keepalive" ); // max [20]
		(*EEventData)[ 0 ].SystemHandle = 0; // sReply->systemhandle[ i ];
		(*EEventData)[ 0 ].Type = KEEPALIVE; // PARAMETER=0, ALARM=1, KEEPALIVE=2
		(*EEventData)[ 0 ].Value.IntValue = 0;

		keepalivePeriodReset();

		return( CAENHV_OK );
	} else {
		keepalivePeriodReset();
	}

	*EEventData = (CAENHVEVENT_TYPE_t *) calloc ( sizeof( CAENHVEVENT_TYPE_t ), nbOfEventItems );

	for ( i = 0; i < nbOfEventItems; i++ ){
		(*EEventData)[ i ].BoardIndex = sReply->boardindex[ i ];
		(*EEventData)[ i ].ChannelIndex = sReply->channelindex[ i ];
		sprintf( (*EEventData)[ i ].ItemID, sReply->itemid[ i ] ); // max [20]
		(*EEventData)[ i ].SystemHandle = sReply->systemhandle[ i ];
		(*EEventData)[ i ].Type = sReply->caenidtype[ i ]; // PARAMETER=0, ALARM=1, KEEPALIVE=2

		// typedef enum { tstr, tint32, treal, tint16, tint8, tuint32, tuint16, tuint8 } HAL_DATA_TYPE_t;
		switch ( sReply->haltype[ i ]){
		case treal : (*EEventData)[ i ].Value.FloatValue = sReply->floatvalue[ i ]; break;
		case tstr : sprintf( (*EEventData)[ i ].Value.StringValue, sReply->stringvalue[ i ] ); break; // max 1024
		default: (*EEventData)[ i ].Value.IntValue = sReply->intvalue[ i ]; break;
		}
#if 0
		// debug: look for a specific item
		if ( strcmp( sReply->itemid[ i ], "V0Set") == 0 ) {
			printf("%s %d SCaen__CAENHVGetEventDataReply (float)= %g (int)= %d (type)= %d ID= %s\n", __FILE__, __LINE__ ,
					(*EEventData)[ i ].Value.FloatValue,
					(*EEventData)[ i ].Value.IntValue,
					(*EEventData)[ i ].Type,
					(*EEventData)[ i ].ItemID
					);
		}

#endif

	}
	s_caen__caen_message__free_unpacked( gReply, NULL );
	return( CAENHV_OK );
}
#endif

CAENHVLIB_API CAENHVRESULT _CAENHV_FreeEventData(CAENHVEVENT_TYPE_t **REventData ){
	free ( *REventData );
	return( CAENHV_OK );
}


CAENHVLIB_API CAENHVRESULT _CAENHV_Free(void *arg){
	// printf("%s %d\n",__FILE__, __LINE__ );
	free( arg );
	return( CAENHV_OK );
}




simulation Pilot UI
===================

michael.ludwig@cern.ch


C++ simulation pilot user interface:

talks to simulationHAL via the pilot socket

performs runtime-actions specific to simulation, which are OUT OF SCOPE of the OPC server
like i.e.:
communication timing behaviour
set noise levels
provoke trips
artificial slow down/speed up


usage: ./simulationPilotUI <command> [ -help | -host_engine <h1> | -port_engine <p1> ]
  defaults: h1=127.0.0.1 p1=8881
  commands: 
      getCrateHandles
      setVSEL <crate handle> <vselvalue={0|1}>
      setISEL <crate handle> <iselvalue={0|1}>
      getChannelLoad <crate handle> <slot> <channel>
      setChannelLoad <crate handle> <slot> <channel> <resistancevalue/Ohm> <capacitancevalue/uFarad>
      setAllChannelsLoad <crate handle> <resistancevalue/Ohm> <capacitancevalue/uFarad>
      rampUpAllChannels <crate handle> <V0Set> <I0Set> <RUp> <RDwn>// by serial user interaction Pw==1, not using VSEL 
      switchOffAllChannels <crate handle> // by serial user interaction Pw==0, not using VSEL 
      getUpdateDelay <crate handle> // get update delay for this crate 
      setUpdateDelay <crate handle> <delay_in_ms>  // set a new update delay for this crate: stress tests 
      tripChannelByVoltage <crate handle> <slot> <channel>  // read out load and current-limit, trip by raising voltage until current-limit is exceeded
      tripChannelByCurrent <crate handle> <slot> <channel>  // read out current and current-limit, trip by lowering current limit below current
      tripChannelByLoad <crate handle> <slot> <channel>  // read current-limit and voltage, trip by lowering load until current-limit is exceeded
      getConfig  // read config details from s.engine, crate handle 0
      getHealth  // read out s.engine health parameters

	 
make sure the server ports are open:
sudo firewall-cmd --zone=public --permanent --add-port=4840/tcp
sudo firewall-cmd --zone=public --permanent --add-port=4841/tcp
sudo firewall-cmd --zone=public --permanent --add-port=8880/tcp
sudo firewall-cmd --zone=public --permanent --add-port=8881/tcp
sudo firewall-cmd --reload

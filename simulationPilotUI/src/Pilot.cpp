/*
 * Pilot.cpp
 *
 *  Created on: Feb 9, 2017
 *      Author: mludwig
 */


#include "Pilot.h"

namespace Pilot {

/// \todo maybe also RESET as pilot
Pilot::Pilot( string engine_ip, uint32_t engine_port ){
	_engineip = engine_ip;
	_engineport = engine_port;
	// create a socket... set up connection ready to go
	ostringstream convert;
	convert << engine_port;
	_engine_connection = "tcp://" + engine_ip + ":" + convert.str();
	cout << " connecting PILOT to simulationHAL server= " << _engine_connection << endl;
	_socket_req = zsock_new_req( _engine_connection.c_str() ); // client, send-recv pattern
	assert( _socket_req );
	cout << "PILOT connected to simulation engine running on " << _engine_connection << endl;
}

Pilot::~Pilot() {
	cout << "disconnecting..." << endl;
	zsock_destroy( &_socket_req );
}


void Pilot::setChannelLoad( int handle, int slot, int channel, float resistance, float capacitance ){

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setLoad_request;
	SCaen__PilotSetLoadRequestM sMessage;
	s_caen__pilot__set_load_request_m__init( &sMessage );
	gRequest.pilotsetloadrequest  = &sMessage;
	gRequest.pilotsetloadrequest->cratehandle = handle;
	gRequest.pilotsetloadrequest->slot = slot;
	gRequest.pilotsetloadrequest->channel = channel;
	gRequest.pilotsetloadrequest->resistance = resistance;
	gRequest.pilotsetloadrequest->capacitance = capacitance;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_setLoad_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " setChannelLoad: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
}

// we could do a loop over all channels and send one separate message for each of them:
// this would be more realistic. Here we simulate all settings "as fast as possible"
// and then a manual user intervention "Pw==1" to start ramping, but we do not use VSEL
void Pilot::setAllChannelsLoad( int handle, float resistance, float capacitance ){

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setAllLoads_request;
	SCaen__PilotSetAllLoadsRequestM sMessage;
	s_caen__pilot__set_all_loads_request_m__init( &sMessage );
	gRequest.pilotsetallloadsrequest  = &sMessage;
	gRequest.pilotsetallloadsrequest->cratehandle = handle;
	gRequest.pilotsetallloadsrequest->resistance = resistance;
	gRequest.pilotsetallloadsrequest->capacitance = capacitance;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_setAllLoads_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " setAllChannelLoads: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
}

void Pilot::rampUpAllChannels( int handle, float v0set, float i0set, float rup, float rdown ){

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_rampAll_request;
	SCaen__PilotRampAllRequestM sMessage;
	s_caen__pilot__ramp_all_request_m__init( &sMessage );
	gRequest.pilotrampallrequest  = &sMessage;
	gRequest.pilotrampallrequest->cratehandle = handle;
	gRequest.pilotrampallrequest->v0set = v0set;
	gRequest.pilotrampallrequest->i0set = i0set;
	gRequest.pilotrampallrequest->rup = rup;
	gRequest.pilotrampallrequest->rdown = rdown;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_rampAll_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " rampUpAllChannels: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
}

void Pilot::rampUpAllOnBoardChannels( int handle, int slot, float v0set, float i0set, float rup, float rdown ){

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_rampAllOnBoard_request;
	SCaen__PilotRampAllOnBoardRequestM sMessage;
	s_caen__pilot__ramp_all_on_board_request_m__init( &sMessage );
	gRequest.pilotrampallonboardrequest  = &sMessage;
	gRequest.pilotrampallonboardrequest->cratehandle = handle;
	gRequest.pilotrampallonboardrequest->slot= slot;
	gRequest.pilotrampallonboardrequest->v0set = v0set;
	gRequest.pilotrampallonboardrequest->i0set = i0set;
	gRequest.pilotrampallonboardrequest->rup = rup;
	gRequest.pilotrampallonboardrequest->rdown = rdown;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_rampAllOnBoard_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " rampUpAllOnBoardChannels: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
}



void Pilot::switchPwAllChannels( int handle, int sw ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_switchPwAll_request;
	SCaen__PilotSwitchPwAllRequestM sMessage;
	s_caen__pilot__switch_pw_all_request_m__init( &sMessage );
	gRequest.pilotswitchpwallrequest  = &sMessage;
	gRequest.pilotswitchpwallrequest->cratehandle = handle;
	gRequest.pilotswitchpwallrequest->sw = sw;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_switchPwAll_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " switchOffAllChannels: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
}

void Pilot::getChannelLoad( int handle, int slot, int channel, float *resistance, float *capacitance ){

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_getLoad_request;
	SCaen__PilotGetLoadRequestM sMessage;
	s_caen__pilot__get_load_request_m__init( &sMessage );
	gRequest.pilotgetloadrequest  = &sMessage;
	gRequest.pilotgetloadrequest->cratehandle = handle;
	gRequest.pilotgetloadrequest->slot = slot;
	gRequest.pilotgetloadrequest->channel = channel;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_getLoad_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getChannelLoad: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	*resistance = gReply.pilotgetloadreply->resistance;
	*capacitance = gReply.pilotgetloadreply->capacitance;
#if 0
	cout << __FILE__ << " " << __LINE__ <<  " getChannelLoad handle= " << handle
			<< " slot= " << slot
			<< " channel= " << channel
			<< " resistance= " << gReply.pilotgetloadreply->resistance
			<< " capacitance= " << gReply.pilotgetloadreply->capacitance << endl;
#endif
}

void Pilot::tripChannelByVoltage( int handle, int slot, int channel ){

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_tripChannelByVoltage_request;
	SCaen__PilotTripChannelByVoltageRequestM sMessage;
	s_caen__pilot__trip_channel_by_voltage_request_m__init( &sMessage );
	gRequest.pilottripchannelbyvoltagerequest  = &sMessage;
	gRequest.pilottripchannelbyvoltagerequest->cratehandle = handle;
	gRequest.pilottripchannelbyvoltagerequest->slot = slot;
	gRequest.pilottripchannelbyvoltagerequest->channel = channel;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_tripChannelByVoltage_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " tripChannelByVoltage: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ <<  " tripChannelByVoltage handle= " << handle
			<< " slot= " << slot
			<< " channel= " << channel
			<< " resistance= " << gReply.pilotgetloadreply->resistance
			<< " capacitance= " << gReply.pilotgetloadreply->capacitance << endl;
}

void Pilot::tripChannelByCurrent( int handle, int slot, int channel ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_tripChannelByCurrent_request;
	SCaen__PilotTripChannelByCurrentRequestM sMessage;
	s_caen__pilot__trip_channel_by_current_request_m__init( &sMessage );
	gRequest.pilottripchannelbycurrentrequest  = &sMessage;
	gRequest.pilottripchannelbycurrentrequest->cratehandle = handle;
	gRequest.pilottripchannelbycurrentrequest->slot = slot;
	gRequest.pilottripchannelbycurrentrequest->channel = channel;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_tripChannelByCurrent_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " tripChannelByCurrent: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ <<  " tripChannelByCurrent handle= " << handle
			<< " slot= " << slot
			<< " channel= " << channel << endl;
}

void Pilot::tripChannelByLoad( int handle, int slot, int channel ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_tripChannelByLoad_request;
	SCaen__PilotTripChannelByLoadRequestM sMessage;
	s_caen__pilot__trip_channel_by_load_request_m__init( &sMessage );
	gRequest.pilottripchannelbyloadrequest  = &sMessage;
	gRequest.pilottripchannelbyloadrequest->cratehandle = handle;
	gRequest.pilottripchannelbyloadrequest->slot = slot;
	gRequest.pilottripchannelbyloadrequest->channel = channel;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_tripChannelByLoad_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " tripChannelByLoad: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ <<  " tripChannelByLoad handle= " << handle
			<< " slot= " << slot
			<< " channel= " << channel << endl;
}

void Pilot::getUpdateDelay( int handle, float *delay ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_getUpdateDelay_request;
	SCaen__PilotGetUpdateDelayRequestM sMessage;
	s_caen__pilot__get_update_delay_request_m__init( &sMessage );
	gRequest.pilotgetupdatedelayrequest  = &sMessage;
	gRequest.pilotgetupdatedelayrequest->cratehandle = handle;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_getUpdateDelay_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getUpdateDelay: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	*delay = gReply.pilotgetupdatedelayreply->updatedelay;
	cout << __FILE__ << " " << __LINE__ <<  " getUpdateDelay handle= " << handle
			<< " delay= " << *delay << endl;
}

void Pilot::setUpdateDelay( int handle, float delay ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setUpdateDelay_request;
	SCaen__PilotSetUpdateDelayRequestM sMessage;
	s_caen__pilot__set_update_delay_request_m__init( &sMessage );
	gRequest.pilotsetupdatedelayrequest  = &sMessage;
	gRequest.pilotsetupdatedelayrequest->cratehandle = handle;
	gRequest.pilotsetupdatedelayrequest->updatedelay = delay;

	_sendCZMQ( gRequest, _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );
	cout << __FILE__ << " " << __LINE__ << endl;

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_setUpdateDelay_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " setUpdateDelay: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ <<  " getUpdateDelay handle= " << handle
			<< " pret= " << gReply.pilotsetupdatedelayreply->pret << endl;
}

void Pilot::setVSEL( int handle, int vsel ) {
	cout << __FILE__ << " " << __LINE__ << endl;

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setCrateVSEL_request;
	SCaen__PilotSetCrateVSELRequestM sMessage;
	s_caen__pilot__set_crate_vsel_request_m__init( &sMessage );
	gRequest.pilotsetcratevselrequest  = &sMessage;
	gRequest.pilotsetcratevselrequest->cratehandle = handle;
	gRequest.pilotsetcratevselrequest->vsel = vsel;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_setCrateVSEL_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " setVSEL: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ << " type= " << gReply.type << endl;
	cout << __FILE__ << " " << __LINE__ << " crate= " << gReply.pilotsetcratevselreply->cratehandle << endl;
}
void Pilot::setISEL( int handle, int isel ) {
	cout << __FILE__ << " " << __LINE__ << endl;

	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setCrateISEL_request;
	SCaen__PilotSetCrateISELRequestM sMessage;
	s_caen__pilot__set_crate_isel_request_m__init( &sMessage );
	gRequest.pilotsetcrateiselrequest  = &sMessage;
	gRequest.pilotsetcrateiselrequest->cratehandle = handle;
	gRequest.pilotsetcrateiselrequest->isel = isel;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_setCrateISEL_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " setISEL: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ << " type= " << gReply.type << endl;
	cout << __FILE__ << " " << __LINE__ << " crate= " << gReply.pilotsetcrateiselreply->cratehandle << endl;
}


void Pilot::getCrateHandles( vector<int32_t> *handlesv, vector<string> *IPsv) {
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_getCrateHandles_request;
	SCaen__PilotGetCrateHandlesRequestM sMessage;
	s_caen__pilot__get_crate_handles_request_m__init( &sMessage );
	gRequest.pilotgetcratehandlesrequest  = &sMessage;
	gRequest.pilotgetcratehandlesrequest->engineip = ( char *) _engineip.c_str();

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_getCrateHandles_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getCrateHandles: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}

	cout << __FILE__ << " " << __LINE__ << " type= " << gReply.type << endl;
	uint32_t dim = gReply.pilotgetcratehandlesreply->n_handles;
	cout << __FILE__ << " " << __LINE__ << " dim= " << dim << endl;
	for ( uint32_t i = 0; i < dim; i++ ){
		_crateHandlesv.push_back( gReply.pilotgetcratehandlesreply->handles[ i ]);

		//		cout << __FILE__ << " " << __LINE__ << i << endl;
		//	cout << __FILE__ << " " << __LINE__ << " ips= " << gReply.pilotgetcratehandlesreply->ips[ i ] << endl;
		/// \todo pilot ip nbs: does not work...
		// _crateIPsv.push_back( gReply.pilotgetcratehandlesreply->ips[ i ]);
		_crateIPsv.push_back( " " );
	}
	*handlesv = _crateHandlesv;
	*IPsv = _crateIPsv;
}

void Pilot::setDebug( int debug ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_setDebuglevel_request;
	SCaen__PilotSetDebuglevelRequestM sMessage;
	s_caen__pilot__set_debuglevel_request_m__init( &sMessage );
	gRequest.pilotsetdebuglevelrequest  = &sMessage;
	gRequest.pilotsetdebuglevelrequest->debuglevel = debug;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_setDebuglevel_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getConfig: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ << " setDebug: " << debug << endl;
}

void Pilot::getConfig( int handle ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage gRequest;
	s_caen__pilot_message__init( &gRequest );

	gRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_getConfiguration_request;
	SCaen__PilotGetConfigurationRequestM sMessage;
	s_caen__pilot__get_configuration_request_m__init( &sMessage );
	gRequest.pilotgetconfigurationrequest  = &sMessage;
	gRequest.pilotgetconfigurationrequest->cratehandle = handle;

	_sendCZMQ( gRequest, _socket_req );
	SCaen__PilotMessage gReply = _receiveCZMQ( _socket_req );

	if ( gReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_getConfiguration_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getConfig: received wrong/unknown message type " << gReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__ << " getConfig: " << endl
			<< "   handle= " << handle
			<< "   nbCrates= " << gReply.pilotgetconfigurationreply->nbcrates<< endl
			<< "   config file= " << gReply.pilotgetconfigurationreply->configfile<< endl
			<< "   engine host= " << gReply.pilotgetconfigurationreply->enginehn<< endl
			<< "   glue   host= " << gReply.pilotgetconfigurationreply->gluehn<< endl
			<< "   glue   version= " << gReply.pilotgetconfigurationreply->glueversion<< endl
			<< "   engine version= " << gReply.pilotgetconfigurationreply->engineversion<< endl;

}

void Pilot::getHealthEngine( int handle ){
	// fill in generic message, set specific message and send to HAL
	SCaen__PilotMessage pRequest;
	s_caen__pilot_message__init( &pRequest );

	pRequest.type = S_CAEN__PILOT_MESSAGE_TYPE__pilot_getHealthEngine_request;
	SCaen__PilotGetHealthEngineRequestM sMessage;
	s_caen__pilot__get_health_engine_request_m__init( &sMessage );
	pRequest.pilotgethealthenginerequest  = &sMessage;
	pRequest.pilotgethealthenginerequest->cratehandle = handle;

	_sendCZMQ( pRequest, _socket_req );
	SCaen__PilotMessage pReply = _receiveCZMQ( _socket_req );
	if ( pReply.type != S_CAEN__PILOT_MESSAGE_TYPE__pilot_getHealthEngine_reply ){
		std::ostringstream os;
		os << __FILE__ << " " << __LINE__ << " getConfig: received wrong/unknown message type " << pReply.type << endl;
		throw std::runtime_error( os.str() );
	}
	cout << __FILE__ << " " << __LINE__
			<<  " getHealthEngine nbcrates= " << pReply.pilotgethealthenginereply->nbcrates
			<<  " nbboards= " << pReply.pilotgethealthenginereply->nbboards
			<<  " nbchannels= " << pReply.pilotgethealthenginereply->nbchannels<< endl
			<<  " 	nbchannelsrampingup= " << pReply.pilotgethealthenginereply->nbchannelsrampingup<< endl
			<<  " 	nbchannelsrampingdwn= " << pReply.pilotgethealthenginereply->nbchannelsrampingdwn<< endl
			<<  " 	nbchannelstripped= " << pReply.pilotgethealthenginereply->nbchannelstripped<< endl
			<<  " 	nbchannelson= " << pReply.pilotgethealthenginereply->nbchannelson<< endl
			<<  " 	eventDelay= " << pReply.pilotgethealthenginereply->eventdelay << " ms"<< endl
			<<  " 	updateDelay= " << pReply.pilotgethealthenginereply->updatedelay << " ms"<< endl
			<< endl;
}

void Pilot::_sendCZMQ( SCaen__PilotMessage gRequest, zsock_t *socket_req ) {
	uint32_t len = s_caen__pilot_message__get_packed_size( &gRequest );
	bool _debug = false;
	if(_debug) printf( "%s %d generic message packed len= %d\n", __FILE__, __LINE__, len );

	uint8_t buf_uint8[ len ];
	memset( buf_uint8, 0, len );
	size_t sz = s_caen__pilot_message__pack( &gRequest, buf_uint8 );
	if(_debug) cout << __FILE__ << " " << __LINE__ << " generic message packed sz= " << sz << endl;

	// convert uint8 array into char* and send
	int char_sz = _ar_uint8_to_char_sz( sz );
	char buf_char[ char_sz ];
	_ar_uint8_to_char( buf_uint8, buf_char, sz );
	if(_debug) printf( "%s %d sending %s len= %d\n", __FILE__, __LINE__, buf_char, len );
	zstr_send ( _socket_req, (const char *) buf_char );
	if(_debug) cout << __FILE__ << " " << __LINE__ << " ...sent OK" << endl;
}

SCaen__PilotMessage Pilot::_receiveCZMQ( zsock_t *socket_rep ) {
	bool _debug = false;
	char *reply;
	if(_debug) cout << __FILE__ << " " << __LINE__ << " ... waiting for reply ..." << endl;
	reply = zstr_recv ( _socket_req );
	int len = strlen( reply );

	if(_debug) cout << __FILE__ << " " << __LINE__ << " received reply " << reply << " len= " << len << endl;

	// convert it to array uint8
	int uint8_sz = _char_to_ar_uint8_sz( len );
	uint8_t buf_uint8[ uint8_sz ];
	_char_to_ar_uint8( reply, buf_uint8 );
	zstr_free( &reply );

	if(_debug) cout << __FILE__ << " " << __LINE__ << " got a reply, unpacking generic message uint8_sz= " << uint8_sz << endl;

	SCaen__PilotMessage *gReply = s_caen__pilot_message__unpack( NULL, uint8_sz, buf_uint8 );

	if(_debug) cout << __FILE__ << " " << __LINE__ << " reply generic message type= " << gReply->type << endl;
	if ( gReply == NULL)  {
		cout << __FILE__ << " " << __LINE__ << "error unpacking generic message" << endl;
		exit(1);
	}
	return( *gReply ); // it is allocated
}
// convert an array of uint8 to a string = char *
// each number takes 2 characters plus a blankspace
// nb is the number of elements in *in
// buf has to be allocated to 3 * nb + 2, therefore call the _sz before
// returns size of char, but not really needed
int Pilot::_ar_uint8_to_char_sz( size_t nb ){
	return( 3 * nb ); // 2 chars plus blank + 2 ending 0
}
void Pilot::_ar_uint8_to_char( uint8_t *in, char *buf, size_t nb ){
	uint32_t len2 = _ar_uint8_to_char_sz( nb );
	memset( buf, 0, len2 );
	char cnum[ 3 ];
	uint32_t i = 0;
	for ( i = 0; i < nb; i++){
		sprintf( cnum, "%2x ", in[ i ]); // always 2 dgits
		strcat ( buf, cnum );
	}
}

// convert a string = char * to an array of uint8
// each number takes 2 characters plus a blankspace "FF "
// buf has to be allocated, call by ref
// the number of uint : call _sz
int Pilot::_char_to_ar_uint8_sz( size_t nb ){
	return( nb / 3 );
}
void Pilot::_char_to_ar_uint8( char *in, uint8_t *buf ){
	uint32_t len = strlen( in );
	uint32_t nb = _char_to_ar_uint8_sz( len );
	memset( buf, 0, nb );
	char delimiter[] = " ";
	int i = 0;
	char *sub = strtok ( in, delimiter);
	while ( sub != NULL)
	{
		int d = 0;
		sscanf( sub, "%x", &d );
		buf[ i++ ] = d;
		sub = strtok ( NULL, delimiter );
	}
}



} /* namespace Pilot */

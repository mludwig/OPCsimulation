/*
 * Pilot.h
 *
 *  Created on: Feb 9, 2017
 *      Author: mludwig
 */

#ifndef SRC_PILOT_H_
#define SRC_PILOT_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>

#include <assert.h>
#include <czmq.h>
#include <google/protobuf-c/protobuf-c.h>
#include <libxml2/libxml/xmlreader.h>

#include "../../simulationOPCglue/protocolBuffers-c/simulationGlueCaen.pb-c.h"

using namespace std;

namespace Pilot {

class Pilot {
public:
	Pilot( string engineip, uint32_t engine_port );
	virtual ~Pilot();

	void getCrateHandles( vector<int32_t> *handlesv, vector<string> *IPsv);
	void setVSEL( int handle, int vsel );
	void setISEL( int handle, int vsel );
	void setChannelLoad( int handle, int slot, int channel, float resistance, float capacitance );
	void getChannelLoad( int handle, int slot, int channel, float *resistance, float *capacitance );
	void setAllChannelsLoad( int handle, float resistance, float capacitance );
	void rampUpAllChannels( int handle, float v0set, float i0set, float rup, float rdown );
	void rampUpAllOnBoardChannels( int handle, int iboard, float v0set, float i0set, float rup, float rdown );
	void rampUpAllChannelsBC( int handle, float v0set, float i0set, float rup, float rdown );
	void switchPwAllChannels( int handle, int sw );
	void getUpdateDelay( int handle, float *delay );
	void setUpdateDelay( int handle, float delay );
	void tripChannelByVoltage( int handle, int slot, int channel );
	void tripChannelByCurrent( int handle, int slot, int channel );
	void tripChannelByLoad( int handle, int slot, int channel );
	void getConfig( int handle );
	void getHealthEngine( int handle );
	void setDebug( int debug );

private:
	string _engineip;
	uint32_t _engineport;
	string _engine_connection;
	zsock_t *_socket_req;
	vector<int32_t> _crateHandlesv;
	vector<string> _crateIPsv;

	void _sendCZMQ( SCaen__PilotMessage gRequest, zsock_t *socket_req );
	SCaen__PilotMessage _receiveCZMQ( zsock_t *socket_rep );
	int _ar_uint8_to_char_sz( size_t nb );
	void _ar_uint8_to_char( uint8_t *in, char *buf, size_t nb );
	int _char_to_ar_uint8_sz( size_t nb );
	void _char_to_ar_uint8( char *in, uint8_t *buf );



};

} /* namespace Pilot */

#endif /* SRC_PILOT_H_ */

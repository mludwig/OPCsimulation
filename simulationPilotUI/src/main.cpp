/// simulationPilotUI main.cpp
/// a pilot client UI, sending pilot message requests to the simulation engine
/// and rceiving pilot message replies. The pilot interface to the engine
/// manages the engine itself and some of the simulation parameters, which are neded
/// but are not part of the vendor API. Example: channel resistive and capacitive loads, noise levels
/// VSEL for caen, some tripping behaviour aspects for caen, some simulated functionality
/// of electrical signals.
#include "main.h"

//#include <stdlib.h>
//#include <unistd.h>
//#include <stdio.h>
//#include <stdbool.h>
//#include <string.h>

#include <czmq.h>
#include <google/protobuf-c/protobuf-c.h>
#include <libxml2/libxml/xmlreader.h>

#include "../../simulationOPCglue/protocolBuffers-c/simulationGlueCaen.pb-c.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "Pilot.h"


using namespace Pilot;
using namespace std;

void usage( char *p ){
	cout << "usage: " << p << " <command> [ -help | -host_engine <h1> | -port_engine <p1> ]" << endl;
	cout << "  defaults: h1=127.0.0.1 p1=8881" << endl;
	cout << "  commands: " << endl;
	cout << "      getCrateHandles" << endl;
	cout << "      setVSEL <crate handle> <vselvalue={0|1}>" << endl;
	cout << "      setISEL <crate handle> <iselvalue={0|1}>" << endl;
	cout << "      getChannelLoad <crate handle> <slot> <channel>" << endl;
	cout << "      setChannelLoad <crate handle> <slot> <channel> <resistancevalue/Ohm> <capacitancevalue/uFarad>" << endl;
	cout << "      setAllChannelsLoad <crate handle> <resistancevalue/Ohm> <capacitancevalue/uFarad>" << endl;
	cout << "      rampUpAllChannels <crate handle> <V0Set> <I0Set> <RUp> <RDwn>using Pw==1, not using VSEL " << endl;
	cout << "      rampUpAllOnBoardChannels <crate handle> <icboard> <V0Set> <I0Set> <RUp> <RDwn>// using Pw==1, not using VSEL " << endl;
	cout << "      switchPwAllChannels <crate handle> <sw> // using Pw==sw, 0=off, 1=on, not using VSEL " << endl;
	cout << "      getUpdateDelay <crate handle> // get update delay for this crate " << endl;
	cout << "      setUpdateDelay <crate handle> <delay_in_ms>  // set a new update delay for this crate: stress tests " << endl;
	cout << "      tripChannelByVoltage <crate handle> <slot> <channel> read out load and current-limit, "
		 << "          trip by raising voltage until current-limit is exceeded" << endl;
	cout << "      tripChannelByCurrent <crate handle> <slot> <channel> read out current and current-limit,"
		 << "          trip by lowering current limit below current" << endl;
	cout << "      tripChannelByLoad <crate handle> <slot> <channel> read current-limit and voltage,"
		 << "          trip by lowering load until current-limit is exceeded" << endl;
	cout << "      setDebug   // set debugging ON=1 or OFF=0" << endl;
	cout << "      getConfig  // read config details from s.engine, crate handle 0" << endl;
	cout << "      getHealth  // read out s.engine health parameters" << endl;
	exit(0);
}

int main( int argc, char **argv ){
	cout << argv[0] << __DATE__ << " " << __TIME__ << endl;
	// Verify that the version of the library that we linked against is
	// compatible with the version of the headers we compiled against.
	// GOOGLE_PROTOBUF_VERIFY_VERSION;

	string engine_ip = "127.0.0.1";
	int engine_port = SIMULATION_PILOT_PORT;

	if ( argc <= 1 ) usage(argv[0] );

	try {
		int i = 0;
		for ( i = 1; i < argc; i++ ){
			if (( 0 == strcmp( argv[i], "-h")) || ( 0 == strcmp( argv[i], "--help")) || ( 0 == strcmp( argv[i], ""))){
				usage( argv[0] );
			}
			if ( 0 == strcmp( argv[i], "-host_engine") && argc > i ){
				char hh[ NAME_SIZE ];
				sscanf( argv[i+1], "%s", &hh[ 0 ] );
				engine_ip = string( hh );
			}
			if ( 0 == strcmp(argv[i], "-port_engine" ) && argc > i){
				sscanf( argv[i+1], "%d", &engine_port );
			}

			if ( 0 == strcmp(argv[i], "getCrateHandles" ) && argc > i){
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				vector<int32_t> handles;
				vector<string> IPs;
				p->getCrateHandles( &handles, &IPs );
				for ( uint32_t i = 0; i < handles.size(); i++ ){
					cout << "found crate handle= " << handles[ i ] << " ip= " << IPs[ i ] << endl;
				}
				delete( p );
			}
			if ( 0 == strcmp(argv[i], "setVSEL" ) && argc > i){
				int handle;
				int vsel;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &vsel );

				cout << "setting VSEL handle= " << handle << " VSEL= " << vsel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->setVSEL( handle, vsel ) ;
				delete( p );
				cout << "OK VSEL= " << vsel << endl;
			}
			if ( 0 == strcmp(argv[i], "setISEL" ) && argc > i){
				int handle;
				int isel;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &isel );

				cout << "setting ISEL handle= " << handle << " ISEL= " << isel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->setISEL( handle, isel ) ;
				delete( p );
				cout << "OK ISEL= " << isel << endl;
			}
			if ( 0 == strcmp(argv[i], "setChannelLoad" ) && argc > i){
				int handle;
				int slot;
				int channel;
				float resistance;
				float capacitance;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &slot );
				sscanf( argv[i+3], "%d", &channel );
				sscanf( argv[i+4], "%g", &resistance );
				sscanf( argv[i+5], "%g", &capacitance );

				cout << "setChannelLoad handle= " << handle << " slot= " << slot << " channel= " << channel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->setChannelLoad( handle, slot, channel, resistance, capacitance * 1e-6 ) ;
				delete( p );
				cout << "OK setChannelLoad= resistance = " << resistance << " [Ohm] capacitance= " << capacitance << " [uF]" << endl;
			}
			if ( 0 == strcmp(argv[i], "setAllChannelsLoad" ) && argc > i){
				int handle;
				float resistance;
				float capacitance;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%g", &resistance );
				sscanf( argv[i+3], "%g", &capacitance );

				cout << "setAllChannelsLoad handle= " << handle << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->setAllChannelsLoad( handle,resistance , capacitance * 1e-6) ;
				delete( p );
				cout << "OK setAllChannelsLoad= resistance = " << resistance << " [Ohm] capacitance= " << capacitance << " [uF]" << endl;
			}
			if ( 0 == strcmp(argv[i], "rampUpAllChannels" ) && argc > i){
				int handle;
				float v0set;
				float i0set;
				float rup;
				float rdown;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%g", &v0set );
				sscanf( argv[i+3], "%g", &i0set );
				sscanf( argv[i+4], "%g", &rup );
				sscanf( argv[i+5], "%g", &rdown );

				cout << "rampUpAllChannels handle= " << handle << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->rampUpAllChannels( handle, v0set, i0set, rup, rdown ) ;
				delete( p );
				cout << "OK rampUpAllChannels: ramping of all channels should have started" << endl;
			}
			if ( 0 == strcmp(argv[i], "rampUpAllOnBoardChannels" ) && argc > i){
					int handle;
					int iboard;
					float v0set;
					float i0set;
					float rup;
					float rdown;
					sscanf( argv[i+1], "%d", &handle );
					sscanf( argv[i+2], "%d", &iboard );
					sscanf( argv[i+3], "%g", &v0set );
					sscanf( argv[i+4], "%g", &i0set );
					sscanf( argv[i+5], "%g", &rup );
					sscanf( argv[i+6], "%g", &rdown );

					cout << "rampUpAllOnBoardChannels handle= " << handle << " iboard= " << iboard << endl;
					Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
					p->rampUpAllOnBoardChannels( handle, iboard, v0set, i0set, rup, rdown ) ;
					delete( p );
					cout << "OK rampUpAllOnBoardChannels: ramping of all channels of the board should have started" << endl;
				}

			if ( 0 == strcmp(argv[i], "switchPwAllChannels" ) && argc > i){
				int handle;
				int sw;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &sw );

				cout << "switchPwAllChannels handle= " << handle << " sw= " << sw << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->switchPwAllChannels( handle, sw ) ;
				delete( p );
				cout << "OK switchPwAllChannels: ramping up/down of all channels should have started" << endl;
			}

			if ( 0 == strcmp(argv[i], "getChannelLoad" ) && argc > i){
				int handle;
				int slot;
				int channel;
				float resistance;
				float capacitance;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &slot );
				sscanf( argv[i+3], "%d", &channel );

				cout << "setChannelLoad handle= " << handle << " slot= " << slot << " channel= " << channel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->getChannelLoad( handle, slot, channel, &resistance, &capacitance ) ;
				delete( p );
				cout << "OK getChannelLoad resistance = " << resistance << " capacitance= " << capacitance << endl;
			}

			if ( 0 == strcmp(argv[i], "getUpdateDelay" ) && argc > i){
				int handle;
				float delay = -1;
				sscanf( argv[i+1], "%d", &handle );
				cout << "getUpdateDelay handle= " << handle << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->getUpdateDelay( handle, &delay ) ;
				delete( p );
				cout << "OK getUpdateDelay delay = " << delay << endl;
			}

			if ( 0 == strcmp(argv[i], "setUpdateDelay" ) && argc > i){
				int handle;
				float delay = -1;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%f", &delay );
				cout << "setUpdateDelay handle= " << handle << " delay= " << delay << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->setUpdateDelay( handle, delay ) ;
				delete( p );
				cout << "OK setUpdateDelay delay = " << delay << endl;
			}

			// cout << "      tripChannelByVoltage <crate handle> <slot> <channel>  // read out load and current-limit, trip by raising voltage until current-limit is exceeded" << endl;
			if ( 0 == strcmp(argv[i], "tripChannelByVoltage" ) && argc > i){
				int handle;
				int slot;
				int channel;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &slot );
				sscanf( argv[i+3], "%d", &channel );

				cout << "tripChannelByVoltage handle= " << handle << " slot= " << slot << " channel= " << channel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->tripChannelByVoltage( handle, slot, channel ) ;
				delete( p );
				cout << "OK tripChannelByVoltage" << endl;
			}
			if ( 0 == strcmp(argv[i], "tripChannelByCurrent" ) && argc > i){
				int handle;
				int slot;
				int channel;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &slot );
				sscanf( argv[i+3], "%d", &channel );

				cout << "tripChannelByCurrent handle= " << handle << " slot= " << slot << " channel= " << channel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->tripChannelByCurrent( handle, slot, channel ) ;
				delete( p );
				cout << "OK tripChannelByCurrent" << endl;
			}
			if ( 0 == strcmp(argv[i], "tripChannelByLoad" ) && argc > i){
				int handle;
				int slot;
				int channel;
				sscanf( argv[i+1], "%d", &handle );
				sscanf( argv[i+2], "%d", &slot );
				sscanf( argv[i+3], "%d", &channel );

				cout << "tripChannelByLoad handle= " << handle << " slot= " << slot << " channel= " << channel << endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->tripChannelByLoad( handle, slot, channel ) ;
				delete( p );
				cout << "OK tripChannelByLoad" << endl;
			}
			if ( 0 == strcmp(argv[i], "setDebug" ) && argc > i){
				int debug = 0;
				sscanf( argv[i+1], "%d", &debug );
				cout << "setDebug debug= " << debug <<  endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->setDebug( debug) ;
				delete( p );
				cout << "OK setDebug" << endl;
			}
			if ( 0 == strcmp(argv[i], "getConfig" ) && argc > i){
				int handle = 0;
				cout << "getConfig handle= " << handle <<  endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->getConfig( handle) ;
				delete( p );
				cout << "OK getConfig" << endl;
			}
			if ( 0 == strcmp(argv[i], "getHealth" ) && argc > i){
				int handle = 0;
				cout << "getHealth handle= " << handle <<  endl;
				Pilot::Pilot *p = new Pilot::Pilot( engine_ip, engine_port );
				p->getHealthEngine( handle ) ;
				delete( p );
				cout << "OK getHealth" << endl;
			}
		}
	}
	catch ( std::runtime_error &e ){
		cout << endl << __FILE__ << " " << __LINE__ << " runtime error: " << e.what() << endl;
		cout << __FILE__ << " " << __LINE__ << " " << argv[0] << " this was a specific exception. Trying to cleanup..." << endl;
		exit(0);
	}
	catch(std::exception& e)	{
		cout << __FILE__ << " " << __LINE__ << " standard exception: " << e.what() << endl;
		exit(0);
	}
	catch ( ... ){
		cout << __FILE__ << " " << __LINE__ << " other exception" << endl;
		exit(0);
	}

	return( 0 );

}

#!/bin/csh
# ramping many crates
#./simulationPilotUIFeb 15 2017 17:49:04
#usage: ./simulationPilotUI <command> [ -help | -host_engine <h1> | -port_engine <p1> ]
#  defaults: h1=127.0.0.1 p1=8881
#  commands: 
#      getCrateHandles
#      setVSEL <crate handle> <vselvalue={0|1}>
#      getChannelLoad <crate handle> <slot> <channel>
#      setChannelLoad <crate handle> <slot> <channel> <resistancevalue/MOhm> <capacitancevalue/pFarad>
#      setAllChannelsLoad <crate handle> <resistancevalue/MOhm> <capacitancevalue/pFarad>
#      rampUpAllChannels <crate handle> <V0Set> <I0Set> <RUp> <RDwn>// by serial user interaction Pw==1, not using VSEL 
#      switchOffAllChannels <crate handle> // by serial user interaction Pw==0, not using VSEL 
./simulationPilotUI rampUpAllChannels 0 500 2 5 5
./simulationPilotUI rampUpAllChannels 1 600 2 10 10
./simulationPilotUI rampUpAllChannels 2 500 2 12 12
./simulationPilotUI rampUpAllChannels 3 200 2 2 2
./simulationPilotUI rampUpAllChannels 4 15000 10 20 20




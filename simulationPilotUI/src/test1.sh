#!/bin/csh
# ramp up/down 
#./simulationPilotUIFeb 12 2018 10:25:51
#usage: ./simulationPilotUI <command> [ -help | -host_engine <h1> | -port_engine <p1> ]
#  defaults: h1=127.0.0.1 p1=8881
#  commands: 
#      getCrateHandles
#      setVSEL <crate handle> <vselvalue={0|1}>
#      setISEL <crate handle> <iselvalue={0|1}>
#      getChannelLoad <crate handle> <slot> <channel>
#      setChannelLoad <crate handle> <slot> <channel> <resistancevalue/Ohm> <capacitancevalue/uFarad>
#      setAllChannelsLoad <crate handle> <resistancevalue/Ohm> <capacitancevalue/uFarad>
#      rampUpAllChannels <crate handle> <V0Set> <I0Set> <RUp> <RDwn>using Pw==1, not using VSEL 
#      rampUpAllOnBoardChannels <crate handle> <icboard> <V0Set> <I0Set> <RUp> <RDwn>// using Pw==1, not using VSEL 
#      switchOffAllChannels <crate handle> // using Pw==0, not using VSEL 
#      getUpdateDelay <crate handle> // get update delay for this crate 
#      setUpdateDelay <crate handle> <delay_in_ms>  // set a new update delay for this crate: stress tests 
#      tripChannelByVoltage <crate handle> <slot> <channel> read out load and current-limit,           trip by raising voltage until current-limit is exceeded
#      tripChannelByCurrent <crate handle> <slot> <channel> read out current and current-limit,          trip by lowering current limit below current
#      tripChannelByLoad <crate handle> <slot> <channel> read current-limit and voltage,          trip by lowering load until current-limit is exceeded
#      setDebug   // set debugging ON=1 or OFF=0
#      getConfig  // read config details from s.engine, crate handle 0
#      getHealth  // read out s.engine health parameters
#
set ENGINE=aliceits     
./simulationPilotUI -host_engine $ENGINE rampUpAllChannels 0 500 2 5 5
sleep 5;
./simulationPilotUI -host_engine $ENGINE getHealth
sleep 5;
./simulationPilotUI -host_engine $ENGINE switchOffAllChannels 0



